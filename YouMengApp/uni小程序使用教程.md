### 简介
uni-app项目发行打包为原生App时可支持运行`uni小程序`，这里`uni小程序`也是uni-app项目，为了便于理解，约定以下概念。
- 宿主App  
用于发行打包为原生App的uni-app项目，需要使用HBuilderX提交云端打包生成原生安装包apk/iap。
- uni小程序  
作为uni程序运行在宿主App环境的uni-app项目，需要使用HBuilderX发行为`原生App-制作应用wgt包`，并在`宿主App`安装后运行
- 小程序API  
在宿主App中管理uni小程序使用的API，详情文档参考《uni小程序API文档.md》。注意：有的API仅支持在`宿主App`中调用，而有的API仅支持在uni小程序中调用
- uni小程序wgt  
将uni-app发行为`原生App-制作应用wgt包`生成的wgt文件，目前DCloud暂不提供uni小程序发布管理服务，需要开发者从自己业务服务器下载到本地，再调用`小程序API`使用



### 使用说明

#### 宿主App云端打包支持uni小程序
在HBuilderX中打开`宿主App`项目的manifest.json文件，切换到`源码视图`，在 "app-plus" -> "modules" 下添加 "UniMP"字段如下：
```json
  "UniMP": {
    "description": "uni小程序"
  }
```
保存后需提交云端打包

**注意：需使用HBuilderX3.2.3及以上版本**

宿主App开发时可以使用HBuilderX标准基座进行调试，调试成功后再提交云端打包


#### 生成uni小程序wgt
在HBuilderX中打开`uni小程序`项目，选择菜单 "发行" -> "原生App-制作应用wgt包"，打开"制作应用wgt包"界面：  

![](https://partner-dcloud-native.oss-cn-hangzhou.aliyuncs.com/images/unimp/hx-wgt.png)

点击"生成wgt"按钮，成功后保存在项目目录下的"unpackage\release"目录中，文件名称为：{$appid}.wgt


#### 宿主App调用uni小程序
将上面生成的`小程序wgt`文件拷贝到`宿主App`项目目录下的"static"目录中

在`宿主App`中安装`小程序wgt`，示例代码如下：
```javascript
installMP(){
  //通过获取小程序版本API来判断是否已经安装
  mp.getUniMPVersion('小程序的appid', (ret) => {
    if(0!=ret.code){//获取失败可以认为没有安装此小程序
      //安装小程序
      mp.installUniMP({
        appid:'小程序的appid',
        wgtFile:'小程序wgt文件路径'  //放在static目录路径为'/static/{$appid}.wgt'，如果小程序wgt在服务器，需要先下载到本地再安装
      }, (r)=>{
        if(0==r.code){
          console.log('安装成功');
        }else{
          console.log('安装失败: '+JSON.stringify(r));
        }
      });
    }
  });
}
```


在`宿主App`中启动`小程序wgt`，示例代码如下：  
```javascript
openMP(){
  mp.openUniMP({
    appid: '小程序的appid'
  }, (ret)=>{
    if(0!=ret.code){
      console.log('启动失败: '+JSON.stringify(ret));
    }
  });
}
```


>更多uni小程序相关API参考《uni小程序API文档.md》

**注意**  
实际项目中uni小程序wgt文件应该从服务器下载，上面示例中将wgt放到宿主App的资源中只是为了方便演示。


#### 宿主App与uni小程序相互通讯  
##### 启动时传递参数给uni小程序  
- 宿主App启动时可以通过extraData属性传递启动参数进行通讯，详情参考《uni小程序API文档.md》中的`openUniMP(options,callback)`
- uni小程序在项目App.vue的应用生命周期`onLaunch`、`onShow`中获取启动参数，详情参考《uni小程序API文档.md》中的`获取启动参数`

##### 运行期uni小程序向宿主App发送事件  
- 宿主App在应用生命周期`onLaunch`中监听事件，详情参考《uni小程序API文档.md》中的`onUniMPEventReceive(callback)`
- uni小程序调用sendHostEvent()方法向宿主App发送事件，详情参考《uni小程序API文档.md》中的`uni.sendHostEvent(event,data,ret,callback)`

##### 运行期宿主App向uni小程序发送事件  
- uni小程序在应用生命周期`onLaunch`中监听事件，详情参考《uni小程序API文档.md》中的`uni.onHostEventReceive(callback)`
- 宿主App调用sendUniMPEvent()方法向uni小程序发送事件，详情参考《uni小程序API文档.md》中的`sendUniMPEvent(appid,event,data,callback)`


### 演示示例
参考"MyUniHost"目录，此示例工程包含完整的示例代码。


