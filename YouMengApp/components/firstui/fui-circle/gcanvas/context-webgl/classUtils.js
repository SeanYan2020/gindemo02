// 本文件由FirstUI授权予严天赠（会员ID：2  4 42，身份证尾号：0 4   30 18）专用，请尊重知识产权，勿私下传播，违者追究法律责任。
export function getTransferedObjectUUID(name, id) {
    return `${name.toLowerCase()}-${id}`;
}