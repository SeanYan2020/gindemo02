// 本文件由FirstUI授权予严天赠（会员ID：  2 442，身份证尾号： 0 4 301  8）专用，请尊重知识产权，勿私下传播，违者追究法律责任。
class FillStylePattern {
    constructor(img, pattern) {
        this._style = pattern;
        this._img = img;
    }
}

export default FillStylePattern;