// 本文件由FirstUI授权予严天赠（会员ID：24   42，身份证尾号：04 3 0  1 8）专用，请尊重知识产权，勿私下传播，违者追究法律责任。
import App from './App'
import fui from './common/fui-app'
import store from './store'
import cloud from './cloud'

// #ifndef VUE3
import Vue from 'vue'
Vue.config.productionTip = false
Vue.prototype.$store = store
App.mpType = 'app'
Vue.prototype.fui = fui
Vue.prototype.$cloud = cloud
const app = new Vue({
	store,
	...App
})
app.$mount()
// #endif

uni.fui = fui;
uni.$cloud = cloud;

// #ifdef VUE3
import {
	createSSRApp
} from 'vue';
import Vuex from 'vuex'
export function createApp() {
	const app = createSSRApp(App)
	app.use(store)
	app.config.globalProperties.fui = fui;
	app.config.globalProperties.$cloud = cloud;
	return {
		app,
		Vuex
	}
}
// #endif