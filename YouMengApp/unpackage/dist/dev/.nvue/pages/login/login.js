import { f as formatAppLog, r as resolveEasycom } from "../../uni-app.es.js";
import { _ as __easycom_5 } from "../../fui-icon.js";
import { resolveDynamicComponent, openBlock, createElementBlock, createElementVNode, createVNode, withCtx, withModifiers, toDisplayString } from "vue";
import { _ as __easycom_1 } from "../../fui-input.js";
import { _ as __easycom_6 } from "../../fui-button.js";
import { mapState } from "vuex";
import { _ as _export_sfc } from "../../_plugin-vue_export-helper.js";
const _style_0 = { "fui-wrap": { "": { "paddingTop": 0, "paddingRight": "48rpx", "paddingBottom": 0, "paddingLeft": "48rpx" } }, "fui-logo": { "": { "width": "500rpx", "height": "500rpx" } }, "fui-text": { "": { "fontWeight": "700" } }, "fui-areacode__box": { "": { "flexDirection": "row", "alignItems": "center", "marginRight": "48rpx" } }, "fui-area__code": { "": { "fontSize": "32rpx" } }, "fui-form__box": { "": { "paddingTop": "96rpx" } }, "fui-btn__box": { "": { "paddingTop": "96rpx" } }, "fui-reg__box": { "": { "flexDirection": "row", "justifyContent": "center", "alignItems": "center", "paddingTop": "48rpx" } }, "fui-reg__text": { "": { "fontSize": "26rpx", "color": "#9E9E9E" } } };
const _sfc_main = {
  computed: {
    ...mapState(["areaCode"]),
    isLogin() {
      return uni.$cloud.isLogin();
    }
  },
  data() {
    return {
      mobile: ""
    };
  },
  onLoad() {
    if (this.isLogin) {
      uni.$cloud.go("/pages/index/index");
    }
    formatAppLog("log", "at pages/login/login.nvue:48", "查看判断登录=A", this.isLogin);
  },
  methods: {
    reg() {
      uni.fui.href("../reg/reg");
    },
    area() {
      uni.fui.href("../areaCode/areaCode");
    },
    captcha() {
      if (this.mobile) {
        uni.fui.href("../captcha/captcha?type=1&mobile=" + this.mobile);
      } else {
        uni.showToast({
          icon: "error",
          title: "手机号不能为空",
          duration: 2e3
        });
      }
    }
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_fui_icon = resolveEasycom(resolveDynamicComponent("fui-icon"), __easycom_5);
  const _component_fui_input = resolveEasycom(resolveDynamicComponent("fui-input"), __easycom_1);
  const _component_fui_button = resolveEasycom(resolveDynamicComponent("fui-button"), __easycom_6);
  return openBlock(), createElementBlock("scroll-view", {
    scrollY: true,
    showScrollbar: true,
    enableBackToTop: true,
    bubble: "true",
    style: { flexDirection: "column" }
  }, [
    createElementVNode("view", { class: "fui-wrap" }, [
      createElementVNode("view", { class: "fui-flex__center" }, [
        createElementVNode("u-image", {
          class: "fui-logo",
          src: "/static/images/common/chat_01.png",
          mode: "widthFix"
        }),
        createElementVNode("u-text", { class: "fui-text fui-size" }, "登录到您的账户")
      ]),
      createElementVNode("view", { class: "fui-form__box" }, [
        createVNode(_component_fui_input, {
          type: "number",
          "border-bottom": false,
          modelValue: $data.mobile,
          "onUpdate:modelValue": _cache[1] || (_cache[1] = ($event) => $data.mobile = $event),
          radius: 24,
          backgroundColor: "#FAFAFA",
          placeholder: "请输入手机号码",
          bottomLeft: "0",
          maxlength: "11"
        }, {
          left: withCtx(() => [
            createElementVNode("view", {
              class: "fui-areacode__box fui--active",
              onClick: _cache[0] || (_cache[0] = withModifiers((...args) => $options.area && $options.area(...args), ["stop"]))
            }, [
              createElementVNode(
                "u-text",
                { class: "fui-area__code fui-color__primary" },
                toDisplayString(_ctx.areaCode),
                1
                /* TEXT */
              ),
              createVNode(_component_fui_icon, {
                name: "arrowdown",
                size: "40",
                color: "#9E9E9E"
              })
            ])
          ]),
          _: 1
          /* STABLE */
        }, 8, ["modelValue"]),
        createElementVNode("view", { class: "fui-btn__box" }, [
          createVNode(_component_fui_button, {
            text: "获取验证码",
            radius: "96rpx",
            bold: "",
            onClick: $options.captcha
          }, null, 8, ["onClick"])
        ]),
        createElementVNode("view", { class: "fui-reg__box" }, [
          createElementVNode("u-text", { class: "fui-reg__text" }, "还没有账号？"),
          createElementVNode("u-text", {
            class: "fui-reg__text fui-primary fui--active",
            onClick: _cache[2] || (_cache[2] = (...args) => $options.reg && $options.reg(...args))
          }, "去注册")
        ])
      ])
    ])
  ]);
}
const login = /* @__PURE__ */ _export_sfc(_sfc_main, [["render", _sfc_render], ["styles", [_style_0]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/pages/login/login.nvue"]]);
export {
  login as default
};
