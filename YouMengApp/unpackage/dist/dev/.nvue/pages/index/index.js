import { a as requireNativePlugin, r as resolveEasycom, f as formatAppLog } from "../../uni-app.es.js";
import { _ as __easycom_0 } from "../../fui-avatar.js";
import { openBlock, createElementBlock, Fragment, createCommentVNode, normalizeClass, normalizeStyle, toDisplayString, createElementVNode, renderSlot, renderList, withModifiers, resolveDynamicComponent, createBlock, createVNode, withCtx } from "vue";
import { _ as _export_sfc } from "../../_plugin-vue_export-helper.js";
import { _ as __easycom_3$1 } from "../../fui-list-cell.js";
import { _ as __easycom_6$1 } from "../../fui-button.js";
import { _ as __easycom_5 } from "../../fui-icon.js";
const _style_0$4 = { "fui-badge__wrap": { "": { "height": "36rpx", "color": "#FFFFFF", "fontSize": "24rpx", "lineHeight": "36rpx", "borderRadius": 100, "flexDirection": "row", "alignItems": "center", "justifyContent": "center", "textAlign": "center", "zIndex": 10 } }, "fui-badge__dot": { "": { "!height": 8, "!width": 8, "borderRadius": 100, "zIndex": 10 } }, "fui-badge__text-color": { "": { "!color": "#FF2B2B" } }, "fui-badge__trans-origin": { "": { "transformOrigin": "center center" } }, "fui-badge__absolute": { "": { "position": "absolute" } } };
uni.getSystemInfoSync();
const _sfc_main$5 = {
  name: "fui-badge",
  emits: ["click"],
  props: {
    value: {
      type: [Number, String],
      default: ""
    },
    max: {
      type: [Number, String],
      default: -1
    },
    //类型：primary，success，warning，danger，purple，white
    type: {
      type: String,
      default: "primary"
    },
    //背景色，如果设置背景则type失效
    background: {
      type: String,
      default: ""
    },
    //字体颜色
    color: {
      type: String,
      default: "#FFFFFF"
    },
    //是否显示为圆点
    dot: {
      type: Boolean,
      default: false
    },
    //margin-top值，单位rpx
    marginTop: {
      type: [Number, String],
      default: 0
    },
    //margin-left值，单位rpx
    marginLeft: {
      type: [Number, String],
      default: 0
    },
    //margin-right值，单位rpx
    marginRight: {
      type: [Number, String],
      default: 0
    },
    //是否绝对定位
    absolute: {
      type: Boolean,
      default: false
    },
    top: {
      type: String,
      default: "-8rpx"
    },
    right: {
      type: String,
      default: "-18rpx"
    },
    //缩放比例
    scaleRatio: {
      type: Number,
      default: 1
    }
  },
  data() {
    let isNvue = false;
    isNvue = true;
    return {
      isNvue,
      width: 0,
      showValue: ""
    };
  },
  computed: {
    getBgColor() {
      let color = this.background;
      if (!color && this.type) {
        const app = uni && uni.$fui && uni.$fui.color;
        const colors = {
          primary: app && app.primary || "#465CFF",
          success: app && app.success || "#09BE4F",
          warning: app && app.warning || "#FFB703",
          danger: app && app.danger || "#FF2B2B",
          purple: app && app.purple || "#6831FF",
          white: "#FFFFFF"
        };
        color = colors[this.type] || colors.primary;
      }
      return color;
    }
  },
  watch: {
    value(val) {
      this.getWidth();
    }
  },
  mounted() {
    this.getWidth();
  },
  methods: {
    _getTextWidth(text) {
      let sum = 0;
      for (let i = 0, len = text.length; i < len; i++) {
        if (text.charCodeAt(i) >= 0 && text.charCodeAt(i) <= 255)
          sum = sum + 1;
        else
          sum = sum + 2;
      }
      const px = uni.upx2px(text.length > 1 ? 32 : 24);
      var strCode = text.charCodeAt();
      let multiplier = 12;
      if (strCode >= 65 && strCode <= 90) {
        multiplier = 15;
      }
      return sum / 2 * multiplier + px + "px";
    },
    getWidth() {
      let max = Number(this.max);
      let val = Number(this.value);
      let value = "";
      if (isNaN(val) || max === -1) {
        value = this.value;
      } else {
        value = val > max ? `${max}+` : val;
      }
      this.showValue = value;
      this.width = this.dot ? "8px" : this._getTextWidth(String(value));
    },
    handleClick() {
      this.$emit("click");
    }
  }
};
function _sfc_render$5(_ctx, _cache, $props, $setup, $data, $options) {
  return openBlock(), createElementBlock(
    Fragment,
    null,
    [
      createCommentVNode("本文件由FirstUI授权予严天赠（会员ID：  244 2，身份证尾号：04 3 01   8）专用，请尊重知识产权，勿私下传播，违者追究法律责任。"),
      $data.showValue || $props.dot ? (openBlock(), createElementBlock(
        "u-text",
        {
          key: 0,
          class: normalizeClass([$props.dot ? "fui-badge__dot" : "fui-badge__wrap", $props.background ? "" : "fui-badge__bg-" + $props.type, $props.absolute ? "fui-badge__absolute" : "", $props.scaleRatio != 1 && $data.isNvue ? "fui-badge__trans-origin" : "", !$props.background && $props.type === "white" ? "fui-badge__text-color" : ""]),
          style: normalizeStyle({ top: $props.absolute ? $props.top : "auto", right: $props.absolute ? $props.right : "auto", zoom: $props.scaleRatio, transform: $data.isNvue ? `scale(${$props.scaleRatio})` : "scale(1)", marginTop: $props.marginTop + "rpx", marginLeft: $props.marginLeft + "rpx", marginRight: $props.marginRight + "rpx", width: $data.width, color: $props.color, background: $options.getBgColor }),
          onClick: _cache[0] || (_cache[0] = (...args) => $options.handleClick && $options.handleClick(...args))
        },
        toDisplayString($props.dot ? "" : $data.showValue),
        7
        /* TEXT, CLASS, STYLE */
      )) : createCommentVNode("v-if", true)
    ],
    2112
    /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
  );
}
const __easycom_1 = /* @__PURE__ */ _export_sfc(_sfc_main$5, [["render", _sfc_render$5], ["styles", [_style_0$4]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-badge/fui-badge.vue"]]);
const mpwxs = {};
const mpjs$1 = {};
const BindingX = requireNativePlugin("bindingx");
const dom$1 = requireNativePlugin("dom");
const animation$2 = requireNativePlugin("animation");
const bindingx$1 = {
  data() {
    return {};
  },
  watch: {
    show(newVal) {
      if (this.stop)
        return;
      this.stop = true;
      if (newVal) {
        this.open(newVal);
      } else {
        this.close();
      }
    },
    buttons(newVal) {
      this.init();
    }
  },
  created() {
    this.group = this.getParent();
    if (this.group.children !== void 0) {
      this.group.children.push(this);
    }
  },
  mounted() {
    this.$nextTick(() => {
      this.box = this.getEl(this.$refs["fui_swipea_wrap"]);
      this.selector = this.getEl(this.$refs["fui_swipea_content"]);
      this.rightButton = this.getEl(this.$refs["fui_swipea_buttons"]);
    });
    this.init();
  },
  methods: {
    init() {
      this.$nextTick(() => {
        this.x = 0;
        this.button = {
          show: false
        };
        setTimeout(() => {
          this.getSelectorQuery();
        }, 200);
      });
    },
    handleClick(e, index2, item) {
      e.stopPropagation();
      this.$emit("click", {
        item,
        index: index2,
        param: this.param
      });
    },
    touchstart(e) {
      if (this.disabled)
        return;
      if (this.stop)
        return;
      this.stop = true;
      if (this.autoClose) {
        this.group && this.group.closeAuto(this);
      }
      const rightWidth = this.button.right.width || 0;
      let expression = this.range(this.x, -rightWidth, 0);
      let rightExpression = this.range(this.x + rightWidth, 0, rightWidth);
      this.eventpan = BindingX.bind({
        anchor: this.box,
        eventType: "pan",
        props: [{
          element: this.selector,
          property: "transform.translateX",
          expression
        }, {
          element: this.rightButton,
          property: "transform.translateX",
          expression: rightExpression
        }]
      }, (e2) => {
        if (e2.state === "end") {
          this.x = e2.deltaX + this.x;
          this.isclick = true;
          this.bindTiming(e2.deltaX);
        }
      });
    },
    touchend(e) {
      if (this.isopen && !this.isclick && this.clickClose) {
        this.open(false);
      }
    },
    bindTiming(x) {
      const left = this.x;
      this.button.right.width || 0;
      const threshold = Number(this.threshold);
      if (!this.isopen) {
        if (left < -threshold) {
          this.open(true);
        } else {
          this.open(false);
        }
      } else {
        if (x < threshold && x > 0 || x < -threshold) {
          this.open(true);
        } else {
          this.open(false);
        }
      }
    },
    range(num, mix, max) {
      return `min(max(x+${num}, ${mix}), ${max})`;
    },
    open(type) {
      this.animation(type);
    },
    close() {
      this.animation(false);
    },
    animation(type) {
      const rightWidth = this.button.right.width || 0;
      if (this.eventpan && this.eventpan.token) {
        BindingX.unbind({
          token: this.eventpan.token,
          eventType: "pan"
        });
      }
      if (type) {
        Promise.all([
          this.move(this.selector, -rightWidth),
          this.move(this.rightButton, 0)
        ]).then(() => {
          this.setEmit(-rightWidth, type);
        });
      } else {
        Promise.all([
          this.move(this.selector, 0),
          this.move(this.rightButton, rightWidth)
        ]).then(() => {
          this.setEmit(0, type);
        });
      }
    },
    setEmit(x, type) {
      const rightWidth = this.button.right.width;
      this.isopen = this.isopen || false;
      this.stop = false;
      this.isclick = false;
      if (this.isopen !== type && this.x !== x) {
        if (type && rightWidth > 0) {
          this.$emit("change", {
            isOpen: true,
            param: this.param
          });
        }
        if (!type) {
          this.$emit("change", {
            isOpen: false,
            param: this.param
          });
        }
      }
      this.x = x;
      this.isopen = type;
    },
    move(ref, value) {
      return new Promise((resolve, reject) => {
        animation$2.transition(ref, {
          styles: {
            transform: `translateX(${value})`
          },
          duration: 150,
          //ms
          timingFunction: "linear",
          needLayout: false,
          delay: 0
          //ms
        }, function(res) {
          resolve(res);
        });
      });
    },
    getEl(el) {
      return el.ref;
    },
    getSelectorQuery() {
      Promise.all([
        this.getDom()
      ]).then((data) => {
        this.open(this.show);
      });
    },
    getDom() {
      return new Promise((resolve, reject) => {
        dom$1.getComponentRect(this.$refs["fui_swipea_buttons"], (data) => {
          if (data) {
            this.button["right"] = data.size;
            resolve(data);
          } else {
            reject();
          }
        });
      });
    }
  }
};
const _style_0$3 = { "fui-swipe__action-wrap": { "": { "position": "relative" } }, "fui-swipe__action-inner": { "": { "position": "relative" } }, "fui-swipe__action-left": { "": { "flex": 1 } }, "fui-swipe__action-right": { "": { "flexDirection": "row", "position": "absolute", "top": 0, "bottom": 0, "right": 0, "transform": "translateX(100%)" } }, "fui-swipe__action-btn": { "": { "flex": 1, "flexDirection": "row", "justifyContent": "center", "alignItems": "center", "paddingTop": 0, "paddingRight": "48rpx", "paddingBottom": 0, "paddingLeft": "48rpx" } }, "fui-swipe__action-ani": { "": { "transitionProperty": "transform", "transitionDuration": 300, "transitionTimingFunction": "cubic-bezier(0.165,0.84,0.44,1)" } }, "@TRANSITION": { "fui-swipe__action-ani": { "property": "transform", "duration": 300, "timingFunction": "cubic-bezier(0.165,0.84,0.44,1)" } } };
const dangerColor = uni && uni.$fui && uni.$fui.color && uni.$fui.color.danger || "#FF2B2B";
const _sfc_main$4 = {
  name: "fui-swipe-action",
  mixins: [mpwxs, mpjs$1, bindingx$1],
  emits: ["click", "change"],
  props: {
    buttons: {
      type: Array,
      default() {
        return [{
          text: "删除",
          background: dangerColor
        }];
      }
    },
    size: {
      type: [Number, String],
      default: 32
    },
    color: {
      type: String,
      default: "#fff"
    },
    show: {
      type: Boolean,
      default: false
    },
    threshold: {
      type: [Number, String],
      default: 30
    },
    disabled: {
      type: Boolean,
      default: false
    },
    autoClose: {
      type: Boolean,
      default: true
    },
    //v2.1.0+ 点击当前菜单是否立即关闭菜单
    clickClose: {
      type: Boolean,
      default: true
    },
    //1.9.9+
    marginTop: {
      type: [Number, String],
      default: 0
    },
    //1.9.9+
    marginBottom: {
      type: [Number, String],
      default: 0
    },
    param: {
      type: [Number, String],
      default: 0
    }
  },
  beforeUnmount() {
    this.__beforeUnmount = true;
    this.unInstall();
  },
  methods: {
    unInstall() {
      if (this.group) {
        this.group.children.forEach((item, index2) => {
          if (item === this) {
            this.group.children.splice(index2, 1);
          }
        });
      }
    },
    //获取父元素实例
    getParent(name = "fui-swipeaction-group") {
      let parent = this.$parent;
      let parentName = parent.$options.name;
      while (parentName !== name) {
        parent = parent.$parent;
        if (!parent)
          return false;
        parentName = parent.$options.name;
      }
      return parent;
    }
  }
};
function _sfc_render$4(_ctx, _cache, $props, $setup, $data, $options) {
  return openBlock(), createElementBlock(
    Fragment,
    null,
    [
      createCommentVNode("本文件由FirstUI授权予严天赠（会员ID：244   2，身份证尾号：0 4 3   018）专用，请尊重知识产权，勿私下传播，违者追究法律责任。"),
      createElementVNode(
        "view",
        {
          class: "fui-swipe__action-wrap",
          style: normalizeStyle({ marginTop: $props.marginTop + "rpx", marginBottom: $props.marginBottom + "rpx" }),
          ref: "fui_swipea_wrap",
          onHorizontalpan: _cache[0] || (_cache[0] = (...args) => _ctx.touchstart && _ctx.touchstart(...args)),
          onTouchend: _cache[1] || (_cache[1] = (...args) => _ctx.touchend && _ctx.touchend(...args))
        },
        [
          createElementVNode(
            "view",
            {
              class: "fui-swipe__action-right",
              ref: "fui_swipea_buttons"
            },
            [
              renderSlot(_ctx.$slots, "buttons", {}, () => [
                (openBlock(true), createElementBlock(
                  Fragment,
                  null,
                  renderList($props.buttons, (item, index2) => {
                    return openBlock(), createElementBlock("view", {
                      class: "fui-swipe__action-btn",
                      style: normalizeStyle({ background: item.background }),
                      key: index2,
                      onClick: withModifiers(($event) => _ctx.handleClick($event, index2, item), ["stop"])
                    }, [
                      createElementVNode(
                        "u-text",
                        {
                          class: "fui-swipe__action-text",
                          style: normalizeStyle({ fontSize: (item.size || $props.size) + "rpx", color: item.color || $props.color })
                        },
                        toDisplayString(item.text),
                        5
                        /* TEXT, STYLE */
                      )
                    ], 12, ["onClick"]);
                  }),
                  128
                  /* KEYED_FRAGMENT */
                ))
              ])
            ],
            512
            /* NEED_PATCH */
          ),
          createElementVNode(
            "view",
            {
              class: "fui-swipe__action-left",
              ref: "fui_swipea_content"
            },
            [
              renderSlot(_ctx.$slots, "default")
            ],
            512
            /* NEED_PATCH */
          )
        ],
        36
        /* STYLE, HYDRATE_EVENTS */
      )
    ],
    2112
    /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
  );
}
const __easycom_3 = /* @__PURE__ */ _export_sfc(_sfc_main$4, [["render", _sfc_render$4], ["styles", [_style_0$3]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-swipe-action/fui-swipe-action.vue"]]);
const _sfc_main$3 = {
  name: "fui-swipeaction-group",
  data() {
    return {};
  },
  created() {
    this.children = [];
  },
  methods: {
    reset() {
      this.children.forEach((child) => {
        child.init();
      });
    },
    close() {
      this.children.forEach((child) => {
        child.close();
      });
    },
    closeAuto(child) {
      if (this.openItem && this.openItem !== child) {
        this.openItem.close();
      }
      this.openItem = child;
    }
  }
};
function _sfc_render$3(_ctx, _cache, $props, $setup, $data, $options) {
  return openBlock(), createElementBlock(
    Fragment,
    null,
    [
      createCommentVNode("本文件由FirstUI授权予严天赠（会员ID：24 4  2，身份证尾号：0  4 30 1 8）专用，请尊重知识产权，勿私下传播，违者追究法律责任。"),
      createElementVNode("view", null, [
        renderSlot(_ctx.$slots, "default")
      ])
    ],
    2112
    /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
  );
}
const __easycom_4 = /* @__PURE__ */ _export_sfc(_sfc_main$3, [["render", _sfc_render$3], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-swipeaction-group/fui-swipeaction-group.vue"]]);
const _style_0$2 = { "fui-empty__wrap": { "": { "flex": 1, "flexDirection": "column", "alignItems": "center", "justifyContent": "center" } }, "fui-empty__fixed": { "": { "position": "fixed", "left": 0, "top": 0, "right": 0, "bottom": 0, "zIndex": 99 } }, "fui-empty__title": { "": { "textAlign": "center", "fontWeight": "500", "paddingTop": "48rpx" } }, "fui-empty__desc": { "": { "textAlign": "center", "fontWeight": "normal", "paddingTop": "8rpx" } } };
const _sfc_main$2 = {
  name: "fui-empty",
  props: {
    src: {
      type: String,
      default: ""
    },
    width: {
      type: [Number, String],
      default: 576
    },
    height: {
      type: [Number, String],
      default: 318
    },
    title: {
      type: String,
      default: ""
    },
    color: {
      type: String,
      default: "#333333"
    },
    size: {
      type: [Number, String],
      default: 32
    },
    descr: {
      type: String,
      default: ""
    },
    descrColor: {
      type: String,
      default: "#B2B2B2"
    },
    descrSize: {
      type: [Number, String],
      default: 24
    },
    isFixed: {
      type: Boolean,
      default: false
    },
    marginTop: {
      type: [Number, String],
      default: 0
    }
  }
};
function _sfc_render$2(_ctx, _cache, $props, $setup, $data, $options) {
  return openBlock(), createElementBlock(
    Fragment,
    null,
    [
      createCommentVNode("本文件由FirstUI授权予严天赠（会员ID：   2442，身份证尾号： 043    018）专用，请尊重知识产权，勿私下传播，违者追究法律责任。"),
      createElementVNode(
        "view",
        {
          class: normalizeClass(["fui-empty__wrap", { "fui-empty__fixed": $props.isFixed }]),
          style: normalizeStyle({ marginTop: $props.marginTop + "rpx" })
        },
        [
          $props.src ? (openBlock(), createElementBlock("u-image", {
            key: 0,
            src: $props.src,
            style: normalizeStyle({ width: $props.width + "rpx", height: $props.height + "rpx" }),
            mode: "widthFix"
          }, null, 12, ["src"])) : createCommentVNode("v-if", true),
          $props.title ? (openBlock(), createElementBlock(
            "u-text",
            {
              key: 1,
              class: normalizeClass(["fui-empty__title", { "fui-empty__title-color": !$props.color }]),
              style: normalizeStyle({ color: $props.color, fontSize: $props.size + "rpx" })
            },
            toDisplayString($props.title),
            7
            /* TEXT, CLASS, STYLE */
          )) : createCommentVNode("v-if", true),
          $props.descr ? (openBlock(), createElementBlock(
            "u-text",
            {
              key: 2,
              class: normalizeClass(["fui-empty__desc", { "fui-empty__descr-color": !$props.descrColor }]),
              style: normalizeStyle({ color: $props.descrColor, fontSize: $props.descrSize + "rpx" })
            },
            toDisplayString($props.descr),
            7
            /* TEXT, CLASS, STYLE */
          )) : createCommentVNode("v-if", true),
          renderSlot(_ctx.$slots, "default")
        ],
        6
        /* CLASS, STYLE */
      )
    ],
    2112
    /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
  );
}
const __easycom_6 = /* @__PURE__ */ _export_sfc(_sfc_main$2, [["render", _sfc_render$2], ["styles", [_style_0$2]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-empty/fui-empty.vue"]]);
const mpjs = {};
const animation$1 = requireNativePlugin("animation");
const bindingx = {
  data() {
    return {
      startX: 0,
      startY: 0,
      lastLeft: 0,
      lastTop: 0,
      isMove: false
    };
  },
  created() {
    this.refFab = null;
    this.loop = null;
  },
  mounted() {
    this.$nextTick(() => {
      setTimeout(() => {
        this.refFab = this.getEl(this.$refs["fui_fab_move_ref"]);
      }, 50);
    });
  },
  methods: {
    getEl(el) {
      return el.ref || el[0].ref;
    },
    _aniMove(x, y) {
      if (!this.refFab || !this.isDrag)
        return;
      animation$1.transition(this.refFab, {
        styles: {
          transform: `translate(${x}px,${y}px)`
        },
        duration: 0,
        timingFunction: "linear",
        needLayout: false,
        delay: 0
      }, () => {
        if (Math.abs(x) > 0.1 || Math.abs(y) > 0.1) {
          this.isMove = true;
        }
      });
    },
    touchstart(e) {
      if (!this.isDrag)
        return;
      var touch = e.touches || e.changedTouches;
      this.startX = touch[0].screenX;
      this.startY = touch[0].screenY;
    },
    touchmove(e) {
      if (!this.isDrag)
        return;
      var touch = e.touches || e.changedTouches;
      let pageX = touch[0].screenX, pageY = touch[0].screenY;
      var left = pageX - this.startX + this.lastLeft;
      left = left < -this.eLeft ? -this.eLeft : left;
      left = left > this.maxWidth ? this.maxWidth : left;
      this.startX = pageX;
      var top = pageY - this.startY + this.lastTop;
      top = top < -this.eTop ? -this.eTop : top;
      top = top > this.maxHeight ? this.maxHeight : top;
      this.startY = pageY;
      this.lastLeft = left;
      this.lastTop = top;
      this._aniMove(left, top);
    },
    touchend(e) {
      clearTimeout(this.loop);
      this.loop = setTimeout(() => {
        this.isMove = false;
      }, 50);
    }
  }
};
const _style_0$1 = { "fui-fab__mask": { "": { "position": "fixed", "left": 0, "right": 0, "top": 0, "bottom": 0, "opacity": 0, "transitionDuration": 250, "transitionProperty": "opacity", "transitionTimingFunction": "ease-in-out", "transform": "translateX(-100%)" } }, "fui-fab__mask-show": { "": { "opacity": 1, "transform": "translateX(0)" } }, "fui-fab__btn-wrap": { "": { "flexDirection": "column", "justifyContent": "center", "position": "fixed", "bottom": "120rpx" } }, "fui-fab__wrap-left": { "": { "alignItems": "flex-start", "left": "80rpx" } }, "fui-fab__wrap-right": { "": { "alignItems": "flex-end", "right": "80rpx" } }, "fui-fab__btn-list": { "": { "flexDirection": "column" } }, "fui-fab__list-left": { "": { "transformOrigin": "0 100%", "alignItems": "flex-start" } }, "fui-fab__list-right": { "": { "transformOrigin": "100% 100%", "alignItems": "flex-end" } }, "fui-fab__btn-hidden": { "": { "width": 0, "height": 0 } }, "fui-fab__button-box": { "": { "flexDirection": "row", "justifyContent": "flex-end", "alignItems": "center", "marginBottom": "32rpx" } }, "fui-fab__button-left": { "": { "flexDirection": "row-reverse", "justifyContent": "flex-start" } }, "fui-fab__button-right": { "": { "flexDirection": "row", "justifyContent": "flex-end" } }, "fui-fab__btn-text": { "": { "paddingLeft": "24rpx", "paddingRight": "24rpx", "fontWeight": "normal" } }, "fui-fab__button": { "": { "borderRadius": 100, "flexDirection": "row", "alignItems": "center", "justifyContent": "center" } }, "fui-fab__btn-abbr": { "": { "textAlign": "center", "fontWeight": "normal" } }, "fui-fab__btn-main": { "": { "borderRadius": 100, "alignItems": "center", "justifyContent": "center", "transform": "rotate(0deg)", "overflow": "hidden" } }, "fui-fab__btn-inner": { "": { "alignItems": "center", "justifyContent": "center" } }, "@TRANSITION": { "fui-fab__mask": { "duration": 250, "property": "opacity", "timingFunction": "ease-in-out" } } };
const animation = requireNativePlugin("animation");
const dom = requireNativePlugin("dom");
const _sfc_main$1 = {
  name: "fui-fab",
  mixins: [mpjs, bindingx],
  emits: ["click", "change"],
  // components:{
  // 	fuiIcon
  // },
  props: {
    fabs: {
      type: Array,
      default() {
        return [];
      }
    },
    position: {
      type: String,
      default: "right"
    },
    distance: {
      type: [Number, String],
      default: 80
    },
    bottom: {
      type: [Number, String],
      default: 120
    },
    width: {
      type: [Number, String],
      default: 108
    },
    background: {
      type: String,
      default: ""
    },
    color: {
      type: String,
      default: "#fff"
    },
    mask: {
      type: Boolean,
      default: true
    },
    maskBackground: {
      type: String,
      default: "rgba(0,0,0,.6)"
    },
    maskClosable: {
      type: Boolean,
      default: false
    },
    zIndex: {
      type: [Number, String],
      default: 99
    },
    //V1.9.8+
    isDrag: {
      type: Boolean,
      default: false
    }
  },
  computed: {
    getStyles() {
      let styles = `bottom:${this.bottom}rpx;z-index:${this.zIndex};`;
      if (this.position === "left") {
        styles += `left:${this.distance}rpx;`;
      } else {
        styles += `right:${this.distance}rpx;`;
      }
      return styles;
    },
    getStyle() {
      return `background:${this.maskBackground};z-index:${Number(this.zIndex) - 10};`;
    },
    getBgColor() {
      let color = this.background;
      if (!color || color === true) {
        const app = uni && uni.$fui && uni.$fui.color;
        color = app && app.primary || "#465CFF";
      }
      return color;
    }
  },
  watch: {
    isShow(val) {
      this.$emit("change", {
        isShow: val
      });
    },
    position(val) {
      this.resetNum++;
      this.$nextTick(() => {
        setTimeout(() => {
          this._getSize();
        }, 80);
      });
    }
  },
  data() {
    return {
      isShow: false,
      isHidden: true,
      timer: null,
      maxWidth: 0,
      maxHeight: 0,
      eLeft: 0,
      eTop: 0,
      resetNum: 0
    };
  },
  mounted() {
    this.$nextTick(() => {
      if (!this.$refs["fui_fab_ani"])
        return;
      let styles = {
        transform: "scale(0)",
        opacity: 0
      };
      animation.transition(
        this.$refs["fui_fab_ani"].ref,
        {
          styles,
          duration: 0,
          needLayout: false,
          delay: 0
        },
        () => {
        }
      );
      setTimeout(() => {
        this._getSize();
      }, 50);
    });
  },
  methods: {
    stop() {
    },
    _getSize() {
      if (!this.isDrag)
        return;
      const sys = uni.getSystemInfoSync();
      dom.getComponentRect(this.$refs["fui_fab_move_ref"], (ret) => {
        const size = ret.size;
        if (size) {
          this.maxWidth = sys.windowWidth - size.width - size.left;
          this.maxHeight = sys.windowHeight - size.height - size.top;
          this.eLeft = size.left || 0;
          this.eTop = size.top || 0;
        }
      });
    },
    _animation(type) {
      let styles = {
        transform: `scale(${type ? 1 : 0})`,
        opacity: type ? 1 : 0
      };
      if (!this.$refs["fui_fab_ani"] || !this.$refs["fui_fm_ani"])
        return;
      if (this.mask && this.$refs["fui_mask_ani"]) {
        animation.transition(
          this.$refs["fui_mask_ani"].ref,
          {
            styles: {
              transform: `translateX(${type ? "0" : "-100%"})`
            },
            duration: 0,
            needLayout: false,
            delay: 0
            //ms
          },
          () => {
          }
        );
      }
      animation.transition(
        this.$refs["fui_fm_ani"].ref,
        {
          styles: {
            transform: `rotate(${type ? "135deg" : "0deg"})`
          },
          duration: 250,
          timingFunction: "ease-in-out",
          needLayout: false,
          delay: 0
          //ms
        },
        () => {
          if (!type) {
            this.isHidden = true;
          }
        }
      );
      animation.transition(
        this.$refs["fui_fab_ani"].ref,
        {
          styles,
          duration: 250,
          timingFunction: "ease-in-out",
          needLayout: false,
          delay: 0
          //ms
        },
        () => {
        }
      );
    },
    handleClick: function(e, index2) {
      e.stopPropagation();
      if (this.isMove) {
        this.isMove = false;
        return;
      }
      this.isHidden = false;
      clearTimeout(this.timer);
      this.$nextTick(() => {
        if (index2 === -1 && this.fabs.length > 0) {
          this.isShow = !this.isShow;
          this._animation(this.isShow);
        } else {
          this.$emit("click", {
            index: index2
          });
          this.isShow = false;
          this._animation(this.isShow);
        }
      });
    },
    maskClick(e) {
      e.stopPropagation();
      if (!this.maskClosable)
        return;
      this.isShow = false;
      this._animation(this.isShow);
    }
  }
};
function _sfc_render$1(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_fui_icon = resolveEasycom(resolveDynamicComponent("fui-icon"), __easycom_5);
  return openBlock(), createElementBlock(
    Fragment,
    null,
    [
      createCommentVNode("本文件由FirstUI授权予严天赠（会员ID： 24 4 2，身份证尾号： 0  430  18）专用，请尊重知识产权，勿私下传播，违者追究法律责任。"),
      createElementVNode(
        "view",
        {
          onTouchmove: _cache[6] || (_cache[6] = withModifiers((...args) => $options.stop && $options.stop(...args), ["stop", "prevent"]))
        },
        [
          $props.mask ? (openBlock(), createElementBlock(
            "view",
            {
              key: 0,
              class: normalizeClass(["fui-fab__mask", { "fui-fab__mask-show": $data.isShow }]),
              style: normalizeStyle($options.getStyle),
              ref: "fui_mask_ani",
              onClick: _cache[0] || (_cache[0] = withModifiers((...args) => $options.maskClick && $options.maskClick(...args), ["stop"]))
            },
            null,
            6
            /* CLASS, STYLE */
          )) : createCommentVNode("v-if", true),
          createElementVNode(
            "view",
            {
              class: normalizeClass(["fui-fab__btn-wrap", [$props.position === "left" ? "fui-fab__wrap-left" : "fui-fab__wrap-right"]]),
              onTouchstart: _cache[2] || (_cache[2] = (...args) => _ctx.touchstart && _ctx.touchstart(...args)),
              onTouchmove: _cache[3] || (_cache[3] = withModifiers((...args) => _ctx.touchmove && _ctx.touchmove(...args), ["stop", "prevent"])),
              onTouchend: _cache[4] || (_cache[4] = (...args) => _ctx.touchend && _ctx.touchend(...args)),
              onTouchcancel: _cache[5] || (_cache[5] = (...args) => _ctx.touchend && _ctx.touchend(...args)),
              ref: "fui_fab_move_ref",
              style: normalizeStyle($options.getStyles)
            },
            [
              createElementVNode(
                "view",
                {
                  class: normalizeClass(["fui-fab__btn-list", { "fui-fab__btn-hidden": $data.isHidden, "fui-fab__list-ani": $data.isShow, "fui-fab__list-left": $props.position === "left", "fui-fab__list-right": $props.position === "right" }]),
                  ref: "fui_fab_ani"
                },
                [
                  (openBlock(true), createElementBlock(
                    Fragment,
                    null,
                    renderList($props.fabs, (btn, idx) => {
                      return openBlock(), createElementBlock("view", {
                        class: normalizeClass(["fui-fab__button-box", [$props.position === "left" ? "fui-fab__button-left" : "fui-fab__button-right"]]),
                        key: idx,
                        onClick: withModifiers(($event) => $options.handleClick($event, idx), ["stop"])
                      }, [
                        btn.text ? (openBlock(), createElementBlock(
                          "u-text",
                          {
                            key: 0,
                            class: "fui-fab__btn-text",
                            style: normalizeStyle({ fontSize: (btn.size || 32) + "rpx", color: btn.color || "#fff", textAlign: $props.position === "left" ? "left" : "right" })
                          },
                          toDisplayString(btn.text),
                          5
                          /* TEXT, STYLE */
                        )) : createCommentVNode("v-if", true),
                        createElementVNode(
                          "view",
                          {
                            class: normalizeClass(["fui-fab__button", { "fui-fab__btn-color": !$options.getBgColor && !btn.background }]),
                            style: normalizeStyle({ width: $props.width + "rpx", height: $props.width + "rpx", background: btn.background || $options.getBgColor })
                          },
                          [
                            btn.name ? (openBlock(), createBlock(_component_fui_icon, {
                              key: 0,
                              name: btn.name,
                              color: btn.abbrColor || "#fff",
                              size: btn.abbrSize || 64
                            }, null, 8, ["name", "color", "size"])) : createCommentVNode("v-if", true),
                            !btn.name && btn.src ? (openBlock(), createElementBlock("u-image", {
                              key: 1,
                              src: btn.src,
                              style: normalizeStyle({ width: (btn.width || 56) + "rpx", height: (btn.height || 56) + "rpx", borderRadius: (btn.radius || 0) + "rpx" }),
                              mode: "widthFix"
                            }, null, 12, ["src"])) : createCommentVNode("v-if", true),
                            !btn.name && !btn.src && btn.abbr ? (openBlock(), createElementBlock(
                              "u-text",
                              {
                                key: 2,
                                class: "fui-fab__btn-abbr",
                                style: normalizeStyle({ fontSize: (btn.abbrSize || 36) + "rpx", lineHeight: (btn.abbrSize || 36) + "rpx", color: btn.abbrColor || "#fff" })
                              },
                              toDisplayString(btn.abbr),
                              5
                              /* TEXT, STYLE */
                            )) : createCommentVNode("v-if", true)
                          ],
                          6
                          /* CLASS, STYLE */
                        )
                      ], 10, ["onClick"]);
                    }),
                    128
                    /* KEYED_FRAGMENT */
                  ))
                ],
                2
                /* CLASS */
              ),
              createElementVNode(
                "view",
                {
                  class: normalizeClass(["fui-fab__btn-main", { "fui-fab__btn-color": !$options.getBgColor }]),
                  style: normalizeStyle({ width: $props.width + "rpx", height: $props.width + "rpx", background: $options.getBgColor }),
                  onClick: _cache[1] || (_cache[1] = withModifiers(($event) => $options.handleClick($event, -1), ["stop"]))
                },
                [
                  createElementVNode(
                    "view",
                    {
                      class: normalizeClass(["fui-fab__btn-inner", { "fui-fab__btn-ani": $data.isShow }]),
                      ref: "fui_fm_ani"
                    },
                    [
                      renderSlot(_ctx.$slots, "default", {}, () => [
                        createVNode(_component_fui_icon, {
                          name: "plus",
                          color: $props.color,
                          size: 80
                        }, null, 8, ["color"])
                      ])
                    ],
                    2
                    /* CLASS */
                  )
                ],
                6
                /* CLASS, STYLE */
              )
            ],
            38
            /* CLASS, STYLE, HYDRATE_EVENTS */
          )
        ],
        32
        /* HYDRATE_EVENTS */
      )
    ],
    2112
    /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
  );
}
const __easycom_7 = /* @__PURE__ */ _export_sfc(_sfc_main$1, [["render", _sfc_render$1], ["styles", [_style_0$1]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-fab/fui-fab.vue"]]);
const _style_0 = { "fui-wrap": { "": { "paddingBottom": "64rpx" } }, "fui-btn__box": { "": { "width": "750rpx", "paddingTop": "96rpx", "paddingRight": "48rpx", "paddingBottom": "96rpx", "paddingLeft": "48rpx" } }, "fui-chat__item": { "": { "flex": 1, "flexDirection": "row", "alignItems": "center", "justifyContent": "space-between" } }, "fui-chat__left": { "": { "flexDirection": "row", "alignItems": "center", "overflow": "hidden", "flex": 1 } }, "fui-img__box": { "": { "width": "96rpx", "height": "96rpx", "position": "relative" } }, "fui-status": { "": { "width": "28rpx", "height": "28rpx", "position": "absolute", "right": 0, "bottom": 0, "borderRadius": "16rpx", "backgroundColor": "#BDBDBD", "borderWidth": 1, "borderStyle": "solid", "borderColor": "#ffffff" } }, "fui-status__online": { "": { "backgroundColor": "#246BFD" } }, "fui-info__box": { "": { "paddingLeft": "20rpx", "paddingRight": "144rpx" } }, "fui-name": { "": { "fontWeight": "700", "fontSize": "36rpx" } }, "fui-content": { "": { "fontSize": "28rpx", "color": "#616161", "paddingTop": "12rpx" } }, "fui-chat__right": { "": { "flexDirection": "column", "alignItems": "flex-end" } }, "fui-time": { "": { "fontSize": "26rpx", "color": "#616161", "paddingTop": "16rpx" } } };
const _sfc_main = {
  data() {
    return {
      list: [],
      listData: [{
        avatar: "/static/images/chat/avatar/img_avatar_1.png",
        name: "Aileen Fullbright",
        msg: "Can't wait to try it out! 😍😍",
        num: 1,
        time: "20:00",
        status: 1
      }, {
        avatar: "/static/images/chat/avatar/img_avatar_2.png",
        name: "王小小",
        msg: "迫不及待地想试试！ 😍😍",
        num: 1,
        time: "昨天",
        status: 1
      }, {
        avatar: "/static/images/chat/avatar/img_avatar_3.png",
        name: "Freida Varnes",
        msg: "Sky blue. Trying it now! 😂",
        num: 0,
        time: "昨天",
        status: 0
      }, {
        avatar: "/static/images/chat/avatar/img_def_1.png",
        name: "Aileen Fullbright",
        msg: "Wow, this is really epic 👍",
        num: 0,
        time: "前天 12:00",
        status: 0
      }, {
        avatar: "/static/images/chat/avatar/img_def.png",
        name: "Aileen Fullbright",
        msg: "Can't wait to try it out! 😍😍",
        num: 1,
        time: "20:00",
        status: 1
      }, {
        avatar: "/static/images/chat/avatar/img_avatar_2.png",
        name: "王小小",
        msg: "迫不及待地想试试！ 😍😍",
        num: 1,
        time: "昨天",
        status: 1
      }, {
        avatar: "/static/images/chat/avatar/img_avatar_3.png",
        name: "Freida Varnes",
        msg: "Sky blue. Trying it now! 😂",
        num: 0,
        time: "昨天",
        status: 0
      }, {
        avatar: "/static/images/chat/avatar/img_def_1.png",
        name: "Aileen Fullbright",
        msg: "Wow, this is really epic 👍",
        num: 0,
        time: "前天 12:00",
        status: 0
      }, {
        avatar: "/static/images/chat/avatar/img_def_1.png",
        name: "Aileen Fullbright",
        msg: "Wow, this is really epic 👍",
        num: 0,
        time: "前天 12:00",
        status: 0
      }, {
        avatar: "/static/images/chat/avatar/img_def_1.png",
        name: "Aileen Fullbright",
        msg: "Wow, this is really epic 👍",
        num: 0,
        time: "前天 12:00",
        status: 0
      }],
      fabs: [{
        name: "my",
        text: "联系人"
      }, {
        name: "community",
        text: "个人中心"
      }, {
        name: "message",
        text: "聊天"
      }]
    };
  },
  computed: {
    //用于判断用户是否登录
    isLogin() {
      return uni.$cloud.store("isLogin");
    }
  },
  onShow() {
    formatAppLog("log", "at pages/index/index.nvue:130", "查看判断登录-A", this.isLogin);
  },
  methods: {
    onClick(e, index2) {
      formatAppLog("log", "at pages/index/index.nvue:134", e, index2);
      const item = this.list[index2];
      uni.fui.toast(`删除 ${item.name}`);
    },
    startChat() {
      this.list = [...this.listData];
    },
    chat(e) {
      formatAppLog("log", "at pages/index/index.nvue:143", e);
      uni.fui.href("../chat/chat");
    },
    handleClick(e) {
      formatAppLog("log", "at pages/index/index.nvue:147", e);
      const index2 = e.index;
      let url = "../chat/chat";
      if (index2 === 0) {
        url = "../contacts/contacts";
      } else if (index2 === 1) {
        url = "../my/my";
      }
      uni.fui.href(url);
    }
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_fui_avatar = resolveEasycom(resolveDynamicComponent("fui-avatar"), __easycom_0);
  const _component_fui_badge = resolveEasycom(resolveDynamicComponent("fui-badge"), __easycom_1);
  const _component_fui_list_cell = resolveEasycom(resolveDynamicComponent("fui-list-cell"), __easycom_3$1);
  const _component_fui_swipe_action = resolveEasycom(resolveDynamicComponent("fui-swipe-action"), __easycom_3);
  const _component_fui_swipeaction_group = resolveEasycom(resolveDynamicComponent("fui-swipeaction-group"), __easycom_4);
  const _component_fui_button = resolveEasycom(resolveDynamicComponent("fui-button"), __easycom_6$1);
  const _component_fui_empty = resolveEasycom(resolveDynamicComponent("fui-empty"), __easycom_6);
  const _component_fui_fab = resolveEasycom(resolveDynamicComponent("fui-fab"), __easycom_7);
  return openBlock(), createElementBlock("scroll-view", {
    scrollY: true,
    showScrollbar: true,
    enableBackToTop: true,
    bubble: "true",
    style: { flexDirection: "column" }
  }, [
    createElementVNode("view", { class: "fui-wrap" }, [
      createVNode(_component_fui_swipeaction_group, null, {
        default: withCtx(() => [
          (openBlock(true), createElementBlock(
            Fragment,
            null,
            renderList($data.list, (item, index2) => {
              return openBlock(), createBlock(_component_fui_swipe_action, {
                onClick: ($event) => $options.onClick($event, index2),
                key: index2
              }, {
                default: withCtx(() => [
                  createVNode(_component_fui_list_cell, {
                    "bottom-border": false,
                    onClick: ($event) => $options.chat(item)
                  }, {
                    default: withCtx(() => [
                      createElementVNode("view", { class: "fui-chat__item" }, [
                        createElementVNode("view", { class: "fui-chat__left" }, [
                          createElementVNode("view", { class: "fui-img__box" }, [
                            createVNode(_component_fui_avatar, {
                              src: item.avatar
                            }, null, 8, ["src"]),
                            createElementVNode(
                              "view",
                              {
                                class: normalizeClass(["fui-status", { "fui-status__online": item.status == 1 }])
                              },
                              null,
                              2
                              /* CLASS */
                            )
                          ]),
                          createElementVNode("view", { class: "fui-info__box fui-ellipsis" }, [
                            createElementVNode(
                              "u-text",
                              { class: "fui-name fui-ellipsis" },
                              toDisplayString(item.name),
                              1
                              /* TEXT */
                            ),
                            createElementVNode(
                              "u-text",
                              { class: "fui-content fui-ellipsis" },
                              toDisplayString(item.msg),
                              1
                              /* TEXT */
                            )
                          ])
                        ]),
                        createElementVNode("view", { class: "fui-chat__right" }, [
                          item.num > 0 ? (openBlock(), createBlock(_component_fui_badge, {
                            key: 0,
                            type: "danger",
                            value: item.num
                          }, null, 8, ["value"])) : createCommentVNode("v-if", true),
                          createElementVNode(
                            "u-text",
                            { class: "fui-time" },
                            toDisplayString(item.time),
                            1
                            /* TEXT */
                          )
                        ])
                      ])
                    ]),
                    _: 2
                    /* DYNAMIC */
                  }, 1032, ["onClick"])
                ]),
                _: 2
                /* DYNAMIC */
              }, 1032, ["onClick"]);
            }),
            128
            /* KEYED_FRAGMENT */
          ))
        ]),
        _: 1
        /* STABLE */
      }),
      $data.list.length === 0 ? (openBlock(), createBlock(_component_fui_empty, {
        key: 0,
        isFixed: "",
        width: "400",
        height: "400",
        src: "/static/images/common/video_call.png",
        title: "欢迎使用FirstUI Chat !",
        descr: "FirstUI Chat将您与家人和朋友联系起来。现在开始聊天！"
      }, {
        default: withCtx(() => [
          createElementVNode("view", { class: "fui-btn__box" }, [
            createVNode(_component_fui_button, {
              radius: "80rpx",
              text: "开始新聊天",
              onClick: $options.startChat
            }, null, 8, ["onClick"])
          ])
        ]),
        _: 1
        /* STABLE */
      })) : createCommentVNode("v-if", true),
      createVNode(_component_fui_fab, {
        zIndex: "999",
        background: "#246BFD",
        position: "right",
        fabs: $data.fabs,
        onClick: $options.handleClick
      }, null, 8, ["fabs", "onClick"])
    ])
  ]);
}
const index = /* @__PURE__ */ _export_sfc(_sfc_main, [["render", _sfc_render], ["styles", [_style_0]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/pages/index/index.nvue"]]);
export {
  index as default
};
