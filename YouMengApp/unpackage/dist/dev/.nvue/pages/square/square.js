import { openBlock, createElementBlock, createElementVNode, Fragment, renderList } from "vue";
import { _ as _export_sfc } from "../../_plugin-vue_export-helper.js";
const _sfc_main = {
  data() {
    return {};
  },
  methods: {}
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return openBlock(), createElementBlock("scroll-view", {
    scrollY: true,
    showScrollbar: true,
    enableBackToTop: true,
    bubble: "true",
    style: { flexDirection: "column" }
  }, [
    createElementVNode("view", null, [
      (openBlock(), createElementBlock(
        Fragment,
        null,
        renderList(120, (i) => {
          return createElementVNode("view", {
            class: "",
            key: i
          }, [
            createElementVNode("u-text", null, " square ")
          ]);
        }),
        64
        /* STABLE_FRAGMENT */
      ))
    ])
  ]);
}
const square = /* @__PURE__ */ _export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/pages/square/square.nvue"]]);
export {
  square as default
};
