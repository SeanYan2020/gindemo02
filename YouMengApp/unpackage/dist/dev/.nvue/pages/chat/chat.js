import { _ as __easycom_0 } from "../../fui-avatar.js";
import { openBlock, createElementBlock, Fragment, createCommentVNode, createElementVNode, normalizeClass, normalizeStyle, withModifiers, renderSlot, resolveDynamicComponent, createVNode, createBlock, renderList, toDisplayString, withCtx } from "vue";
import { a as requireNativePlugin, r as resolveEasycom } from "../../uni-app.es.js";
import { _ as __easycom_5 } from "../../fui-icon.js";
import { _ as _export_sfc } from "../../_plugin-vue_export-helper.js";
const _style_0$2 = { "fui-safe__area-wrap": { "": { "flex": 1, "flexDirection": "row" } }, "fui-safe__area-weex": { "": { "paddingBottom": 34 } } };
const _sfc_main$2 = {
  name: "fui-safe-area",
  props: {
    //背景颜色
    background: {
      type: String,
      default: "#FFFFFF"
    }
  },
  created() {
    this.iphonex = this.isPhoneX();
  },
  data() {
    return {
      iphonex: false
    };
  },
  methods: {
    isPhoneX() {
      const res = uni.getSystemInfoSync();
      let iphonex = false;
      let models = [
        "iphonex",
        "iphonexr",
        "iphonexsmax",
        "iphone11",
        "iphone11pro",
        "iphone11promax",
        "iphone12",
        "iphone12mini",
        "iphone12pro",
        "iphone12promax",
        "iphone13",
        "iphone13mini",
        "iphone13pro",
        "iphone13promax",
        "iphone14",
        "iphone14mini",
        "iphone14pro",
        "iphone14promax",
        "iphone15",
        "iphone15mini",
        "iphone15pro",
        "iphone15promax"
      ];
      const model = res.model.replace(/\s/g, "").toLowerCase();
      const newModel = model.split("<")[0];
      if (models.includes(model) || models.includes(newModel) || res.safeAreaInsets && res.safeAreaInsets.bottom > 0) {
        iphonex = true;
      }
      return iphonex;
    }
  }
};
function _sfc_render$2(_ctx, _cache, $props, $setup, $data, $options) {
  return openBlock(), createElementBlock(
    Fragment,
    null,
    [
      createCommentVNode("本文件由FirstUI授权予严天赠（会员ID：2  44 2，身份证尾号：   04 30 18）专用，请尊重知识产权，勿私下传播，违者追究法律责任。"),
      createElementVNode(
        "view",
        {
          class: normalizeClass(["fui-safe__area-wrap", { "fui-safe__area-weex": $data.iphonex }]),
          style: normalizeStyle({ background: $props.background })
        },
        null,
        6
        /* CLASS, STYLE */
      )
    ],
    2112
    /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
  );
}
const __easycom_2 = /* @__PURE__ */ _export_sfc(_sfc_main$2, [["render", _sfc_render$2], ["styles", [_style_0$2]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-safe-area/fui-safe-area.vue"]]);
const _style_0$1 = { "fui-bottom__popup-wrap": { "": { "position": "fixed", "left": 0, "right": 0, "top": 0, "bottom": 0, "zIndex": 1001, "flexDirection": "row", "alignItems": "flex-end", "justifyContent": "center", "opacity": 1e-3 } }, "fui-bottom__popup-border": { "": { "position": "absolute", "left": 0, "right": 0, "bottom": 0 } }, "fui-bottom__popup": { "": { "flex": 1, "transform": "translateY(100%)", "flexDirection": "row" } }, "fui-bp__safe-weex": { "": { "paddingBottom": 34 } } };
const animation = requireNativePlugin("animation");
const _sfc_main$1 = {
  name: "fui-bottom-popup",
  emits: ["close"],
  props: {
    show: {
      type: Boolean,
      default: false
    },
    //背景颜色
    background: {
      type: String,
      default: "#fff"
    },
    //圆角
    radius: {
      type: [Number, String],
      default: 24
    },
    zIndex: {
      type: [Number, String],
      default: 996
    },
    //点击遮罩 是否可关闭
    maskClosable: {
      type: Boolean,
      default: true
    },
    maskBackground: {
      type: String,
      default: "rgba(0,0,0,.6)"
    },
    //是否适配底部安全区
    safeArea: {
      type: Boolean,
      default: true
    }
  },
  data() {
    let isAndroid = false;
    let isNvue = false;
    isNvue = true;
    const res = uni.getSystemInfoSync();
    isAndroid = res.platform.toLocaleLowerCase() == "android";
    return {
      iphoneX: false,
      isNvue,
      isShow: false,
      isAndroid
    };
  },
  watch: {
    show: {
      handler(newVal) {
        if (newVal) {
          this.open();
        } else {
          this.close();
        }
      },
      immediate: true
    }
  },
  created() {
    this.iphoneX = this.isPhoneX();
  },
  methods: {
    handleClose(e) {
      if (!this.maskClosable)
        return;
      this.$emit("close", {});
    },
    isPhoneX() {
      if (!this.safeArea)
        return false;
      const res = uni.getSystemInfoSync();
      let iphonex = false;
      let models = [
        "iphonex",
        "iphonexr",
        "iphonexsmax",
        "iphone11",
        "iphone11pro",
        "iphone11promax",
        "iphone12",
        "iphone12mini",
        "iphone12pro",
        "iphone12promax",
        "iphone13",
        "iphone13mini",
        "iphone13pro",
        "iphone13promax",
        "iphone14",
        "iphone14mini",
        "iphone14pro",
        "iphone14promax"
      ];
      const model = res.model.replace(/\s/g, "").toLowerCase();
      const newModel = model.split("<")[0];
      if (models.includes(model) || models.includes(newModel) || res.safeAreaInsets && res.safeAreaInsets.bottom > 0) {
        iphonex = true;
      }
      return iphonex;
    },
    open() {
      this.isShow = true;
      this.$nextTick(() => {
        setTimeout(() => {
          this._animation(true);
        }, 50);
      });
    },
    close() {
      this._animation(false);
    },
    _animation(type) {
      if (!this.$refs["fui_bp_ani"] || !this.$refs["fui_bp_mk_ani"])
        return;
      animation.transition(
        this.$refs["fui_bp_mk_ani"].ref,
        {
          styles: {
            opacity: type ? 1 : 0
          },
          duration: 250,
          timingFunction: "ease-in-out",
          needLayout: false,
          delay: 0
          //ms
        },
        () => {
          if (!type) {
            this.isShow = false;
          }
        }
      );
      animation.transition(
        this.$refs["fui_bp_ani"].ref,
        {
          styles: {
            transform: `translateY(${type ? "0" : "100%"})`
          },
          duration: !type && this.isAndroid ? 20 : 250,
          timingFunction: "ease-in-out",
          needLayout: false,
          delay: 0
          //ms
        },
        () => {
        }
      );
    },
    stop(e, tap) {
      tap && e.stopPropagation();
    }
  }
};
function _sfc_render$1(_ctx, _cache, $props, $setup, $data, $options) {
  return openBlock(), createElementBlock(
    Fragment,
    null,
    [
      createCommentVNode("本文件由FirstUI授权予严天赠（会员ID：24   42，身份证尾号：0     43018）专用，请尊重知识产权，勿私下传播，违者追究法律责任。"),
      $data.isShow || !$data.isNvue ? (openBlock(), createElementBlock(
        "view",
        {
          key: 0,
          class: normalizeClass(["fui-bottom__popup-wrap", { "fui-bottom__popwrap-show": $props.show }]),
          style: normalizeStyle({ zIndex: $props.zIndex, background: $props.maskBackground }),
          onClick: _cache[1] || (_cache[1] = withModifiers((...args) => $options.handleClose && $options.handleClose(...args), ["stop"])),
          onTouchmove: _cache[2] || (_cache[2] = withModifiers((...args) => $options.stop && $options.stop(...args), ["stop", "prevent"])),
          ref: "fui_bp_mk_ani"
        },
        [
          $props.radius ? (openBlock(), createElementBlock(
            "view",
            {
              key: 0,
              class: "fui-bottom__popup-border",
              style: normalizeStyle({ height: $props.radius + "rpx", background: $props.background })
            },
            null,
            4
            /* STYLE */
          )) : createCommentVNode("v-if", true),
          createElementVNode(
            "view",
            {
              ref: "fui_bp_ani",
              class: normalizeClass(["fui-bottom__popup", { "fui-bottom__popup-show": $props.show, "fui-bp__safe-weex": $data.iphoneX && $props.safeArea }]),
              style: normalizeStyle({ borderTopLeftRadius: $props.radius + "rpx", borderTopRightRadius: $props.radius + "rpx", background: $props.background }),
              onClick: _cache[0] || (_cache[0] = withModifiers(($event) => $options.stop($event, true), ["stop"]))
            },
            [
              renderSlot(_ctx.$slots, "default")
            ],
            6
            /* CLASS, STYLE */
          )
        ],
        38
        /* CLASS, STYLE, HYDRATE_EVENTS */
      )) : createCommentVNode("v-if", true)
    ],
    2112
    /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
  );
}
const __easycom_3 = /* @__PURE__ */ _export_sfc(_sfc_main$1, [["render", _sfc_render$1], ["styles", [_style_0$1]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-bottom-popup/fui-bottom-popup.vue"]]);
const faceList = [
  "😀",
  "😁",
  "😃",
  "😄",
  "😅",
  "😆",
  "😉",
  "😊",
  "😋",
  "😎",
  "😍",
  "😘",
  "😗",
  "😙",
  "😚",
  "😇",
  "😐",
  "😑",
  "😶",
  "😏",
  "😣",
  "😥",
  "😮",
  "😯",
  "😪",
  "😫",
  "😴",
  "😌",
  "😛",
  "😜",
  "😝",
  "😒",
  "😓",
  "😔",
  "😕",
  "😲",
  "😷",
  "😖",
  "😞",
  "😟",
  "😤",
  "😢",
  "😭",
  "😦",
  "😧",
  "😨",
  "😬",
  "😰",
  "😱",
  "😳",
  "😵",
  "😡",
  "😠",
  "👦",
  "👧",
  "👨",
  "👩",
  "👴",
  "👵",
  "👶",
  "👱",
  "👮",
  "👲",
  "👳",
  "👷",
  "👸",
  "💂",
  "🎅",
  "👰",
  "👼",
  "💆",
  "💇",
  "🙍",
  "🙎",
  "🙅",
  "🙆",
  "💁",
  "🙋",
  "🙇",
  "🙌",
  "🙏",
  "👤",
  "👥",
  "🚶",
  "🏃",
  "👯",
  "💃",
  "👫",
  "👬",
  "👭",
  "💏",
  "💑",
  "👪",
  "💪",
  "👈",
  "👉",
  "☝",
  "👆",
  "👇",
  "✌",
  "✋",
  "👌",
  "👍",
  "👎",
  "✊",
  "👊",
  "👋",
  "👏",
  "👐",
  "✍",
  "👣",
  "👀",
  "👂",
  "👃",
  "👅",
  "👄",
  "💋",
  "👓",
  "👔",
  "👙",
  "👛",
  "👜",
  "👝",
  "🎒",
  "💼",
  "👞",
  "👟",
  "👠",
  "👡",
  "👢",
  "👑",
  "👒",
  "🎩",
  "🎓",
  "💄",
  "💅",
  "💍",
  "🌂",
  "📶",
  "📳",
  "📴",
  "♻",
  "🏧",
  "🚮",
  "🚰",
  "♿",
  "🚹",
  "🚺",
  "🚻",
  "🚼",
  "🚾",
  "⚠",
  "🚸",
  "⛔",
  "🚫",
  "🚳",
  "🚭",
  "🚯",
  "🚱",
  "🚷",
  "🔞",
  "💈",
  "🙈",
  "🐒",
  "🐶",
  "🐕",
  "🐩",
  "🐺",
  "🐱",
  "🐈",
  "🐯",
  "🐅",
  "🐆",
  "🐴",
  "🐎",
  "🐮",
  "🐂",
  "🐃",
  "🐄",
  "🐷",
  "🐖",
  "🐗",
  "🐽",
  "🐏",
  "🐑",
  "🐐",
  "🐪",
  "🐫",
  "🐘",
  "🐭",
  "🐁",
  "🐀",
  "🐹",
  "🐰",
  "🐇",
  "🐻",
  "🐨",
  "🐼",
  "🐾",
  "🐔",
  "🐓",
  "🐣",
  "🐤",
  "🐥",
  "🐦",
  "🐧",
  "🐸",
  "🐊",
  "🐢",
  "🐍",
  "🐲",
  "🐉",
  "🐳",
  "🐋",
  "🐬",
  "🐟",
  "🐠",
  "🐡",
  "🐙",
  "🐚",
  "🐌",
  "🐛",
  "🐜",
  "🐝",
  "🐞",
  "🦋",
  "💐",
  "🌸",
  "💮",
  "🌹",
  "🌺",
  "🌻",
  "🌼",
  "🌷",
  "🌱",
  "🌲",
  "🌳",
  "🌴",
  "🌵",
  "🌾",
  "🌿",
  "🍀",
  "🍁",
  "🍂",
  "🍃",
  "🌍",
  "🌎",
  "🌏",
  "🌐",
  "🌑",
  "🌒",
  "🌓",
  "🌔",
  "🌕",
  "🌖",
  "🌗",
  "🌘",
  "🌙",
  "🌚",
  "🌛",
  "🌜",
  "☀",
  "🌝",
  "🌞",
  "⭐",
  "🌟",
  "🌠",
  "☁",
  "⛅",
  "☔",
  "⚡",
  "❄",
  "🔥",
  "💧",
  "🌊",
  "🏀",
  "🏈",
  "🏉",
  "🎾",
  "🎱",
  "🎳",
  "⛳",
  "🎣",
  "🎽",
  "🎿",
  "😈",
  "👿",
  "👹",
  "👺",
  "💀",
  "☠",
  "👻",
  "👽",
  "👾",
  "💣",
  "🌋",
  "🗻",
  "🏠",
  "🏡",
  "🏢",
  "🏣",
  "🏤",
  "🏥",
  "🏦",
  "🏨",
  "⛲",
  "🌁",
  "🌃",
  "🌆",
  "🌇",
  "🎠",
  "🎡",
  "🎢",
  "🚂",
  "🚌",
  "🚍",
  "🚎",
  "🚏",
  "🚐",
  "🚑",
  "🚒",
  "🚓",
  "🚔",
  "🚕",
  "🚖",
  "🚗",
  "🚘",
  "💌",
  "💎",
  "🔪",
  "💈",
  "🚪",
  "🚽",
  "🚿",
  "🛁",
  "⌛",
  "⏳",
  "⌚",
  "⏰",
  "🎈",
  "🎉",
  "💤",
  "💢",
  "💬",
  "💭",
  "♨",
  "🌀",
  "🔔",
  "🔕",
  "✡",
  "✝",
  "🔯",
  "📛",
  "🔰",
  "🔱",
  "⭕",
  "✅",
  "☑",
  "✔",
  "✖",
  "❌",
  "❎",
  "➕",
  "➖",
  "➗",
  "➰",
  "➿",
  "〽",
  "✳",
  "✴",
  "❇",
  "‼",
  "⁉",
  "❓",
  "❔",
  "❕",
  "❗",
  "🕛",
  "🕧",
  "🕐",
  "🕜",
  "🕑",
  "🕝",
  "🕒",
  "🕞",
  "🕓",
  "🕟",
  "🕔",
  "🕠",
  "🕕",
  "🕡",
  "🕖",
  "🕢",
  "🕗",
  "🕣",
  "🕘",
  "🕤",
  "🕙",
  "🕥",
  "🕚",
  "🕦",
  "⏱",
  "⏲",
  "🕰",
  "💘",
  "❤",
  "💓",
  "💔",
  "💕",
  "💖",
  "💗",
  "💙",
  "💚",
  "💛",
  "💜",
  "💝",
  "💞",
  "💟",
  "🍇",
  "🍈",
  "🍉",
  "🍊",
  "🍋",
  "🍌",
  "🍍",
  "🍎",
  "🍏",
  "🍐",
  "🍑",
  "🍒",
  "🍓"
];
const _style_0 = { "fui-wrap": { "": { "paddingBottom": "108rpx" } }, "fui-chat__list": { "": { "paddingTop": "48rpx" } }, "fui-chat__pbm": { "": { "paddingBottom": "540rpx" } }, "fui-chat__item": { "": { "width": "750rpx", "paddingLeft": "24rpx", "paddingRight": "120rpx", "marginBottom": "48rpx", "flexDirection": "row", "overflow": "hidden" } }, "fui-content__box": { "": { "flex": 1, "marginLeft": "24rpx", "flexDirection": "row", "alignItems": "center" }, ".fui-item__right ": { "marginLeft": 0, "marginRight": "24rpx", "flexDirection": "row-reverse" } }, "fui-content__text": { "": { "fontSize": "32rpx", "color": "#212121", "paddingTop": "20rpx", "paddingRight": "24rpx", "paddingBottom": "20rpx", "paddingLeft": "24rpx", "backgroundColor": "#f8f8f8", "borderTopLeftRadius": "8rpx", "borderTopRightRadius": "32rpx", "borderBottomRightRadius": "32rpx", "borderBottomLeftRadius": "32rpx", "maxWidth": "486rpx" }, ".fui-item__right ": { "borderTopLeftRadius": "32rpx", "borderTopRightRadius": "8rpx", "borderBottomRightRadius": "32rpx", "borderBottomLeftRadius": "32rpx", "backgroundColor": "#246BFD", "color": "#ffffff" } }, "fui-item__right": { "": { "paddingLeft": "120rpx", "paddingRight": "24rpx", "flexDirection": "row-reverse" } }, "fui-chat__img": { "": { "width": "160rpx", "height": "160rpx" } }, "fui-audio__box": { "": { "flexDirection": "row", "alignItems": "center", "height": "80rpx", "borderRadius": "40rpx", "backgroundColor": "rgba(36,107,253,0.08)", "paddingTop": 0, "paddingRight": "48rpx", "paddingBottom": 0, "paddingLeft": "32rpx" } }, "fui-icon__audio": { "": { "width": "32rpx", "height": "32rpx" } }, "fui-time": { "": { "fontSize": "24rpx", "lineHeight": "24rpx", "paddingLeft": "16rpx", "color": "#246BFD" } }, "fui-op__icon": { "": { "paddingLeft": "12rpx" }, ".fui-item__right ": { "paddingRight": "12rpx" } }, "fui-chatbar__wrap": { "": { "paddingTop": 6, "paddingRight": 0, "paddingBottom": 6, "paddingLeft": 0, "flexDirection": "row", "alignItems": "flex-end", "justifyContent": "space-between", "backgroundColor": "#FAFAFA" } }, "fui-chatbar__input-box": { "": { "flexDirection": "row", "flex": 1, "position": "relative" } }, "fui-chatbar__input": { "": { "flex": 1, "height": "72rpx", "paddingTop": "16rpx", "paddingRight": "20rpx", "paddingBottom": "16rpx", "paddingLeft": "20rpx", "borderRadius": "8rpx", "fontSize": "32rpx", "backgroundColor": "#ffffff" } }, "fui-chatbar__voice": { "": { "position": "absolute", "left": 0, "right": 0, "top": 0, "bottom": 0, "backgroundColor": "#ffffff", "borderRadius": "8rpx", "textAlign": "center", "alignItems": "center", "justifyContent": "center", "backgroundColor:active": "#dddddd" } }, "fui-chatbar__voice-text": { "": { "textAlign": "center", "fontWeight": "bold", "fontSize": "32rpx" } }, "fui-chatbar__icon-box": { "": { "height": "72rpx", "paddingTop": 0, "paddingRight": "16rpx", "paddingBottom": 0, "paddingLeft": "16rpx", "alignItems": "center", "justifyContent": "center", "opacity:active": 0.5 } }, "fui-btn__send": { "": { "width": "64rpx", "height": "64rpx" } }, "fui-chatbar__pdl": { "": { "paddingLeft": 0 } }, "fui-chatbar__fixed": { "": { "width": "750rpx", "position": "fixed", "left": 0, "right": 0, "bottom": 0, "backgroundColor": "#f8f8f8", "transitionProperty": "transform", "transitionDuration": 200 } }, "fui-face__show": { "": { "transform": "translateY(0)" } }, "fui-scroll__box": { "": { "position": "relative" } }, "fui-face__box": { "": { "width": "750rpx", "height": "520rpx", "backgroundColor": "#f8f8f8" } }, "fui-face__inner": { "": { "flexDirection": "row", "flexWrap": "wrap", "paddingTop": "32rpx", "paddingRight": "2rpx", "paddingBottom": "32rpx", "paddingLeft": "2rpx" } }, "fui-face__text": { "": { "width": "106rpx", "fontSize": "60rpx", "paddingTop": "16rpx", "paddingRight": 0, "paddingBottom": "16rpx", "paddingLeft": 0, "textAlign": "center", "justifyContent": "center", "alignItems": "center" } }, "fui-del__box": { "": { "position": "absolute", "right": "32rpx", "bottom": "32rpx", "width": "120rpx", "height": "72rpx", "backgroundColor": "#ffffff", "borderRadius": "16rpx", "alignItems": "center", "justifyContent": "center", "backgroundColor:active": "#dddddd" } }, "fui-custom__wrap": { "": { "width": "750rpx", "height": "520rpx", "flexDirection": "row", "position": "relative" } }, "fui-icon__close": { "": { "position": "absolute", "right": "32rpx", "top": "32rpx" } }, "fui-op__list": { "": { "flex": 1, "flexDirection": "row", "flexWrap": "wrap", "paddingTop": "100rpx", "paddingRight": "44rpx", "paddingBottom": 0, "paddingLeft": "44rpx" } }, "fui-op__item": { "": { "width": "220rpx", "alignItems": "center", "justifyContent": "center", "marginBottom": "32rpx" } }, "fui-op--img": { "": { "width": "120rpx", "height": "120rpx" } }, "fui-op--text": { "": { "fontSize": "28rpx", "paddingTop": "12rpx" } }, "@TRANSITION": { "fui-chatbar__fixed": { "property": "transform", "duration": 200 } } };
const _sfc_main = {
  data() {
    let spacing = 16;
    spacing = 6;
    return {
      opList: [{
        src: "doc",
        text: "文件"
      }, {
        src: "camera",
        text: "拍摄"
      }, {
        src: "pic",
        text: "照片"
      }, {
        src: "addr",
        text: "位置"
      }, {
        src: "user",
        text: "联系人"
      }, {
        src: "audio",
        text: "语音输入"
      }],
      faceList,
      spacing,
      //keyboard
      isVoice: true,
      focus: false,
      faceShow: false,
      msg: "",
      show: false,
      isSend: false,
      //vue3 & nvue下 rpx失效
      translateY: uni.upx2px(520)
    };
  },
  watch: {
    msg(newValue, oldValue) {
      if (newValue) {
        this.isSend = true;
      } else {
        this.isSend = false;
      }
    }
  },
  methods: {
    change() {
      if (this.msg) {
        uni.fui.toast("发送信息！");
        this.msg = "";
      } else {
        this.isVoice = !this.isVoice;
        this.faceShow = false;
        uni.hideKeyboard();
        this.$refs.textareaRef.blur();
      }
    },
    focusChange() {
      this.focus = true;
    },
    blurChange() {
      this.focus = false;
    },
    faceChange() {
      this.faceShow = !this.faceShow;
      if (this.faceShow) {
        this.isVoice = true;
      }
      uni.hideKeyboard();
      this.$refs.textareaRef.blur();
    },
    faceClick(item) {
      if (!this.faceShow)
        return;
      this.msg += item;
    },
    openPopup() {
      this.show = true;
      this.faceShow = false;
      uni.hideKeyboard();
      this.$refs.textareaRef.blur();
    },
    closePopup() {
      this.show = false;
    },
    delMsg() {
      if (!this.msg)
        return;
      let number = 1;
      if (this.msg.length > 1) {
        number = this.msg.substr(this.msg.length - 2, this.msg.length).search(
          /(\ud83c[\udf00-\udfff])|(\ud83d[\udc00-\ude4f])|(\ud83d[\ude80-\udeff])/i
        ) == -1 ? 1 : 2;
      }
      this.msg = this.msg.substr(0, this.msg.length - number);
    }
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_fui_avatar = resolveEasycom(resolveDynamicComponent("fui-avatar"), __easycom_0);
  const _component_fui_icon = resolveEasycom(resolveDynamicComponent("fui-icon"), __easycom_5);
  const _component_fui_safe_area = resolveEasycom(resolveDynamicComponent("fui-safe-area"), __easycom_2);
  const _component_fui_bottom_popup = resolveEasycom(resolveDynamicComponent("fui-bottom-popup"), __easycom_3);
  return openBlock(), createElementBlock("scroll-view", {
    scrollY: true,
    showScrollbar: true,
    enableBackToTop: true,
    bubble: "true",
    style: { flexDirection: "column" }
  }, [
    createElementVNode("view", { class: "fui-wrap" }, [
      createElementVNode(
        "view",
        {
          class: normalizeClass(["fui-chat__list", { "fui-chat__pbm": $data.faceShow }])
        },
        [
          createElementVNode("view", { class: "fui-chat__item" }, [
            createVNode(_component_fui_avatar, { src: "/static/images/chat/avatar/img_avatar_2.png" }),
            createElementVNode("view", { class: "fui-content__box" }, [
              createElementVNode("u-text", { class: "fui-content__text" }, "FirstUI GPT机器人聊天地址：chatgpt.firstui.cn，使用微信打开链接即可体验！")
            ])
          ]),
          createElementVNode("view", { class: "fui-chat__item fui-item__right" }, [
            createVNode(_component_fui_avatar, { src: "/static/images/chat/avatar/img_avatar_1.png" }),
            createElementVNode("view", { class: "fui-content__box" }, [
              createElementVNode("u-text", { class: "fui-content__text" }, "Can't wait to try it out! 😍😍")
            ])
          ]),
          createCommentVNode(" 图片 "),
          createElementVNode("view", { class: "fui-chat__item" }, [
            createVNode(_component_fui_avatar, { src: "/static/images/chat/avatar/img_avatar_2.png" }),
            createElementVNode("view", { class: "fui-content__box" }, [
              createElementVNode("u-image", {
                src: "/static/images/imface/emoji_11.png",
                class: "fui-chat__img",
                mode: "widthFix"
              })
            ])
          ]),
          createCommentVNode(" 语音 "),
          createElementVNode("view", { class: "fui-chat__item fui-item__right" }, [
            createVNode(_component_fui_avatar, { src: "/static/images/chat/avatar/img_avatar_1.png" }),
            createElementVNode("view", { class: "fui-content__box" }, [
              createElementVNode("view", { class: "fui-audio__box" }, [
                createCommentVNode(" play "),
                createElementVNode("u-image", {
                  class: "fui-icon__audio",
                  src: "/static/images/chat/suspend.png",
                  mode: "widthFix"
                }),
                createElementVNode("u-text", { class: "fui-time" }, "00:21")
              ]),
              createElementVNode("view", { class: "fui-op__icon" }, [
                createVNode(_component_fui_icon, {
                  name: "warning-fill",
                  size: 52,
                  color: "#F75555"
                })
              ])
            ])
          ]),
          createElementVNode("view", { class: "fui-chat__item" }, [
            createVNode(_component_fui_avatar, { src: "/static/images/chat/avatar/img_avatar_2.png" }),
            createElementVNode("view", { class: "fui-content__box" }, [
              createElementVNode("u-text", { class: "fui-content__text" }, "FirstUI GPT机器人聊天地址：chatgpt.firstui.cn，使用微信打开链接即可体验！")
            ])
          ])
        ],
        2
        /* CLASS */
      ),
      createElementVNode(
        "view",
        {
          class: "fui-chatbar__fixed",
          style: normalizeStyle({ transform: `translateY(${$data.faceShow ? 0 : $data.translateY})px` })
        },
        [
          createElementVNode("view", { class: "fui-chatbar__wrap" }, [
            createElementVNode("view", {
              class: "fui-chatbar__icon-box",
              onClick: _cache[0] || (_cache[0] = (...args) => $options.openPopup && $options.openPopup(...args))
            }, [
              createVNode(_component_fui_icon, { name: "plussign" })
            ]),
            createElementVNode("view", {
              class: "fui-chatbar__icon-box fui-chatbar__pdl",
              onClick: _cache[1] || (_cache[1] = withModifiers((...args) => $options.faceChange && $options.faceChange(...args), ["stop"]))
            }, [
              createVNode(_component_fui_icon, { name: "face" })
            ]),
            createElementVNode("view", { class: "fui-chatbar__input-box" }, [
              createElementVNode("u-textarea", {
                ref: "textareaRef",
                fixed: true,
                autoHeight: "",
                showCount: false,
                disableDefaultPadding: "",
                confirmType: "send",
                class: "fui-chatbar__input",
                cursorSpacing: $data.spacing,
                onFocus: _cache[2] || (_cache[2] = (...args) => $options.focusChange && $options.focusChange(...args)),
                onBlur: _cache[3] || (_cache[3] = (...args) => $options.blurChange && $options.blurChange(...args)),
                modelValue: $data.msg,
                onInput: _cache[4] || (_cache[4] = ($event) => $data.msg = $event.detail.value)
              }, null, 40, ["cursorSpacing", "modelValue"]),
              !$data.isVoice ? (openBlock(), createElementBlock("view", {
                key: 0,
                class: "fui-chatbar__voice"
              }, [
                createElementVNode("u-text", { class: "fui-chatbar__voice-text" }, "按住 说话")
              ])) : createCommentVNode("v-if", true)
            ]),
            createElementVNode("view", {
              class: "fui-chatbar__icon-box",
              onClick: _cache[5] || (_cache[5] = (...args) => $options.change && $options.change(...args))
            }, [
              !$data.isSend ? (openBlock(), createBlock(_component_fui_icon, {
                key: 0,
                name: $data.isVoice ? "voice" : "keyboard"
              }, null, 8, ["name"])) : (openBlock(), createElementBlock("u-image", {
                key: 1,
                class: "fui-btn__send",
                src: "/static/images/chat/send.png"
              }))
            ])
          ]),
          createElementVNode("view", { class: "fui-scroll__box" }, [
            createElementVNode(
              "scroll-view",
              {
                scrollY: "",
                class: "fui-face__box",
                style: normalizeStyle({ opacity: $data.faceShow ? 1 : 0 })
              },
              [
                createElementVNode("view", { class: "fui-face__inner" }, [
                  (openBlock(true), createElementBlock(
                    Fragment,
                    null,
                    renderList($data.faceList, (item, index) => {
                      return openBlock(), createElementBlock("u-text", {
                        class: "fui-face__text fui--active",
                        key: index,
                        onClick: ($event) => $options.faceClick(item)
                      }, toDisplayString(item), 9, ["onClick"]);
                    }),
                    128
                    /* KEYED_FRAGMENT */
                  ))
                ])
              ],
              4
              /* STYLE */
            ),
            createElementVNode("view", {
              class: "fui-del__box",
              onClick: _cache[6] || (_cache[6] = withModifiers((...args) => $options.delMsg && $options.delMsg(...args), ["stop"]))
            }, [
              createVNode(_component_fui_icon, {
                name: "backspace",
                size: 56
              })
            ])
          ]),
          createVNode(_component_fui_safe_area, { background: "#f8f8f8" })
        ],
        4
        /* STYLE */
      ),
      createVNode(_component_fui_bottom_popup, {
        show: $data.show,
        onClose: $options.closePopup
      }, {
        default: withCtx(() => [
          createElementVNode("view", { class: "fui-custom__wrap" }, [
            createElementVNode("view", { class: "fui-op__list" }, [
              (openBlock(true), createElementBlock(
                Fragment,
                null,
                renderList($data.opList, (item, index) => {
                  return openBlock(), createElementBlock("view", {
                    class: "fui-op__item fui--active",
                    key: index
                  }, [
                    createElementVNode("u-image", {
                      class: "fui-op--img",
                      src: `/static/images/chat/img_${item.src}.png`
                    }, null, 8, ["src"]),
                    createElementVNode(
                      "u-text",
                      { class: "fui-op--text" },
                      toDisplayString(item.text),
                      1
                      /* TEXT */
                    )
                  ]);
                }),
                128
                /* KEYED_FRAGMENT */
              ))
            ]),
            createElementVNode("view", {
              class: "fui-icon__close fui--active",
              onClick: _cache[7] || (_cache[7] = (...args) => $options.closePopup && $options.closePopup(...args))
            }, [
              createVNode(_component_fui_icon, {
                name: "close",
                size: 52
              })
            ])
          ])
        ]),
        _: 1
        /* STABLE */
      }, 8, ["show", "onClose"])
    ])
  ]);
}
const chat = /* @__PURE__ */ _export_sfc(_sfc_main, [["render", _sfc_render], ["styles", [_style_0]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/pages/chat/chat.nvue"]]);
export {
  chat as default
};
