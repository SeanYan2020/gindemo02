import { f as formatAppLog, r as resolveEasycom } from "../../uni-app.es.js";
import { _ as __easycom_0, a as __easycom_2, b as __easycom_4$1 } from "../../fui-index-list.js";
import { resolveDynamicComponent, openBlock, createElementBlock, createVNode, withCtx, createBlock, createElementVNode } from "vue";
import { _ as __easycom_3 } from "../../fui-list-cell.js";
import { _ as __easycom_4 } from "../../fui-list.js";
import { _ as _export_sfc } from "../../_plugin-vue_export-helper.js";
const lists = [{
  letter: "A",
  data: [{
    text: "阿尔巴尼亚",
    src: "/static/images/chat/avatar/img_avatar_3.png"
  }, {
    text: "阿尔及利亚",
    src: "/static/images/chat/avatar/img_avatar_1.png"
  }, {
    text: "阿富汗",
    src: "/static/images/chat/avatar/img_avatar_2.png"
  }, {
    text: "阿根廷",
    src: "/static/images/chat/avatar/img_avatar_3.png"
  }, {
    text: "爱尔兰",
    src: "/static/images/chat/avatar/img_def.png"
  }, {
    text: "埃及",
    src: "/static/images/chat/avatar/img_def.png"
  }, {
    text: "埃塞俄比亚",
    src: "/static/images/chat/avatar/img_def.png"
  }, {
    text: "爱沙尼亚",
    src: "/static/images/chat/avatar/img_def_1.png"
  }, {
    text: "阿拉伯联合酋长国",
    src: "/static/images/chat/avatar/img_def_1.png"
  }, {
    text: "阿鲁巴",
    src: "/static/images/chat/avatar/img_def_1.png"
  }, {
    text: "阿曼",
    src: "/static/images/chat/avatar/img_def_1.png"
  }, {
    text: "安道尔",
    src: "/static/images/chat/avatar/img_def_1.png"
  }, {
    text: "安哥拉",
    src: "/static/images/chat/avatar/img_avatar_1.png"
  }, {
    text: "安圭拉",
    src: "/static/images/chat/avatar/img_avatar_2.png"
  }, {
    text: "安提瓜岛和巴布达",
    src: "/static/images/chat/avatar/img_avatar_3.png"
  }, {
    text: "澳大利亚",
    src: "/static/images/chat/avatar/img_avatar_1.png"
  }, {
    text: "奥地利",
    src: "/static/images/chat/avatar/img_avatar_2.png"
  }, {
    text: "奥兰群岛",
    src: "/static/images/chat/avatar/img_avatar_3.png"
  }]
}, {
  letter: "B",
  data: [{
    text: "巴巴多斯岛",
    src: "/static/images/chat/avatar/img_avatar_1.png"
  }, {
    text: "巴布亚新几内亚",
    src: "/static/images/chat/avatar/img_avatar_2.png"
  }, {
    text: "巴哈马",
    src: "/static/images/chat/avatar/img_avatar_3.png"
  }, {
    text: "白俄罗斯",
    src: "/static/images/chat/avatar/img_avatar_1.png"
  }, {
    text: "百慕大",
    src: "/static/images/chat/avatar/img_avatar_2.png"
  }, {
    text: "巴基斯坦",
    src: "/static/images/chat/avatar/img_avatar_3.png"
  }]
}, {
  letter: "C",
  data: [{
    text: "朝鲜",
    src: "/static/images/chat/avatar/img_avatar_1.png"
  }, {
    text: "赤道几内亚",
    src: "/static/images/chat/avatar/img_avatar_2.png"
  }]
}, {
  letter: "D",
  data: [{
    text: "丹麦",
    src: "/static/images/chat/avatar/img_avatar_1.png"
  }, {
    text: "德国",
    src: "/static/images/chat/avatar/img_avatar_2.png"
  }, {
    text: "东帝汶",
    src: "/static/images/chat/avatar/img_avatar_3.png"
  }]
}, {
  letter: "E",
  data: [{
    text: "厄瓜多尔",
    src: "/static/images/chat/avatar/img_avatar_1.png"
  }, {
    text: "厄立特里亚",
    src: "/static/images/chat/avatar/img_avatar_2.png"
  }, {
    text: "俄罗斯",
    src: "/static/images/chat/avatar/img_avatar_3.png"
  }]
}, {
  letter: "F",
  data: [{
    text: "法国",
    src: "/static/images/chat/avatar/img_avatar_1.png"
  }, {
    text: "法罗群岛",
    src: "/static/images/chat/avatar/img_avatar_2.png"
  }, {
    text: "梵蒂冈",
    src: "/static/images/chat/avatar/img_avatar_3.png"
  }]
}, {
  letter: "G",
  data: [{
    text: "冈比亚",
    src: "/static/images/chat/avatar/img_avatar_1.png"
  }, {
    text: "刚果（布）",
    src: "/static/images/chat/avatar/img_avatar_3.png"
  }]
}, {
  letter: "H",
  data: [{
    text: "海地",
    src: "/static/images/chat/avatar/img_avatar_2.png"
  }, {
    text: "韩国",
    src: "/static/images/chat/avatar/img_avatar_1.png"
  }]
}, {
  letter: "J",
  data: [{
    text: "加勒比荷兰",
    src: "/static/images/chat/avatar/img_avatar_1.png"
  }, {
    text: "加纳",
    src: "/static/images/chat/avatar/img_avatar_2.png"
  }]
}, {
  letter: "K",
  data: [{
    text: "开曼群岛",
    src: "/static/images/chat/avatar/img_avatar_1.png"
  }, {
    text: "喀麦隆",
    src: "/static/images/chat/avatar/img_avatar_2.png"
  }]
}, {
  letter: "L",
  data: [{
    text: "莱索托",
    src: "/static/images/chat/avatar/img_avatar_1.png"
  }, {
    text: "老挝",
    src: "/static/images/chat/avatar/img_avatar_2.png"
  }, {
    text: "拉脱维亚",
    src: "/static/images/chat/avatar/img_avatar_3.png"
  }]
}, {
  letter: "M",
  data: [{
    text: "马达加斯加",
    src: "/static/images/chat/avatar/img_avatar_1.png"
  }, {
    text: "马尔代夫",
    src: "/static/images/chat/avatar/img_avatar_2.png"
  }, {
    text: "马耳他",
    src: "/static/images/chat/avatar/img_avatar_3.png"
  }]
}, {
  letter: "N",
  data: [{
    text: "纳米比亚",
    src: "/static/images/chat/avatar/img_avatar_1.png"
  }, {
    text: "南非",
    src: "/static/images/chat/avatar/img_avatar_2.png"
  }, {
    text: "南苏丹",
    src: "/static/images/chat/avatar/img_avatar_3.png"
  }]
}, {
  letter: "P",
  data: [{
    text: "帕劳群岛",
    src: "/static/images/chat/avatar/img_avatar_1.png"
  }, {
    text: "葡萄牙",
    src: "/static/images/chat/avatar/img_avatar_2.png"
  }]
}, {
  letter: "Q",
  data: [{
    text: "乔治亚",
    src: "/static/images/chat/avatar/img_avatar_1.png"
  }]
}, {
  letter: "R",
  data: [{
    text: "日本",
    src: "/static/images/chat/avatar/img_avatar_1.png"
  }, {
    text: "瑞典",
    src: "/static/images/chat/avatar/img_avatar_2.png"
  }, {
    text: "瑞士",
    src: "/static/images/chat/avatar/img_avatar_3.png"
  }]
}, {
  letter: "S",
  data: [, {
    text: "萨尔瓦多",
    src: "/static/images/chat/avatar/img_avatar_1.png"
  }, {
    text: "圣马丁",
    src: "/static/images/chat/avatar/img_avatar_2.png"
  }, {
    text: "斯洛伐克",
    src: "/static/images/chat/avatar/img_avatar_3.png"
  }]
}, {
  letter: "T",
  data: [{
    text: "泰国",
    src: "/static/images/chat/avatar/img_avatar_1.png"
  }, {
    text: "塔吉克斯坦",
    src: "/static/images/chat/avatar/img_avatar_2.png"
  }, {
    text: "汤加",
    src: "/static/images/chat/avatar/img_avatar_3.png"
  }, {
    text: "坦桑尼亚",
    src: "/static/images/chat/avatar/img_avatar_1.png"
  }]
}, {
  letter: "W",
  data: [{
    text: "瓦利斯群岛和富图纳群岛",
    src: "/static/images/chat/avatar/img_avatar_1.png"
  }, {
    text: "瓦努阿图",
    src: "/static/images/chat/avatar/img_avatar_2.png"
  }, {
    text: "危地马拉",
    src: "/static/images/chat/avatar/img_avatar_3.png"
  }]
}, {
  letter: "X",
  data: [{
    text: "西班牙",
    src: "/static/images/chat/avatar/img_avatar_1.png"
  }, {
    text: "希腊",
    src: "/static/images/chat/avatar/img_avatar_2.png"
  }, {
    text: "新加坡",
    src: "/static/images/chat/avatar/img_avatar_3.png"
  }]
}, {
  letter: "Y",
  data: [{
    text: "牙买加",
    src: "/static/images/chat/avatar/img_avatar_2.png"
  }, {
    text: "亚美尼亚",
    src: "/static/images/chat/avatar/img_avatar_1.png"
  }, {
    text: "也门",
    src: "/static/images/chat/avatar/img_avatar_3.png"
  }]
}, {
  letter: "Z",
  data: [{
    text: "赞比亚",
    src: "/static/images/chat/avatar/img_avatar_1.png"
  }, {
    text: "泽西岛",
    src: "/static/images/chat/avatar/img_avatar_2.png"
  }, {
    text: "乍得",
    src: "/static/images/chat/avatar/img_avatar_3.png"
  }, {
    text: "直布罗陀",
    src: "/static/images/chat/avatar/img_avatar_1.png"
  }]
}];
const _style_0 = { "fui-divider": { "": { "height": "120rpx", "flexDirection": "row", "alignItems": "center", "justifyContent": "center" } }, "fui-divider__text": { "": { "color": "#9E9E9E", "textAlign": "center" } }, "fui-flex__between": { "": { "flexDirection": "row", "alignItems": "center" } }, "fui-icon__add": { "": { "width": "88rpx", "height": "88rpx", "marginRight": "24rpx" } }, "fui-text": { "": { "fontSize": "32rpx" } } };
const _sfc_main = {
  data() {
    return {
      lists,
      show: false
    };
  },
  onLoad() {
  },
  methods: {
    handleInit() {
      this.show = true;
    },
    itemClick(e) {
      formatAppLog("log", "at pages/contacts/contacts.nvue:38", e);
      uni.fui.href("../userInfo/userInfo");
    },
    /**
     * 用于接在好友
     */
    handleLoadFriends() {
      uni.$cloud.get("/contact/list").then((data) => {
        formatAppLog("log", "at pages/contacts/contacts.nvue:46", "获取好友列表", data);
      }).catch((error) => {
      });
    },
    /**
     * 添加好友跳转
     */
    handleClick(e) {
      formatAppLog("log", "at pages/contacts/contacts.nvue:58", "index", e);
      formatAppLog("log", "at pages/contacts/contacts.nvue:59", "执行到我出");
      uni.$cloud.go("/pages/addFriends/addFriends");
    },
    /**
     * 用于加载好友数据
     */
    handleLoadFriedns() {
    }
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_fui_search_bar = resolveEasycom(resolveDynamicComponent("fui-search-bar"), __easycom_0);
  const _component_fui_list_cell = resolveEasycom(resolveDynamicComponent("fui-list-cell"), __easycom_3);
  const _component_fui_loadmore = resolveEasycom(resolveDynamicComponent("fui-loadmore"), __easycom_2);
  const _component_fui_list = resolveEasycom(resolveDynamicComponent("fui-list"), __easycom_4);
  const _component_fui_index_list = resolveEasycom(resolveDynamicComponent("fui-index-list"), __easycom_4$1);
  return openBlock(), createElementBlock("scroll-view", {
    scrollY: true,
    showScrollbar: true,
    enableBackToTop: true,
    bubble: "true",
    style: { flexDirection: "column" }
  }, [
    createVNode(_component_fui_index_list, {
      isSrc: "",
      background: "#FAFAFA",
      listData: $data.lists,
      keyColor: "#555",
      onInit: $options.handleInit,
      onClick: $options.itemClick
    }, {
      footer: withCtx(() => [
        !$data.show ? (openBlock(), createBlock(_component_fui_loadmore, { key: 0 })) : (openBlock(), createBlock(_component_fui_list, {
          key: 1,
          "top-left": "32"
        }, {
          default: withCtx(() => [
            createElementVNode("view", { class: "fui-divider" }, [
              createElementVNode("u-text", { class: "fui-divider__text" }, "375个朋友")
            ])
          ]),
          _: 1
          /* STABLE */
        }))
      ]),
      default: withCtx(() => [
        createVNode(_component_fui_search_bar, { background: "#FAFAFA" }),
        createVNode(_component_fui_list_cell, {
          "bottom-border": false,
          index: 1,
          onClick: $options.handleClick
        }, {
          default: withCtx(() => [
            createElementVNode("view", { class: "fui-flex__between" }, [
              createElementVNode("u-image", {
                src: "/static/images/chat/img_add.png",
                class: "fui-icon__add"
              }),
              createElementVNode("u-text", { class: "fui-text" }, "新的朋友")
            ])
          ]),
          _: 1
          /* STABLE */
        }, 8, ["onClick"])
      ]),
      _: 1
      /* STABLE */
    }, 8, ["listData", "onInit", "onClick"])
  ]);
}
const contacts = /* @__PURE__ */ _export_sfc(_sfc_main, [["render", _sfc_render], ["styles", [_style_0]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/pages/contacts/contacts.nvue"]]);
export {
  contacts as default
};
