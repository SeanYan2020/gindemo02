import { _ as __easycom_0 } from "../../fui-avatar.js";
import { openBlock, createElementBlock, Fragment, createCommentVNode, createElementVNode, normalizeStyle, renderSlot, normalizeClass, resolveDynamicComponent, createVNode, withCtx } from "vue";
import { r as resolveEasycom } from "../../uni-app.es.js";
import { _ as _export_sfc } from "../../_plugin-vue_export-helper.js";
import { _ as __easycom_3 } from "../../fui-list-cell.js";
import { _ as __easycom_4 } from "../../fui-list.js";
import { _ as __easycom_5 } from "../../fui-icon.js";
import { _ as __easycom_6 } from "../../fui-button.js";
const _style_0$2 = {};
const _sfc_main$2 = {
  name: "fui-col",
  props: {
    //栅格占据的列数
    span: {
      type: Number,
      default: 24
    },
    //栅格左侧的间隔格数
    offset: {
      type: Number,
      default: 0
    },
    //栅格向右移动格数
    pushLeft: {
      type: Number,
      default: -1
    },
    //栅格向左移动格数
    pullRight: {
      type: Number,
      default: -1
    },
    //max-width:767px 响应式栅格数或者栅格属性对象
    //Number时表示在此屏幕宽度下，栅格占据的列数。Object时可配置多个描述{span: 4, offset: 4}
    xs: {
      type: [Number, Object],
      default: -1
    },
    //max-width:768px 响应式栅格数或者栅格属性对象
    sm: {
      type: [Number, Object],
      default: -1
    },
    //max-width:992px 响应式栅格数或者栅格属性对象
    md: {
      type: [Number, Object],
      default: -1
    },
    //max-width:1200px 响应式栅格数或者栅格属性对象
    lg: {
      type: [Number, Object],
      default: -1
    },
    //max-width:1920px 响应式栅格数或者栅格属性对象
    xl: {
      type: [Number, Object],
      default: -1
    }
  },
  data() {
    return {
      classList: "fui-col",
      gutter: 0,
      sizeClass: "",
      parentWidth: 0,
      nvueWidth: 0,
      marginLeft: 0,
      right: 0,
      left: 0
    };
  },
  created() {
    let parent = this.$parent;
    while (parent && parent.$options.componentName !== "fuiRow") {
      parent = parent.$parent;
    }
    this.updateGutter(parent.gutter);
    parent.$watch("gutter", (gutter) => {
      this.updateGutter(gutter);
    });
    this.updateNvueWidth(parent.width);
    parent.$watch("width", (width) => {
      this.updateNvueWidth(width);
    });
  },
  computed: {
    sizeChange() {
      let {
        span,
        offset,
        pullRight,
        pushLeft
      } = this;
      return `${span}-${offset}-${pullRight}-${pushLeft}`;
    }
  },
  watch: {
    sizeChange: {
      immediate: true,
      handler(newVal) {
        this.updateNvueWidth(this.parentWidth);
      }
    }
  },
  methods: {
    updateGutter(parentGutter) {
      parentGutter = Number(parentGutter);
      if (!isNaN(parentGutter)) {
        this.gutter = parentGutter / 2;
      }
    },
    updateNvueWidth(width) {
      this.parentWidth = width;
      ["span", "offset", "pull", "push"].forEach((size) => {
        const curSize = this[size];
        if ((curSize || curSize === 0) && curSize !== -1) {
          let RPX = 1 / 24 * curSize * width;
          RPX = Number(RPX);
          switch (size) {
            case "span":
              this.nvueWidth = RPX;
              break;
            case "offset":
              this.marginLeft = RPX;
              break;
            case "pull":
              this.right = RPX;
              break;
            case "push":
              this.left = RPX;
              break;
          }
        }
      });
    },
    updateCol() {
      let classList = ["fui-col"];
      classList.push("fui-col-" + this.span);
      classList.push("fui-col-offset-" + this.offset);
      if (this.pushLeft !== -1) {
        this.pushLeft && classList.push("fui-col-push-" + this.pushLeft);
      }
      if (this.pullRight !== -1) {
        this.pullRight && classList.push("fui-col-pull-" + this.pullRight);
      }
      this.screenSizeSet("xs", classList);
      this.screenSizeSet("sm", classList);
      this.screenSizeSet("md", classList);
      this.screenSizeSet("lg", classList);
      this.screenSizeSet("xl", classList);
      this.classList = classList;
    },
    screenSizeSet(screen, classList) {
      if (typeof this[screen] === "number" && this[screen] !== -1) {
        classList.push("fui-col-" + screen + "-" + this[screen]);
      } else if (typeof this[screen] === "object") {
        typeof this[screen].offset === "number" && classList.push("fui-col-" + screen + "-offset-" + this[screen].offset);
        typeof this[screen].pushLeft === "number" && classList.push("fui-col-" + screen + "-push-" + this[screen].pushLeft);
        typeof this[screen].pullRight === "number" && classList.push("fui-col-" + screen + "-pull-" + this[screen].pullRight);
        typeof this[screen].span === "number" && classList.push("fui-col-" + screen + "-" + this[screen].span);
      }
    }
  }
};
function _sfc_render$2(_ctx, _cache, $props, $setup, $data, $options) {
  return openBlock(), createElementBlock(
    Fragment,
    null,
    [
      createCommentVNode("本文件由FirstUI授权予严天赠（会员ID：2  44 2，身份证尾号：0 43 01   8）专用，请尊重知识产权，勿私下传播，违者追究法律责任。"),
      createElementVNode(
        "view",
        {
          style: normalizeStyle({
            paddingLeft: `${Number($data.gutter)}rpx`,
            paddingRight: `${Number($data.gutter)}rpx`,
            width: `${$data.nvueWidth}rpx`,
            position: "relative",
            marginLeft: `${$data.marginLeft}rpx`,
            left: `${$data.right === 0 ? $data.left : -$data.right}rpx`
          })
        },
        [
          renderSlot(_ctx.$slots, "default")
        ],
        4
        /* STYLE */
      )
    ],
    2112
    /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
  );
}
const __easycom_1 = /* @__PURE__ */ _export_sfc(_sfc_main$2, [["render", _sfc_render$2], ["styles", [_style_0$2]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-col/fui-col.vue"]]);
const _style_0$1 = { "fui-row__box": { "": { "flex": 1, "position": "relative", "flexDirection": "row" } }, "fui-row__flex": { "": { "flexDirection": "row" } }, "fui-row__middle": { "": { "alignItems": "center" } }, "fui-row__bottom": { "": { "alignItems": "flex-end" } }, "fui-row__end": { "": { "justifyContent": "flex-end" } }, "fui-row__center": { "": { "justifyContent": "center" } }, "fui-row__space-around": { "": { "justifyContent": "space-around" } }, "fui-row__space-between": { "": { "justifyContent": "space-between" } } };
const _sfc_main$1 = {
  name: "fui-row",
  componentName: "fuiRow",
  props: {
    //是否为flex布局
    isFlex: {
      type: Boolean,
      default: false
    },
    //flex 布局下的水平排列方式 start/end/center/space-around/space-between
    justify: {
      type: String,
      default: "start"
    },
    //flex 布局下的垂直排列方式	top/middle/bottom
    align: {
      type: String,
      default: "top"
    },
    marginTop: {
      type: String,
      default: "0"
    },
    marginBottom: {
      type: String,
      default: "0"
    },
    //栅格间隔
    gutter: {
      type: Number,
      default: 0
    },
    // nvue如果使用span等属性，需要配置宽度
    width: {
      type: [String, Number],
      default: 750
    }
  },
  data() {
    return {
      flex: false
    };
  },
  watch: {
    isFlex(val) {
    }
  },
  created() {
    this.flex = true;
  },
  computed: {
    marginValue() {
      return 0;
    },
    justifyClass() {
      return this.justify !== "start" ? `fui-row__${this.justify}` : "";
    },
    alignClass() {
      return this.align !== "top" ? `fui-row__${this.align}` : "";
    }
  }
};
function _sfc_render$1(_ctx, _cache, $props, $setup, $data, $options) {
  return openBlock(), createElementBlock(
    Fragment,
    null,
    [
      createCommentVNode("本文件由FirstUI授权予严天赠（会员ID： 2 4 42，身份证尾号：0 4   30 18）专用，请尊重知识产权，勿私下传播，违者追究法律责任。"),
      createElementVNode(
        "view",
        {
          class: normalizeClass(["fui-row__box", [$data.flex ? "fui-row__flex" : "", $options.justifyClass, $options.alignClass]]),
          style: normalizeStyle({
            marginTop: $props.marginTop,
            marginBottom: $props.marginBottom,
            marginLeft: `-${$options.marginValue}rpx`,
            marginRight: `-${$options.marginValue}rpx`
          })
        },
        [
          renderSlot(_ctx.$slots, "default")
        ],
        6
        /* CLASS, STYLE */
      )
    ],
    2112
    /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
  );
}
const __easycom_2 = /* @__PURE__ */ _export_sfc(_sfc_main$1, [["render", _sfc_render$1], ["styles", [_style_0$1]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-row/fui-row.vue"]]);
const _style_0 = { "fui-list__item": { "": { "flex": 1, "width": 100, "display": "flex", "alignItems": "center", "justifyContent": "flex-start" } }, "fui-user__avatar": { "": { "display": "flex", "alignItems": "center", "justifyContent": "center" } }, "fui-text__title": { "": { "width": "160rpx" } }, "fui-user__title": { "": { "fontSize": "38rpx", "fontWeight": "800", "marginBottom": "6rpx" } }, "fui-user__explain": { "": { "marginBottom": "6rpx", "fontSize": "28rpx", "color": "#7F7F7F", "flex": 1 } }, "fui-text__explain": { "": { "fontSize": "28rpx", "color": "#7F7F7F", "flex": 1 } } };
const _sfc_main = {
  data() {
    return {};
  },
  methods: {}
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_fui_avatar = resolveEasycom(resolveDynamicComponent("fui-avatar"), __easycom_0);
  const _component_fui_col = resolveEasycom(resolveDynamicComponent("fui-col"), __easycom_1);
  const _component_fui_row = resolveEasycom(resolveDynamicComponent("fui-row"), __easycom_2);
  const _component_fui_list_cell = resolveEasycom(resolveDynamicComponent("fui-list-cell"), __easycom_3);
  const _component_fui_list = resolveEasycom(resolveDynamicComponent("fui-list"), __easycom_4);
  const _component_fui_icon = resolveEasycom(resolveDynamicComponent("fui-icon"), __easycom_5);
  const _component_fui_button = resolveEasycom(resolveDynamicComponent("fui-button"), __easycom_6);
  return openBlock(), createElementBlock("scroll-view", {
    scrollY: true,
    showScrollbar: true,
    enableBackToTop: true,
    bubble: "true",
    style: { flexDirection: "column" }
  }, [
    createElementVNode("view", null, [
      createVNode(_component_fui_row, { "margin-bottom": "24rpx" }, {
        default: withCtx(() => [
          createVNode(_component_fui_col, {
            span: 6,
            class: "fui-user__avatar"
          }, {
            default: withCtx(() => [
              createVNode(_component_fui_avatar, {
                text: "刘",
                radius: 12,
                background: "#FFB703"
              })
            ]),
            _: 1
            /* STABLE */
          }),
          createVNode(_component_fui_col, {
            span: 18,
            class: "fui-user__info"
          }, {
            default: withCtx(() => [
              createElementVNode("u-text", { class: "fui-user__title" }, " 某某某人 "),
              createElementVNode("u-text", { class: "fui-user__explain" }, " 昵称： "),
              createElementVNode("u-text", { class: "fui-user__explain" }, " 地点： ")
            ]),
            _: 1
            /* STABLE */
          })
        ]),
        _: 1
        /* STABLE */
      }),
      createVNode(_component_fui_list, null, {
        default: withCtx(() => [
          createVNode(_component_fui_list_cell, { arrow: "" }, {
            default: withCtx(() => [
              createElementVNode("u-text", { class: "fui-text__title" }, "备注和标签")
            ]),
            _: 1
            /* STABLE */
          }),
          createVNode(_component_fui_list_cell, null, {
            default: withCtx(() => [
              createElementVNode("u-text", { class: "fui-text__title" }, "手机号"),
              createElementVNode("u-text", { class: "fui-text__explain" }, "说明文字")
            ]),
            _: 1
            /* STABLE */
          }),
          createVNode(_component_fui_list_cell, { arrow: "" }, {
            default: withCtx(() => [
              createElementVNode("u-text", { class: "fui-text__title" }, "朋友权限"),
              createElementVNode("u-text", { class: "fui-text__explain" }, "聊天、宠物圈")
            ]),
            _: 1
            /* STABLE */
          })
        ]),
        _: 1
        /* STABLE */
      }),
      createElementVNode("view", { class: "" }, [
        createVNode(_component_fui_button, {
          radius: "0",
          type: "gray"
        }, {
          default: withCtx(() => [
            createVNode(_component_fui_icon, {
              name: "comment",
              size: 38
            }),
            createElementVNode("u-text", null, "发消息")
          ]),
          _: 1
          /* STABLE */
        }),
        createVNode(_component_fui_button, {
          radius: "0",
          type: "gray"
        }, {
          default: withCtx(() => [
            createElementVNode("u-text", null, "发送好友申请")
          ]),
          _: 1
          /* STABLE */
        })
      ])
    ])
  ]);
}
const userInfo = /* @__PURE__ */ _export_sfc(_sfc_main, [["render", _sfc_render], ["styles", [_style_0]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/pages/userInfo/userInfo.nvue"]]);
export {
  userInfo as default
};
