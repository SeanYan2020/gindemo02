import { a as requireNativePlugin, f as formatAppLog } from "../../uni-app.es.js";
import { resolveComponent, openBlock, createElementBlock, createElementVNode, toDisplayString, createVNode, withCtx, createTextVNode } from "vue";
import { _ as _export_sfc } from "../../_plugin-vue_export-helper.js";
const mp = requireNativePlugin("uniMP");
const _sfc_main = {
  data() {
    return {
      title: "uni小程序演示",
      appid: "__UNI__43CDF4D",
      wgt: "/static/__UNI__43CDF4D.wgt"
    };
  },
  onLoad() {
    if (!mp) {
      this.title = "当前环境不支持uni小程序";
    }
  },
  methods: {
    installMP() {
      mp.getUniMPVersion(this.appid, (ret) => {
        if (0 != ret.code) {
          this.doInstallMP();
        } else {
          uni.showModal({
            title: "提示",
            content: "uni小程序已安装，是否覆盖？",
            success: (res) => {
              res.confirm && this.doInstallMP();
            }
          });
        }
        formatAppLog("log", "at pages/demo/demo.nvue:47", "getUniMPVersion: " + JSON.stringify(ret));
      });
    },
    doInstallMP() {
      mp.installUniMP({
        appid: this.appid,
        wgtFile: this.wgt
      }, (r) => {
        if (0 == r.code) {
          uni.showToast({
            title: "安装成功"
          });
        } else {
          uni.showModal({
            title: "安装失败",
            content: JSON.stringify(r),
            showCancel: false
          });
        }
        formatAppLog("log", "at pages/demo/demo.nvue:66", "installUniMP: " + JSON.stringify(r));
      });
    },
    openMP() {
      mp.openUniMP({
        appid: this.appid
      }, (ret) => {
        if (0 != ret.code) {
          uni.showModal({
            title: "启动失败",
            content: JSON.stringify(ret),
            showCancel: false
          });
        }
        formatAppLog("log", "at pages/demo/demo.nvue:80", "openUniMP: " + JSON.stringify(ret));
      });
    },
    openMP2Path() {
      mp.openUniMP({
        appid: this.appid,
        path: "pages/component/view/view"
      }, (ret) => {
        if (0 != ret.code) {
          uni.showModal({
            title: "启动失败",
            content: JSON.stringify(ret),
            showCancel: false
          });
        }
        formatAppLog("log", "at pages/demo/demo.nvue:95", "openUniMP: " + JSON.stringify(ret));
      });
    },
    openMP2Extra() {
      mp.openUniMP({
        appid: this.appid,
        extraData: {
          name: "my name",
          id: "my id"
        }
      }, (ret) => {
        if (0 != ret.code) {
          uni.showModal({
            title: "启动失败",
            content: JSON.stringify(ret),
            showCancel: false
          });
        }
        formatAppLog("log", "at pages/demo/demo.nvue:113", "openUniMP: " + JSON.stringify(ret));
      });
    },
    sendEvent() {
      mp.sendUniMPEvent(
        this.appid,
        "event",
        {
          "key": "value",
          "name": "data"
        },
        (ret) => {
          formatAppLog("log", "at pages/demo/demo.nvue:124", "Host sendEvent: " + JSON.stringify(ret));
        }
      );
    },
    sendEventInterval() {
      setInterval(this.sendEvent, 5e3);
    }
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_button = resolveComponent("button");
  return openBlock(), createElementBlock("scroll-view", {
    scrollY: true,
    showScrollbar: true,
    enableBackToTop: true,
    bubble: "true",
    style: { flexDirection: "column" }
  }, [
    createElementVNode("view", { class: "content" }, [
      createElementVNode("u-image", {
        class: "logo",
        src: "/static/logo.png"
      }),
      createElementVNode("view", { class: "text-area" }, [
        createElementVNode(
          "u-text",
          { class: "title" },
          toDisplayString($data.title),
          1
          /* TEXT */
        )
      ]),
      createElementVNode("view", { class: "wrap" }, [
        createVNode(_component_button, {
          type: "primary",
          onClick: _cache[0] || (_cache[0] = ($event) => $options.installMP())
        }, {
          default: withCtx(() => [
            createTextVNode("安装Hello uni-app小程序")
          ]),
          _: 1
          /* STABLE */
        }),
        createVNode(_component_button, {
          type: "primary",
          onClick: _cache[1] || (_cache[1] = ($event) => $options.openMP())
        }, {
          default: withCtx(() => [
            createTextVNode("启动Hello uni-app小程序")
          ]),
          _: 1
          /* STABLE */
        }),
        createVNode(_component_button, {
          type: "primary",
          onClick: _cache[2] || (_cache[2] = ($event) => $options.openMP2Path())
        }, {
          default: withCtx(() => [
            createTextVNode("启动Hello uni-app小程序直达页面")
          ]),
          _: 1
          /* STABLE */
        }),
        createVNode(_component_button, {
          type: "primary",
          onClick: _cache[3] || (_cache[3] = ($event) => $options.openMP2Extra())
        }, {
          default: withCtx(() => [
            createTextVNode("启动Hello uni-app小程序传递参数")
          ]),
          _: 1
          /* STABLE */
        }),
        createVNode(_component_button, {
          type: "primary",
          onClick: _cache[4] || (_cache[4] = ($event) => $options.sendEvent())
        }, {
          default: withCtx(() => [
            createTextVNode("向Hello uni-app小程序发送事件")
          ]),
          _: 1
          /* STABLE */
        }),
        createVNode(_component_button, {
          type: "primary",
          onClick: _cache[5] || (_cache[5] = ($event) => $options.sendEventInterval())
        }, {
          default: withCtx(() => [
            createTextVNode("定时向Hello uni-app小程序发送事件")
          ]),
          _: 1
          /* STABLE */
        })
      ])
    ])
  ]);
}
const demo = /* @__PURE__ */ _export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/pages/demo/demo.nvue"]]);
export {
  demo as default
};
