import { resolveComponent, openBlock, createElementBlock, Fragment, createCommentVNode, createElementVNode, normalizeStyle, createBlock, normalizeClass, createVNode, resolveDynamicComponent, withCtx } from "vue";
import { _ as _export_sfc } from "../../_plugin-vue_export-helper.js";
import { r as resolveEasycom } from "../../uni-app.es.js";
import { _ as __easycom_3 } from "../../fui-list-cell.js";
const _style_0$1 = { "fui-switch__input": { "": { "borderWidth": 1, "borderStyle": "solid", "borderColor": "rgba(0,0,0,0)" } }, "fui-checkbox__self": { "": { "fontSize": 0, "width": "40rpx", "height": "40rpx", "borderRadius": "40rpx", "flexDirection": "row", "alignItems": "center", "justifyContent": "center", "overflow": "hidden", "position": "relative" } }, "uni-switch-input": { "": { "!marginRight": 0 } }, "fui-check__mark": { "": { "width": "20rpx", "height": "40rpx", "borderBottomStyle": "solid", "borderBottomWidth": 3, "borderBottomColor": "#FFFFFF", "borderRightStyle": "solid", "borderRightWidth": 3, "borderRightColor": "#FFFFFF", "transform": "rotate(45deg) scale(0.5)", "transformOrigin": "54% 48%" } }, "fui-switch__hidden": { "": { "position": "absolute", "top": -1, "left": -1, "opacity": 0, "width": "100wx", "height": "100wx", "right": 0, "bottom": 0, "borderWidth": 0 } }, "fui-checkbox__disabled": { "": { "opacity": 0.6 } } };
const _sfc_main$1 = {
  name: "fui-switch",
  emits: ["change"],
  props: {
    //开关选择器名称
    name: {
      type: String,
      default: ""
    },
    checked: {
      type: Boolean,
      default: false
    },
    disabled: {
      type: Boolean,
      default: false
    },
    //样式，有效值：switch, checkbox
    type: {
      type: String,
      default: "switch"
    },
    //switch选中颜色
    color: {
      type: String,
      default: ""
    },
    //边框颜色，type=checkbox时生效
    borderColor: {
      type: String,
      default: "#ccc"
    },
    //对号颜色，type=checkbox时生效
    checkMarkColor: {
      type: String,
      default: "#fff"
    },
    scaleRatio: {
      type: [Number, String],
      default: 1
    }
  },
  computed: {
    getColor() {
      let color = this.color;
      if (!color || color === true) {
        const app = uni && uni.$fui && uni.$fui.color;
        color = app && app.primary || "#465CFF";
      }
      return color;
    }
  },
  data() {
    let isNvue = false;
    isNvue = true;
    return {
      val: false,
      isNvue,
      isLabel: false
    };
  },
  watch: {
    checked(val) {
      this.val = val;
    }
  },
  created() {
    this.val = this.checked;
    this.label = this.getParent();
    if (this.label) {
      this.isLabel = true;
      this.label.childrens.push(this);
    }
  },
  methods: {
    change(e, label) {
      if (this.label && !label)
        return;
      this.val = e.detail.value;
      this.$emit("change", e);
    },
    labelClick() {
      if (this.disabled)
        return;
      let e = {
        detail: {
          value: !this.val
        }
      };
      this.change(e, true);
    },
    getParent(name = "fui-label") {
      let parent = this.$parent;
      let parentName = parent.$options.name;
      while (parentName !== name) {
        parent = parent.$parent;
        if (!parent)
          return false;
        parentName = parent.$options.name;
      }
      return parent;
    }
  }
};
function _sfc_render$1(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_switch = resolveComponent("switch");
  return openBlock(), createElementBlock(
    Fragment,
    null,
    [
      createCommentVNode("本文件由FirstUI授权予严天赠（会员ID：2   442，身份证尾号： 0 43   018）专用，请尊重知识产权，勿私下传播，违者追究法律责任。"),
      createElementVNode(
        "view",
        {
          class: "fui-switch__input",
          style: normalizeStyle({ zoom: $data.isNvue ? 1 : $props.scaleRatio, transform: `scale(${$data.isNvue ? $props.scaleRatio : 1})` })
        },
        [
          $props.type === "switch" ? (openBlock(), createBlock(_component_switch, {
            key: 0,
            onChange: $options.change,
            name: $props.name,
            checked: $data.val,
            disabled: $props.disabled,
            color: $options.getColor
          }, null, 8, ["onChange", "name", "checked", "disabled", "color"])) : (openBlock(), createElementBlock(
            "view",
            {
              key: 1,
              class: normalizeClass(["fui-checkbox__self", { "fui-checkbox__disabled": $props.disabled, "fui-switch__color": !$options.getColor && $data.val }]),
              style: normalizeStyle({ background: $data.val ? $options.getColor : "#fff", border: $data.val ? `1px solid ${$options.getColor}` : `1px solid ${$props.borderColor}` })
            },
            [
              $data.val ? (openBlock(), createElementBlock(
                "view",
                {
                  key: 0,
                  class: "fui-check__mark",
                  style: normalizeStyle({ borderBottomColor: $props.checkMarkColor, borderRightColor: $props.checkMarkColor })
                },
                null,
                4
                /* STYLE */
              )) : createCommentVNode("v-if", true),
              createVNode(_component_switch, {
                class: normalizeClass(["fui-switch__hidden", { "fui-pointer__events": $data.isLabel }]),
                style: { "opacity": "0", "position": "absolute" },
                onChange: $options.change,
                name: $props.name,
                type: "checkbox",
                checked: $data.val,
                disabled: $props.disabled
              }, null, 8, ["class", "onChange", "name", "checked", "disabled"])
            ],
            6
            /* CLASS, STYLE */
          ))
        ],
        4
        /* STYLE */
      )
    ],
    2112
    /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
  );
}
const __easycom_0 = /* @__PURE__ */ _export_sfc(_sfc_main$1, [["render", _sfc_render$1], ["styles", [_style_0$1]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-switch/fui-switch.vue"]]);
const _style_0 = { "fui-cell__box": { "": { "flex": 1, "flexDirection": "row", "alignItems": "center", "justifyContent": "space-between" } }, "fui-text": { "": { "fontSize": "28rpx", "color": "#9E9E9E", "paddingTop": "32rpx", "paddingRight": "32rpx", "paddingBottom": 0, "paddingLeft": "32rpx" } }, "fui-title": { "": { "fontSize": "32rpx" } } };
const _sfc_main = {
  data() {
    return {};
  },
  methods: {}
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_fui_switch = resolveEasycom(resolveDynamicComponent("fui-switch"), __easycom_0);
  const _component_fui_list_cell = resolveEasycom(resolveDynamicComponent("fui-list-cell"), __easycom_3);
  return openBlock(), createElementBlock("scroll-view", {
    scrollY: true,
    showScrollbar: true,
    enableBackToTop: true,
    bubble: "true",
    style: { flexDirection: "column" }
  }, [
    createElementVNode("view", { case: "fui-wrap" }, [
      createVNode(_component_fui_list_cell, {
        highlight: false,
        "bottom-right": "32"
      }, {
        default: withCtx(() => [
          createElementVNode("view", { class: "fui-cell__box" }, [
            createElementVNode("u-text", { class: "fui-title" }, "通知显示消息详情"),
            createVNode(_component_fui_switch, { color: "#246BFD" })
          ])
        ]),
        _: 1
        /* STABLE */
      }),
      createElementVNode("u-text", { class: "fui-text" }, "应用未打开时"),
      createVNode(_component_fui_list_cell, {
        highlight: false,
        "bottom-right": "32"
      }, {
        default: withCtx(() => [
          createElementVNode("view", { class: "fui-cell__box" }, [
            createElementVNode("u-text", { class: "fui-title" }, "消息通知"),
            createVNode(_component_fui_switch, { color: "#246BFD" })
          ])
        ]),
        _: 1
        /* STABLE */
      }),
      createVNode(_component_fui_list_cell, {
        highlight: false,
        "bottom-right": "32"
      }, {
        default: withCtx(() => [
          createElementVNode("view", { class: "fui-cell__box" }, [
            createElementVNode("u-text", { class: "fui-title" }, "语音和视频通话提醒"),
            createVNode(_component_fui_switch, { color: "#246BFD" })
          ])
        ]),
        _: 1
        /* STABLE */
      }),
      createElementVNode("u-text", { class: "fui-text" }, "应用打开时"),
      createVNode(_component_fui_list_cell, {
        highlight: false,
        "bottom-right": "32"
      }, {
        default: withCtx(() => [
          createElementVNode("view", { class: "fui-cell__box" }, [
            createElementVNode("u-text", { class: "fui-title" }, "消息通知横幅"),
            createVNode(_component_fui_switch, { color: "#246BFD" })
          ])
        ]),
        _: 1
        /* STABLE */
      }),
      createVNode(_component_fui_list_cell, {
        highlight: false,
        "bottom-right": "32"
      }, {
        default: withCtx(() => [
          createElementVNode("view", { class: "fui-cell__box" }, [
            createElementVNode("u-text", { class: "fui-title" }, "消息通知提示音"),
            createVNode(_component_fui_switch, { color: "#246BFD" })
          ])
        ]),
        _: 1
        /* STABLE */
      }),
      createVNode(_component_fui_list_cell, {
        highlight: false,
        "bottom-right": "32"
      }, {
        default: withCtx(() => [
          createElementVNode("view", { class: "fui-cell__box" }, [
            createElementVNode("u-text", { class: "fui-title" }, "震动"),
            createVNode(_component_fui_switch, { color: "#246BFD" })
          ])
        ]),
        _: 1
        /* STABLE */
      })
    ])
  ]);
}
const notice = /* @__PURE__ */ _export_sfc(_sfc_main, [["render", _sfc_render], ["styles", [_style_0]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/pages/notice/notice.nvue"]]);
export {
  notice as default
};
