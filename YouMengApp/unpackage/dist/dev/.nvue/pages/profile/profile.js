import { _ as __easycom_0 } from "../../fui-avatar.js";
import { resolveDynamicComponent, openBlock, createElementBlock, createElementVNode, createVNode, withCtx } from "vue";
import { r as resolveEasycom } from "../../uni-app.es.js";
import { _ as __easycom_1 } from "../../fui-input.js";
import { _ as __easycom_5 } from "../../fui-icon.js";
import { _ as __easycom_6 } from "../../fui-button.js";
import { _ as _export_sfc } from "../../_plugin-vue_export-helper.js";
const _style_0 = { "fui-wrap": { "": { "paddingTop": 0, "paddingRight": "48rpx", "paddingBottom": 0, "paddingLeft": "48rpx" } }, "fui-avatar__box": { "": { "flexDirection": "row", "justifyContent": "center", "paddingTop": "48rpx" } }, "fui-form__box": { "": { "paddingTop": "32rpx" } }, "fui-btn__box": { "": { "paddingTop": "96rpx" } } };
const _sfc_main = {
  data() {
    return {};
  },
  methods: {
    save() {
      uni.navigateBack({
        delta: 3
      });
    }
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_fui_avatar = resolveEasycom(resolveDynamicComponent("fui-avatar"), __easycom_0);
  const _component_fui_input = resolveEasycom(resolveDynamicComponent("fui-input"), __easycom_1);
  const _component_fui_icon = resolveEasycom(resolveDynamicComponent("fui-icon"), __easycom_5);
  const _component_fui_button = resolveEasycom(resolveDynamicComponent("fui-button"), __easycom_6);
  return openBlock(), createElementBlock("scroll-view", {
    scrollY: true,
    showScrollbar: true,
    enableBackToTop: true,
    bubble: "true",
    style: { flexDirection: "column" }
  }, [
    createElementVNode("view", { class: "fui-wrap" }, [
      createElementVNode("view", { class: "fui-avatar__box" }, [
        createVNode(_component_fui_avatar, {
          background: "transparent",
          width: "200",
          height: "200",
          src: "/static/images/chat/avatar/img_def.png"
        })
      ]),
      createElementVNode("view", { class: "fui-form__box" }, [
        createVNode(_component_fui_input, {
          "border-bottom": false,
          radius: 24,
          backgroundColor: "#FAFAFA",
          placeholder: "姓名",
          bottomLeft: "0"
        }),
        createVNode(_component_fui_input, {
          marginTop: "24",
          "border-bottom": false,
          radius: 24,
          backgroundColor: "#FAFAFA",
          placeholder: "昵称",
          bottomLeft: "0"
        }),
        createVNode(_component_fui_input, {
          marginTop: "24",
          "border-bottom": false,
          radius: 24,
          backgroundColor: "#FAFAFA",
          placeholder: "邮箱",
          bottomLeft: "0"
        }, {
          default: withCtx(() => [
            createVNode(_component_fui_icon, {
              name: "mail",
              color: "#9E9E9E",
              size: 36
            })
          ]),
          _: 1
          /* STABLE */
        }),
        createVNode(_component_fui_input, {
          marginTop: "24",
          "border-bottom": false,
          radius: 24,
          backgroundColor: "#FAFAFA",
          placeholder: "一句话简介",
          bottomLeft: "0"
        }, {
          default: withCtx(() => [
            createVNode(_component_fui_icon, {
              name: "edit",
              color: "#9E9E9E",
              size: 36
            })
          ]),
          _: 1
          /* STABLE */
        }),
        createElementVNode("view", { class: "fui-btn__box" }, [
          createVNode(_component_fui_button, {
            text: "保存",
            radius: "96rpx",
            bold: "",
            onClick: $options.save
          }, null, 8, ["onClick"])
        ])
      ])
    ])
  ]);
}
const profile = /* @__PURE__ */ _export_sfc(_sfc_main, [["render", _sfc_render], ["styles", [_style_0]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/pages/profile/profile.nvue"]]);
export {
  profile as default
};
