import { openBlock, createElementBlock, Fragment, createCommentVNode, createElementVNode, normalizeClass, normalizeStyle, renderSlot, withModifiers, toDisplayString, resolveComponent, renderList, createBlock } from "vue";
import { _ as _export_sfc } from "./_plugin-vue_export-helper.js";
import { a as requireNativePlugin } from "./uni-app.es.js";
const _style_0$3 = { "fui-search__bar-wrap": { "": { "flex": 1, "flexDirection": "row", "position": "relative", "alignItems": "center" } }, "fui-search__bar-form": { "": { "position": "relative", "flex": 1 } }, "fui-search__bar-box": { "": { "flex": 1, "paddingLeft": "24rpx", "paddingRight": "24rpx", "flexDirection": "row", "zIndex": 1, "alignItems": "center" } }, "fui-search__bar-input": { "": { "paddingTop": 0, "paddingRight": "16rpx", "paddingBottom": 0, "paddingLeft": "16rpx", "borderWidth": 0, "borderStyle": "solid", "borderColor": "#000000", "fontSize": "28rpx", "flex": 1, "backgroundColor": "rgba(0,0,0,0)" } }, "fui-search__bar-pl": { "": { "color": "#B2B2B2" } }, "fui-search__bar-label": { "": { "position": "absolute", "top": 0, "right": 0, "bottom": 0, "left": 0, "zIndex": 2, "flexDirection": "row", "alignItems": "center" } }, "fui-sb__label-center": { "": { "justifyContent": "center" } }, "fui-sb__label-left": { "": { "paddingLeft": "24rpx" } }, "fui-search__bar-btn": { "": { "fontSize": "30rpx", "marginLeft": "24rpx", "opacity:active": 0.5 } }, "fui-search__bar-text": { "": { "fontSize": "28rpx", "paddingLeft": "16rpx", "color": "#B2B2B2" } }, "fui-search__bar-icon": { "": { "alignItems": "center", "justifyContent": "center", "flexDirection": "column", "transform": "rotate(-45deg)", "transformOrigin": "56% center" } }, "fui-sbi__circle": { "": { "width": "24rpx", "height": "24rpx", "borderWidth": 1, "borderStyle": "solid", "borderColor": "#B2B2B2", "borderRadius": 50 } }, "fui-sbi__line": { "": { "width": 1, "height": "12rpx", "backgroundColor": "#B2B2B2", "borderBottomLeftRadius": "6rpx", "borderBottomRightRadius": "6rpx" } }, "fui-sbi__clear-wrap": { "": { "width": "32rpx", "height": "32rpx", "backgroundColor": "#B2B2B2", "transform": "rotate(45deg)", "position": "relative", "borderRadius": "32rpx" } }, "fui-sbi__clear": { "": { "width": "32rpx", "height": "32rpx", "flexDirection": "row", "alignItems": "center", "justifyContent": "center", "position": "absolute", "left": 0, "top": 0, "transform": "scale(0.5) translateZ(0)" } }, "fui-sbi__clear-a": { "": { "width": "32rpx", "borderWidth": "2rpx", "borderStyle": "solid", "borderColor": "#ffffff", "backgroundColor": "#ffffff" } }, "fui-sbi__clear-b": { "": { "height": "32rpx", "borderWidth": "2rpx", "borderStyle": "solid", "borderColor": "#ffffff", "backgroundColor": "#ffffff" } } };
const _sfc_main$3 = {
  name: "fui-search-bar",
  emits: ["clear", "focus", "blur", "click", "cancel", "input", "search"],
  props: {
    //搜索栏背景色
    background: {
      type: String,
      default: "#F1F4FA"
    },
    //搜索栏上下padding（padding-top，padding-bottom）
    paddingTb: {
      type: [Number, String],
      default: 16
    },
    paddingLr: {
      type: [Number, String],
      default: 24
    },
    height: {
      type: [Number, String],
      default: 72
    },
    radius: {
      type: [Number, String],
      default: 8
    },
    color: {
      type: String,
      default: "#181818"
    },
    //input框背景色
    inputBackground: {
      type: String,
      default: "#fff"
    },
    focus: {
      type: Boolean,
      default: false
    },
    placeholder: {
      type: String,
      default: "请输入搜索关键词"
    },
    isLeft: {
      type: Boolean,
      default: false
    },
    value: {
      type: String,
      default: ""
    },
    disabled: {
      type: Boolean,
      default: false
    },
    cancel: {
      type: Boolean,
      default: true
    },
    cancelText: {
      type: String,
      default: "取消"
    },
    cancelColor: {
      type: String,
      default: "#7F7F7F"
    },
    searchText: {
      type: String,
      default: "搜索"
    },
    searchColor: {
      type: String,
      default: ""
    },
    //是否显示搜索输入框
    showInput: {
      type: Boolean,
      default: true
    },
    //是否显示输入框占位标签，当平台不支持focus属性时可隐藏
    showLabel: {
      type: Boolean,
      default: true
    },
    //v2.1.0
    fixed: {
      type: Boolean,
      default: false
    }
  },
  created() {
    this.val = this.value;
    this.plholder = this.placeholder;
    if (this.focus || this.val.length > 0) {
      this.isSearch = true;
    }
  },
  mounted() {
    this.$nextTick(() => {
      setTimeout(() => {
        this.isFocus = this.focus;
      }, 300);
    });
  },
  watch: {
    focus(val) {
      this.$nextTick(() => {
        setTimeout(() => {
          this.isFocus = val;
        }, 20);
      });
    },
    isFocus(val) {
      if (!this.$refs.searchBarRef)
        return;
      this.$nextTick(() => {
        setTimeout(() => {
          if (val) {
            this.$refs.searchBarRef.focus();
          }
        }, 50);
      });
    },
    value(val) {
      this.val = val;
      if (this.focus || this.val.length > 0) {
        this.isSearch = true;
      }
    },
    placeholder(val) {
      this.plholder = this.placeholder;
    }
  },
  computed: {
    getSearchColor() {
      let color = this.searchColor;
      if (!color || color === true) {
        const app = uni && uni.$fui && uni.$fui.color;
        color = app && app.primary || "#465CFF";
      }
      return color;
    }
  },
  data() {
    return {
      isSearch: false,
      isFocus: false,
      val: "",
      plholder: ""
    };
  },
  methods: {
    clearInput() {
      this.val = "";
      this.isFocus = true;
      this.$emit("clear");
    },
    inputFocus(e) {
      if (!this.showLabel) {
        this.isSearch = true;
      }
      this.$emit("focus", e);
    },
    inputBlur(e) {
      this.isFocus = false;
      if (!this.cancel && !this.val) {
        this.isSearch = false;
      }
      this.$emit("blur", e);
    },
    onShowInput() {
      if (!this.disabled && this.showInput) {
        this.isSearch = true;
        this.$nextTick(() => {
          setTimeout(() => {
            this.isFocus = true;
          }, 50);
        });
      }
      this.$emit("click", {});
    },
    hideInput() {
      this.isSearch = false;
      this.isFocus = false;
      uni.hideKeyboard();
      this.$emit("cancel", {});
    },
    inputChange(e) {
      this.val = e.detail.value;
      this.$emit("input", e);
    },
    search() {
      this.$emit("search", {
        detail: {
          value: this.val
        }
      });
    },
    reset() {
      this.isSearch = false;
      this.isFocus = false;
      this.val = "";
      uni.hideKeyboard();
    }
  }
};
function _sfc_render$3(_ctx, _cache, $props, $setup, $data, $options) {
  return openBlock(), createElementBlock(
    Fragment,
    null,
    [
      createCommentVNode("本文件由FirstUI授权予严天赠（会员ID：24   42，身份证尾号：0 4  30  18）专用，请尊重知识产权，勿私下传播，违者追究法律责任。"),
      createElementVNode(
        "view",
        {
          class: normalizeClass(["fui-search__bar-wrap", { "fui-searchbar__wrap-bg": !$props.background }]),
          style: normalizeStyle({ background: $props.background, paddingTop: $props.paddingTb + "rpx", paddingBottom: $props.paddingTb + "rpx", paddingLeft: $props.paddingLr + "rpx", paddingRight: $props.paddingLr + "rpx" })
        },
        [
          renderSlot(_ctx.$slots, "default"),
          createElementVNode(
            "view",
            {
              class: "fui-search__bar-form",
              style: normalizeStyle({ height: $props.height + "rpx" })
            },
            [
              $props.showInput ? (openBlock(), createElementBlock(
                "view",
                {
                  key: 0,
                  class: normalizeClass(["fui-search__bar-box", { "fui-searchbar__focus-invalid": !$data.isFocus && !$data.isSearch && $props.showLabel && !$props.disabled }]),
                  style: normalizeStyle({ height: $props.height + "rpx", borderRadius: $props.radius + "rpx", background: $props.inputBackground })
                },
                [
                  createElementVNode("view", { class: "fui-search__bar-icon" }, [
                    createElementVNode("view", { class: "fui-sbi__circle" }),
                    createElementVNode("view", { class: "fui-sbi__line" })
                  ]),
                  createElementVNode("u-input", {
                    ref: "searchBarRef",
                    class: normalizeClass(["fui-search__bar-input", { "fui-sb__input-color": !$props.color }]),
                    style: normalizeStyle({ color: $props.color, height: $props.height + "rpx" }),
                    placeholderClass: "fui-search__bar-pl",
                    placeholder: $data.plholder,
                    value: $data.val,
                    focus: $data.isFocus,
                    disabled: $props.disabled,
                    confirmType: "search",
                    onBlur: _cache[0] || (_cache[0] = (...args) => $options.inputBlur && $options.inputBlur(...args)),
                    onFocus: _cache[1] || (_cache[1] = (...args) => $options.inputFocus && $options.inputFocus(...args)),
                    onInput: _cache[2] || (_cache[2] = (...args) => $options.inputChange && $options.inputChange(...args)),
                    onConfirm: _cache[3] || (_cache[3] = (...args) => $options.search && $options.search(...args))
                  }, null, 46, ["placeholder", "value", "focus", "disabled"]),
                  $data.val.length > 0 && !$props.disabled ? (openBlock(), createElementBlock("view", {
                    key: 0,
                    class: "fui-sbi__clear-wrap",
                    onClick: _cache[4] || (_cache[4] = withModifiers((...args) => $options.clearInput && $options.clearInput(...args), ["stop"]))
                  }, [
                    createElementVNode("view", { class: "fui-sbi__clear" }, [
                      createElementVNode("view", { class: "fui-sbi__clear-a" })
                    ]),
                    createElementVNode("view", { class: "fui-sbi__clear" }, [
                      createElementVNode("view", { class: "fui-sbi__clear-b" })
                    ])
                  ])) : createCommentVNode("v-if", true)
                ],
                6
                /* CLASS, STYLE */
              )) : createCommentVNode("v-if", true),
              !$data.isFocus && !$data.isSearch && $props.showLabel ? (openBlock(), createElementBlock(
                "view",
                {
                  key: 1,
                  class: normalizeClass(["fui-search__bar-label", [$props.isLeft ? "fui-sb__label-left" : "fui-sb__label-center"]]),
                  style: normalizeStyle({ borderRadius: $props.radius + "rpx", background: $props.inputBackground }),
                  onClick: _cache[5] || (_cache[5] = (...args) => $options.onShowInput && $options.onShowInput(...args))
                },
                [
                  createElementVNode("view", { class: "fui-search__bar-icon" }, [
                    createElementVNode("view", { class: "fui-sbi__circle" }),
                    createElementVNode("view", { class: "fui-sbi__line" })
                  ]),
                  createElementVNode(
                    "u-text",
                    { class: "fui-search__bar-text" },
                    toDisplayString($props.placeholder),
                    1
                    /* TEXT */
                  )
                ],
                6
                /* CLASS, STYLE */
              )) : createCommentVNode("v-if", true)
            ],
            4
            /* STYLE */
          ),
          $props.cancel && $data.isSearch && !$data.val && $props.cancelText && $props.cancelText !== true && $props.cancelText !== "true" ? (openBlock(), createElementBlock(
            "u-text",
            {
              key: 0,
              class: "fui-search__bar-btn",
              style: normalizeStyle({ color: $props.cancelColor }),
              onClick: _cache[6] || (_cache[6] = (...args) => $options.hideInput && $options.hideInput(...args))
            },
            toDisplayString($props.cancelText),
            5
            /* TEXT, STYLE */
          )) : createCommentVNode("v-if", true),
          $data.val && !$props.disabled && $data.isSearch && $props.searchText && $props.searchText !== true && $props.searchText !== "true" ? (openBlock(), createElementBlock(
            "u-text",
            {
              key: 1,
              class: normalizeClass(["fui-search__bar-btn", { "fui-sb__btn-color": !$props.searchColor }]),
              style: normalizeStyle({ color: $options.getSearchColor }),
              onClick: _cache[7] || (_cache[7] = (...args) => $options.search && $options.search(...args))
            },
            toDisplayString($props.searchText),
            7
            /* TEXT, CLASS, STYLE */
          )) : createCommentVNode("v-if", true)
        ],
        6
        /* CLASS, STYLE */
      )
    ],
    2112
    /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
  );
}
const __easycom_0 = /* @__PURE__ */ _export_sfc(_sfc_main$3, [["render", _sfc_render$3], ["styles", [_style_0$3]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-search-bar/fui-search-bar.vue"]]);
const _style_0$2 = { "fui-loadmore__wrap": { "": { "alignItems": "center", "justifyContent": "center" } }, "fui-loadmore__col": { "": { "flexDirection": "column" } }, "fui-loadmore__row": { "": { "flexDirection": "row" } }, "fui-loadmore__icon": { "": { "marginTop": 0, "marginRight": 6, "marginBottom": 0, "marginLeft": 6, "borderWidth": 2, "borderStyle": "solid", "borderRadius": 100 } }, "fui-loadmore__icon-ani": { "": { "marginTop": 0, "marginRight": 6, "marginBottom": 0, "marginLeft": 6 } }, "fui-loadmore__text": { "": { "paddingTop": "16rpx" } } };
const animation = requireNativePlugin("animation");
const _sfc_main$2 = {
  name: "fui-loadmore",
  props: {
    //占据高度，单位rx
    height: {
      type: [Number, String],
      default: 100
    },
    //1-上拉加载 2-正在加载... 3-没有更多了
    state: {
      type: [Number, String],
      default: 2
    },
    initText: {
      type: String,
      default: "上拉加载"
    },
    //提示文字
    text: {
      type: String,
      default: "正在加载..."
    },
    noneText: {
      type: String,
      default: "没有更多了"
    },
    //文字颜色
    color: {
      type: String,
      default: "#7F7F7F"
    },
    //文字大小，单位rpx
    size: {
      type: [Number, String],
      default: 24
    },
    //loading图标背景色
    iconColor: {
      type: String,
      default: "#B2B2B2"
    },
    //loading图标高亮部分颜色
    activeColor: {
      type: String,
      default: ""
    },
    //loading 图标的宽度，单位rpx
    iconWidth: {
      type: [Number, String],
      default: 32
    },
    //自定义loading图标image路径，若自定义图标则iconColor、activeColor属性失效
    src: {
      type: String,
      default: ""
    },
    //loading图标和文字排列方向，可取值：col，row
    direction: {
      type: String,
      default: "row"
    }
  },
  watch: {
    state(newValue, oldValue) {
      this.$nextTick(() => {
        if (newValue == 2) {
          this.stop = false;
          setTimeout(() => {
            this._animation();
          }, 50);
        } else {
          this.stop = true;
        }
      });
    }
  },
  computed: {
    getActiveColor() {
      let color = this.activeColor;
      if (!color || color === true) {
        const app = uni && uni.$fui && uni.$fui.color;
        color = app && app.primary || "#465CFF";
      }
      return color;
    }
  },
  data() {
    let isNvue = false;
    isNvue = true;
    return {
      isNvue,
      deg: 0,
      stop: false
    };
  },
  mounted() {
    this.$nextTick(() => {
      setTimeout(() => {
        this.deg += 360;
        this._animation();
      }, 50);
    });
  },
  beforeUnmount() {
    this.deg = 0;
    this.stop = true;
  },
  methods: {
    getStateText(state) {
      state = Number(state);
      return [this.initText, this.text, this.noneText][state - 1];
    },
    _animation() {
      if (!this.$refs["fui_loadmore"] || this.stop)
        return;
      animation.transition(
        this.$refs["fui_loadmore"].ref,
        {
          styles: {
            transform: `rotate(${this.deg}deg)`
          },
          duration: 700,
          //ms
          timingFunction: "linear",
          iterationCount: "infinite",
          needLayout: false,
          delay: 0
          //ms
        },
        () => {
          this.deg += 360;
          this._animation();
        }
      );
    }
  }
};
function _sfc_render$2(_ctx, _cache, $props, $setup, $data, $options) {
  return openBlock(), createElementBlock(
    Fragment,
    null,
    [
      createCommentVNode("本文件由FirstUI授权予严天赠（会员ID：  2 442，身份证尾号：  0  4 3018）专用，请尊重知识产权，勿私下传播，违者追究法律责任。"),
      createElementVNode(
        "view",
        {
          class: normalizeClass(["fui-loadmore__wrap", ["fui-loadmore__" + $props.direction]]),
          style: normalizeStyle({ height: $props.height + "rpx" })
        },
        [
          !$props.src && $props.state == 2 ? (openBlock(), createElementBlock(
            "view",
            {
              key: 0,
              class: normalizeClass(["fui-loadmore__icon", { "fui-loadmore__border-left": !$data.isNvue && !$props.activeColor }]),
              ref: "fui_loadmore",
              style: normalizeStyle({ width: $props.iconWidth + "rpx", height: $props.iconWidth + "rpx", "border-left-color": $options.getActiveColor, "border-right-color": $props.iconColor, "border-top-color": $props.iconColor, "border-bottom-color": $props.iconColor })
            },
            null,
            6
            /* CLASS, STYLE */
          )) : createCommentVNode("v-if", true),
          $props.src && $props.state == 2 ? (openBlock(), createElementBlock("u-image", {
            key: 1,
            class: "fui-loadmore__icon-ani",
            ref: "fui_loadmore",
            src: $props.src,
            style: normalizeStyle({ width: $props.iconWidth + "rpx", height: $props.iconWidth + "rpx" })
          }, null, 12, ["src"])) : createCommentVNode("v-if", true),
          createElementVNode(
            "u-text",
            {
              class: normalizeClass({ "fui-loadmore__text": $props.direction === "col" }),
              style: normalizeStyle({ color: $props.color, "font-size": $props.size + "rpx", "line-height": $props.size + "rpx" })
            },
            toDisplayString($options.getStateText($props.state)),
            7
            /* TEXT, CLASS, STYLE */
          )
        ],
        6
        /* CLASS, STYLE */
      )
    ],
    2112
    /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
  );
}
const __easycom_2 = /* @__PURE__ */ _export_sfc(_sfc_main$2, [["render", _sfc_render$2], ["styles", [_style_0$2]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-loadmore/fui-loadmore.vue"]]);
const _style_0$1 = { "fui-index__list-item": { "": { "flex": 1, "flexDirection": "row", "alignItems": "center", "paddingTop": "32rpx", "paddingRight": "64rpx", "paddingBottom": "32rpx", "paddingLeft": "32rpx", "backgroundColor": "#FFFFFF", "position": "relative", "!backgroundColor:active": "rgba(0,0,0,0.2)" } }, "fui-il__border-bottom": { "": { "position": "absolute", "bottom": 0, "height": 0.5, "zIndex": -1, "backgroundColor": "#EEEEEE", "left": "32rpx", "right": 0 } }, "fui-il__between": { "": { "justifyContent": "space-between" } }, "fui-index__list-left": { "": { "flexDirection": "row", "alignItems": "center", "overflow": "hidden" } }, "fui-il__checkbox": { "": { "fontSize": 0, "color": "rgba(0,0,0,0)", "width": "40rpx", "height": "40rpx", "borderWidth": 1, "borderStyle": "solid", "borderRadius": "40rpx", "flexDirection": "row", "alignItems": "center", "justifyContent": "center", "overflow": "hidden", "position": "relative", "marginRight": "24rpx" } }, "fui-il__checkmark": { "": { "width": "20rpx", "height": "40rpx", "borderBottomStyle": "solid", "borderBottomWidth": 3, "borderBottomColor": "#FFFFFF", "borderRightStyle": "solid", "borderRightWidth": 3, "borderRightColor": "#FFFFFF", "transform": "rotate(45deg) scale(0.5)", "transformOrigin": "54% 48%" } }, "fui-il__img-box": { "": { "width": "72rpx", "height": "72rpx", "alignItems": "center", "justifyContent": "center", "borderRadius": "8rpx", "overflow": "hidden", "backgroundColor": "#F8F8F8", "marginRight": "24rpx" } }, "fui-index__list-img": { "": { "width": "72rpx", "height": "72rpx", "borderRadius": "8rpx" } }, "fui-index__list-main": { "": { "lines": 1, "fontSize": "32rpx", "fontWeight": "normal", "overflow": "hidden", "textOverflow": "ellipsis" } }, "fui-index__list-sub": { "": { "fontWeight": "normal", "fontSize": "28rpx", "color": "#999999", "lines": 1, "paddingLeft": "24rpx" } } };
const _sfc_main$1 = {
  name: "f-index-list-item",
  props: {
    model: {
      type: Object,
      default() {
        return {};
      }
    },
    isSelect: {
      type: Boolean,
      default: false
    },
    selectedColor: {
      type: String,
      default: ""
    },
    //checkbox未选中时边框颜色
    borderColor: {
      type: String,
      default: "#ccc"
    },
    //是否显示图片
    isSrc: {
      type: Boolean,
      default: false
    },
    //次要内容是否居右侧
    subRight: {
      type: Boolean,
      default: true
    },
    last: {
      type: Boolean,
      default: false
    },
    idx: {
      type: Number,
      default: 0
    },
    index: {
      type: Number,
      default: 0
    }
  },
  computed: {
    getSelectedColor() {
      let color = this.selectedColor;
      if (!color || color === true) {
        const app = uni && uni.$fui && uni.$fui.color;
        color = app && app.primary || "#465CFF";
      }
      return color;
    }
  },
  methods: {
    onClick() {
      this.$emit("itemClick", {
        idx: this.idx,
        index: this.index
      });
    }
  }
};
function _sfc_render$1(_ctx, _cache, $props, $setup, $data, $options) {
  return openBlock(), createElementBlock(
    Fragment,
    null,
    [
      createCommentVNode("本文件由FirstUI授权予严天赠（会员ID： 2 4 42，身份证尾号：0 4  301  8）专用，请尊重知识产权，勿私下传播，违者追究法律责任。"),
      createElementVNode(
        "view",
        {
          class: normalizeClass(["fui-index__list-item", { "fui-il__between": $props.subRight }]),
          onClick: _cache[0] || (_cache[0] = (...args) => $options.onClick && $options.onClick(...args))
        },
        [
          createElementVNode("view", { class: "fui-index__list-left" }, [
            $props.isSelect ? (openBlock(), createElementBlock(
              "view",
              {
                key: 0,
                class: normalizeClass(["fui-il__checkbox", { "fui-il__checkbox-color": (!$props.selectedColor || $props.selectedColor === true) && $props.model.checked }]),
                style: normalizeStyle({ background: $props.model.checked ? $options.getSelectedColor : "#fff", borderColor: $props.model.checked ? $options.getSelectedColor : $props.borderColor })
              },
              [
                $props.model.checked ? (openBlock(), createElementBlock("view", {
                  key: 0,
                  class: "fui-il__checkmark"
                })) : createCommentVNode("v-if", true)
              ],
              6
              /* CLASS, STYLE */
            )) : createCommentVNode("v-if", true),
            $props.isSrc ? (openBlock(), createElementBlock("view", {
              key: 1,
              class: "fui-il__img-box"
            }, [
              $props.model.src ? (openBlock(), createElementBlock("u-image", {
                key: 0,
                src: $props.model.src,
                class: "fui-index__list-img",
                mode: "widthFix"
              }, null, 8, ["src"])) : createCommentVNode("v-if", true)
            ])) : createCommentVNode("v-if", true),
            createElementVNode(
              "u-text",
              { class: "fui-index__list-main" },
              toDisplayString($props.model.text || ""),
              1
              /* TEXT */
            )
          ]),
          createElementVNode(
            "u-text",
            { class: "fui-index__list-sub" },
            toDisplayString($props.model.subText || ""),
            1
            /* TEXT */
          ),
          !$props.last ? (openBlock(), createElementBlock("view", {
            key: 0,
            class: "fui-il__border-bottom"
          })) : createCommentVNode("v-if", true)
        ],
        2
        /* CLASS */
      )
    ],
    2112
    /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
  );
}
const fIndexListItem = /* @__PURE__ */ _export_sfc(_sfc_main$1, [["render", _sfc_render$1], ["styles", [_style_0$1]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-index-list/f-index-list-item.vue"]]);
const _style_0 = { "fui-index__list": { "": { "position": "absolute", "left": 0, "top": 0, "right": 0, "bottom": 0, "flexDirection": "row" } }, "fui-index__list-sv": { "": { "flex": 1 } }, "fui-index__list-letter": { "": { "flex": 1, "paddingTop": 0, "paddingRight": "32rpx", "paddingBottom": 0, "paddingLeft": "32rpx", "height": "64rpx", "lineHeight": "64rpx", "flexDirection": "row", "alignItems": "center", "borderTopStyle": "solid", "borderTopWidth": 0.5, "borderTopColor": "#eeeeee", "borderBottomStyle": "solid", "borderBottomWidth": 0.5, "borderBottomColor": "#eeeeee" } }, "fui-il__letter-text": { "": { "fontSize": "28rpx", "fontWeight": "600" } }, "fui-index__letter": { "": { "position": "fixed", "right": 0, "textAlign": "center", "zIndex": 10, "flexDirection": "column" } }, "fui-letter__item": { "": { "flex": 1, "paddingTop": 0, "paddingRight": "8rpx", "paddingBottom": 0, "paddingLeft": "8rpx", "fontWeight": "bold", "alignItems": "center", "justifyContent": "center" } }, "fui-letter__key": { "": { "width": "40rpx", "height": "40rpx", "fontSize": "26rpx", "transform": "scale(0.8)", "transformOrigin": "center center", "borderRadius": "40rpx", "lineHeight": "40rpx", "alignItems": "center", "justifyContent": "center", "textAlign": "center" } }, "fui-il__indicator": { "": { "position": "fixed", "width": "100rpx", "height": "100rpx", "zIndex": 999, "justifyContent": "center", "alignItems": "center", "borderRadius": "10rpx", "right": "100rpx" } }, "fui-il__nvue-android": { "": { "!width": "128rpx", "paddingRight": "28rpx", "!right": "72rpx" } }, "fui-il__indicator-text": { "": { "textAlign": "center", "color": "#ffffff", "fontSize": "60rpx", "fontWeight": "bold" } }, "fui-il__indicator-after": { "": { "width": "100rpx", "position": "absolute", "top": 0, "right": 0, "left": 0, "bottom": 0, "zIndex": -1, "borderTopLeftRadius": "100rpx", "borderTopRightRadius": 0, "borderBottomRightRadius": "100rpx", "borderBottomLeftRadius": "100rpx", "backgroundColor": "#c9c9c9", "transform": "rotate(45deg)" } } };
const dom = weex.requireModule("dom");
function throttle(func, delay) {
  var prev = Date.now();
  return function() {
    var context = this;
    var args = arguments;
    var now = Date.now();
    if (now - prev >= delay) {
      func.apply(context, args);
      prev = Date.now();
    }
  };
}
function touchMove(e) {
  let pageY = e.touches[0].pageY;
  let index = this.getIndex(pageY - this.winOffsetY);
  if (this.touchmoveIndex === index) {
    return false;
  }
  let item = this.lists[index];
  if (item) {
    dom.scrollToElement(this.$refs[`fui_il_letter_${index}`][0], {
      animated: false
    });
    this.touchmoveIndex = index;
  }
}
const throttleTouchMove = throttle(touchMove, 40);
const _sfc_main = {
  name: "fui-index-list",
  emits: ["click", "init", "scrolltolower"],
  components: {
    fIndexListItem
  },
  props: {
    //列表数据
    listData: {
      type: Array,
      default() {
        return [];
      }
    },
    height: {
      type: [Number, String],
      default: 64
    },
    color: {
      type: String,
      default: "#181818"
    },
    background: {
      type: String,
      default: "#F1F4FA"
    },
    keyColor: {
      type: String,
      default: "#7F7F7F"
    },
    activeColor: {
      type: String,
      default: "#FFFFFF"
    },
    activeBackground: {
      type: String,
      default: ""
    },
    isSelect: {
      type: Boolean,
      default: false
    },
    //checkbox未选中时边框颜色
    borderColor: {
      type: String,
      default: "#ccc"
    },
    selectedColor: {
      type: String,
      default: ""
    },
    //是否显示图片
    isSrc: {
      type: Boolean,
      default: false
    },
    //次要内容是否居右侧
    subRight: {
      type: Boolean,
      default: true
    },
    custom: {
      type: Boolean,
      default: false
    },
    //H5端使用，是否使用了默认导航栏，默认44px
    isHeader: {
      type: Boolean,
      default: false
    }
  },
  watch: {
    listData(val) {
      this.initData();
    }
  },
  computed: {
    getActiveBgColor() {
      let color = this.activeBackground;
      if (!color || color === true) {
        const app = uni && uni.$fui && uni.$fui.color;
        color = app && app.primary || "#465CFF";
      }
      return color;
    }
  },
  data() {
    let isNvue = false;
    isNvue = true;
    return {
      lists: [],
      idtHeight: 0,
      winOffsetY: 0,
      winHeight: 0,
      styles: "",
      indicators: [],
      top: -1,
      start: 0,
      touching: false,
      touchmoveIndex: -1,
      scrollViewId: "",
      touchmovable: true,
      loaded: false,
      isPC: false,
      nvueAndroid: false,
      isNvue
    };
  },
  mounted() {
    const res = uni.getSystemInfoSync();
    if (res.platform.toLocaleLowerCase() == "android") {
      this.nvueAndroid = true;
    }
    this.$nextTick(() => {
      setTimeout(() => {
        this.initData();
      }, 50);
    });
  },
  methods: {
    //滚动到底部，会触发 scrolltolower 事件
    scrolltolower() {
      this.$emit("scrolltolower", {});
    },
    getIndex(y) {
      let index = -1;
      if (this.nvueAndroid) {
        index = Math.floor(y / uni.upx2px(40));
      } else {
        index = Math.floor((y - this.start) / uni.upx2px(40));
      }
      return index;
    },
    initData() {
      this.lists = [];
      let height = 0;
      let lists = [];
      let tempArr = [...this.listData || []];
      for (let i = 0, len = tempArr.length; i < len; i++) {
        let model = tempArr[i];
        if (!model.data || model.data.length === 0) {
          continue;
        }
        height += 40;
        model.originalIndex = i;
        model.key = `fui_key_${Math.ceil(Math.random() * 1e6).toString(36)}`;
        lists.push(model);
      }
      this.idtHeight = height;
      this.styles = `height:${height}rpx;`;
      this.idtHeight = uni.upx2px(height);
      this.styles = `height:${this.idtHeight}px;`;
      this.lists = lists;
      dom.getComponentRect(this.$refs["fui_index_list"], (res) => {
        this.winOffsetY = res.size.top;
        this.winHeight = res.size.height;
        this.setStyles();
      });
      this.$nextTick(() => {
        this.$emit("init");
      });
    },
    setStyles() {
      this.indicators = [];
      this.styles = `height:${this.idtHeight}rpx;top:${this.winHeight / 2}px;-webkit-transform: translateY(-${this.idtHeight / 2}rpx);transform: translateY(-${this.idtHeight / 2}rpx)`;
      this.styles = `height:${this.idtHeight}px;top:${this.winHeight / 2}px;-webkit-transform: translateY(-${this.idtHeight / 2}px);transform: translateY(-${this.idtHeight / 2}px)`;
      let start = this.winHeight / 2 - uni.upx2px(this.idtHeight) / 2;
      this.start = start;
      this.lists.forEach((item, index) => {
        const top = start + uni.upx2px(index * 40 + 20 - 50);
        this.indicators.push(top);
      });
    },
    startEmits(idx, index) {
      let item = this.lists[idx];
      let data = item.data[index] || {};
      this.$emit("click", {
        index: item.originalIndex,
        letter: item.letter,
        subIndex: index,
        ...data
      });
    },
    onTap(idx, index) {
      this.startEmits(idx, index);
    },
    onClick(e) {
      const {
        idx,
        index
      } = e;
      this.startEmits(idx, index);
    },
    touchStart(e) {
      this.touching = true;
      let pageY = this.isPC ? e.pageY : e.touches[0].pageY;
      let index = this.getIndex(pageY - this.winOffsetY);
      let item = this.lists[index];
      if (item) {
        this.scrollViewId = `fui_il_letter_${index}`;
        this.touchmoveIndex = index;
        dom.scrollToElement(this.$refs[`fui_il_letter_${index}`][0], {
          animated: false
        });
      }
    },
    touchMove(e) {
      throttleTouchMove.call(this, e);
    },
    touchEnd() {
      this.touching = false;
      this.touchmoveIndex = -1;
    },
    mousedown(e) {
      if (!this.isPC)
        return;
      this.touchStart(e);
    },
    mousemove(e) {
      if (!this.isPC)
        return;
      this.touchMove(e);
    },
    mouseleave(e) {
      if (!this.isPC)
        return;
      this.touchEnd(e);
    }
    //开发工具中移动端如果touch事件失效，请检查开发工具或者真机调试
    // letterTap(index) {
    // }
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_f_index_list_item = resolveComponent("f-index-list-item");
  return openBlock(), createElementBlock(
    Fragment,
    null,
    [
      createCommentVNode("本文件由FirstUI授权予严天赠（会员ID：  24 42，身份证尾号：04    30 18）专用，请尊重知识产权，勿私下传播，违者追究法律责任。"),
      createElementVNode(
        "view",
        {
          class: "fui-index__list",
          ref: "fui_index_list",
          id: "fui_index_list"
        },
        [
          createElementVNode(
            "list",
            {
              class: "fui-index__list-sv",
              scrollable: true,
              showScrollbar: false,
              loadmoreoffset: 10,
              onLoadmore: _cache[0] || (_cache[0] = (...args) => $options.scrolltolower && $options.scrolltolower(...args))
            },
            [
              createElementVNode("cell", null, [
                renderSlot(_ctx.$slots, "default")
              ]),
              (openBlock(true), createElementBlock(
                Fragment,
                null,
                renderList($data.lists, (list, idx) => {
                  return openBlock(), createElementBlock(
                    Fragment,
                    {
                      key: list.key
                    },
                    [
                      createElementVNode(
                        "header",
                        {
                          ref_for: true,
                          ref: "fui_il_letter_" + idx
                        },
                        [
                          createElementVNode(
                            "view",
                            {
                              class: normalizeClass(["fui-index__list-letter", { "fui-il__key-bg": !$props.background }]),
                              style: normalizeStyle({ background: $props.background })
                            },
                            [
                              createElementVNode(
                                "u-text",
                                {
                                  class: "fui-il__letter-text",
                                  style: normalizeStyle({ color: $props.color })
                                },
                                toDisplayString(list.descr || list.letter),
                                5
                                /* TEXT, STYLE */
                              )
                            ],
                            6
                            /* CLASS, STYLE */
                          )
                        ],
                        512
                        /* NEED_PATCH */
                      ),
                      createElementVNode("cell", null, [
                        createCommentVNode(" 解构插槽 item项样式内容自定义 "),
                        $props.custom ? (openBlock(), createElementBlock("view", { key: 0 }, [
                          (openBlock(true), createElementBlock(
                            Fragment,
                            null,
                            renderList(list.data, (model, index) => {
                              return openBlock(), createElementBlock("view", {
                                key: index,
                                onClick: ($event) => $options.onTap(idx, index)
                              }, [
                                renderSlot(_ctx.$slots, "item", {
                                  model,
                                  idx,
                                  index,
                                  last: list.data.length - 1 === index,
                                  isSelect: $props.isSelect,
                                  isSrc: $props.isSrc,
                                  subRight: $props.subRight
                                })
                              ], 8, ["onClick"]);
                            }),
                            128
                            /* KEYED_FRAGMENT */
                          ))
                        ])) : (openBlock(), createElementBlock("view", { key: 1 }, [
                          (openBlock(true), createElementBlock(
                            Fragment,
                            null,
                            renderList(list.data, (model, index) => {
                              return openBlock(), createBlock(_component_f_index_list_item, {
                                onItemClick: $options.onClick,
                                model,
                                idx,
                                index,
                                last: list.data.length - 1 === index,
                                isSelect: $props.isSelect,
                                borderColor: $props.borderColor,
                                selectedColor: $props.selectedColor,
                                isSrc: $props.isSrc,
                                subRight: $props.subRight,
                                key: index
                              }, null, 8, ["onItemClick", "model", "idx", "index", "last", "isSelect", "borderColor", "selectedColor", "isSrc", "subRight"]);
                            }),
                            128
                            /* KEYED_FRAGMENT */
                          ))
                        ]))
                      ])
                    ],
                    64
                    /* STABLE_FRAGMENT */
                  );
                }),
                128
                /* KEYED_FRAGMENT */
              )),
              createElementVNode("cell", null, [
                renderSlot(_ctx.$slots, "footer")
              ])
            ],
            32
            /* HYDRATE_EVENTS */
          ),
          $data.touching && $data.touchmoveIndex !== -1 ? (openBlock(), createElementBlock(
            "view",
            {
              key: 0,
              class: normalizeClass(["fui-il__indicator", { "fui-il__nvue-android": $data.nvueAndroid }]),
              style: normalizeStyle({ top: $data.idtHeight / 2 + "px" })
            },
            [
              createElementVNode("view", { class: "fui-il__indicator-after" }),
              createElementVNode(
                "u-text",
                { class: "fui-il__indicator-text" },
                toDisplayString($data.lists[$data.touchmoveIndex] && $data.lists[$data.touchmoveIndex].letter),
                1
                /* TEXT */
              )
            ],
            6
            /* CLASS, STYLE */
          )) : createCommentVNode("v-if", true),
          !$data.isNvue || $data.styles ? (openBlock(), createElementBlock(
            "view",
            {
              key: 1,
              class: "fui-index__letter",
              style: normalizeStyle($data.styles),
              onTouchstart: _cache[1] || (_cache[1] = (...args) => $options.touchStart && $options.touchStart(...args)),
              onTouchmove: _cache[2] || (_cache[2] = withModifiers((...args) => $options.touchMove && $options.touchMove(...args), ["stop", "prevent"])),
              onTouchend: _cache[3] || (_cache[3] = (...args) => $options.touchEnd && $options.touchEnd(...args)),
              onMousedown: _cache[4] || (_cache[4] = withModifiers((...args) => $options.mousedown && $options.mousedown(...args), ["stop"])),
              onMousemove: _cache[5] || (_cache[5] = withModifiers((...args) => $options.mousemove && $options.mousemove(...args), ["stop", "prevent"])),
              onMouseleave: _cache[6] || (_cache[6] = withModifiers((...args) => $options.mouseleave && $options.mouseleave(...args), ["stop"]))
            },
            [
              (openBlock(true), createElementBlock(
                Fragment,
                null,
                renderList($data.lists, (item, i) => {
                  return openBlock(), createElementBlock("view", {
                    class: "fui-letter__item",
                    key: i
                  }, [
                    createCommentVNode(' @tap="letterTap(i)" '),
                    createElementVNode(
                      "u-text",
                      {
                        class: normalizeClass(["fui-letter__key", { "fui-il__key-color": i === $data.touchmoveIndex && !$props.activeBackground }]),
                        style: normalizeStyle({ background: i === $data.touchmoveIndex ? $options.getActiveBgColor : "transparent", color: i === $data.touchmoveIndex ? $props.activeColor : $props.keyColor })
                      },
                      toDisplayString(item.letter),
                      7
                      /* TEXT, CLASS, STYLE */
                    )
                  ]);
                }),
                128
                /* KEYED_FRAGMENT */
              ))
            ],
            36
            /* STYLE, HYDRATE_EVENTS */
          )) : createCommentVNode("v-if", true)
        ],
        512
        /* NEED_PATCH */
      )
    ],
    2112
    /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
  );
}
const __easycom_4 = /* @__PURE__ */ _export_sfc(_sfc_main, [["render", _sfc_render], ["styles", [_style_0]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-index-list/fui-index-list.vue"]]);
export {
  __easycom_0 as _,
  __easycom_2 as a,
  __easycom_4 as b
};
