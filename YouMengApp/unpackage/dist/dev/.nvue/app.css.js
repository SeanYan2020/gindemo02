var __getOwnPropNames = Object.getOwnPropertyNames;
var __commonJS = (cb, mod) => function __require() {
  return mod || (0, cb[__getOwnPropNames(cb)[0]])((mod = { exports: {} }).exports, mod), mod.exports;
};
var require_app_css = __commonJS({
  "app.css.js"(exports) {
    const _style_0 = { "fui-wrap": { "": { "flexDirection": "column" } }, "fui-flex__center": { "": { "flexDirection": "column", "alignItems": "center" } }, "fui--active": { "": { "opacity:active": 0.5 } }, "fui-primary": { "": { "!color": "#246BFD" } }, "fui-ellipsis": { "": { "textOverflow": "ellipsis", "overflow": "hidden", "lines": 1 } }, "fui-size": { "": { "fontSize": "32rpx" } } };
    exports.styles = [_style_0];
  }
});
export default require_app_css();
