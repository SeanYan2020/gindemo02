import { openBlock, createElementBlock, Fragment, createCommentVNode, createElementVNode, normalizeClass, normalizeStyle, toDisplayString, renderSlot, withModifiers } from "vue";
import { _ as _export_sfc } from "./_plugin-vue_export-helper.js";
const _style_0 = { "fui-input__wrap": { "": { "flexDirection": "row", "flex": 1, "alignItems": "center", "position": "relative", "borderWidth": 0 } }, "fui-input__border-top": { "": { "position": "absolute", "top": 0, "height": 0.5, "zIndex": -1 } }, "fui-input__border-bottom": { "": { "position": "absolute", "bottom": 0, "height": 0.5, "zIndex": -1 } }, "fui-input__required": { "": { "position": "absolute", "left": "12rpx", "top": "28rpx", "bottom": "28rpx", "alignItems": "center", "justifyContent": "center" } }, "fui-form__asterisk-text": { "": { "fontSize": "32rpx", "height": "32rpx" } }, "fui-input__label": { "": { "paddingRight": "12rpx" } }, "fui-input__self-wrap": { "": { "flex": 1, "flexDirection": "row", "position": "relative" } }, "fui-input__cover": { "": { "position": "absolute", "left": 0, "right": 0, "top": 0, "bottom": 0 } }, "fui-input__self": { "": { "flex": 1, "paddingRight": "12rpx", "backgroundColor": "rgba(0,0,0,0)" } }, "fui-input__clear-wrap": { "": { "width": "32rpx", "height": "32rpx", "transform": "rotate(45deg) scale(1.1)", "position": "relative", "borderRadius": "32rpx" } }, "fui-input__clear": { "": { "width": "32rpx", "height": "32rpx", "flexDirection": "row", "alignItems": "center", "justifyContent": "center", "position": "absolute", "left": 0, "top": 0, "transform": "scale(0.5) translateZ(0)" } }, "fui-input__clear-a": { "": { "width": "32rpx", "borderWidth": "2rpx", "borderStyle": "solid", "borderColor": "#ffffff", "backgroundColor": "#ffffff" } }, "fui-input__clear-b": { "": { "height": "32rpx", "borderWidth": "2rpx", "borderStyle": "solid", "borderColor": "#ffffff", "backgroundColor": "#ffffff" } }, "fui-input__placeholder": { "": { "color": "#cccccc", "fontSize": "32rpx" } }, "fui-input__border-nvue": { "": { "borderWidth": 0.5, "borderStyle": "solid" } }, "fui-input__text-left": { "": { "textAlign": "left" } }, "fui-input__text-right": { "": { "textAlign": "right" } }, "fui-input__text-center": { "": { "textAlign": "center" } }, "fui-input__disabled-styl": { "": { "opacity": 0.6 } } };
const _sfc_main = {
  name: "fui-input",
  emits: ["input", "update:modelValue", "focus", "blur", "confirm", "click", "keyboardheightchange"],
  props: {
    //是否为必填项
    required: {
      type: Boolean,
      default: false
    },
    requiredColor: {
      type: String,
      default: ""
    },
    //左侧标题
    label: {
      type: String,
      default: ""
    },
    //标题字体大小
    labelSize: {
      type: [Number, String],
      default: 0
    },
    labelColor: {
      type: String,
      default: "#333"
    },
    //label 最小宽度 rpx
    labelWidth: {
      type: [Number, String],
      default: 140
    },
    clearable: {
      type: Boolean,
      default: false
    },
    clearColor: {
      type: String,
      default: "#CCCCCC"
    },
    //获取焦点
    focus: {
      type: Boolean,
      default: false
    },
    placeholder: {
      type: String,
      default: ""
    },
    placeholderStyle: {
      type: String,
      default: ""
    },
    //输入框名称
    name: {
      type: String,
      default: ""
    },
    //输入框值 vue2
    value: {
      type: [Number, String],
      default: ""
    },
    //输入框值
    modelValue: {
      type: [Number, String],
      default: ""
    },
    //vue3
    modelModifiers: {
      default: () => ({})
    },
    //兼容写法，type为text时也做Number处理，NaN时返回原值
    number: {
      type: Boolean,
      default: false
    },
    //与官方input type属性一致
    type: {
      type: String,
      default: "text"
    },
    password: {
      type: Boolean,
      default: false
    },
    disabled: {
      type: Boolean,
      default: false
    },
    //V2.1.0+
    disabledStyle: {
      type: Boolean,
      default: false
    },
    readonly: {
      type: Boolean,
      default: false
    },
    maxlength: {
      type: [Number, String],
      default: 140
    },
    min: {
      type: [Number, String],
      default: "NaN"
    },
    max: {
      type: [Number, String],
      default: "NaN"
    },
    cursorSpacing: {
      type: Number,
      default: 0
    },
    confirmType: {
      type: String,
      default: "done"
    },
    confirmHold: {
      type: Boolean,
      default: false
    },
    cursor: {
      type: Number,
      default: -1
    },
    selectionStart: {
      type: Number,
      default: -1
    },
    selectionEnd: {
      type: Number,
      default: -1
    },
    adjustPosition: {
      type: Boolean,
      default: true
    },
    holdKeyboard: {
      type: Boolean,
      default: false
    },
    autoBlur: {
      type: Boolean,
      default: false
    },
    alwaysEmbed: {
      type: Boolean,
      default: false
    },
    size: {
      type: [Number, String],
      default: 0
    },
    color: {
      type: String,
      default: "#333"
    },
    inputBorder: {
      type: Boolean,
      default: false
    },
    isFillet: {
      type: Boolean,
      default: false
    },
    radius: {
      type: [Number, String],
      default: 8
    },
    borderTop: {
      type: Boolean,
      default: false
    },
    topLeft: {
      type: [Number, String],
      default: 0
    },
    topRight: {
      type: [Number, String],
      default: 0
    },
    borderBottom: {
      type: Boolean,
      default: true
    },
    bottomLeft: {
      type: [Number, String],
      default: 32
    },
    bottomRight: {
      type: [Number, String],
      default: 0
    },
    borderColor: {
      type: String,
      default: "#EEEEEE"
    },
    trim: {
      type: Boolean,
      default: true
    },
    //即将废弃，请使用textAlign属性
    textRight: {
      type: Boolean,
      default: false
    },
    //V2.2.0+ 可选值：left/center/right
    textAlign: {
      type: String,
      default: "left"
    },
    padding: {
      type: Array,
      default() {
        return ["28rpx", "32rpx"];
      }
    },
    backgroundColor: {
      type: String,
      default: "#FFFFFF"
    },
    marginTop: {
      type: [Number, String],
      default: 0
    }
  },
  data() {
    return {
      placeholderStyl: "",
      focused: false,
      val: ""
    };
  },
  computed: {
    getRadius() {
      let radius = this.radius + "rpx";
      if (this.isFillet) {
        radius = "120px";
      }
      return radius;
    },
    getBorderRadius() {
      let radius = Number(this.radius) * 2 + "rpx";
      if (this.isFillet) {
        radius = "240px";
      }
      return radius;
    },
    getSize() {
      const size = uni.$fui && uni.$fui.fuiInput && uni.$fui.fuiInput.size || 32;
      return `${this.size || size}rpx`;
    },
    getLabelSize() {
      const labelSize = uni.$fui && uni.$fui.fuiInput && uni.$fui.fuiInput.labelSize || 32;
      return `${this.labelSize || labelSize}rpx`;
    },
    dangerColor() {
      const app = uni && uni.$fui && uni.$fui.color;
      return app && app.danger || "#FF2B2B";
    }
  },
  watch: {
    focus(val) {
      this.$nextTick(() => {
        setTimeout(() => {
          this.focused = val;
        }, 20);
      });
    },
    focused(val) {
      if (!this.$refs.fuiInput)
        return;
      this.$nextTick(() => {
        setTimeout(() => {
          if (val) {
            this.$refs.fuiInput.focus();
          } else {
            this.$refs.fuiInput.blur();
          }
        }, 50);
      });
    },
    placeholderStyle() {
      this.fieldPlaceholderStyle();
    },
    modelValue(newVal) {
      this.val = newVal;
    },
    value(newVal) {
      this.val = newVal;
    }
  },
  created() {
    this.fieldPlaceholderStyle();
    setTimeout(() => {
      if (this.value && !this.modelValue) {
        this.val = this.value;
      } else {
        this.val = this.modelValue;
      }
    }, 50);
  },
  mounted() {
    this.$nextTick(() => {
      setTimeout(() => {
        this.focused = this.focus;
      }, 300);
    });
  },
  methods: {
    fieldPlaceholderStyle() {
      if (this.placeholderStyle) {
        this.placeholderStyl = this.placeholderStyle;
      } else {
        const _size = uni.$fui && uni.$fui.fuiInput && uni.$fui.fuiInput.size || 32;
        const size = uni.upx2px(this.size || _size);
        this.placeholderStyl = `fontSize:${size}px;`;
      }
    },
    onInput(event) {
      let value = event.detail.value;
      if (this.trim)
        value = this.trimStr(value);
      this.val = value;
      const currentVal = Number(value);
      if ((this.modelModifiers.number || this.number || this.type === "digit" || this.type === "number") && !isNaN(currentVal) && Number.isSafeInteger(currentVal)) {
        let eVal = this.type === "digit" ? value : currentVal;
        if (typeof currentVal === "number") {
          const min = Number(this.min);
          const max = Number(this.max);
          if (typeof min === "number" && currentVal < min) {
            eVal = min;
          } else if (typeof max === "number" && max < currentVal) {
            eVal = max;
          }
        }
        value = isNaN(eVal) ? value : eVal;
      }
      this.$nextTick(() => {
        event.detail.value !== "" && (this.val = String(value));
      });
      const inputValue = event.detail.value !== "" ? value : "";
      this.$emit("input", inputValue);
      this.$emit("update:modelValue", inputValue);
    },
    onFocus(event) {
      this.$emit("focus", event);
    },
    onBlur(event) {
      this.$emit("blur", event);
    },
    onConfirm(e) {
      this.$emit("confirm", e);
    },
    onClear(event) {
      if (this.disabled && !this.readonly)
        return;
      uni.hideKeyboard();
      this.val = "";
      this.$emit("input", "");
      this.$emit("update:modelValue", "");
    },
    fieldClick() {
      this.$emit("click", {
        name: this.name,
        target: "wrap"
      });
    },
    /**
     * 在安卓nvue上，事件无法冒泡
     * 外层容器点击事件无法触发，需要单独处理
     */
    fieldClickAndroid(e) {
    },
    getParent(name = "fui-form-item") {
      let parent = this.$parent;
      let parentName = parent.$options.name;
      while (parentName !== name) {
        parent = parent.$parent;
        if (!parent)
          return false;
        parentName = parent.$options.name;
      }
      return parent;
    },
    onKeyboardheightchange(e) {
      this.$emit("keyboardheightchange", e.detail);
    },
    trimStr(str) {
      return str.replace(/^\s+|\s+$/g, "");
    }
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return openBlock(), createElementBlock(
    Fragment,
    null,
    [
      createCommentVNode("本文件由FirstUI授权予严天赠（会员ID：2 4 4 2，身份证尾号：     043018）专用，请尊重知识产权，勿私下传播，违者追究法律责任。"),
      createElementVNode(
        "view",
        {
          class: normalizeClass(["fui-input__wrap", { "fui-input__border-nvue": $props.inputBorder }]),
          style: normalizeStyle({ paddingTop: $props.padding[0] || 0, paddingRight: $props.padding[1] || 0, paddingBottom: $props.padding[2] || $props.padding[0] || 0, paddingLeft: $props.padding[3] || $props.padding[1] || 0, background: $props.backgroundColor, marginTop: $props.marginTop + "rpx", borderRadius: $options.getRadius, borderColor: $props.borderColor }),
          onClick: _cache[7] || (_cache[7] = (...args) => $options.fieldClick && $options.fieldClick(...args))
        },
        [
          $props.borderTop && !$props.inputBorder ? (openBlock(), createElementBlock(
            "view",
            {
              key: 0,
              style: normalizeStyle({ background: $props.borderColor, left: $props.topLeft + "rpx", right: $props.topRight + "rpx" }),
              class: normalizeClass(["fui-input__border-top", { "fui-input__background": !$props.borderColor }])
            },
            null,
            6
            /* CLASS, STYLE */
          )) : createCommentVNode("v-if", true),
          $props.required ? (openBlock(), createElementBlock("view", {
            key: 1,
            class: "fui-input__required"
          }, [
            createElementVNode(
              "u-text",
              {
                style: normalizeStyle({ color: $props.requiredColor || $options.dangerColor }),
                class: "fui-form__asterisk-text"
              },
              "*",
              4
              /* STYLE */
            )
          ])) : createCommentVNode("v-if", true),
          $props.label ? (openBlock(), createElementBlock(
            "view",
            {
              key: 2,
              class: "fui-input__label",
              style: normalizeStyle({ minWidth: $props.labelWidth + "rpx" })
            },
            [
              createElementVNode(
                "u-text",
                {
                  style: normalizeStyle({ fontSize: $options.getLabelSize, color: $props.labelColor })
                },
                toDisplayString($props.label),
                5
                /* TEXT, STYLE */
              )
            ],
            4
            /* STYLE */
          )) : createCommentVNode("v-if", true),
          renderSlot(_ctx.$slots, "left"),
          createElementVNode("view", { class: "fui-input__self-wrap" }, [
            createElementVNode("u-input", {
              ref: "fuiInput",
              class: normalizeClass(["fui-input__self", { "fui-input__text-right": $props.textRight }]),
              style: normalizeStyle({ fontSize: $options.getSize, color: $props.color, textAlign: $props.textRight ? "right" : $props.textAlign }),
              placeholderClass: "fui-input__placeholder",
              type: $props.type,
              name: $props.name,
              value: $data.val,
              placeholder: $props.placeholder,
              password: $props.password || $props.type === "password",
              placeholderStyle: $data.placeholderStyl,
              disabled: $props.disabled || $props.readonly,
              cursorSpacing: $props.cursorSpacing,
              maxlength: $props.maxlength,
              focus: $data.focused,
              confirmType: $props.confirmType,
              confirmHold: $props.confirmHold,
              cursor: $props.cursor,
              selectionStart: $props.selectionStart,
              selectionEnd: $props.selectionEnd,
              adjustPosition: $props.adjustPosition,
              holdKeyboard: $props.holdKeyboard,
              autoBlur: $props.autoBlur,
              enableNative: false,
              alwaysEmbed: $props.alwaysEmbed,
              onFocus: _cache[0] || (_cache[0] = (...args) => $options.onFocus && $options.onFocus(...args)),
              onBlur: _cache[1] || (_cache[1] = (...args) => $options.onBlur && $options.onBlur(...args)),
              onInput: _cache[2] || (_cache[2] = (...args) => $options.onInput && $options.onInput(...args)),
              onConfirm: _cache[3] || (_cache[3] = (...args) => $options.onConfirm && $options.onConfirm(...args)),
              onKeyboardheightchange: _cache[4] || (_cache[4] = (...args) => $options.onKeyboardheightchange && $options.onKeyboardheightchange(...args))
            }, null, 46, ["type", "name", "value", "placeholder", "password", "placeholderStyle", "disabled", "cursorSpacing", "maxlength", "focus", "confirmType", "confirmHold", "cursor", "selectionStart", "selectionEnd", "adjustPosition", "holdKeyboard", "autoBlur", "alwaysEmbed"]),
            $props.disabled || $props.readonly ? (openBlock(), createElementBlock("view", {
              key: 0,
              class: "fui-input__cover",
              onClick: _cache[5] || (_cache[5] = (...args) => $options.fieldClickAndroid && $options.fieldClickAndroid(...args))
            })) : createCommentVNode("v-if", true)
          ]),
          $props.clearable && $data.val != "" ? (openBlock(), createElementBlock(
            "view",
            {
              key: 3,
              class: "fui-input__clear-wrap",
              style: normalizeStyle({ background: $props.clearColor }),
              onClick: _cache[6] || (_cache[6] = withModifiers((...args) => $options.onClear && $options.onClear(...args), ["stop"]))
            },
            [
              createElementVNode("view", { class: "fui-input__clear" }, [
                createElementVNode("view", { class: "fui-input__clear-a" })
              ]),
              createElementVNode("view", { class: "fui-input__clear" }, [
                createElementVNode("view", { class: "fui-input__clear-b" })
              ])
            ],
            4
            /* STYLE */
          )) : createCommentVNode("v-if", true),
          renderSlot(_ctx.$slots, "default"),
          $props.borderBottom && !$props.inputBorder ? (openBlock(), createElementBlock(
            "view",
            {
              key: 4,
              style: normalizeStyle({ background: $props.borderColor, left: $props.bottomLeft + "rpx", right: $props.bottomRight + "rpx" }),
              class: normalizeClass(["fui-input__border-bottom", { "fui-input__background": !$props.borderColor }])
            },
            null,
            6
            /* CLASS, STYLE */
          )) : createCommentVNode("v-if", true)
        ],
        6
        /* CLASS, STYLE */
      )
    ],
    2112
    /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
  );
}
const __easycom_1 = /* @__PURE__ */ _export_sfc(_sfc_main, [["render", _sfc_render], ["styles", [_style_0]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-input/fui-input.vue"]]);
export {
  __easycom_1 as _
};
