import { openBlock, createElementBlock, Fragment, createCommentVNode, createElementVNode, normalizeStyle, toDisplayString, normalizeClass, renderSlot } from "vue";
import { _ as _export_sfc } from "./_plugin-vue_export-helper.js";
const _style_0 = { "fui-list__wrap": { "": { "flex": 1 } }, "fui-list__title": { "": { "lineHeight": 1 } }, "fui-list__footer": { "": { "lineHeight": 1 } }, "fui-list__container": { "": { "position": "relative", "flex": 1 } }, "fui-list__border-top": { "": { "position": "absolute", "top": 0, "height": 0.5, "zIndex": -1 } }, "fui-list__border-bottom": { "": { "position": "absolute", "bottom": 0, "height": 0.5, "zIndex": -1 } } };
const _sfc_main = {
  name: "fui-list",
  props: {
    //fui-list margin-top值,单位rpx
    marginTop: {
      type: [Number, String],
      default: 0
    },
    //标题
    title: {
      type: String,
      default: ""
    },
    //标题颜色
    color: {
      type: String,
      default: "#7F7F7F"
    },
    //标题字体大小 单位rpx
    size: {
      type: [Number, String],
      default: 28
    },
    //标题padding值['32rpx','32rpx','20rpx','32rpx'] 上、右、下、左
    padding: {
      type: Array,
      default() {
        return ["32rpx", "32rpx", "20rpx", "32rpx"];
      }
    },
    //标题背景色
    background: {
      type: String,
      default: "transparent"
    },
    //底部说明文字
    footer: {
      type: String,
      default: ""
    },
    //底部说明文字颜色
    footerColor: {
      type: String,
      default: "#7F7F7F"
    },
    //底部说明文字大小 单位rpx
    footerSize: {
      type: [Number, String],
      default: 28
    },
    //底部说明文本padding值['20rpx','32rpx','0','32rpx'] 上、右、下、左
    footerPadding: {
      type: Array,
      default() {
        return ["20rpx", "32rpx", "0"];
      }
    },
    //是否显示上边框
    topBorder: {
      type: Boolean,
      default: true
    },
    //是否显示下边框
    bottomBorder: {
      type: Boolean,
      default: false
    },
    //边框颜色，非nvue下传值则全局默认样式失效
    borderColor: {
      type: String,
      default: "#EEEEEE"
    },
    //上边框left值，单位rpx
    topLeft: {
      type: [Number, String],
      default: 0
    },
    //上边框right值，单位rpx
    topRight: {
      type: [Number, String],
      default: 0
    },
    //下边框left值，单位rpx
    bottomLeft: {
      type: [Number, String],
      default: 0
    },
    //下边框right值，单位rpx
    bottomRight: {
      type: [Number, String],
      default: 0
    }
  }
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return openBlock(), createElementBlock(
    Fragment,
    null,
    [
      createCommentVNode("本文件由FirstUI授权予严天赠（会员ID：24 4  2，身份证尾号：0 43 0   18）专用，请尊重知识产权，勿私下传播，违者追究法律责任。"),
      createElementVNode(
        "view",
        {
          class: "fui-list__wrap",
          style: normalizeStyle({ marginTop: $props.marginTop + "rpx" })
        },
        [
          $props.title ? (openBlock(), createElementBlock(
            "view",
            {
              key: 0,
              class: "fui-list__title",
              style: normalizeStyle({ paddingTop: $props.padding[0] || 0, paddingRight: $props.padding[1] || 0, paddingBottom: $props.padding[2] || $props.padding[0] || 0, paddingLeft: $props.padding[3] || $props.padding[1] || 0, background: $props.background })
            },
            [
              createElementVNode(
                "u-text",
                {
                  style: normalizeStyle({ color: $props.color, fontSize: $props.size + "rpx" })
                },
                toDisplayString($props.title),
                5
                /* TEXT, STYLE */
              )
            ],
            4
            /* STYLE */
          )) : createCommentVNode("v-if", true),
          createElementVNode("view", { class: "fui-list__container" }, [
            $props.topBorder ? (openBlock(), createElementBlock(
              "view",
              {
                key: 0,
                style: normalizeStyle({ background: $props.borderColor, left: $props.topLeft + "rpx", right: $props.topRight + "rpx" }),
                class: normalizeClass(["fui-list__border-top", { "fui-list__border-color": !$props.borderColor }])
              },
              null,
              6
              /* CLASS, STYLE */
            )) : createCommentVNode("v-if", true),
            renderSlot(_ctx.$slots, "default"),
            $props.bottomBorder ? (openBlock(), createElementBlock(
              "view",
              {
                key: 1,
                style: normalizeStyle({ background: $props.borderColor, left: $props.bottomLeft + "rpx", right: $props.bottomRight + "rpx" }),
                class: normalizeClass(["fui-list__border-bottom", { "fui-list__border-color": !$props.borderColor }])
              },
              null,
              6
              /* CLASS, STYLE */
            )) : createCommentVNode("v-if", true)
          ]),
          $props.footer ? (openBlock(), createElementBlock(
            "view",
            {
              key: 1,
              class: "fui-list__footer",
              style: normalizeStyle({ paddingTop: $props.footerPadding[0] || 0, paddingRight: $props.footerPadding[1] || 0, paddingBottom: $props.footerPadding[2] || $props.footerPadding[0] || 0, paddingLeft: $props.footerPadding[3] || $props.footerPadding[1] || 0 })
            },
            [
              createElementVNode(
                "u-text",
                {
                  style: normalizeStyle({ color: $props.footerColor, fontSize: $props.footerSize + "rpx" })
                },
                toDisplayString($props.footer),
                5
                /* TEXT, STYLE */
              )
            ],
            4
            /* STYLE */
          )) : createCommentVNode("v-if", true)
        ],
        4
        /* STYLE */
      )
    ],
    2112
    /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
  );
}
const __easycom_4 = /* @__PURE__ */ _export_sfc(_sfc_main, [["render", _sfc_render], ["styles", [_style_0]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-list/fui-list.vue"]]);
export {
  __easycom_4 as _
};
