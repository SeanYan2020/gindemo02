"use weex:vue";

if (typeof Promise !== 'undefined' && !Promise.prototype.finally) {
  Promise.prototype.finally = function(callback) {
    const promise = this.constructor
    return this.then(
      value => promise.resolve(callback()).then(() => value),
      reason => promise.resolve(callback()).then(() => {
        throw reason
      })
    )
  }
};

if (typeof uni !== 'undefined' && uni && uni.requireGlobal) {
  const global = uni.requireGlobal()
  ArrayBuffer = global.ArrayBuffer
  Int8Array = global.Int8Array
  Uint8Array = global.Uint8Array
  Uint8ClampedArray = global.Uint8ClampedArray
  Int16Array = global.Int16Array
  Uint16Array = global.Uint16Array
  Int32Array = global.Int32Array
  Uint32Array = global.Uint32Array
  Float32Array = global.Float32Array
  Float64Array = global.Float64Array
  BigInt64Array = global.BigInt64Array
  BigUint64Array = global.BigUint64Array
};


(() => {
  var __create = Object.create;
  var __defProp = Object.defineProperty;
  var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
  var __getOwnPropNames = Object.getOwnPropertyNames;
  var __getProtoOf = Object.getPrototypeOf;
  var __hasOwnProp = Object.prototype.hasOwnProperty;
  var __commonJS = (cb, mod) => function __require() {
    return mod || (0, cb[__getOwnPropNames(cb)[0]])((mod = { exports: {} }).exports, mod), mod.exports;
  };
  var __copyProps = (to, from, except, desc) => {
    if (from && typeof from === "object" || typeof from === "function") {
      for (let key of __getOwnPropNames(from))
        if (!__hasOwnProp.call(to, key) && key !== except)
          __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
    }
    return to;
  };
  var __toESM = (mod, isNodeMode, target) => (target = mod != null ? __create(__getProtoOf(mod)) : {}, __copyProps(
    // If the importer is in node compatibility mode or this is not an ESM
    // file that has been converted to a CommonJS file using a Babel-
    // compatible transform (i.e. "__esModule" has not been set), then set
    // "default" to the CommonJS "module.exports" for node compatibility.
    isNodeMode || !mod || !mod.__esModule ? __defProp(target, "default", { value: mod, enumerable: true }) : target,
    mod
  ));

  // vue-ns:vue
  var require_vue = __commonJS({
    "vue-ns:vue"(exports, module) {
      module.exports = Vue;
    }
  });

  // vuex-ns:vuex
  var require_vuex = __commonJS({
    "vuex-ns:vuex"(exports, module) {
      module.exports = uni.Vuex;
    }
  });

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/uni-app.es.js
  var import_vue = __toESM(require_vue());
  var isString = (val) => typeof val === "string";
  function requireNativePlugin(name) {
    return weex.requireModule(name);
  }
  function formatAppLog(type, filename, ...args) {
    if (uni.__log__) {
      uni.__log__(type, filename, ...args);
    } else {
      console[type].apply(console, [...args, filename]);
    }
  }
  function resolveEasycom(component, easycom) {
    return isString(component) ? easycom : component;
  }

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/pages/captcha/captcha.js
  var import_vue3 = __toESM(require_vue());

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/_plugin-vue_export-helper.js
  var _export_sfc = (sfc, props) => {
    const target = sfc.__vccOpts || sfc;
    for (const [key, val] of props) {
      target[key] = val;
    }
    return target;
  };

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/fui-button.js
  var import_vue2 = __toESM(require_vue());
  var _style_0 = { "fui-button__wrap": { "": { "position": "relative" } }, "fui-button": { "": { "borderWidth": 0, "borderStyle": "solid", "position": "relative", "paddingLeft": 0, "paddingRight": 0, "borderWidth::after": 0, "borderStyle::after": "solid", "borderColor::after": "#000000" } }, "fui-button__flex-1": { "": { "flex": 1 } }, "fui-button__text": { "": { "textAlign": "center", "flexDirection": "row", "alignItems": "center", "!justifyContent": "center", "!paddingLeft": 0 } }, "fui-button__opacity": { "": { "opacity": 0.5 } }, "fui-text__bold": { "": { "fontWeight": "bold" } }, "fui-button__link": { "": { "!borderColor": "rgba(0,0,0,0)", "!backgroundColor": "rgba(0,0,0,0)" } } };
  var _sfc_main = {
    name: "fui-button",
    emits: ["click", "getuserinfo", "contact", "getphonenumber", "error", "opensetting", "chooseavatar", "launchapp"],
    props: {
      //样式类型：primary，success， warning，danger，link，purple，gray
      type: {
        type: String,
        default: "primary"
      },
      //按钮背景色，当传入值时type失效
      background: {
        type: String,
        default: ""
      },
      //按钮显示文本
      text: {
        type: String,
        default: ""
      },
      //按钮字体颜色
      color: {
        type: String,
        default: ""
      },
      //按钮禁用背景色
      disabledBackground: {
        type: String,
        default: ""
      },
      //按钮禁用字体颜色
      disabledColor: {
        type: String,
        default: ""
      },
      borderWidth: {
        type: String,
        default: "0.5px"
      },
      borderColor: {
        type: String,
        default: ""
      },
      //V1.9.8+ 按钮大小，优先级高于width和height，medium、small、mini
      btnSize: {
        type: String,
        default: ""
      },
      //宽度
      width: {
        type: String,
        default: "100%"
      },
      //高度
      height: {
        type: String,
        default: ""
      },
      //字体大小，单位rpx
      size: {
        type: [Number, String],
        default: 0
      },
      bold: {
        type: Boolean,
        default: false
      },
      //['20rpx','30rpx','20rpx','30rpx']->[上，右，下，左]
      margin: {
        type: Array,
        default() {
          return ["0", "0"];
        }
      },
      //圆角
      radius: {
        type: String,
        default: ""
      },
      plain: {
        type: Boolean,
        default: false
      },
      disabled: {
        type: Boolean,
        default: false
      },
      loading: {
        type: Boolean,
        default: false
      },
      formType: {
        type: String,
        default: ""
      },
      openType: {
        type: String,
        default: ""
      },
      //支付宝小程序
      //当 open-type 为 getAuthorize 时，可以设置 scope 为：phoneNumber、userInfo
      scope: {
        type: String,
        default: ""
      },
      appParameter: {
        type: String,
        default: ""
      },
      index: {
        type: [Number, String],
        default: 0
      }
    },
    computed: {
      getTypeColor() {
        const app = uni && uni.$fui && uni.$fui.color;
        let colors = {
          primary: app && app.primary || "#465CFF",
          success: app && app.success || "#09BE4F",
          warning: app && app.warning || "#FFB703",
          danger: app && app.danger || "#FF2B2B",
          link: "transparent",
          purple: app && app.purple || "#6831FF",
          gray: "#F8F8F8"
        };
        return colors[this.type] || "transparent";
      },
      getBackground() {
        let color = this.getTypeColor;
        if (this.disabled || this.plain) {
          color = "transparent";
        }
        if (!this.disabled && !this.plain && this.background) {
          color = this.background;
        }
        return color;
      },
      getColor() {
        let color = "#fff";
        if (this.color) {
          color = this.disabled && this.disabledBackground ? this.disabledColor : this.color;
        } else {
          if (this.disabled && this.disabledBackground) {
            color = this.disabledColor || "#FFFFFF";
          } else {
            const app = uni && uni.$fui && uni.$fui.color;
            const primary = app && app.primary || "#465CFF";
            color = this.type === "gray" ? primary : "#FFFFFF";
          }
        }
        return color;
      },
      getSize() {
        let size = this.size || uni && uni.$fui && uni.$fui.fuiButton && uni.$fui.fuiButton.size || 32;
        if (this.btnSize === "small") {
          size = size > 28 ? 28 : size;
        } else if (this.btnSize === "mini") {
          size = size > 28 ? 24 : size;
        }
        return `${size}rpx`;
      },
      getWidth() {
        let width = this.width;
        if (this.btnSize && this.btnSize !== true) {
          width = {
            "medium": "400rpx",
            "small": "200rpx",
            "mini": "120rpx"
          }[this.btnSize] || width;
        }
        return width;
      },
      getHeight() {
        let height = this.height || uni && uni.$fui && uni.$fui.fuiButton && uni.$fui.fuiButton.height || "96rpx";
        if (this.btnSize && this.btnSize !== true) {
          height = {
            "medium": "84rpx",
            "small": "72rpx",
            "mini": "64rpx"
          }[this.btnSize] || height;
        }
        return height;
      },
      getRadius() {
        const radius = uni && uni.$fui && uni.$fui.fuiButton && uni.$fui.fuiButton.radius || "16rpx";
        return this.radius || radius;
      }
    },
    data() {
      let isNvue = false;
      isNvue = true;
      return {
        isNvue,
        time: 0,
        trigger: false,
        pc: false
      };
    },
    created() {
    },
    methods: {
      handleStart(e) {
      },
      handleClick() {
        if (this.disabled || !this.trigger)
          return;
        this.time = 0;
      },
      handleTap() {
        if (this.disabled)
          return;
        this.$emit("click", {
          index: Number(this.index)
        });
      },
      handleEnd(e) {
      },
      bindgetuserinfo({
        detail = {}
      } = {}) {
        this.$emit("getuserinfo", detail);
      },
      bindcontact({
        detail = {}
      } = {}) {
        this.$emit("contact", detail);
      },
      bindgetphonenumber({
        detail = {}
      } = {}) {
        this.$emit("getphonenumber", detail);
      },
      binderror({
        detail = {}
      } = {}) {
        this.$emit("error", detail);
      },
      bindopensetting({
        detail = {}
      } = {}) {
        this.$emit("opensetting", detail);
      },
      bindchooseavatar({
        detail = {}
      } = {}) {
        this.$emit("chooseavatar", detail);
      },
      bindlaunchapp({
        detail = {}
      } = {}) {
        this.$emit("launchapp", detail);
      }
    }
  };
  function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_button = (0, import_vue2.resolveComponent)("button");
    return (0, import_vue2.openBlock)(), (0, import_vue2.createElementBlock)(
      import_vue2.Fragment,
      null,
      [
        (0, import_vue2.createCommentVNode)("\u672C\u6587\u4EF6\u7531FirstUI\u6388\u6743\u4E88\u4E25\u5929\u8D60\uFF08\u4F1A\u5458ID\uFF1A24 4  2\uFF0C\u8EAB\u4EFD\u8BC1\u5C3E\u53F7\uFF1A0 4   3 018\uFF09\u4E13\u7528\uFF0C\u8BF7\u5C0A\u91CD\u77E5\u8BC6\u4EA7\u6743\uFF0C\u52FF\u79C1\u4E0B\u4F20\u64AD\uFF0C\u8FDD\u8005\u8FFD\u7A76\u6CD5\u5F8B\u8D23\u4EFB\u3002"),
        (0, import_vue2.createElementVNode)(
          "view",
          {
            class: (0, import_vue2.normalizeClass)(["fui-button__wrap", [!$options.getWidth || $options.getWidth === "100%" || $options.getWidth === true ? "fui-button__flex-1" : "", $props.disabled && !$props.disabledBackground ? "fui-button__opacity" : ""]]),
            style: (0, import_vue2.normalizeStyle)({ width: $options.getWidth, height: $options.getHeight, marginTop: $props.margin[0] || 0, marginRight: $props.margin[1] || 0, marginBottom: $props.margin[2] || $props.margin[0] || 0, marginLeft: $props.margin[3] || $props.margin[1] || 0, borderRadius: $options.getRadius, background: $options.getBackground }),
            onTouchstart: _cache[0] || (_cache[0] = (...args) => $options.handleStart && $options.handleStart(...args)),
            onTouchend: _cache[1] || (_cache[1] = (...args) => $options.handleClick && $options.handleClick(...args)),
            onTouchcancel: _cache[2] || (_cache[2] = (...args) => $options.handleEnd && $options.handleEnd(...args))
          },
          [
            (0, import_vue2.createVNode)(_component_button, {
              class: (0, import_vue2.normalizeClass)(["fui-button", [
                $props.bold ? "fui-text__bold" : "",
                $data.time && ($props.plain || $props.type === "link") ? "fui-button__opacity" : "",
                !$props.background && !$props.disabledBackground && !$props.plain ? "fui-button__" + $props.type : "",
                !$options.getWidth || $options.getWidth === "100%" || $options.getWidth === true ? "fui-button__flex-1" : "",
                $data.time && !$props.plain && $props.type !== "link" ? "fui-button__active" : "",
                $data.pc && !$props.disabled ? $props.plain || $props.type === "link" ? "fui-button__opacity-pc" : "fui-button__active-pc" : ""
              ]]),
              style: (0, import_vue2.normalizeStyle)({
                width: $options.getWidth,
                height: $options.getHeight,
                lineHeight: $options.getHeight,
                background: $props.disabled ? $props.disabledBackground || $options.getTypeColor : $props.plain ? "transparent" : $options.getBackground,
                borderWidth: !$props.borderColor || !$data.isNvue ? "0" : $props.borderWidth,
                borderColor: $props.borderColor ? $props.borderColor : $props.disabled && $props.disabledBackground ? $props.disabledBackground : $props.background || "transparent",
                borderRadius: $options.getRadius,
                fontSize: $options.getSize,
                color: $options.getColor
              }),
              loading: $props.loading,
              "form-type": $props.formType,
              "open-type": $props.openType,
              "app-parameter": $props.appParameter,
              onGetuserinfo: $options.bindgetuserinfo,
              onGetphonenumber: $options.bindgetphonenumber,
              onContact: $options.bindcontact,
              onError: $options.binderror,
              onOpensetting: $options.bindopensetting,
              onChooseavatar: $options.bindchooseavatar,
              onLaunchapp: $options.bindlaunchapp,
              disabled: $props.disabled,
              scope: $props.scope,
              onClick: (0, import_vue2.withModifiers)($options.handleTap, ["stop"])
            }, {
              default: (0, import_vue2.withCtx)(() => [
                $props.text ? ((0, import_vue2.openBlock)(), (0, import_vue2.createElementBlock)(
                  "u-text",
                  {
                    key: 0,
                    class: (0, import_vue2.normalizeClass)(["fui-button__text", { "fui-btn__gray-color": !$props.background && !$props.disabledBackground && !$props.plain && $props.type === "gray" && $props.color === "#fff", "fui-text__bold": $props.bold }]),
                    style: (0, import_vue2.normalizeStyle)({ fontSize: $options.getSize, lineHeight: $options.getSize, color: $options.getColor })
                  },
                  (0, import_vue2.toDisplayString)($props.text),
                  7
                  /* TEXT, CLASS, STYLE */
                )) : (0, import_vue2.createCommentVNode)("v-if", true),
                (0, import_vue2.renderSlot)(_ctx.$slots, "default")
              ]),
              _: 3
              /* FORWARDED */
            }, 8, ["class", "style", "loading", "form-type", "open-type", "app-parameter", "onGetuserinfo", "onGetphonenumber", "onContact", "onError", "onOpensetting", "onChooseavatar", "onLaunchapp", "disabled", "scope", "onClick"])
          ],
          38
          /* CLASS, STYLE, HYDRATE_EVENTS */
        )
      ],
      2112
      /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
    );
  }
  var __easycom_6 = /* @__PURE__ */ _export_sfc(_sfc_main, [["render", _sfc_render], ["styles", [_style_0]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-button/fui-button.vue"]]);

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/pages/captcha/captcha.js
  var import_vuex = __toESM(require_vuex());
  var _style_0$2 = { "fui-single__input-wrap": { "": { "position": "relative" } }, "fui-single__input": { "": { "flex": 1, "flexDirection": "row", "alignItems": "center", "justifyContent": "space-between" } }, "fui-sinput__item": { "": { "flexDirection": "row", "justifyContent": "center", "alignItems": "center", "borderStyle": "solid", "position": "relative", "overflow": "hidden" } }, "fui-sinput__text": { "": { "position": "absolute", "left": 0, "top": 0, "flex": 1, "flexDirection": "row", "alignItems": "center", "justifyContent": "center", "textAlign": "center" } }, "fui-sinput__placeholder": { "": { "textAlign": "center", "opacity": 0 } }, "fui-sinput__cursor": { "": { "borderRadius": 2, "width": 0 } }, "fui-sinput__cursor-ani": { "": { "width": 2 } }, "fui-sinput__hidden": { "": { "position": "absolute", "width": 100, "height": 100, "left": 0, "top": 0, "right": 0, "bottom": 0, "flex": 1, "zIndex": 2, "marginTop": 0, "marginRight": 0, "marginBottom": 0, "marginLeft": 0, "paddingTop": 0, "paddingRight": 0, "paddingBottom": 0, "paddingLeft": 0, "opacity": 0, "color": "rgba(0,0,0,0)" } }, "fui-sinput__disabled": { "": { "opacity": 0.5 } } };
  var animation$1 = requireNativePlugin("animation");
  var _sfc_main$2 = {
    name: "fui-single-input",
    emits: ["complete", "focus", "input", "blur", "confirm"],
    props: {
      //组件外层左右padding值
      padding: {
        type: [Number, String],
        default: 88
      },
      marginTop: {
        type: [Number, String],
        default: 0
      },
      marginBottom: {
        type: [Number, String],
        default: 0
      },
      //native为false时，自定义键盘时输入的值；native为true时初始值。不可超过length长度
      value: {
        type: String,
        default: ""
      },
      //native为true时有效，H5不支持动态切换type类型
      type: {
        type: String,
        default: "text"
      },
      password: {
        type: Boolean,
        default: false
      },
      disabled: {
        type: Boolean,
        default: false
      },
      //获取焦点
      isFocus: {
        type: Boolean,
        default: false
      },
      //是否弹起原生键盘，设为false，则结合自定义键盘使用
      native: {
        type: Boolean,
        default: true
      },
      cursor: {
        type: Boolean,
        default: true
      },
      cursorColor: {
        type: String,
        default: ""
      },
      cursorHeight: {
        type: [Number, String],
        default: 60
      },
      //内容长度/输入框个数，一般4~6个字符，请控制在6个或以下
      length: {
        type: Number,
        default: 4
      },
      width: {
        type: [Number, String],
        default: 112
      },
      height: {
        type: [Number, String],
        default: 112
      },
      background: {
        type: String,
        default: "transparent"
      },
      //1-显示所有边框 2-只显示底部边框，3-无边框
      border: {
        type: [Number, String],
        default: 2
      },
      borderColor: {
        type: String,
        default: "#eee"
      },
      activeColor: {
        type: String,
        default: ""
      },
      borderWidth: {
        type: [Number, String],
        default: 4
      },
      radius: {
        type: [Number, String],
        default: 0
      },
      size: {
        type: [Number, String],
        default: 48
      },
      color: {
        type: String,
        default: "#181818"
      },
      fontWeight: {
        type: [Number, String],
        default: 600
      }
    },
    computed: {
      getCursorColor() {
        let color = this.cursorColor;
        if (!color || color === true) {
          const app = uni && uni.$fui && uni.$fui.color;
          color = app && app.primary || "#465CFF";
        }
        return color;
      },
      getActiveColor() {
        let color = this.activeColor;
        if (!color || color === true) {
          const app = uni && uni.$fui && uni.$fui.color;
          color = app && app.primary || "#465CFF";
        }
        return color;
      }
    },
    data() {
      return {
        inputArr: [],
        inputVal: [],
        focus: false,
        opacity: 0,
        stop: false,
        ref: "",
        activeIndex: -1,
        aliZero: false,
        val: ""
      };
    },
    watch: {
      length(val) {
        const nums = Number(val);
        if (nums !== this.inputArr.length) {
          this.inputArr = this.getArr(nums);
        }
      },
      value(val) {
        this.focus = true;
        val = val.replace(/\s+/g, "");
        this.getVals(val);
      },
      activeIndex(val) {
        if (val == -1 || val == this.length) {
          this.stop = true;
          this.opacity = 1;
        } else {
          this.stop = false;
          const ref = this.inputArr[val];
          this.ref = ref;
          this.opacity = 0;
          this._animation(ref);
        }
      },
      focus(val) {
        if (!this.$refs.inputRef)
          return;
        if (val) {
          this.$refs.inputRef.focus();
        } else {
          this.$refs.inputRef.blur();
        }
      },
      isFocus(val) {
        this.initFocus(val);
      }
    },
    created() {
      this.inputArr = this.getArr(Number(this.length));
      let val = this.value.replace(/\s+/g, "");
      this.getVals(val, true);
    },
    mounted() {
      this.$nextTick(() => {
        setTimeout(() => {
          this.initFocus(this.isFocus);
        }, 300);
      });
    },
    //nvue暂不支持vue3，所以不需要做兼容,此处以防后续兼容
    beforeUnmount() {
      this.stop = true;
    },
    methods: {
      initFocus(val) {
        if (this.disabled)
          return;
        if (val && this.activeIndex === -1) {
          this.activeIndex = 0;
        }
        if (!this.value && !val) {
          this.activeIndex = -1;
        }
        this.$nextTick(() => {
          this.focus = val;
          if (this.focus && !this.native) {
            this.onTap();
          }
        });
      },
      getArr(end) {
        let arr = Array.from(new Array(end + 1).keys()).slice(1);
        arr = arr.map((item) => {
          const ref = `fui_ref_${Math.ceil(Math.random() * 1e6).toString(36)}`;
          return ref;
        });
        return arr;
      },
      _animation(ref) {
        if (!this.$refs[ref] || this.stop || this.ref != ref)
          return;
        animation$1.transition(this.$refs[ref][0].ref, {
          styles: {
            opacity: this.opacity
          },
          duration: 400,
          //ms
          timingFunction: "linear",
          iterationCount: "infinite",
          needLayout: false,
          delay: 0
          //ms
        }, () => {
          this.opacity = this.opacity == 0 ? 1 : 0;
          setTimeout(() => {
            this._animation(ref);
          }, 200);
        });
      },
      getVals(val, init = false) {
        this.val = val;
        if (!val) {
          this.inputVal = [];
          this.activeIndex = init ? -1 : 0;
        } else {
          let vals = val.split("");
          let arr = [];
          this.inputArr.forEach((item, index) => {
            arr.push(vals[index] || "");
          });
          this.inputVal = arr;
          const len = vals.length;
          this.activeIndex = len > this.length ? this.length : len;
          if (len === this.length) {
            this.$emit("complete", {
              detail: {
                value: val
              }
            });
            this.focus = false;
            uni.hideKeyboard();
          }
        }
      },
      onTap() {
        if (this.disabled)
          return;
        this.focus = true;
        if (this.activeIndex === -1) {
          this.activeIndex = 0;
        }
        if (this.activeIndex === this.length) {
          this.activeIndex--;
        }
        this.$emit("focus", {});
      },
      onInput(e) {
        let value = e.detail.value;
        value = value.replace(/\s+/g, "");
        this.getVals(value);
        this.$emit("input", {
          detail: {
            value
          }
        });
      },
      onBlur(e) {
        let value = e.detail.value;
        value = value.replace(/\s+/g, "");
        this.focus = false;
        if (!value) {
          this.activeIndex = -1;
        }
        this.$emit("blur", {
          detail: {
            value
          }
        });
      },
      onConfirm(e) {
        this.focus = false;
        uni.hideKeyboard();
        this.$emit("confirm", e);
      },
      onAliClick() {
      },
      clear() {
        this.val = "";
        this.inputVal = [];
        this.activeIndex = -1;
        this.$nextTick(() => {
          this.onTap();
        });
      }
    }
  };
  function _sfc_render$2(_ctx, _cache, $props, $setup, $data, $options) {
    return (0, import_vue3.openBlock)(), (0, import_vue3.createElementBlock)(
      import_vue3.Fragment,
      null,
      [
        (0, import_vue3.createCommentVNode)("\u672C\u6587\u4EF6\u7531FirstUI\u6388\u6743\u4E88\u4E25\u5929\u8D60\uFF08\u4F1A\u5458ID\uFF1A  2 442\uFF0C\u8EAB\u4EFD\u8BC1\u5C3E\u53F7\uFF1A 0 43 01  8\uFF09\u4E13\u7528\uFF0C\u8BF7\u5C0A\u91CD\u77E5\u8BC6\u4EA7\u6743\uFF0C\u52FF\u79C1\u4E0B\u4F20\u64AD\uFF0C\u8FDD\u8005\u8FFD\u7A76\u6CD5\u5F8B\u8D23\u4EFB\u3002"),
        (0, import_vue3.createElementVNode)(
          "view",
          {
            class: "fui-single__input-wrap",
            style: (0, import_vue3.normalizeStyle)({ marginTop: $props.marginTop + "rpx", marginBottom: $props.marginBottom + "rpx" }),
            onClick: _cache[5] || (_cache[5] = (...args) => $options.onAliClick && $options.onAliClick(...args))
          },
          [
            (0, import_vue3.createElementVNode)(
              "view",
              {
                class: (0, import_vue3.normalizeClass)(["fui-single__input", { "fui-sinput__disabled": $props.disabled }]),
                style: (0, import_vue3.normalizeStyle)({ paddingLeft: $props.padding + "rpx", paddingRight: $props.padding + "rpx" })
              },
              [
                ((0, import_vue3.openBlock)(true), (0, import_vue3.createElementBlock)(
                  import_vue3.Fragment,
                  null,
                  (0, import_vue3.renderList)($data.inputArr, (item, index) => {
                    return (0, import_vue3.openBlock)(), (0, import_vue3.createElementBlock)(
                      "view",
                      {
                        class: (0, import_vue3.normalizeClass)(["fui-sinput__item", { "fui-sinput__border-color": !$props.borderColor && $data.activeIndex !== index && !$data.inputVal[index], "fui-sinput__active-color": !$props.activeColor && ($data.activeIndex === index || $data.inputVal[index]) }]),
                        style: (0, import_vue3.normalizeStyle)({ width: $props.width + "rpx", height: $props.height + "rpx", background: $props.background, borderRadius: $props.radius + "rpx", borderColor: $data.activeIndex === index || $data.inputVal[index] ? $options.getActiveColor : $props.borderColor, borderTopWidth: ($props.border == 1 ? $props.borderWidth : 0) + "rpx", borderLeftWidth: ($props.border == 1 ? $props.borderWidth : 0) + "rpx", borderRightWidth: ($props.border == 1 ? $props.borderWidth : 0) + "rpx", borderBottomWidth: ($props.border == 1 || $props.border == 2 ? $props.borderWidth : 0) + "rpx" }),
                        onClick: _cache[0] || (_cache[0] = (...args) => $options.onTap && $options.onTap(...args)),
                        key: index
                      },
                      [
                        (0, import_vue3.createElementVNode)(
                          "u-text",
                          {
                            class: (0, import_vue3.normalizeClass)(["fui-sinput__text", { "fui-sinput__color": !$props.color }]),
                            style: (0, import_vue3.normalizeStyle)({ width: $props.width + "rpx", height: $props.height + "rpx", fontSize: $props.size + "rpx", lineHeight: $props.height + "rpx", color: $props.color, fontWeight: $props.fontWeight })
                          },
                          (0, import_vue3.toDisplayString)($props.password ? $data.inputVal[index] ? "\u25CF" : "" : $data.inputVal[index] || ""),
                          7
                          /* TEXT, CLASS, STYLE */
                        ),
                        (0, import_vue3.createElementVNode)(
                          "u-text",
                          {
                            class: "fui-sinput__placeholder",
                            style: (0, import_vue3.normalizeStyle)({ fontSize: $props.size + "rpx", fontWeight: $props.fontWeight })
                          },
                          (0, import_vue3.toDisplayString)($props.password ? $data.inputVal[index] ? "\u25CF" : "" : $data.inputVal[index] || ""),
                          5
                          /* TEXT, STYLE */
                        ),
                        $props.cursor && !$props.disabled ? ((0, import_vue3.openBlock)(), (0, import_vue3.createElementBlock)(
                          "view",
                          {
                            key: 0,
                            class: (0, import_vue3.normalizeClass)(["fui-sinput__cursor", { "fui-sinput__cursor-color": !$props.cursorColor, "fui-sinput__cursor-ani": $data.activeIndex === index && $data.focus }]),
                            ref_for: true,
                            ref: item,
                            style: (0, import_vue3.normalizeStyle)({ height: $props.cursorHeight + "rpx", background: $options.getCursorColor })
                          },
                          null,
                          6
                          /* CLASS, STYLE */
                        )) : (0, import_vue3.createCommentVNode)("v-if", true)
                      ],
                      6
                      /* CLASS, STYLE */
                    );
                  }),
                  128
                  /* KEYED_FRAGMENT */
                ))
              ],
              6
              /* CLASS, STYLE */
            ),
            $props.native ? ((0, import_vue3.openBlock)(), (0, import_vue3.createElementBlock)("u-input", {
              key: 0,
              ref: "inputRef",
              value: $data.val,
              password: $props.password,
              type: $props.type,
              class: (0, import_vue3.normalizeClass)(["fui-sinput__hidden", { "fui-sinput__alizero": $data.aliZero }]),
              onInput: _cache[1] || (_cache[1] = (...args) => $options.onInput && $options.onInput(...args)),
              onBlur: _cache[2] || (_cache[2] = (...args) => $options.onBlur && $options.onBlur(...args)),
              maxlength: $props.length,
              disabled: $props.disabled,
              onConfirm: _cache[3] || (_cache[3] = (...args) => $options.onConfirm && $options.onConfirm(...args)),
              onFocus: _cache[4] || (_cache[4] = (...args) => $options.onTap && $options.onTap(...args))
            }, null, 42, ["value", "password", "type", "maxlength", "disabled"])) : (0, import_vue3.createCommentVNode)("v-if", true)
          ],
          4
          /* STYLE */
        )
      ],
      2112
      /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
    );
  }
  var __easycom_0 = /* @__PURE__ */ _export_sfc(_sfc_main$2, [["render", _sfc_render$2], ["styles", [_style_0$2]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-single-input/fui-single-input.vue"]]);
  var _style_0$1 = { "fui-count__down-wrap": { "": { "flexDirection": "row", "alignItems": "center", "justifyContent": "center", "textAlign": "center" } }, "fui-count__down-item": { "": { "alignItems": "center", "justifyContent": "center", "borderRadius": "6rpx", "borderWidth": "1rpx", "borderStyle": "solid", "textAlign": "center" } }, "fui-count__down-colon": { "": { "justifyContent": "center", "alignItems": "center", "textAlign": "center" } }, "fui-count__down-ms": { "": { "overflow": "hidden", "borderRadius": "6rpx", "borderWidth": "1rpx", "borderStyle": "solid" } }, "fui-count__down-ms_item": { "": { "alignItems": "center", "justifyContent": "center", "textAlign": "center" } } };
  var animation = requireNativePlugin("animation");
  var _sfc_main$1 = {
    name: "fui-count-down",
    emits: ["end", "time"],
    props: {
      width: {
        type: [Number, String],
        default: 40
      },
      height: {
        type: [Number, String],
        default: 40
      },
      borderColor: {
        type: String,
        default: "#333"
      },
      background: {
        type: String,
        default: "#fff"
      },
      size: {
        type: [Number, String],
        default: 28
      },
      color: {
        type: String,
        default: "#333"
      },
      colonSize: {
        type: [Number, String],
        default: 28
      },
      colonColor: {
        type: String,
        default: "#333"
      },
      value: {
        type: [Number, String],
        default: 0
      },
      //是否包含天
      isDays: {
        type: Boolean,
        default: false
      },
      isHours: {
        type: Boolean,
        default: true
      },
      isMinutes: {
        type: Boolean,
        default: true
      },
      isSeconds: {
        type: Boolean,
        default: true
      },
      unitEn: {
        type: Boolean,
        default: false
      },
      isMs: {
        type: Boolean,
        default: false
      },
      msWidth: {
        type: [Number, String],
        default: 36
      },
      msSize: {
        type: [Number, String],
        default: 24
      },
      msColor: {
        type: String,
        default: ""
      },
      isColon: {
        type: Boolean,
        default: true
      },
      returnTime: {
        type: Boolean,
        default: false
      },
      autoStart: {
        type: Boolean,
        default: true
      }
    },
    watch: {
      value(val) {
        this.clearTimer();
        this.time = Number(val) || 0;
        this.countDown(this.time);
        if (this.returnTime) {
          this.$emit("time", {
            seconds: this.time
          });
        }
        setTimeout(() => {
          if (this.autoStart) {
            this.startCountdown();
          }
        }, 0);
      }
    },
    data() {
      return {
        countdown: null,
        d: "0",
        h: "00",
        i: "00",
        s: "00",
        ms: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
        ani: false,
        percent: 0,
        time: 0
      };
    },
    mounted() {
      this.clearTimer();
      this.time = Number(this.value) || 0;
      this.countDown(this.time);
      if (this.time > 0 && this.autoStart) {
        if (this.returnTime) {
          this.$emit("time", {
            seconds: this.time
          });
        }
        this.startCountdown();
      }
    },
    beforeUnmount() {
      this.clearTimer();
    },
    methods: {
      _animation() {
        if (!this.$refs["fui_countdown_ms"] || !this.isMs || !this.ani)
          return;
        animation.transition(
          this.$refs["fui_countdown_ms"].ref,
          {
            styles: {
              transform: `translateY(-${this.percent}%)`
            },
            duration: 0,
            //ms
            timingFunction: "linear",
            iterationCount: "infinite",
            needLayout: false,
            delay: 0
            //ms
          },
          () => {
            if (this.percent >= 90) {
              this.percent = 0;
            } else {
              this.percent += 10;
            }
            setTimeout(() => {
              this._animation();
            }, 100);
          }
        );
      },
      _aniEnd() {
        if (!this.$refs["fui_countdown_ms"] || !this.isMs)
          return;
        animation.transition(
          this.$refs["fui_countdown_ms"].ref,
          {
            styles: {
              transform: "translateY(0)"
            },
            duration: 0,
            needLayout: false,
            delay: 0
          }
        );
      },
      getWidth: function(num, width) {
        num = Number(num);
        let w = Math.ceil(num > 99 ? width / 2 * num.toString().length : width);
        w = w % 2 === 0 ? w : w + 1;
        return w;
      },
      clearTimer() {
        this.ani = false;
        this._aniEnd();
        clearInterval(this.countdown);
        this.countdown = null;
      },
      endCountdown(isSuspend = false) {
        this.clearTimer();
        if (!isSuspend) {
          this.$emit("end", {});
        }
      },
      startCountdown: function() {
        this.clearTimer();
        if (this.time <= 0) {
          this.endCountdown();
          return;
        }
        this.ani = true;
        this._animation();
        this.countdown = setInterval(() => {
          this.time--;
          this.countDown(this.time);
          if (this.returnTime) {
            this.$emit("time", {
              seconds: this.time
            });
          }
          if (this.time <= 0) {
            this.endCountdown();
            return;
          }
        }, 1e3);
      },
      resetCountdown(seconds = 0) {
        this.time = seconds || Number(this.value);
        this.clearTimer();
        this.countDown(this.time);
        if (this.returnTime) {
          this.$emit("time", {
            seconds: this.time
          });
        }
        if (this.autoStart) {
          this.startCountdown();
        }
      },
      countDown(seconds) {
        let [day, hour, minute, second] = [0, 0, 0, 0];
        if (seconds > 0) {
          day = this.isDays ? Math.floor(seconds / (60 * 60 * 24)) : 0;
          hour = this.isHours ? Math.floor(seconds / (60 * 60)) - day * 24 : 0;
          minute = this.isMinutes ? Math.floor(seconds / 60) - hour * 60 - day * 24 * 60 : 0;
          second = Math.floor(seconds) - day * 24 * 60 * 60 - hour * 60 * 60 - minute * 60;
        }
        hour = hour < 10 ? "0" + hour : hour;
        minute = minute < 10 ? "0" + minute : minute;
        second = second < 10 ? "0" + second : second;
        this.d = day;
        this.h = hour;
        this.i = minute;
        this.s = second;
      }
    }
  };
  function _sfc_render$1(_ctx, _cache, $props, $setup, $data, $options) {
    return (0, import_vue3.openBlock)(), (0, import_vue3.createElementBlock)(
      import_vue3.Fragment,
      null,
      [
        (0, import_vue3.createCommentVNode)("\u672C\u6587\u4EF6\u7531FirstUI\u6388\u6743\u4E88\u4E25\u5929\u8D60\uFF08\u4F1A\u5458ID\uFF1A   2442\uFF0C\u8EAB\u4EFD\u8BC1\u5C3E\u53F7\uFF1A  0  4 3018\uFF09\u4E13\u7528\uFF0C\u8BF7\u5C0A\u91CD\u77E5\u8BC6\u4EA7\u6743\uFF0C\u52FF\u79C1\u4E0B\u4F20\u64AD\uFF0C\u8FDD\u8005\u8FFD\u7A76\u6CD5\u5F8B\u8D23\u4EFB\u3002"),
        (0, import_vue3.createElementVNode)("view", { class: "fui-count__down-wrap" }, [
          $props.isDays ? ((0, import_vue3.openBlock)(), (0, import_vue3.createElementBlock)(
            "u-text",
            {
              key: 0,
              class: "fui-count__down-item",
              style: (0, import_vue3.normalizeStyle)({ background: $props.background, borderColor: $props.borderColor, width: $options.getWidth($data.d, $props.width) + "rpx", height: $props.height + "rpx", fontSize: $props.size + "rpx", color: $props.color })
            },
            (0, import_vue3.toDisplayString)($data.d),
            5
            /* TEXT, STYLE */
          )) : (0, import_vue3.createCommentVNode)("v-if", true),
          $props.isDays ? ((0, import_vue3.openBlock)(), (0, import_vue3.createElementBlock)(
            "u-text",
            {
              key: 1,
              class: "fui-count__down-colon",
              style: (0, import_vue3.normalizeStyle)({ lineHeight: $props.colonSize + "rpx", fontSize: $props.colonSize + "rpx", color: $props.colonColor })
            },
            (0, import_vue3.toDisplayString)($props.isColon ? ":" : "\u5929"),
            5
            /* TEXT, STYLE */
          )) : (0, import_vue3.createCommentVNode)("v-if", true),
          $props.isHours ? ((0, import_vue3.openBlock)(), (0, import_vue3.createElementBlock)(
            "u-text",
            {
              key: 2,
              class: "fui-count__down-item",
              style: (0, import_vue3.normalizeStyle)({ background: $props.background, borderColor: $props.borderColor, width: $options.getWidth($data.h, $props.width) + "rpx", height: $props.height + "rpx", fontSize: $props.size + "rpx", color: $props.color })
            },
            (0, import_vue3.toDisplayString)($data.h),
            5
            /* TEXT, STYLE */
          )) : (0, import_vue3.createCommentVNode)("v-if", true),
          $props.isHours ? ((0, import_vue3.openBlock)(), (0, import_vue3.createElementBlock)(
            "u-text",
            {
              key: 3,
              class: "fui-count__down-colon",
              style: (0, import_vue3.normalizeStyle)({ lineHeight: $props.colonSize + "rpx", fontSize: $props.colonSize + "rpx", color: $props.colonColor })
            },
            (0, import_vue3.toDisplayString)($props.isColon ? ":" : "\u65F6"),
            5
            /* TEXT, STYLE */
          )) : (0, import_vue3.createCommentVNode)("v-if", true),
          $props.isMinutes ? ((0, import_vue3.openBlock)(), (0, import_vue3.createElementBlock)(
            "u-text",
            {
              key: 4,
              class: "fui-count__down-item",
              style: (0, import_vue3.normalizeStyle)({ background: $props.background, borderColor: $props.borderColor, width: $options.getWidth($data.i, $props.width) + "rpx", height: $props.height + "rpx", fontSize: $props.size + "rpx", color: $props.color })
            },
            (0, import_vue3.toDisplayString)($data.i),
            5
            /* TEXT, STYLE */
          )) : (0, import_vue3.createCommentVNode)("v-if", true),
          $props.isMinutes ? ((0, import_vue3.openBlock)(), (0, import_vue3.createElementBlock)(
            "u-text",
            {
              key: 5,
              class: "fui-count__down-colon",
              style: (0, import_vue3.normalizeStyle)({ lineHeight: $props.colonSize + "rpx", fontSize: $props.colonSize + "rpx", color: $props.colonColor })
            },
            (0, import_vue3.toDisplayString)($props.isColon ? ":" : "\u5206"),
            5
            /* TEXT, STYLE */
          )) : (0, import_vue3.createCommentVNode)("v-if", true),
          $props.isSeconds ? ((0, import_vue3.openBlock)(), (0, import_vue3.createElementBlock)(
            "u-text",
            {
              key: 6,
              class: "fui-count__down-item",
              style: (0, import_vue3.normalizeStyle)({ background: $props.background, borderColor: $props.borderColor, width: $options.getWidth($data.s, $props.width) + "rpx", height: $props.height + "rpx", fontSize: $props.size + "rpx", color: $props.color })
            },
            (0, import_vue3.toDisplayString)($data.s),
            5
            /* TEXT, STYLE */
          )) : (0, import_vue3.createCommentVNode)("v-if", true),
          $props.isSeconds && !$props.isColon ? ((0, import_vue3.openBlock)(), (0, import_vue3.createElementBlock)(
            "u-text",
            {
              key: 7,
              class: "fui-count__down-colon",
              style: (0, import_vue3.normalizeStyle)({ lineHeight: $props.colonSize + "rpx", fontSize: $props.colonSize + "rpx", color: $props.colonColor })
            },
            (0, import_vue3.toDisplayString)($props.unitEn ? "s" : "\u79D2"),
            5
            /* TEXT, STYLE */
          )) : (0, import_vue3.createCommentVNode)("v-if", true),
          $props.isSeconds && $props.isMs && $props.isColon ? ((0, import_vue3.openBlock)(), (0, import_vue3.createElementBlock)(
            "u-text",
            {
              key: 8,
              class: "fui-count__down-colon",
              style: (0, import_vue3.normalizeStyle)({ lineHeight: $props.colonSize + "rpx", fontSize: $props.colonSize + "rpx", color: $props.colonColor })
            },
            ".",
            4
            /* STYLE */
          )) : (0, import_vue3.createCommentVNode)("v-if", true),
          $props.isSeconds && $props.isMs ? ((0, import_vue3.openBlock)(), (0, import_vue3.createElementBlock)(
            "view",
            {
              key: 9,
              class: "fui-count__down-ms",
              style: (0, import_vue3.normalizeStyle)({
                background: $props.background,
                borderColor: $props.borderColor,
                height: $props.height + "rpx",
                width: $props.msWidth > 0 ? $props.msWidth + "rpx" : "auto"
              })
            },
            [
              (0, import_vue3.createElementVNode)(
                "view",
                {
                  class: (0, import_vue3.normalizeClass)({ "fui-count__down-ms_list": $data.ani }),
                  ref: "fui_countdown_ms"
                },
                [
                  ((0, import_vue3.openBlock)(true), (0, import_vue3.createElementBlock)(
                    import_vue3.Fragment,
                    null,
                    (0, import_vue3.renderList)($data.ms, (item, index) => {
                      return (0, import_vue3.openBlock)(), (0, import_vue3.createElementBlock)(
                        "u-text",
                        {
                          class: "fui-count__down-ms_item",
                          style: (0, import_vue3.normalizeStyle)({ height: $props.height + "rpx", lineHeight: $props.height + "rpx", fontSize: $props.msSize + "rpx", color: $props.msColor || $props.color }),
                          key: index
                        },
                        (0, import_vue3.toDisplayString)(item),
                        5
                        /* TEXT, STYLE */
                      );
                    }),
                    128
                    /* KEYED_FRAGMENT */
                  ))
                ],
                2
                /* CLASS */
              )
            ],
            4
            /* STYLE */
          )) : (0, import_vue3.createCommentVNode)("v-if", true)
        ])
      ],
      2112
      /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
    );
  }
  var __easycom_1 = /* @__PURE__ */ _export_sfc(_sfc_main$1, [["render", _sfc_render$1], ["styles", [_style_0$1]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-count-down/fui-count-down.vue"]]);
  var utils = {
    /**
     * @desc 英文首字母大写：english=>English
     * @param {String}  value 需要处理的英文字符串
     **/
    titleCase(value) {
      if (value == null || value.length === 0)
        return value;
      return value.replace(/^[a-z]/, (matchStr) => {
        return matchStr.toLocaleUpperCase();
      });
    },
    /**
     * 把连续出现多次的字母字符串进行压缩。aaabbbbcccccd=>3a4b5cd
     * @param {String} value 需要压缩的字符串
     * @param {Boolean} ignoreCase 是否忽略大小写
     */
    compressLetter(value, ignoreCase) {
      let pattern = new RegExp("([a-zA-Z])\\1+", ignoreCase ? "ig" : "g");
      return value.replace(pattern, (matchStr, group_1) => {
        return matchStr.length + group_1;
      });
    },
    /**
     * @desc 等待多少毫秒再执行 ，同步阻塞
     * @param {String} millisecond 毫秒
     **/
    sleep(millisecond) {
      let now = /* @__PURE__ */ new Date();
      let exitTime = now.getTime() + millisecond;
      while (true) {
        now = /* @__PURE__ */ new Date();
        if (now.getTime() > exitTime)
          return;
      }
    },
    /**
     * @desc 去左右空格
     * @param {String} value 需要处理的字符串
     **/
    trim(value) {
      return value.replace(/(^\s*)|(\s*$)/g, "");
    },
    /**
     * @desc 去所有空格
     * @param {String} value 需要处理的字符串
     **/
    trimAll(value) {
      return value.replace(/\s+/g, "");
    },
    /**
     * @desc 替换所有相同字符串
     * @param {String} text 需要处理的字符串
     * @param {String} repstr 被替换的字符
     * @param {String} newstr 替换后的字符
     **/
    replaceAll(text, repstr, newstr) {
      return text.replace(new RegExp(repstr, "gm"), newstr);
    },
    /**
     * @desc 格式化手机号码
     * @param {String} num 手机号码
     **/
    numberFormatter(num) {
      return num.length === 11 ? num.replace(/^(\d{3})\d{4}(\d{4})$/, "$1****$2") : num;
    },
    /**
     * @desc 金额格式化，保留两位小数
     * @param {String | Number} money 金额值
     **/
    moneyFormatter(money) {
      return parseFloat(money).toFixed(2).toString().split("").reverse().join("").replace(/(\d{3})/g, "$1,").replace(
        /\,$/,
        ""
      ).split("").reverse().join("");
    },
    /**
     * @desc 日期时间格式化
     * @param date 需要格式化的日期
     * @param format 格式化字符串(y-m-d h:i:s)
     * @param type  date的格式类型：1-日期字符串(2017/12/04 12:12:12) 2-时间戳(1603676514690) 3-日期字符串，无连接符(20171204121212)
     * 4-new Date()时间格式(Thu Oct 01 2020 00:00:00 GMT+0800 (中国标准时间))
     * @param isMs  时间戳精度是否为毫秒，默认为true（当精度为秒时传false），type=2时有效
     **/
    dateFormatter(date, format, type = 1, isMs = true) {
      let formatDate = "";
      if (type === 3) {
        formatDate = utils._formatTimeStr(date, format);
      } else {
        formatDate = utils._formatDate(format, date, type, isMs);
      }
      return formatDate;
    },
    _formatDate(formatStr, fdate, type = 1, isMs) {
      if (!fdate)
        return "";
      let fTime, fStr = "ymdhis";
      if (type === 4) {
        fTime = fdate;
      } else {
        fdate = fdate.toString();
        if (~fdate.indexOf(".")) {
          fdate = fdate.substring(0, fdate.indexOf("."));
        }
        fdate = fdate.replace("T", " ").replace(/\-/g, "/");
        if (!formatStr)
          formatStr = "y-m-d h:i:s";
        if (fdate) {
          if (type === 2) {
            fdate = isMs ? Number(fdate) : Number(fdate) * 1e3;
          }
          fTime = new Date(fdate);
        } else {
          fTime = /* @__PURE__ */ new Date();
        }
      }
      let month = fTime.getMonth() + 1;
      let day = fTime.getDate();
      let hours = fTime.getHours();
      let minu = fTime.getMinutes();
      let second = fTime.getSeconds();
      month = month < 10 ? "0" + month : month;
      day = day < 10 ? "0" + day : day;
      hours = hours < 10 ? "0" + hours : hours;
      minu = minu < 10 ? "0" + minu : minu;
      second = second < 10 ? "0" + second : second;
      let formatArr = [
        fTime.getFullYear().toString(),
        month.toString(),
        day.toString(),
        hours.toString(),
        minu.toString(),
        second.toString()
      ];
      for (let i = 0; i < formatArr.length; i++) {
        formatStr = formatStr.replace(fStr.charAt(i), formatArr[i]);
      }
      return formatStr;
    },
    /**
     * @desc 格式化时间
     * @param timeStr 时间字符串 20191212162001
     * @param formatStr 需要的格式 如 y-m-d h:i:s | y/m/d h:i:s | y/m/d | y年m月d日 等
     **/
    _formatTimeStr(timeStr, formatStr) {
      if (!timeStr)
        return;
      timeStr = timeStr.toString();
      if (timeStr.length === 14) {
        let timeArr = timeStr.split("");
        let fStr = "ymdhis";
        if (!formatStr) {
          formatStr = "y-m-d h:i:s";
        }
        let formatArr = [
          [...timeArr].splice(0, 4).join(""),
          [...timeArr].splice(4, 2).join(""),
          [...timeArr].splice(6, 2).join(""),
          [...timeArr].splice(8, 2).join(""),
          [...timeArr].splice(10, 2).join(""),
          [...timeArr].splice(12, 2).join("")
        ];
        for (let i = 0; i < formatArr.length; i++) {
          formatStr = formatStr.replace(fStr.charAt(i), formatArr[i]);
        }
        return formatStr;
      }
      return timeStr;
    },
    /**
     * @desc RGB颜色转十六进制颜色
     * @param r
     * @param g
     * @param b
     **/
    rgbToHex(r, g, b) {
      return "#" + utils._toHex(r) + utils._toHex(g) + utils._toHex(b);
    },
    _toHex(n) {
      n = parseInt(n, 10);
      if (isNaN(n))
        return "00";
      n = Math.max(0, Math.min(n, 255));
      return "0123456789ABCDEF".charAt((n - n % 16) / 16) + "0123456789ABCDEF".charAt(n % 16);
    },
    /**
     * @desc 十六进制颜色转RGB颜色
     * @param hex 颜色值 #333 或 #333333
     **/
    hexToRGB(hex) {
      if (hex.length === 4) {
        let text = hex.substring(1, 4);
        hex = "#" + text + text;
      }
      let result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
      return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
      } : null;
    },
    /**
     * @desc 唯一标识，随机数
     * @param n 随机数位数
     **/
    unique(n) {
      n = n || 6;
      let rnd = "";
      for (let i = 0; i < n; i++)
        rnd += Math.floor(Math.random() * 10);
      return "firstui_" + (/* @__PURE__ */ new Date()).getTime() + rnd;
    },
    /**
     * @desc 获取uuid
     */
    getUUID() {
      return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, (c) => {
        return (c === "x" ? Math.random() * 16 | 0 : "r&0x3" | "0x8").toString(16);
      });
    },
    /**
     * @desc 简单数组合并去重
     * @param arr1 数组1
     * @param arr2 数组2 可不传
     **/
    distinctArray(arr1, arr2) {
      arr1 = arr1 || [];
      arr2 = arr2 || [];
      return [.../* @__PURE__ */ new Set([...arr1, ...arr2])];
    },
    /**
     * @desc 获取日期时间段
     * @param type 1-今天 2-昨天 3-本周 4-本月 5-本年
     **/
    getDateTimeSlot(type) {
      let now = /* @__PURE__ */ new Date();
      let start = now.toDateString();
      let end = now.toDateString();
      switch (type) {
        case 1:
          start = `${start} 00:00:00`;
          end = `${end} 23:59:59`;
          break;
        case 2:
          now.setTime(now.getTime() - 3600 * 1e3 * 24 * 1);
          start = `${now.toDateString()} 00:00:00`;
          end = `${now.toDateString()} 23:59:59`;
          break;
        case 3:
          let weekday = now.getDay() || 7;
          now.setDate(now.getDate() - weekday + 1);
          start = `${now.toDateString()} 00:00:00`;
          end = `${end} 23:59:59`;
          break;
        case 4:
          start = `${now.getFullYear()}-${now.getMonth() + 1}-01 00:00:00`;
          end = `${end} 23:59:59`;
          break;
        case 5:
          start = `${now.getFullYear()}-01-01 00:00:00`;
          end = `${end} 23:59:59`;
          break;
      }
      return {
        start: new Date(start.replace(/\-/g, "/")),
        end: new Date(end.replace(/\-/g, "/"))
      };
    },
    /*
     * @desc 获取Url参数，返回一个对象
     * @param url url地址
     * ?a=1&b=2 ==> {a: "1", b: "2"}
     */
    getUrlParam(url) {
      let arrObj = url.split("?");
      let params = {};
      if (arrObj.length > 1) {
        arrObj = arrObj[1].split("&");
        arrObj.forEach((item) => {
          item = item.split("=");
          params[item[0]] = item[1];
        });
      }
      return params;
    },
    /**
     * @method 函数防抖
     * @desc 短时间内多次触发同一事件，只执行最后一次，或者只执行最开始的一次，中间的不执行。
     * @param func 目标函数
     * @param wait 延迟执行毫秒数
     * @param immediate true - 立即执行， false - 延迟执行
     */
    debounce(func, wait = 1e3, immediate = true) {
      let timer;
      return function() {
        let context = this, args = arguments;
        if (timer)
          clearTimeout(timer);
        if (immediate) {
          let callNow = !timer;
          timer = setTimeout(() => {
            timer = null;
          }, wait);
          if (callNow)
            func.apply(context, args);
        } else {
          timer = setTimeout(() => {
            func.apply(context, args);
          }, wait);
        }
      };
    },
    /**
     * @method 函数节流
     * @desc 指连续触发事件，但是在 n 秒内只执行一次函数。即 2n 秒内执行 2 次... 。会稀释函数的执行频率。
     * @param func 函数
     * @param wait 延迟执行毫秒数
     * @param type 1 在时间段开始的时候触发 2 在时间段结束的时候触发
     */
    throttle(func, wait = 1e3, type = 1) {
      let previous = 0;
      let timeout;
      return function() {
        let context = this;
        let args = arguments;
        if (type === 1) {
          let now = Date.now();
          if (now - previous > wait) {
            func.apply(context, args);
            previous = now;
          }
        } else if (type === 2) {
          if (!timeout) {
            timeout = setTimeout(() => {
              timeout = null;
              func.apply(context, args);
            }, wait);
          }
        }
      };
    },
    /**
     * @desc 日期时间格式化为多久之前 如:1分钟前
     * @param date 需要格式化的日期
     * @param type  date的格式类型：1-日期字符串(2017/12/04 12:12:12) 2-时间戳(1603676514690) 3-日期字符串，无连接符(20171204121212)
     * 4-new Date()时间格式(Thu Oct 01 2020 00:00:00 GMT+0800 (中国标准时间))
     * @param isMs  时间戳精度是否为毫秒，默认为true（当精度为秒时传false），type=2时有效
     * @param suffix 后缀，如：30小时+ 后缀。[刚刚、昨天、前天 等为固定文本，后缀无效]
     * @param endUnit 转化截止单位，1-秒 2-分钟 3-小时 4-天 5-月 6-年，如传3（小时），则天，月，年不做转化直接返回空
     * @param seconds 多少秒之前显示为刚刚，不可超过60
     * @param fixedDay 是否需要天的固定文本，如昨天、前天
     **/
    formatTimeAgo(date, type = 1, isMs = true, suffix = "\u524D", endUnit = 6, seconds = 10, fixedDay = true) {
      const formatDate = utils.dateFormatter(date, "y/m/d h:i:s", type, isMs);
      const beforeStamp = new Date(formatDate).getTime();
      const nowStamp = (/* @__PURE__ */ new Date()).getTime();
      let res = "";
      const diff = nowStamp - beforeStamp;
      if (diff > 0) {
        const _minute = 1e3 * 60;
        const _hour = _minute * 60;
        const _day = _hour * 24;
        const _month = _day * 30;
        const _year = _month * 12;
        const year = Math.floor(diff / _year);
        const month = Math.floor(diff / _month);
        const day = Math.floor(diff / _day);
        const hour = Math.floor(diff / _hour);
        const minute = Math.floor(diff / _minute);
        const second = Math.floor(diff / 1e3);
        let isEmpty = false;
        switch (endUnit) {
          case 1:
            isEmpty = minute || hour || day || month || year ? true : false;
            break;
          case 2:
            isEmpty = hour || day || month || year ? true : false;
            break;
          case 3:
            isEmpty = day || month || year ? true : false;
            break;
          case 4:
            isEmpty = month || year ? true : false;
            break;
          case 5:
            isEmpty = year ? true : false;
            break;
        }
        if (!isEmpty) {
          if (year) {
            res = `${year}\u5E74${suffix}`;
          } else if (month) {
            res = `${month}\u4E2A\u6708${suffix}`;
          } else if (day) {
            if (day === 1 && fixedDay) {
              res = "\u6628\u5929";
            } else if (day === 2 && fixedDay) {
              res = "\u524D\u5929";
            } else {
              res = `${day}\u5929${suffix}`;
            }
          } else if (hour) {
            res = `${hour}\u5C0F\u65F6${suffix}`;
          } else if (minute) {
            res = `${minute}\u5206\u949F${suffix}`;
          } else {
            seconds = seconds < 60 ? seconds : 59;
            res = second < seconds ? "\u521A\u521A" : `${second}\u79D2${suffix}`;
          }
        }
      }
      return res;
    }
  };
  var utils$1 = {
    titleCase: utils.titleCase,
    compressLetter: utils.compressLetter,
    sleep: utils.sleep,
    trim: utils.trim,
    trimAll: utils.trimAll,
    replaceAll: utils.replaceAll,
    numberFormatter: utils.numberFormatter,
    moneyFormatter: utils.moneyFormatter,
    dateFormatter: utils.dateFormatter,
    rgbToHex: utils.rgbToHex,
    hexToRGB: utils.hexToRGB,
    unique: utils.unique,
    distinctArray: utils.distinctArray,
    getDateTimeSlot: utils.getDateTimeSlot,
    getUrlParam: utils.getUrlParam,
    getUUID: utils.getUUID,
    debounce: utils.debounce,
    throttle: utils.throttle,
    formatTimeAgo: utils.formatTimeAgo
  };
  var _style_02 = { "fui-wrap": { "": { "paddingTop": 0, "paddingRight": "48rpx", "paddingBottom": 0, "paddingLeft": "48rpx" } }, "fui-mobile__box": { "": { "justifyContent": "center", "alignItems": "center", "flexDirection": "row", "paddingTop": "128rpx" } }, "fui-mobile__text": { "": { "paddingLeft": "16rpx" } }, "fui-form__box": { "": { "paddingTop": "96rpx" } }, "fui-btn__box": { "": { "paddingTop": "128rpx" } }, "fui-countdown__box": { "": { "flexDirection": "row", "justifyContent": "center", "alignItems": "center", "paddingTop": "96rpx" } }, "fui-countdown__text": { "": { "fontSize": "26rpx", "paddingRight": "16rpx" } } };
  var _sfc_main2 = {
    computed: (0, import_vuex.mapState)(["areaCode"]),
    data() {
      return {
        time: 60,
        code: "",
        type: 1,
        mobile: "",
        disabled: true
      };
    },
    onLoad(e) {
      formatAppLog("log", "at pages/captcha/captcha.nvue:48", "this.type", e.mobile);
      this.type = e.type || 1;
      this.mobile = e.mobile || null;
      if (!this.mobile) {
        uni.navigateBack(-1);
        return;
      }
      this.sendCode();
    },
    methods: {
      sendCode() {
        this.time = 60;
        this.disabled = true;
        uni.$cloud.get("login/code", {
          mobile: this.mobile
        }).then((data) => {
          formatAppLog("log", "at pages/captcha/captcha.nvue:64", "\u6267\u884C\u7ED3\u679C", data);
        }).catch((error) => {
        });
      },
      end() {
        this.disabled = false;
      },
      getMobile() {
        return utils$1.numberFormatter(this.mobile);
      },
      onComplete(e) {
        this.code = e.detail.value;
        this.onClick();
      },
      onClick() {
        if (this.type == 1) {
          uni.$cloud.login("login/index", {
            mobile: this.mobile,
            code: this.code
          }).then((data) => {
            uni.fui.toast("\u767B\u5F55\u6210\u529F");
            setTimeout(() => {
              uni.switchTab({
                url: "/pages/index/index"
              });
            }, 1e3);
          }).catch((error) => {
            formatAppLog("log", "at pages/captcha/captcha.nvue:93", "data\u767B\u5F55\u9519\u8BEF", error);
          });
        } else {
          uni.$cloud.post("login/signup", {
            mobile: this.mobile,
            code: this.code
          }).then((data) => {
            uni.fui.toast("\u6CE8\u518C\u6210\u529F");
            setTimeout(() => {
              uni.reLaunch({
                url: "/pages/login/login"
              });
            }, 1e3);
          }).catch((error) => {
            formatAppLog("log", "at pages/captcha/captcha.nvue:108", "data\u6CE8\u518C\u9519\u8BEF", error);
          });
        }
      }
    }
  };
  function _sfc_render2(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_fui_single_input = resolveEasycom((0, import_vue3.resolveDynamicComponent)("fui-single-input"), __easycom_0);
    const _component_fui_count_down = resolveEasycom((0, import_vue3.resolveDynamicComponent)("fui-count-down"), __easycom_1);
    const _component_fui_button = resolveEasycom((0, import_vue3.resolveDynamicComponent)("fui-button"), __easycom_6);
    return (0, import_vue3.openBlock)(), (0, import_vue3.createElementBlock)("scroll-view", {
      scrollY: true,
      showScrollbar: true,
      enableBackToTop: true,
      bubble: "true",
      style: { flexDirection: "column" }
    }, [
      (0, import_vue3.createElementVNode)("view", { class: "fui-wrap" }, [
        (0, import_vue3.createElementVNode)("view", { class: "fui-mobile__box" }, [
          (0, import_vue3.createElementVNode)("u-text", { class: "fui-size" }, "\u5DF2\u53D1\u9001\u9A8C\u8BC1\u7801\u81F3"),
          (0, import_vue3.createElementVNode)(
            "u-text",
            { class: "fui-mobile__text fui-size" },
            "+86 " + (0, import_vue3.toDisplayString)($options.getMobile()),
            1
            /* TEXT */
          )
        ]),
        (0, import_vue3.createElementVNode)("view", { class: "fui-form__box" }, [
          (0, import_vue3.createVNode)(_component_fui_single_input, {
            border: "1",
            borderWidth: "2",
            radius: "16",
            activeColor: "#246BFD",
            cursorHeight: "40",
            cursorColor: "#246BFD",
            background: "#FAFAFA",
            length: 4,
            width: "120",
            height: "120",
            padding: "0",
            isFocus: "",
            onComplete: $options.onComplete
          }, null, 8, ["onComplete"]),
          (0, import_vue3.createElementVNode)("view", { class: "fui-countdown__box" }, [
            $data.disabled ? ((0, import_vue3.openBlock)(), (0, import_vue3.createElementBlock)("view", {
              key: 0,
              class: "fui-countdown__text"
            }, [
              (0, import_vue3.createVNode)(_component_fui_count_down, {
                isHours: false,
                isMinutes: false,
                isColon: false,
                unitEn: "",
                borderColor: "transparent",
                background: "transparent",
                value: $data.time,
                onEnd: $options.end
              }, null, 8, ["value", "onEnd"])
            ])) : (0, import_vue3.createCommentVNode)("v-if", true),
            (0, import_vue3.createVNode)(_component_fui_button, {
              type: "link",
              btnSize: "small",
              disabled: $data.disabled,
              onClick: $options.sendCode
            }, {
              default: (0, import_vue3.withCtx)(() => [
                (0, import_vue3.createElementVNode)("u-text", null, "\u91CD\u65B0\u53D1\u9001")
              ]),
              _: 1
              /* STABLE */
            }, 8, ["disabled", "onClick"])
          ]),
          (0, import_vue3.createElementVNode)("view", { class: "fui-btn__box" }, [
            (0, import_vue3.createVNode)(_component_fui_button, {
              text: "\u786E\u5B9A",
              radius: "96rpx",
              bold: "",
              onClick: $options.onClick
            }, null, 8, ["onClick"])
          ])
        ])
      ])
    ]);
  }
  var captcha = /* @__PURE__ */ _export_sfc(_sfc_main2, [["render", _sfc_render2], ["styles", [_style_02]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/pages/captcha/captcha.nvue"]]);

  // <stdin>
  var webview = plus.webview.currentWebview();
  if (webview) {
    const __pageId = parseInt(webview.id);
    const __pagePath = "pages/captcha/captcha";
    let __pageQuery = {};
    try {
      __pageQuery = JSON.parse(webview.__query__);
    } catch (e) {
    }
    captcha.mpType = "page";
    const app = Vue.createPageApp(captcha, { $store: getApp({ allowDefault: true }).$store, __pageId, __pagePath, __pageQuery });
    app.provide("__globalStyles", Vue.useCssStyles([...__uniConfig.styles, ...captcha.styles || []]));
    app.mount("#root");
  }
})();
/*!
 * 工具类：常用数据处理工具
 * Utils - v1.0.0 (2021/7/9, 11:07:14 AM)
 * https://github.com/FirstUI/FirstUI | Released under Apache License 2.0
 *
 * 官网地址：https://firstui.cn/
 * 文档地址：https://doc.firstui.cn/
 */
