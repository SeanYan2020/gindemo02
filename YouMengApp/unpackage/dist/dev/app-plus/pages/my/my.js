"use weex:vue";

if (typeof Promise !== 'undefined' && !Promise.prototype.finally) {
  Promise.prototype.finally = function(callback) {
    const promise = this.constructor
    return this.then(
      value => promise.resolve(callback()).then(() => value),
      reason => promise.resolve(callback()).then(() => {
        throw reason
      })
    )
  }
};

if (typeof uni !== 'undefined' && uni && uni.requireGlobal) {
  const global = uni.requireGlobal()
  ArrayBuffer = global.ArrayBuffer
  Int8Array = global.Int8Array
  Uint8Array = global.Uint8Array
  Uint8ClampedArray = global.Uint8ClampedArray
  Int16Array = global.Int16Array
  Uint16Array = global.Uint16Array
  Int32Array = global.Int32Array
  Uint32Array = global.Uint32Array
  Float32Array = global.Float32Array
  Float64Array = global.Float64Array
  BigInt64Array = global.BigInt64Array
  BigUint64Array = global.BigUint64Array
};


(() => {
  var __create = Object.create;
  var __defProp = Object.defineProperty;
  var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
  var __getOwnPropNames = Object.getOwnPropertyNames;
  var __getOwnPropSymbols = Object.getOwnPropertySymbols;
  var __getProtoOf = Object.getPrototypeOf;
  var __hasOwnProp = Object.prototype.hasOwnProperty;
  var __propIsEnum = Object.prototype.propertyIsEnumerable;
  var __defNormalProp = (obj, key, value) => key in obj ? __defProp(obj, key, { enumerable: true, configurable: true, writable: true, value }) : obj[key] = value;
  var __spreadValues = (a, b) => {
    for (var prop in b || (b = {}))
      if (__hasOwnProp.call(b, prop))
        __defNormalProp(a, prop, b[prop]);
    if (__getOwnPropSymbols)
      for (var prop of __getOwnPropSymbols(b)) {
        if (__propIsEnum.call(b, prop))
          __defNormalProp(a, prop, b[prop]);
      }
    return a;
  };
  var __commonJS = (cb, mod) => function __require() {
    return mod || (0, cb[__getOwnPropNames(cb)[0]])((mod = { exports: {} }).exports, mod), mod.exports;
  };
  var __copyProps = (to, from, except, desc) => {
    if (from && typeof from === "object" || typeof from === "function") {
      for (let key of __getOwnPropNames(from))
        if (!__hasOwnProp.call(to, key) && key !== except)
          __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
    }
    return to;
  };
  var __toESM = (mod, isNodeMode, target) => (target = mod != null ? __create(__getProtoOf(mod)) : {}, __copyProps(
    // If the importer is in node compatibility mode or this is not an ESM
    // file that has been converted to a CommonJS file using a Babel-
    // compatible transform (i.e. "__esModule" has not been set), then set
    // "default" to the CommonJS "module.exports" for node compatibility.
    isNodeMode || !mod || !mod.__esModule ? __defProp(target, "default", { value: mod, enumerable: true }) : target,
    mod
  ));

  // vue-ns:vue
  var require_vue = __commonJS({
    "vue-ns:vue"(exports, module) {
      module.exports = Vue;
    }
  });

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/uni-app.es.js
  var import_vue = __toESM(require_vue());
  var isString = (val) => typeof val === "string";
  function requireNativePlugin(name) {
    return weex.requireModule(name);
  }
  function formatAppLog(type, filename, ...args) {
    if (uni.__log__) {
      uni.__log__(type, filename, ...args);
    } else {
      console[type].apply(console, [...args, filename]);
    }
  }
  function resolveEasycom(component, easycom) {
    return isString(component) ? easycom : component;
  }

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/fui-avatar.js
  var import_vue2 = __toESM(require_vue());

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/_plugin-vue_export-helper.js
  var _export_sfc = (sfc, props) => {
    const target = sfc.__vccOpts || sfc;
    for (const [key, val] of props) {
      target[key] = val;
    }
    return target;
  };

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/fui-avatar.js
  var _style_0 = { "fui-avatar__wrap": { "": { "position": "relative", "flexDirection": "row", "alignItems": "center", "justifyContent": "center" } }, "fui-avatar__img": { "": { "flex": 1 } }, "fui-avatar__text": { "": { "flex": 1, "lines": 1, "overflow": "hidden", "textOverflow": "ellipsis", "textAlign": "center" } }, "fui-avatar__size-small": { "": { "!width": "64rpx", "!height": "64rpx" } }, "fui-avatar__text-small": { "": { "!fontSize": "32rpx" } }, "fui-avatar__size-middle": { "": { "!width": "96rpx", "!height": "96rpx" } }, "fui-avatar__text-middle": { "": { "!fontSize": "44rpx" } }, "fui-avatar__size-large": { "": { "!width": "128rpx", "!height": "128rpx" } }, "fui-avatar__text-large": { "": { "!fontSize": "56rpx" } }, "fui-avatar__circle": { "": { "!borderRadius": 500 } }, "fui-avatar__square": { "": { "!borderRadius": "8rpx" } } };
  var _sfc_main = {
    name: "fui-avatar",
    emits: ["click", "error"],
    props: {
      src: {
        type: String,
        default: ""
      },
      errorSrc: {
        type: String,
        default: ""
      },
      mode: {
        type: String,
        default: "scaleToFill"
      },
      //微信小程序、百度小程序、字节跳动小程序
      //图片懒加载。只针对page与scroll-view下的image有效
      lazyLoad: {
        type: Boolean,
        default: true
      },
      //默认不解析 webP 格式，只支持网络资源 微信小程序2.9.0
      webp: {
        type: Boolean,
        default: false
      },
      background: {
        type: String,
        default: "#D1D1D1"
      },
      //small（64）、middle（96）、large（128）
      size: {
        type: String,
        default: "middle"
      },
      //图片宽度，设置大于0的数值生效，默认使用size
      width: {
        type: [Number, String],
        default: 0
      },
      //默认等宽，设置图大于0的数值且设置了图片宽度生效
      height: {
        type: [Number, String],
        default: 0
      },
      //指定头像的形状，可选值为 circle、square
      shape: {
        type: String,
        default: "circle"
      },
      //图片圆角值，默认使用shape，当设置大于等于0的数值，shape失效
      radius: {
        type: [Number, String],
        default: -1
      },
      //没有src时可以使用文本代替
      text: {
        type: String,
        default: ""
      },
      color: {
        type: String,
        default: "#fff"
      },
      //默认使用size下字体大小
      fontSize: {
        type: [Number, String],
        default: 0
      },
      fontWeight: {
        type: [Number, String],
        default: 600
      },
      marginRight: {
        type: [Number, String],
        default: 0
      },
      marginBottom: {
        type: [Number, String],
        default: 0
      },
      //在列表中的索引值
      index: {
        type: Number,
        default: 0
      },
      //其他参数
      params: {
        type: [Number, String],
        default: 0
      }
    },
    computed: {
      styles() {
        let styles = "";
        if (this.width) {
          styles = `width:${this.width}rpx;height:${this.height || this.width}rpx;`;
        }
        if (this.radius !== -1) {
          styles += `border-radius:${this.radius}rpx;`;
        }
        return styles;
      },
      wrapStyles() {
        return `background:${this.background};margin-right:${this.marginRight}rpx;margin-bottom:${this.marginBottom}rpx;${this.styles};`;
      },
      textStyles() {
        let styles = `color:${this.color};font-weight:${this.fontWeight};`;
        if (this.fontSize) {
          styles += `font-size:${this.fontSize}rpx;`;
        }
        return styles;
      }
    },
    watch: {
      src(val) {
        this.src && (this.showImg = this.src);
      }
    },
    data() {
      return {
        showImg: ""
      };
    },
    created() {
      this.src && (this.showImg = this.src);
    },
    methods: {
      handleError(e) {
        if (this.src) {
          this.errorSrc && (this.showImg = this.errorSrc);
          this.$emit("error", {
            index: this.index,
            params: this.params
          });
        }
      },
      handleClick() {
        this.$emit("click", {
          index: this.index,
          params: this.params
        });
      }
    }
  };
  function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
    return (0, import_vue2.openBlock)(), (0, import_vue2.createElementBlock)(
      import_vue2.Fragment,
      null,
      [
        (0, import_vue2.createCommentVNode)("\u672C\u6587\u4EF6\u7531FirstUI\u6388\u6743\u4E88\u4E25\u5929\u8D60\uFF08\u4F1A\u5458ID\uFF1A244   2\uFF0C\u8EAB\u4EFD\u8BC1\u5C3E\u53F7\uFF1A04   3  018\uFF09\u4E13\u7528\uFF0C\u8BF7\u5C0A\u91CD\u77E5\u8BC6\u4EA7\u6743\uFF0C\u52FF\u79C1\u4E0B\u4F20\u64AD\uFF0C\u8FDD\u8005\u8FFD\u7A76\u6CD5\u5F8B\u8D23\u4EFB\u3002"),
        (0, import_vue2.createElementVNode)(
          "view",
          {
            class: (0, import_vue2.normalizeClass)(["fui-avatar__wrap", [$props.width ? "" : "fui-avatar__size-" + $props.size, $props.radius === -1 ? "fui-avatar__" + $props.shape : ""]]),
            style: (0, import_vue2.normalizeStyle)($options.wrapStyles),
            onClick: _cache[1] || (_cache[1] = (...args) => $options.handleClick && $options.handleClick(...args))
          },
          [
            $props.src && $props.src !== true ? ((0, import_vue2.openBlock)(), (0, import_vue2.createElementBlock)("u-image", {
              key: 0,
              class: (0, import_vue2.normalizeClass)(["fui-avatar__img", [$props.radius === -1 ? "fui-avatar__" + $props.shape : "", $props.width ? "" : "fui-avatar__size-" + $props.size]]),
              style: (0, import_vue2.normalizeStyle)($options.styles),
              src: $data.showImg,
              mode: $props.mode,
              webp: $props.webp,
              lazyLoad: $props.lazyLoad,
              onError: _cache[0] || (_cache[0] = (...args) => $options.handleError && $options.handleError(...args))
            }, null, 46, ["src", "mode", "webp", "lazyLoad"])) : (0, import_vue2.createCommentVNode)("v-if", true),
            !$props.src && $props.src !== true && $props.text ? ((0, import_vue2.openBlock)(), (0, import_vue2.createElementBlock)(
              "u-text",
              {
                key: 1,
                class: (0, import_vue2.normalizeClass)(["fui-avatar__text", [$props.width ? "" : "fui-avatar__text-" + $props.size]]),
                style: (0, import_vue2.normalizeStyle)($options.textStyles)
              },
              (0, import_vue2.toDisplayString)($props.text),
              7
              /* TEXT, CLASS, STYLE */
            )) : (0, import_vue2.createCommentVNode)("v-if", true),
            (0, import_vue2.renderSlot)(_ctx.$slots, "default")
          ],
          6
          /* CLASS, STYLE */
        )
      ],
      2112
      /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
    );
  }
  var __easycom_0 = /* @__PURE__ */ _export_sfc(_sfc_main, [["render", _sfc_render], ["styles", [_style_0]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-avatar/fui-avatar.vue"]]);

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/pages/my/my.js
  var import_vue4 = __toESM(require_vue());

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/fui-list-cell.js
  var import_vue3 = __toESM(require_vue());
  var _style_02 = { "fui-list__cell": { "": { "position": "relative", "flex": 1, "flexDirection": "row", "alignItems": "center", "justifyContent": "space-between" } }, "fui-list__item": { "": { "flex": 1 } }, "fui-cell__arrow": { "": { "height": "40rpx", "width": "40rpx", "borderTopWidth": 3, "borderRightWidth": 3, "borderBottomWidth": 0, "borderLeftWidth": 0, "borderStyle": "solid", "transform": "rotate(45deg) scale(0.5)", "borderTopRightRadius": "3rpx", "transformOrigin": "center center", "marginRight": "-5.8579rpx" } }, "fui-cell__border-top": { "": { "position": "absolute", "top": 0, "height": 0.5, "zIndex": -1 } }, "fui-cell__border-bottom": { "": { "position": "absolute", "bottom": 0, "height": 0.5, "zIndex": -1 } }, "fui-highlight": { "": { "!backgroundColor:active": "rgba(0,0,0,0.2)" } } };
  var _sfc_main2 = {
    name: "fui-list-cell",
    emits: ["click"],
    props: {
      //padding值，上、右、下、左,nvue下padding-right(右)无效
      padding: {
        type: Array,
        default() {
          return [];
        }
      },
      //margin-top 单位rpx
      marginTop: {
        type: [Number, String],
        default: 0
      },
      //margin-bottom 单位rpx
      marginBottom: {
        type: [Number, String],
        default: 0
      },
      //背景颜色
      background: {
        type: String,
        default: "#fff"
      },
      //是否有点击效果
      highlight: {
        type: Boolean,
        default: true
      },
      //是否需要右侧箭头
      arrow: {
        type: Boolean,
        default: false
      },
      arrowColor: {
        type: String,
        default: ""
      },
      //是否显示上边框
      topBorder: {
        type: Boolean,
        default: false
      },
      //是否显示下边框
      bottomBorder: {
        type: Boolean,
        default: true
      },
      //边框颜色，非nvue下传值则全局默认样式失效
      borderColor: {
        type: String,
        default: ""
      },
      //上边框left值，单位rpx
      topLeft: {
        type: [Number, String],
        default: 0
      },
      //上边框right值，单位rpx
      topRight: {
        type: [Number, String],
        default: 0
      },
      //下边框left值，单位rpx
      bottomLeft: {
        type: [Number, String],
        default: -1
      },
      //下边框right值，单位rpx
      bottomRight: {
        type: [Number, String],
        default: 0
      },
      //border-radius圆角值
      radius: {
        type: String,
        default: "0"
      },
      index: {
        type: Number,
        default: 0
      }
    },
    computed: {
      getPadding() {
        let padding = this.padding;
        if (Array.isArray(padding) && padding.length === 0) {
          const app = uni && uni.$fui && uni.$fui.fuiListCell;
          padding = app && app.padding;
          if (!padding || Array.isArray(padding) && padding.length === 0) {
            padding = ["32rpx", "32rpx"];
          }
        }
        return padding;
      },
      getArrowColor() {
        const app = uni && uni.$fui && uni.$fui.fuiListCell;
        return this.arrowColor || app && app.arrowColor || "#B2B2B2";
      },
      getBorderColor() {
        let color = this.borderColor;
        if (!color || color === true) {
          const app = uni && uni.$fui && uni.$fui.fuiListCell;
          color = app && app.borderColor || "#EEEEEE";
        }
        return color;
      },
      getBottomLeft() {
        const app = uni && uni.$fui && uni.$fui.fuiListCell;
        let left = this.bottomLeft;
        const c_left = app && app.bottomLeft;
        if (left === -1) {
          left = c_left === void 0 || c_left === null ? 32 : c_left;
        }
        return left;
      }
    },
    methods: {
      handleClick() {
        this.$emit("click", {
          index: this.index
        });
      }
    }
  };
  function _sfc_render2(_ctx, _cache, $props, $setup, $data, $options) {
    return (0, import_vue3.openBlock)(), (0, import_vue3.createElementBlock)(
      import_vue3.Fragment,
      null,
      [
        (0, import_vue3.createCommentVNode)("\u672C\u6587\u4EF6\u7531FirstUI\u6388\u6743\u4E88\u4E25\u5929\u8D60\uFF08\u4F1A\u5458ID\uFF1A2 4 4 2\uFF0C\u8EAB\u4EFD\u8BC1\u5C3E\u53F7\uFF1A043 0   1 8\uFF09\u4E13\u7528\uFF0C\u8BF7\u5C0A\u91CD\u77E5\u8BC6\u4EA7\u6743\uFF0C\u52FF\u79C1\u4E0B\u4F20\u64AD\uFF0C\u8FDD\u8005\u8FFD\u7A76\u6CD5\u5F8B\u8D23\u4EFB\u3002"),
        (0, import_vue3.createElementVNode)(
          "view",
          {
            class: (0, import_vue3.normalizeClass)(["fui-list__cell", { "fui-highlight": $props.highlight, "fui-list__cell-background": !$props.background }]),
            style: (0, import_vue3.normalizeStyle)({ paddingTop: $options.getPadding[0] || 0, paddingRight: $options.getPadding[1] || 0, paddingBottom: $options.getPadding[2] || $options.getPadding[0] || 0, paddingLeft: $options.getPadding[3] || $options.getPadding[1] || 0, background: $props.background, marginTop: $props.marginTop + "rpx", marginBottom: $props.marginBottom + "rpx", borderRadius: $props.radius }),
            onClick: _cache[0] || (_cache[0] = (...args) => $options.handleClick && $options.handleClick(...args))
          },
          [
            $props.topBorder ? ((0, import_vue3.openBlock)(), (0, import_vue3.createElementBlock)(
              "view",
              {
                key: 0,
                style: (0, import_vue3.normalizeStyle)({ background: $options.getBorderColor, left: $props.topLeft + "rpx", right: $props.topRight + "rpx" }),
                class: (0, import_vue3.normalizeClass)(["fui-cell__border-top", { "fui-cell__border-color": !$options.getBorderColor }])
              },
              null,
              6
              /* CLASS, STYLE */
            )) : (0, import_vue3.createCommentVNode)("v-if", true),
            (0, import_vue3.renderSlot)(_ctx.$slots, "default"),
            $props.bottomBorder ? ((0, import_vue3.openBlock)(), (0, import_vue3.createElementBlock)(
              "view",
              {
                key: 1,
                style: (0, import_vue3.normalizeStyle)({ background: $options.getBorderColor, left: $options.getBottomLeft + "rpx", right: $props.bottomRight + "rpx" }),
                class: (0, import_vue3.normalizeClass)(["fui-cell__border-bottom", { "fui-cell__border-color": !$options.getBorderColor }])
              },
              null,
              6
              /* CLASS, STYLE */
            )) : (0, import_vue3.createCommentVNode)("v-if", true),
            $props.arrow ? ((0, import_vue3.openBlock)(), (0, import_vue3.createElementBlock)(
              "view",
              {
                key: 2,
                class: "fui-cell__arrow",
                style: (0, import_vue3.normalizeStyle)({ "border-color": $options.getArrowColor })
              },
              null,
              4
              /* STYLE */
            )) : (0, import_vue3.createCommentVNode)("v-if", true)
          ],
          6
          /* CLASS, STYLE */
        )
      ],
      2112
      /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
    );
  }
  var __easycom_3 = /* @__PURE__ */ _export_sfc(_sfc_main2, [["render", _sfc_render2], ["styles", [_style_02]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-list-cell/fui-list-cell.vue"]]);

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/pages/my/my.js
  var _style_0$1 = { "fui-actionsheet__wrap": { "": { "position": "fixed", "left": 0, "right": 0, "bottom": 0, "transform": "translateY(100%)", "transformOrigin": "center center" } }, "fui-as__bg-light": { "": { "backgroundColor": "#F8F8F8" } }, "fui-as__bg-dark": { "": { "backgroundColor": "#111111" } }, "fui-actionsheet__radius": { "": { "borderTopLeftRadius": "24rpx", "borderTopRightRadius": "24rpx" } }, "fui-actionsheet__tips": { "": { "flex": 1, "paddingTop": "40rpx", "paddingRight": "60rpx", "paddingBottom": "40rpx", "paddingLeft": "60rpx", "textAlign": "center", "alignItems": "center", "justifyContent": "center", "fontWeight": "normal" } }, "fui-as__btn-light": { "": { "backgroundColor": "#FFFFFF" } }, "fui-as__btn-dark": { "": { "backgroundColor": "#222222" } }, "fui-actionsheet__operate-box": { "": { "paddingBottom": "12rpx" } }, "fui-actionsheet__btn": { "": { "height": "100rpx", "lineHeight": "100rpx", "flex": 1, "alignItems": "center", "justifyContent": "center", "textAlign": "center", "fontWeight": "normal", "position": "relative", "backgroundColor:active": "rgba(0,0,0,0.2)" } }, "fui-as__divider-light": { "": { "borderTopWidth": 0.5, "borderTopStyle": "solid", "borderTopColor": "#EEEEEE" } }, "fui-as__divider-dark": { "": { "borderTopWidth": 0.5, "borderTopStyle": "solid", "borderTopColor": "#333333" } }, "fui-as__safe-weex": { "": { "height": "168rpx", "paddingBottom": 34 } }, "fui-actionsheet__mask": { "": { "position": "fixed", "top": 0, "left": 0, "right": 0, "bottom": 0, "backgroundColor": "rgba(0,0,0,0.6)" } } };
  var animation = requireNativePlugin("animation");
  var _sfc_main$1 = {
    name: "fui-actionsheet",
    emits: ["click", "cancel"],
    props: {
      //是否显示操作菜单
      show: {
        type: Boolean,
        default: false
      },
      //菜单按钮数组，可自定义文本颜色
      itemList: {
        type: Array,
        default() {
          return [];
        }
      },
      //菜单按钮字体大小 rpx
      itemSize: {
        type: [Number, String],
        default: 32
      },
      //提示信息
      tips: {
        type: String,
        default: ""
      },
      //提示信息文本颜色
      color: {
        type: String,
        default: "#7F7F7F"
      },
      //提示文字大小 rpx
      size: {
        type: [Number, String],
        default: 26
      },
      //是否需要圆角
      radius: {
        type: Boolean,
        default: true
      },
      //是否需要取消按钮
      isCancel: {
        type: Boolean,
        default: true
      },
      //light/dark
      theme: {
        type: String,
        default: "light"
      },
      //点击遮罩 是否可关闭
      maskClosable: {
        type: Boolean,
        default: false
      },
      zIndex: {
        type: [Number, String],
        default: 996
      },
      //是否适配底部安全区
      safeArea: {
        type: Boolean,
        default: true
      }
    },
    data() {
      let isNvue = false;
      isNvue = true;
      return {
        iphoneX: false,
        vals: [],
        isNvue,
        isShow: false
      };
    },
    computed: {
      getStyle() {
        return `z-index:${Number(this.zIndex) - 10};`;
      }
    },
    watch: {
      show: {
        handler(newVal) {
          if (newVal) {
            this.open();
          } else {
            this.close();
          }
        },
        immediate: true
      },
      itemList(newVal) {
        this.initData(newVal);
      }
    },
    created() {
      this.iphoneX = this.isPhoneX();
      this.initData(this.itemList);
    },
    methods: {
      initData(vals) {
        if (vals && vals.length > 0) {
          if (typeof vals[0] !== "object") {
            vals = vals.map((item) => {
              return {
                text: item
              };
            });
          }
          this.vals = vals;
        }
      },
      handleClickMask() {
        if (!this.maskClosable)
          return;
        this.handleClickCancel();
      },
      handleClickItem(index) {
        if (!this.show)
          return;
        this.$emit("click", __spreadValues({
          index
        }, this.vals[index]));
      },
      handleClickCancel() {
        this.$emit("cancel");
      },
      open() {
        this.isShow = true;
        this.$nextTick(() => {
          setTimeout(() => {
            this._animation(true);
          }, 50);
        });
      },
      close() {
        this._animation(false);
      },
      _animation(type) {
        if (!this.$refs["fui_asm_ani"] || !this.$refs["fui_as_ani"])
          return;
        let styles = {
          transform: `translateY(${type ? "0" : "100%"})`
        };
        animation.transition(
          this.$refs["fui_asm_ani"].ref,
          {
            styles: {
              opacity: type ? 1 : 0
            },
            duration: 250,
            timingFunction: "ease-in-out",
            needLayout: false,
            delay: 0
            //ms
          },
          () => {
          }
        );
        animation.transition(
          this.$refs["fui_as_ani"].ref,
          {
            styles,
            duration: 250,
            timingFunction: "ease-in-out",
            needLayout: false,
            delay: 0
            //ms
          },
          () => {
            if (!type) {
              this.isShow = false;
            }
          }
        );
      },
      isPhoneX() {
        if (!this.safeArea)
          return false;
        const res = uni.getSystemInfoSync();
        let iphonex = false;
        let models = [
          "iphonex",
          "iphonexr",
          "iphonexsmax",
          "iphone11",
          "iphone11pro",
          "iphone11promax",
          "iphone12",
          "iphone12mini",
          "iphone12pro",
          "iphone12promax",
          "iphone13",
          "iphone13mini",
          "iphone13pro",
          "iphone13promax",
          "iphone14",
          "iphone14mini",
          "iphone14pro",
          "iphone14promax",
          "iphone15",
          "iphone15mini",
          "iphone15pro",
          "iphone15promax"
        ];
        const model = res.model.replace(/\s/g, "").toLowerCase();
        const newModel = model.split("<")[0];
        if (models.includes(model) || models.includes(newModel) || res.safeAreaInsets && res.safeAreaInsets.bottom > 0) {
          iphonex = true;
        }
        return iphonex;
      },
      stop() {
      }
    }
  };
  function _sfc_render$1(_ctx, _cache, $props, $setup, $data, $options) {
    return (0, import_vue4.openBlock)(), (0, import_vue4.createElementBlock)(
      import_vue4.Fragment,
      null,
      [
        (0, import_vue4.createCommentVNode)("\u672C\u6587\u4EF6\u7531FirstUI\u6388\u6743\u4E88\u4E25\u5929\u8D60\uFF08\u4F1A\u5458ID\uFF1A 24  42\uFF0C\u8EAB\u4EFD\u8BC1\u5C3E\u53F7\uFF1A0 4  30 1 8\uFF09\u4E13\u7528\uFF0C\u8BF7\u5C0A\u91CD\u77E5\u8BC6\u4EA7\u6743\uFF0C\u52FF\u79C1\u4E0B\u4F20\u64AD\uFF0C\u8FDD\u8005\u8FFD\u7A76\u6CD5\u5F8B\u8D23\u4EFB\u3002"),
        $data.isShow || !$data.isNvue ? ((0, import_vue4.openBlock)(), (0, import_vue4.createElementBlock)(
          "view",
          {
            key: 0,
            onTouchmove: _cache[2] || (_cache[2] = (0, import_vue4.withModifiers)((...args) => $options.stop && $options.stop(...args), ["stop", "prevent"]))
          },
          [
            (0, import_vue4.createElementVNode)(
              "view",
              {
                class: (0, import_vue4.normalizeClass)(["fui-actionsheet__mask", { "fui-actionsheet__mask-show": $props.show }]),
                ref: "fui_asm_ani",
                onClick: _cache[0] || (_cache[0] = (...args) => $options.handleClickMask && $options.handleClickMask(...args)),
                style: (0, import_vue4.normalizeStyle)($options.getStyle)
              },
              null,
              6
              /* CLASS, STYLE */
            ),
            (0, import_vue4.createElementVNode)(
              "view",
              {
                class: (0, import_vue4.normalizeClass)(["fui-actionsheet__wrap", { "fui-actionsheet__show": $props.show, "fui-actionsheet__radius": $props.radius, "fui-as__bg-light": $props.theme === "light", "fui-as__bg-dark": $props.theme === "dark" }]),
                ref: "fui_as_ani",
                style: (0, import_vue4.normalizeStyle)({ zIndex: $props.zIndex })
              },
              [
                $props.tips ? ((0, import_vue4.openBlock)(), (0, import_vue4.createElementBlock)(
                  "u-text",
                  {
                    key: 0,
                    class: (0, import_vue4.normalizeClass)(["fui-actionsheet__tips", { "fui-actionsheet__radius": $props.radius, "fui-as__btn-light": $props.theme === "light", "fui-as__btn-dark": $props.theme === "dark" }]),
                    style: (0, import_vue4.normalizeStyle)({ fontSize: $props.size + "rpx", color: $props.color })
                  },
                  (0, import_vue4.toDisplayString)($props.tips),
                  7
                  /* TEXT, CLASS, STYLE */
                )) : (0, import_vue4.createCommentVNode)("v-if", true),
                (0, import_vue4.createElementVNode)(
                  "view",
                  {
                    class: (0, import_vue4.normalizeClass)({ "fui-actionsheet__operate-box": $props.isCancel })
                  },
                  [
                    ((0, import_vue4.openBlock)(true), (0, import_vue4.createElementBlock)(
                      import_vue4.Fragment,
                      null,
                      (0, import_vue4.renderList)($data.vals, (item, index) => {
                        return (0, import_vue4.openBlock)(), (0, import_vue4.createElementBlock)("u-text", {
                          class: (0, import_vue4.normalizeClass)(["fui-actionsheet__btn", { "fui-actionsheet__btn-last": !$props.isCancel && index == $data.vals.length - 1, "fui-as__safe-weex": !$props.isCancel && index == $data.vals.length - 1 && $data.iphoneX && $props.safeArea, "fui-actionsheet__radius": $props.radius && !$props.tips && index === 0, "fui-as__divider-light": (index !== 0 || $props.tips) && $props.theme === "light", "fui-as__divider-dark": (index !== 0 || $props.tips) && $props.theme === "dark", "fui-as__btn-light": $props.theme === "light", "fui-as__btn-dark": $props.theme === "dark" }]),
                          style: (0, import_vue4.normalizeStyle)({ color: $props.theme === "dark" ? item.darkColor || "#D1D1D1" : item.color || "#181818", fontSize: $props.itemSize + "rpx" }),
                          key: index,
                          onClick: ($event) => $options.handleClickItem(index)
                        }, (0, import_vue4.toDisplayString)(item.text), 15, ["onClick"]);
                      }),
                      128
                      /* KEYED_FRAGMENT */
                    ))
                  ],
                  2
                  /* CLASS */
                ),
                $props.isCancel ? ((0, import_vue4.openBlock)(), (0, import_vue4.createElementBlock)(
                  "u-text",
                  {
                    key: 1,
                    style: (0, import_vue4.normalizeStyle)({ color: $props.theme === "dark" ? "#D1D1D1" : "#181818", fontSize: $props.itemSize + "rpx" }),
                    class: (0, import_vue4.normalizeClass)(["fui-actionsheet__btn fui-actionsheet__cancel", { "fui-as__safe-weex": $data.iphoneX && $props.safeArea, "fui-as__btn-light": $props.theme === "light", "fui-as__btn-dark": $props.theme === "dark" }]),
                    onClick: _cache[1] || (_cache[1] = (...args) => $options.handleClickCancel && $options.handleClickCancel(...args))
                  },
                  "\u53D6\u6D88",
                  6
                  /* CLASS, STYLE */
                )) : (0, import_vue4.createCommentVNode)("v-if", true)
              ],
              6
              /* CLASS, STYLE */
            )
          ],
          32
          /* HYDRATE_EVENTS */
        )) : (0, import_vue4.createCommentVNode)("v-if", true)
      ],
      2112
      /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
    );
  }
  var __easycom_2 = /* @__PURE__ */ _export_sfc(_sfc_main$1, [["render", _sfc_render$1], ["styles", [_style_0$1]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-actionsheet/fui-actionsheet.vue"]]);
  var _style_03 = { "fui-banner__box": { "": { "position": "relative" } }, "fui-banner": { "": { "width": "750rpx", "height": "336rpx" } }, "fui-info__box": { "": { "position": "absolute", "left": 0, "top": 0, "paddingLeft": "64rpx", "paddingTop": "64rpx", "flexDirection": "row", "alignItems": "center" } }, "fui-name__box": { "": { "paddingLeft": "32rpx" } }, "fui-nickname": { "": { "fontSize": "36rpx", "fontWeight": "600", "color": "#ffffff" } }, "fui-id": { "": { "fontSize": "28rpx", "fontWeight": "400", "paddingTop": "16rpx", "color": "#ffffff" } }, "fui-body__box": { "": { "backgroundColor": "#ffffff", "borderTopLeftRadius": "48rpx", "borderTopRightRadius": "48rpx", "borderBottomRightRadius": 0, "borderBottomLeftRadius": 0, "paddingTop": "32rpx", "paddingRight": "32rpx", "paddingBottom": 0, "paddingLeft": "32rpx", "marginTop": "-80rpx" } }, "fui-cell__box": { "": { "flexDirection": "row", "alignItems": "center" } }, "fui-img__box": { "": { "width": "96rpx", "height": "96rpx", "borderRadius": "24rpx", "alignItems": "center", "justifyContent": "center" } }, "fui-img": { "": { "width": "64rpx", "height": "64rpx" } }, "fui-title": { "": { "paddingLeft": "24rpx", "fontSize": "32rpx" } } };
  var _sfc_main3 = {
    data() {
      return {
        list: [{
          img: "user",
          title: "\u4E2A\u4EBA\u4FE1\u606F",
          background: "rgba(234,244,255,.5)"
        }, {
          img: "userlist",
          title: "\u8054\u7CFB\u4EBA",
          background: "rgba(227,255,243,.5)"
        }, {
          img: "notification",
          title: "\u6D88\u606F\u901A\u77E5",
          background: "rgba(255,241,219,.5)"
        }, {
          img: "logout",
          title: "\u9000\u51FA\u767B\u5F55",
          background: "rgba(255,227,219,.5)"
        }],
        show: false,
        tips: "\u9000\u51FA\u540E\u4E0D\u4F1A\u5220\u9664\u4EFB\u4F55\u5386\u53F2\u6570\u636E\uFF0C\u4E0B\u6B21\u767B\u5F55\u4F9D\u7136\u53EF\u4EE5\u4F7F\u7528\u672C\u8D26\u53F7\u3002",
        itemList: [{
          text: "\u9000\u51FA\u767B\u5F55",
          color: "#FF2B2B"
        }]
      };
    },
    methods: {
      handleUserInfo() {
        uni.$cloud.get("account/detail").then((data) => {
          formatAppLog("log", "at pages/my/my.nvue:61", "data", data);
        }).catch((error) => {
          formatAppLog("log", "at pages/my/my.nvue:63", "error", error);
        });
      },
      handleClick(index) {
        if (index === 0) {
          uni.fui.href("../profile/profile");
        } else if (index === 1) {
          uni.fui.href("../contacts/contacts");
        } else if (index === 2) {
          uni.fui.href("../notice/notice");
        } else {
          this.show = true;
        }
      },
      itemClick(e) {
        uni.fui.toast(e.text);
        this.cancel();
        uni.$cloud.logout().then((data) => {
          uni.$cloud.go("/pages/login/login");
        }).catch((error) => {
        });
      },
      cancel() {
        this.show = false;
      }
    }
  };
  function _sfc_render3(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_fui_avatar = resolveEasycom((0, import_vue4.resolveDynamicComponent)("fui-avatar"), __easycom_0);
    const _component_fui_list_cell = resolveEasycom((0, import_vue4.resolveDynamicComponent)("fui-list-cell"), __easycom_3);
    const _component_fui_actionsheet = resolveEasycom((0, import_vue4.resolveDynamicComponent)("fui-actionsheet"), __easycom_2);
    return (0, import_vue4.openBlock)(), (0, import_vue4.createElementBlock)("scroll-view", {
      scrollY: true,
      showScrollbar: true,
      enableBackToTop: true,
      bubble: "true",
      style: { flexDirection: "column" }
    }, [
      (0, import_vue4.createElementVNode)("view", { class: "fui-wrap" }, [
        (0, import_vue4.createElementVNode)("view", { class: "fui-banner__box" }, [
          (0, import_vue4.createElementVNode)("u-image", {
            src: "/static/images/my/img_banner_3x.png",
            class: "fui-banner",
            mode: "widthFix"
          }),
          (0, import_vue4.createElementVNode)("view", { class: "fui-info__box" }, [
            (0, import_vue4.createVNode)(_component_fui_avatar, {
              size: "large",
              src: "/static/images/common/logo.png"
            }),
            (0, import_vue4.createElementVNode)("view", { class: "fui-name__box" }, [
              (0, import_vue4.createElementVNode)("u-text", { class: "fui-nickname" }, "FirstUI-AI"),
              (0, import_vue4.createElementVNode)("u-text", { class: "fui-id" }, "ID\uFF1A10001")
            ])
          ])
        ]),
        (0, import_vue4.createElementVNode)("view", { class: "fui-body__box" }, [
          ((0, import_vue4.openBlock)(true), (0, import_vue4.createElementBlock)(
            import_vue4.Fragment,
            null,
            (0, import_vue4.renderList)($data.list, (item, index) => {
              return (0, import_vue4.openBlock)(), (0, import_vue4.createBlock)(_component_fui_list_cell, {
                arrow: "",
                radius: "24rpx",
                bottomRight: "32",
                onClick: ($event) => $options.handleClick(index),
                key: index
              }, {
                default: (0, import_vue4.withCtx)(() => [
                  (0, import_vue4.createElementVNode)("view", { class: "fui-cell__box" }, [
                    (0, import_vue4.createElementVNode)(
                      "view",
                      {
                        class: "fui-img__box",
                        style: (0, import_vue4.normalizeStyle)({ backgroundColor: item.background })
                      },
                      [
                        (0, import_vue4.createElementVNode)("u-image", {
                          src: `/static/images/my/${item.img}.png?v=1`,
                          class: "fui-img",
                          mode: "widthFix"
                        }, null, 8, ["src"])
                      ],
                      4
                      /* STYLE */
                    ),
                    (0, import_vue4.createElementVNode)(
                      "u-text",
                      { class: "fui-title" },
                      (0, import_vue4.toDisplayString)(item.title),
                      1
                      /* TEXT */
                    )
                  ])
                ]),
                _: 2
                /* DYNAMIC */
              }, 1032, ["onClick"]);
            }),
            128
            /* KEYED_FRAGMENT */
          ))
        ]),
        (0, import_vue4.createVNode)(_component_fui_actionsheet, {
          show: $data.show,
          tips: $data.tips,
          itemList: $data.itemList,
          onClick: $options.itemClick,
          onCancel: $options.cancel
        }, null, 8, ["show", "tips", "itemList", "onClick", "onCancel"])
      ])
    ]);
  }
  var my = /* @__PURE__ */ _export_sfc(_sfc_main3, [["render", _sfc_render3], ["styles", [_style_03]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/pages/my/my.nvue"]]);

  // <stdin>
  var webview = plus.webview.currentWebview();
  if (webview) {
    const __pageId = parseInt(webview.id);
    const __pagePath = "pages/my/my";
    let __pageQuery = {};
    try {
      __pageQuery = JSON.parse(webview.__query__);
    } catch (e) {
    }
    my.mpType = "page";
    const app = Vue.createPageApp(my, { $store: getApp({ allowDefault: true }).$store, __pageId, __pagePath, __pageQuery });
    app.provide("__globalStyles", Vue.useCssStyles([...__uniConfig.styles, ...my.styles || []]));
    app.mount("#root");
  }
})();
