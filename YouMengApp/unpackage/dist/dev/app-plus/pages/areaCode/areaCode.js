"use weex:vue";

if (typeof Promise !== 'undefined' && !Promise.prototype.finally) {
  Promise.prototype.finally = function(callback) {
    const promise = this.constructor
    return this.then(
      value => promise.resolve(callback()).then(() => value),
      reason => promise.resolve(callback()).then(() => {
        throw reason
      })
    )
  }
};

if (typeof uni !== 'undefined' && uni && uni.requireGlobal) {
  const global = uni.requireGlobal()
  ArrayBuffer = global.ArrayBuffer
  Int8Array = global.Int8Array
  Uint8Array = global.Uint8Array
  Uint8ClampedArray = global.Uint8ClampedArray
  Int16Array = global.Int16Array
  Uint16Array = global.Uint16Array
  Int32Array = global.Int32Array
  Uint32Array = global.Uint32Array
  Float32Array = global.Float32Array
  Float64Array = global.Float64Array
  BigInt64Array = global.BigInt64Array
  BigUint64Array = global.BigUint64Array
};


(() => {
  var __create = Object.create;
  var __defProp = Object.defineProperty;
  var __defProps = Object.defineProperties;
  var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
  var __getOwnPropDescs = Object.getOwnPropertyDescriptors;
  var __getOwnPropNames = Object.getOwnPropertyNames;
  var __getOwnPropSymbols = Object.getOwnPropertySymbols;
  var __getProtoOf = Object.getPrototypeOf;
  var __hasOwnProp = Object.prototype.hasOwnProperty;
  var __propIsEnum = Object.prototype.propertyIsEnumerable;
  var __defNormalProp = (obj, key, value) => key in obj ? __defProp(obj, key, { enumerable: true, configurable: true, writable: true, value }) : obj[key] = value;
  var __spreadValues = (a, b) => {
    for (var prop in b || (b = {}))
      if (__hasOwnProp.call(b, prop))
        __defNormalProp(a, prop, b[prop]);
    if (__getOwnPropSymbols)
      for (var prop of __getOwnPropSymbols(b)) {
        if (__propIsEnum.call(b, prop))
          __defNormalProp(a, prop, b[prop]);
      }
    return a;
  };
  var __spreadProps = (a, b) => __defProps(a, __getOwnPropDescs(b));
  var __commonJS = (cb, mod) => function __require() {
    return mod || (0, cb[__getOwnPropNames(cb)[0]])((mod = { exports: {} }).exports, mod), mod.exports;
  };
  var __copyProps = (to, from, except, desc) => {
    if (from && typeof from === "object" || typeof from === "function") {
      for (let key of __getOwnPropNames(from))
        if (!__hasOwnProp.call(to, key) && key !== except)
          __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
    }
    return to;
  };
  var __toESM = (mod, isNodeMode, target) => (target = mod != null ? __create(__getProtoOf(mod)) : {}, __copyProps(
    // If the importer is in node compatibility mode or this is not an ESM
    // file that has been converted to a CommonJS file using a Babel-
    // compatible transform (i.e. "__esModule" has not been set), then set
    // "default" to the CommonJS "module.exports" for node compatibility.
    isNodeMode || !mod || !mod.__esModule ? __defProp(target, "default", { value: mod, enumerable: true }) : target,
    mod
  ));

  // vue-ns:vue
  var require_vue = __commonJS({
    "vue-ns:vue"(exports, module) {
      module.exports = Vue;
    }
  });

  // vuex-ns:vuex
  var require_vuex = __commonJS({
    "vuex-ns:vuex"(exports, module) {
      module.exports = uni.Vuex;
    }
  });

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/uni-app.es.js
  var import_vue = __toESM(require_vue());
  var isString = (val) => typeof val === "string";
  function requireNativePlugin(name) {
    return weex.requireModule(name);
  }
  function formatAppLog(type, filename, ...args) {
    if (uni.__log__) {
      uni.__log__(type, filename, ...args);
    } else {
      console[type].apply(console, [...args, filename]);
    }
  }
  function resolveEasycom(component, easycom) {
    return isString(component) ? easycom : component;
  }

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/fui-index-list.js
  var import_vue2 = __toESM(require_vue());

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/_plugin-vue_export-helper.js
  var _export_sfc = (sfc, props) => {
    const target = sfc.__vccOpts || sfc;
    for (const [key, val] of props) {
      target[key] = val;
    }
    return target;
  };

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/fui-index-list.js
  var _style_0$3 = { "fui-search__bar-wrap": { "": { "flex": 1, "flexDirection": "row", "position": "relative", "alignItems": "center" } }, "fui-search__bar-form": { "": { "position": "relative", "flex": 1 } }, "fui-search__bar-box": { "": { "flex": 1, "paddingLeft": "24rpx", "paddingRight": "24rpx", "flexDirection": "row", "zIndex": 1, "alignItems": "center" } }, "fui-search__bar-input": { "": { "paddingTop": 0, "paddingRight": "16rpx", "paddingBottom": 0, "paddingLeft": "16rpx", "borderWidth": 0, "borderStyle": "solid", "borderColor": "#000000", "fontSize": "28rpx", "flex": 1, "backgroundColor": "rgba(0,0,0,0)" } }, "fui-search__bar-pl": { "": { "color": "#B2B2B2" } }, "fui-search__bar-label": { "": { "position": "absolute", "top": 0, "right": 0, "bottom": 0, "left": 0, "zIndex": 2, "flexDirection": "row", "alignItems": "center" } }, "fui-sb__label-center": { "": { "justifyContent": "center" } }, "fui-sb__label-left": { "": { "paddingLeft": "24rpx" } }, "fui-search__bar-btn": { "": { "fontSize": "30rpx", "marginLeft": "24rpx", "opacity:active": 0.5 } }, "fui-search__bar-text": { "": { "fontSize": "28rpx", "paddingLeft": "16rpx", "color": "#B2B2B2" } }, "fui-search__bar-icon": { "": { "alignItems": "center", "justifyContent": "center", "flexDirection": "column", "transform": "rotate(-45deg)", "transformOrigin": "56% center" } }, "fui-sbi__circle": { "": { "width": "24rpx", "height": "24rpx", "borderWidth": 1, "borderStyle": "solid", "borderColor": "#B2B2B2", "borderRadius": 50 } }, "fui-sbi__line": { "": { "width": 1, "height": "12rpx", "backgroundColor": "#B2B2B2", "borderBottomLeftRadius": "6rpx", "borderBottomRightRadius": "6rpx" } }, "fui-sbi__clear-wrap": { "": { "width": "32rpx", "height": "32rpx", "backgroundColor": "#B2B2B2", "transform": "rotate(45deg)", "position": "relative", "borderRadius": "32rpx" } }, "fui-sbi__clear": { "": { "width": "32rpx", "height": "32rpx", "flexDirection": "row", "alignItems": "center", "justifyContent": "center", "position": "absolute", "left": 0, "top": 0, "transform": "scale(0.5) translateZ(0)" } }, "fui-sbi__clear-a": { "": { "width": "32rpx", "borderWidth": "2rpx", "borderStyle": "solid", "borderColor": "#ffffff", "backgroundColor": "#ffffff" } }, "fui-sbi__clear-b": { "": { "height": "32rpx", "borderWidth": "2rpx", "borderStyle": "solid", "borderColor": "#ffffff", "backgroundColor": "#ffffff" } } };
  var _sfc_main$3 = {
    name: "fui-search-bar",
    emits: ["clear", "focus", "blur", "click", "cancel", "input", "search"],
    props: {
      //搜索栏背景色
      background: {
        type: String,
        default: "#F1F4FA"
      },
      //搜索栏上下padding（padding-top，padding-bottom）
      paddingTb: {
        type: [Number, String],
        default: 16
      },
      paddingLr: {
        type: [Number, String],
        default: 24
      },
      height: {
        type: [Number, String],
        default: 72
      },
      radius: {
        type: [Number, String],
        default: 8
      },
      color: {
        type: String,
        default: "#181818"
      },
      //input框背景色
      inputBackground: {
        type: String,
        default: "#fff"
      },
      focus: {
        type: Boolean,
        default: false
      },
      placeholder: {
        type: String,
        default: "\u8BF7\u8F93\u5165\u641C\u7D22\u5173\u952E\u8BCD"
      },
      isLeft: {
        type: Boolean,
        default: false
      },
      value: {
        type: String,
        default: ""
      },
      disabled: {
        type: Boolean,
        default: false
      },
      cancel: {
        type: Boolean,
        default: true
      },
      cancelText: {
        type: String,
        default: "\u53D6\u6D88"
      },
      cancelColor: {
        type: String,
        default: "#7F7F7F"
      },
      searchText: {
        type: String,
        default: "\u641C\u7D22"
      },
      searchColor: {
        type: String,
        default: ""
      },
      //是否显示搜索输入框
      showInput: {
        type: Boolean,
        default: true
      },
      //是否显示输入框占位标签，当平台不支持focus属性时可隐藏
      showLabel: {
        type: Boolean,
        default: true
      },
      //v2.1.0
      fixed: {
        type: Boolean,
        default: false
      }
    },
    created() {
      this.val = this.value;
      this.plholder = this.placeholder;
      if (this.focus || this.val.length > 0) {
        this.isSearch = true;
      }
    },
    mounted() {
      this.$nextTick(() => {
        setTimeout(() => {
          this.isFocus = this.focus;
        }, 300);
      });
    },
    watch: {
      focus(val) {
        this.$nextTick(() => {
          setTimeout(() => {
            this.isFocus = val;
          }, 20);
        });
      },
      isFocus(val) {
        if (!this.$refs.searchBarRef)
          return;
        this.$nextTick(() => {
          setTimeout(() => {
            if (val) {
              this.$refs.searchBarRef.focus();
            }
          }, 50);
        });
      },
      value(val) {
        this.val = val;
        if (this.focus || this.val.length > 0) {
          this.isSearch = true;
        }
      },
      placeholder(val) {
        this.plholder = this.placeholder;
      }
    },
    computed: {
      getSearchColor() {
        let color = this.searchColor;
        if (!color || color === true) {
          const app = uni && uni.$fui && uni.$fui.color;
          color = app && app.primary || "#465CFF";
        }
        return color;
      }
    },
    data() {
      return {
        isSearch: false,
        isFocus: false,
        val: "",
        plholder: ""
      };
    },
    methods: {
      clearInput() {
        this.val = "";
        this.isFocus = true;
        this.$emit("clear");
      },
      inputFocus(e) {
        if (!this.showLabel) {
          this.isSearch = true;
        }
        this.$emit("focus", e);
      },
      inputBlur(e) {
        this.isFocus = false;
        if (!this.cancel && !this.val) {
          this.isSearch = false;
        }
        this.$emit("blur", e);
      },
      onShowInput() {
        if (!this.disabled && this.showInput) {
          this.isSearch = true;
          this.$nextTick(() => {
            setTimeout(() => {
              this.isFocus = true;
            }, 50);
          });
        }
        this.$emit("click", {});
      },
      hideInput() {
        this.isSearch = false;
        this.isFocus = false;
        uni.hideKeyboard();
        this.$emit("cancel", {});
      },
      inputChange(e) {
        this.val = e.detail.value;
        this.$emit("input", e);
      },
      search() {
        this.$emit("search", {
          detail: {
            value: this.val
          }
        });
      },
      reset() {
        this.isSearch = false;
        this.isFocus = false;
        this.val = "";
        uni.hideKeyboard();
      }
    }
  };
  function _sfc_render$3(_ctx, _cache, $props, $setup, $data, $options) {
    return (0, import_vue2.openBlock)(), (0, import_vue2.createElementBlock)(
      import_vue2.Fragment,
      null,
      [
        (0, import_vue2.createCommentVNode)("\u672C\u6587\u4EF6\u7531FirstUI\u6388\u6743\u4E88\u4E25\u5929\u8D60\uFF08\u4F1A\u5458ID\uFF1A24   42\uFF0C\u8EAB\u4EFD\u8BC1\u5C3E\u53F7\uFF1A0 4  30  18\uFF09\u4E13\u7528\uFF0C\u8BF7\u5C0A\u91CD\u77E5\u8BC6\u4EA7\u6743\uFF0C\u52FF\u79C1\u4E0B\u4F20\u64AD\uFF0C\u8FDD\u8005\u8FFD\u7A76\u6CD5\u5F8B\u8D23\u4EFB\u3002"),
        (0, import_vue2.createElementVNode)(
          "view",
          {
            class: (0, import_vue2.normalizeClass)(["fui-search__bar-wrap", { "fui-searchbar__wrap-bg": !$props.background }]),
            style: (0, import_vue2.normalizeStyle)({ background: $props.background, paddingTop: $props.paddingTb + "rpx", paddingBottom: $props.paddingTb + "rpx", paddingLeft: $props.paddingLr + "rpx", paddingRight: $props.paddingLr + "rpx" })
          },
          [
            (0, import_vue2.renderSlot)(_ctx.$slots, "default"),
            (0, import_vue2.createElementVNode)(
              "view",
              {
                class: "fui-search__bar-form",
                style: (0, import_vue2.normalizeStyle)({ height: $props.height + "rpx" })
              },
              [
                $props.showInput ? ((0, import_vue2.openBlock)(), (0, import_vue2.createElementBlock)(
                  "view",
                  {
                    key: 0,
                    class: (0, import_vue2.normalizeClass)(["fui-search__bar-box", { "fui-searchbar__focus-invalid": !$data.isFocus && !$data.isSearch && $props.showLabel && !$props.disabled }]),
                    style: (0, import_vue2.normalizeStyle)({ height: $props.height + "rpx", borderRadius: $props.radius + "rpx", background: $props.inputBackground })
                  },
                  [
                    (0, import_vue2.createElementVNode)("view", { class: "fui-search__bar-icon" }, [
                      (0, import_vue2.createElementVNode)("view", { class: "fui-sbi__circle" }),
                      (0, import_vue2.createElementVNode)("view", { class: "fui-sbi__line" })
                    ]),
                    (0, import_vue2.createElementVNode)("u-input", {
                      ref: "searchBarRef",
                      class: (0, import_vue2.normalizeClass)(["fui-search__bar-input", { "fui-sb__input-color": !$props.color }]),
                      style: (0, import_vue2.normalizeStyle)({ color: $props.color, height: $props.height + "rpx" }),
                      placeholderClass: "fui-search__bar-pl",
                      placeholder: $data.plholder,
                      value: $data.val,
                      focus: $data.isFocus,
                      disabled: $props.disabled,
                      confirmType: "search",
                      onBlur: _cache[0] || (_cache[0] = (...args) => $options.inputBlur && $options.inputBlur(...args)),
                      onFocus: _cache[1] || (_cache[1] = (...args) => $options.inputFocus && $options.inputFocus(...args)),
                      onInput: _cache[2] || (_cache[2] = (...args) => $options.inputChange && $options.inputChange(...args)),
                      onConfirm: _cache[3] || (_cache[3] = (...args) => $options.search && $options.search(...args))
                    }, null, 46, ["placeholder", "value", "focus", "disabled"]),
                    $data.val.length > 0 && !$props.disabled ? ((0, import_vue2.openBlock)(), (0, import_vue2.createElementBlock)("view", {
                      key: 0,
                      class: "fui-sbi__clear-wrap",
                      onClick: _cache[4] || (_cache[4] = (0, import_vue2.withModifiers)((...args) => $options.clearInput && $options.clearInput(...args), ["stop"]))
                    }, [
                      (0, import_vue2.createElementVNode)("view", { class: "fui-sbi__clear" }, [
                        (0, import_vue2.createElementVNode)("view", { class: "fui-sbi__clear-a" })
                      ]),
                      (0, import_vue2.createElementVNode)("view", { class: "fui-sbi__clear" }, [
                        (0, import_vue2.createElementVNode)("view", { class: "fui-sbi__clear-b" })
                      ])
                    ])) : (0, import_vue2.createCommentVNode)("v-if", true)
                  ],
                  6
                  /* CLASS, STYLE */
                )) : (0, import_vue2.createCommentVNode)("v-if", true),
                !$data.isFocus && !$data.isSearch && $props.showLabel ? ((0, import_vue2.openBlock)(), (0, import_vue2.createElementBlock)(
                  "view",
                  {
                    key: 1,
                    class: (0, import_vue2.normalizeClass)(["fui-search__bar-label", [$props.isLeft ? "fui-sb__label-left" : "fui-sb__label-center"]]),
                    style: (0, import_vue2.normalizeStyle)({ borderRadius: $props.radius + "rpx", background: $props.inputBackground }),
                    onClick: _cache[5] || (_cache[5] = (...args) => $options.onShowInput && $options.onShowInput(...args))
                  },
                  [
                    (0, import_vue2.createElementVNode)("view", { class: "fui-search__bar-icon" }, [
                      (0, import_vue2.createElementVNode)("view", { class: "fui-sbi__circle" }),
                      (0, import_vue2.createElementVNode)("view", { class: "fui-sbi__line" })
                    ]),
                    (0, import_vue2.createElementVNode)(
                      "u-text",
                      { class: "fui-search__bar-text" },
                      (0, import_vue2.toDisplayString)($props.placeholder),
                      1
                      /* TEXT */
                    )
                  ],
                  6
                  /* CLASS, STYLE */
                )) : (0, import_vue2.createCommentVNode)("v-if", true)
              ],
              4
              /* STYLE */
            ),
            $props.cancel && $data.isSearch && !$data.val && $props.cancelText && $props.cancelText !== true && $props.cancelText !== "true" ? ((0, import_vue2.openBlock)(), (0, import_vue2.createElementBlock)(
              "u-text",
              {
                key: 0,
                class: "fui-search__bar-btn",
                style: (0, import_vue2.normalizeStyle)({ color: $props.cancelColor }),
                onClick: _cache[6] || (_cache[6] = (...args) => $options.hideInput && $options.hideInput(...args))
              },
              (0, import_vue2.toDisplayString)($props.cancelText),
              5
              /* TEXT, STYLE */
            )) : (0, import_vue2.createCommentVNode)("v-if", true),
            $data.val && !$props.disabled && $data.isSearch && $props.searchText && $props.searchText !== true && $props.searchText !== "true" ? ((0, import_vue2.openBlock)(), (0, import_vue2.createElementBlock)(
              "u-text",
              {
                key: 1,
                class: (0, import_vue2.normalizeClass)(["fui-search__bar-btn", { "fui-sb__btn-color": !$props.searchColor }]),
                style: (0, import_vue2.normalizeStyle)({ color: $options.getSearchColor }),
                onClick: _cache[7] || (_cache[7] = (...args) => $options.search && $options.search(...args))
              },
              (0, import_vue2.toDisplayString)($props.searchText),
              7
              /* TEXT, CLASS, STYLE */
            )) : (0, import_vue2.createCommentVNode)("v-if", true)
          ],
          6
          /* CLASS, STYLE */
        )
      ],
      2112
      /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
    );
  }
  var __easycom_0 = /* @__PURE__ */ _export_sfc(_sfc_main$3, [["render", _sfc_render$3], ["styles", [_style_0$3]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-search-bar/fui-search-bar.vue"]]);
  var _style_0$2 = { "fui-loadmore__wrap": { "": { "alignItems": "center", "justifyContent": "center" } }, "fui-loadmore__col": { "": { "flexDirection": "column" } }, "fui-loadmore__row": { "": { "flexDirection": "row" } }, "fui-loadmore__icon": { "": { "marginTop": 0, "marginRight": 6, "marginBottom": 0, "marginLeft": 6, "borderWidth": 2, "borderStyle": "solid", "borderRadius": 100 } }, "fui-loadmore__icon-ani": { "": { "marginTop": 0, "marginRight": 6, "marginBottom": 0, "marginLeft": 6 } }, "fui-loadmore__text": { "": { "paddingTop": "16rpx" } } };
  var animation = requireNativePlugin("animation");
  var _sfc_main$2 = {
    name: "fui-loadmore",
    props: {
      //占据高度，单位rx
      height: {
        type: [Number, String],
        default: 100
      },
      //1-上拉加载 2-正在加载... 3-没有更多了
      state: {
        type: [Number, String],
        default: 2
      },
      initText: {
        type: String,
        default: "\u4E0A\u62C9\u52A0\u8F7D"
      },
      //提示文字
      text: {
        type: String,
        default: "\u6B63\u5728\u52A0\u8F7D..."
      },
      noneText: {
        type: String,
        default: "\u6CA1\u6709\u66F4\u591A\u4E86"
      },
      //文字颜色
      color: {
        type: String,
        default: "#7F7F7F"
      },
      //文字大小，单位rpx
      size: {
        type: [Number, String],
        default: 24
      },
      //loading图标背景色
      iconColor: {
        type: String,
        default: "#B2B2B2"
      },
      //loading图标高亮部分颜色
      activeColor: {
        type: String,
        default: ""
      },
      //loading 图标的宽度，单位rpx
      iconWidth: {
        type: [Number, String],
        default: 32
      },
      //自定义loading图标image路径，若自定义图标则iconColor、activeColor属性失效
      src: {
        type: String,
        default: ""
      },
      //loading图标和文字排列方向，可取值：col，row
      direction: {
        type: String,
        default: "row"
      }
    },
    watch: {
      state(newValue, oldValue) {
        this.$nextTick(() => {
          if (newValue == 2) {
            this.stop = false;
            setTimeout(() => {
              this._animation();
            }, 50);
          } else {
            this.stop = true;
          }
        });
      }
    },
    computed: {
      getActiveColor() {
        let color = this.activeColor;
        if (!color || color === true) {
          const app = uni && uni.$fui && uni.$fui.color;
          color = app && app.primary || "#465CFF";
        }
        return color;
      }
    },
    data() {
      let isNvue = false;
      isNvue = true;
      return {
        isNvue,
        deg: 0,
        stop: false
      };
    },
    mounted() {
      this.$nextTick(() => {
        setTimeout(() => {
          this.deg += 360;
          this._animation();
        }, 50);
      });
    },
    beforeUnmount() {
      this.deg = 0;
      this.stop = true;
    },
    methods: {
      getStateText(state) {
        state = Number(state);
        return [this.initText, this.text, this.noneText][state - 1];
      },
      _animation() {
        if (!this.$refs["fui_loadmore"] || this.stop)
          return;
        animation.transition(
          this.$refs["fui_loadmore"].ref,
          {
            styles: {
              transform: `rotate(${this.deg}deg)`
            },
            duration: 700,
            //ms
            timingFunction: "linear",
            iterationCount: "infinite",
            needLayout: false,
            delay: 0
            //ms
          },
          () => {
            this.deg += 360;
            this._animation();
          }
        );
      }
    }
  };
  function _sfc_render$2(_ctx, _cache, $props, $setup, $data, $options) {
    return (0, import_vue2.openBlock)(), (0, import_vue2.createElementBlock)(
      import_vue2.Fragment,
      null,
      [
        (0, import_vue2.createCommentVNode)("\u672C\u6587\u4EF6\u7531FirstUI\u6388\u6743\u4E88\u4E25\u5929\u8D60\uFF08\u4F1A\u5458ID\uFF1A  2 442\uFF0C\u8EAB\u4EFD\u8BC1\u5C3E\u53F7\uFF1A  0  4 3018\uFF09\u4E13\u7528\uFF0C\u8BF7\u5C0A\u91CD\u77E5\u8BC6\u4EA7\u6743\uFF0C\u52FF\u79C1\u4E0B\u4F20\u64AD\uFF0C\u8FDD\u8005\u8FFD\u7A76\u6CD5\u5F8B\u8D23\u4EFB\u3002"),
        (0, import_vue2.createElementVNode)(
          "view",
          {
            class: (0, import_vue2.normalizeClass)(["fui-loadmore__wrap", ["fui-loadmore__" + $props.direction]]),
            style: (0, import_vue2.normalizeStyle)({ height: $props.height + "rpx" })
          },
          [
            !$props.src && $props.state == 2 ? ((0, import_vue2.openBlock)(), (0, import_vue2.createElementBlock)(
              "view",
              {
                key: 0,
                class: (0, import_vue2.normalizeClass)(["fui-loadmore__icon", { "fui-loadmore__border-left": !$data.isNvue && !$props.activeColor }]),
                ref: "fui_loadmore",
                style: (0, import_vue2.normalizeStyle)({ width: $props.iconWidth + "rpx", height: $props.iconWidth + "rpx", "border-left-color": $options.getActiveColor, "border-right-color": $props.iconColor, "border-top-color": $props.iconColor, "border-bottom-color": $props.iconColor })
              },
              null,
              6
              /* CLASS, STYLE */
            )) : (0, import_vue2.createCommentVNode)("v-if", true),
            $props.src && $props.state == 2 ? ((0, import_vue2.openBlock)(), (0, import_vue2.createElementBlock)("u-image", {
              key: 1,
              class: "fui-loadmore__icon-ani",
              ref: "fui_loadmore",
              src: $props.src,
              style: (0, import_vue2.normalizeStyle)({ width: $props.iconWidth + "rpx", height: $props.iconWidth + "rpx" })
            }, null, 12, ["src"])) : (0, import_vue2.createCommentVNode)("v-if", true),
            (0, import_vue2.createElementVNode)(
              "u-text",
              {
                class: (0, import_vue2.normalizeClass)({ "fui-loadmore__text": $props.direction === "col" }),
                style: (0, import_vue2.normalizeStyle)({ color: $props.color, "font-size": $props.size + "rpx", "line-height": $props.size + "rpx" })
              },
              (0, import_vue2.toDisplayString)($options.getStateText($props.state)),
              7
              /* TEXT, CLASS, STYLE */
            )
          ],
          6
          /* CLASS, STYLE */
        )
      ],
      2112
      /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
    );
  }
  var __easycom_2 = /* @__PURE__ */ _export_sfc(_sfc_main$2, [["render", _sfc_render$2], ["styles", [_style_0$2]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-loadmore/fui-loadmore.vue"]]);
  var _style_0$1 = { "fui-index__list-item": { "": { "flex": 1, "flexDirection": "row", "alignItems": "center", "paddingTop": "32rpx", "paddingRight": "64rpx", "paddingBottom": "32rpx", "paddingLeft": "32rpx", "backgroundColor": "#FFFFFF", "position": "relative", "!backgroundColor:active": "rgba(0,0,0,0.2)" } }, "fui-il__border-bottom": { "": { "position": "absolute", "bottom": 0, "height": 0.5, "zIndex": -1, "backgroundColor": "#EEEEEE", "left": "32rpx", "right": 0 } }, "fui-il__between": { "": { "justifyContent": "space-between" } }, "fui-index__list-left": { "": { "flexDirection": "row", "alignItems": "center", "overflow": "hidden" } }, "fui-il__checkbox": { "": { "fontSize": 0, "color": "rgba(0,0,0,0)", "width": "40rpx", "height": "40rpx", "borderWidth": 1, "borderStyle": "solid", "borderRadius": "40rpx", "flexDirection": "row", "alignItems": "center", "justifyContent": "center", "overflow": "hidden", "position": "relative", "marginRight": "24rpx" } }, "fui-il__checkmark": { "": { "width": "20rpx", "height": "40rpx", "borderBottomStyle": "solid", "borderBottomWidth": 3, "borderBottomColor": "#FFFFFF", "borderRightStyle": "solid", "borderRightWidth": 3, "borderRightColor": "#FFFFFF", "transform": "rotate(45deg) scale(0.5)", "transformOrigin": "54% 48%" } }, "fui-il__img-box": { "": { "width": "72rpx", "height": "72rpx", "alignItems": "center", "justifyContent": "center", "borderRadius": "8rpx", "overflow": "hidden", "backgroundColor": "#F8F8F8", "marginRight": "24rpx" } }, "fui-index__list-img": { "": { "width": "72rpx", "height": "72rpx", "borderRadius": "8rpx" } }, "fui-index__list-main": { "": { "lines": 1, "fontSize": "32rpx", "fontWeight": "normal", "overflow": "hidden", "textOverflow": "ellipsis" } }, "fui-index__list-sub": { "": { "fontWeight": "normal", "fontSize": "28rpx", "color": "#999999", "lines": 1, "paddingLeft": "24rpx" } } };
  var _sfc_main$1 = {
    name: "f-index-list-item",
    props: {
      model: {
        type: Object,
        default() {
          return {};
        }
      },
      isSelect: {
        type: Boolean,
        default: false
      },
      selectedColor: {
        type: String,
        default: ""
      },
      //checkbox未选中时边框颜色
      borderColor: {
        type: String,
        default: "#ccc"
      },
      //是否显示图片
      isSrc: {
        type: Boolean,
        default: false
      },
      //次要内容是否居右侧
      subRight: {
        type: Boolean,
        default: true
      },
      last: {
        type: Boolean,
        default: false
      },
      idx: {
        type: Number,
        default: 0
      },
      index: {
        type: Number,
        default: 0
      }
    },
    computed: {
      getSelectedColor() {
        let color = this.selectedColor;
        if (!color || color === true) {
          const app = uni && uni.$fui && uni.$fui.color;
          color = app && app.primary || "#465CFF";
        }
        return color;
      }
    },
    methods: {
      onClick() {
        this.$emit("itemClick", {
          idx: this.idx,
          index: this.index
        });
      }
    }
  };
  function _sfc_render$1(_ctx, _cache, $props, $setup, $data, $options) {
    return (0, import_vue2.openBlock)(), (0, import_vue2.createElementBlock)(
      import_vue2.Fragment,
      null,
      [
        (0, import_vue2.createCommentVNode)("\u672C\u6587\u4EF6\u7531FirstUI\u6388\u6743\u4E88\u4E25\u5929\u8D60\uFF08\u4F1A\u5458ID\uFF1A 2 4 42\uFF0C\u8EAB\u4EFD\u8BC1\u5C3E\u53F7\uFF1A0 4  301  8\uFF09\u4E13\u7528\uFF0C\u8BF7\u5C0A\u91CD\u77E5\u8BC6\u4EA7\u6743\uFF0C\u52FF\u79C1\u4E0B\u4F20\u64AD\uFF0C\u8FDD\u8005\u8FFD\u7A76\u6CD5\u5F8B\u8D23\u4EFB\u3002"),
        (0, import_vue2.createElementVNode)(
          "view",
          {
            class: (0, import_vue2.normalizeClass)(["fui-index__list-item", { "fui-il__between": $props.subRight }]),
            onClick: _cache[0] || (_cache[0] = (...args) => $options.onClick && $options.onClick(...args))
          },
          [
            (0, import_vue2.createElementVNode)("view", { class: "fui-index__list-left" }, [
              $props.isSelect ? ((0, import_vue2.openBlock)(), (0, import_vue2.createElementBlock)(
                "view",
                {
                  key: 0,
                  class: (0, import_vue2.normalizeClass)(["fui-il__checkbox", { "fui-il__checkbox-color": (!$props.selectedColor || $props.selectedColor === true) && $props.model.checked }]),
                  style: (0, import_vue2.normalizeStyle)({ background: $props.model.checked ? $options.getSelectedColor : "#fff", borderColor: $props.model.checked ? $options.getSelectedColor : $props.borderColor })
                },
                [
                  $props.model.checked ? ((0, import_vue2.openBlock)(), (0, import_vue2.createElementBlock)("view", {
                    key: 0,
                    class: "fui-il__checkmark"
                  })) : (0, import_vue2.createCommentVNode)("v-if", true)
                ],
                6
                /* CLASS, STYLE */
              )) : (0, import_vue2.createCommentVNode)("v-if", true),
              $props.isSrc ? ((0, import_vue2.openBlock)(), (0, import_vue2.createElementBlock)("view", {
                key: 1,
                class: "fui-il__img-box"
              }, [
                $props.model.src ? ((0, import_vue2.openBlock)(), (0, import_vue2.createElementBlock)("u-image", {
                  key: 0,
                  src: $props.model.src,
                  class: "fui-index__list-img",
                  mode: "widthFix"
                }, null, 8, ["src"])) : (0, import_vue2.createCommentVNode)("v-if", true)
              ])) : (0, import_vue2.createCommentVNode)("v-if", true),
              (0, import_vue2.createElementVNode)(
                "u-text",
                { class: "fui-index__list-main" },
                (0, import_vue2.toDisplayString)($props.model.text || ""),
                1
                /* TEXT */
              )
            ]),
            (0, import_vue2.createElementVNode)(
              "u-text",
              { class: "fui-index__list-sub" },
              (0, import_vue2.toDisplayString)($props.model.subText || ""),
              1
              /* TEXT */
            ),
            !$props.last ? ((0, import_vue2.openBlock)(), (0, import_vue2.createElementBlock)("view", {
              key: 0,
              class: "fui-il__border-bottom"
            })) : (0, import_vue2.createCommentVNode)("v-if", true)
          ],
          2
          /* CLASS */
        )
      ],
      2112
      /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
    );
  }
  var fIndexListItem = /* @__PURE__ */ _export_sfc(_sfc_main$1, [["render", _sfc_render$1], ["styles", [_style_0$1]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-index-list/f-index-list-item.vue"]]);
  var _style_0 = { "fui-index__list": { "": { "position": "absolute", "left": 0, "top": 0, "right": 0, "bottom": 0, "flexDirection": "row" } }, "fui-index__list-sv": { "": { "flex": 1 } }, "fui-index__list-letter": { "": { "flex": 1, "paddingTop": 0, "paddingRight": "32rpx", "paddingBottom": 0, "paddingLeft": "32rpx", "height": "64rpx", "lineHeight": "64rpx", "flexDirection": "row", "alignItems": "center", "borderTopStyle": "solid", "borderTopWidth": 0.5, "borderTopColor": "#eeeeee", "borderBottomStyle": "solid", "borderBottomWidth": 0.5, "borderBottomColor": "#eeeeee" } }, "fui-il__letter-text": { "": { "fontSize": "28rpx", "fontWeight": "600" } }, "fui-index__letter": { "": { "position": "fixed", "right": 0, "textAlign": "center", "zIndex": 10, "flexDirection": "column" } }, "fui-letter__item": { "": { "flex": 1, "paddingTop": 0, "paddingRight": "8rpx", "paddingBottom": 0, "paddingLeft": "8rpx", "fontWeight": "bold", "alignItems": "center", "justifyContent": "center" } }, "fui-letter__key": { "": { "width": "40rpx", "height": "40rpx", "fontSize": "26rpx", "transform": "scale(0.8)", "transformOrigin": "center center", "borderRadius": "40rpx", "lineHeight": "40rpx", "alignItems": "center", "justifyContent": "center", "textAlign": "center" } }, "fui-il__indicator": { "": { "position": "fixed", "width": "100rpx", "height": "100rpx", "zIndex": 999, "justifyContent": "center", "alignItems": "center", "borderRadius": "10rpx", "right": "100rpx" } }, "fui-il__nvue-android": { "": { "!width": "128rpx", "paddingRight": "28rpx", "!right": "72rpx" } }, "fui-il__indicator-text": { "": { "textAlign": "center", "color": "#ffffff", "fontSize": "60rpx", "fontWeight": "bold" } }, "fui-il__indicator-after": { "": { "width": "100rpx", "position": "absolute", "top": 0, "right": 0, "left": 0, "bottom": 0, "zIndex": -1, "borderTopLeftRadius": "100rpx", "borderTopRightRadius": 0, "borderBottomRightRadius": "100rpx", "borderBottomLeftRadius": "100rpx", "backgroundColor": "#c9c9c9", "transform": "rotate(45deg)" } } };
  var dom = weex.requireModule("dom");
  function throttle(func, delay) {
    var prev = Date.now();
    return function() {
      var context = this;
      var args = arguments;
      var now = Date.now();
      if (now - prev >= delay) {
        func.apply(context, args);
        prev = Date.now();
      }
    };
  }
  function touchMove(e) {
    let pageY = e.touches[0].pageY;
    let index = this.getIndex(pageY - this.winOffsetY);
    if (this.touchmoveIndex === index) {
      return false;
    }
    let item = this.lists[index];
    if (item) {
      dom.scrollToElement(this.$refs[`fui_il_letter_${index}`][0], {
        animated: false
      });
      this.touchmoveIndex = index;
    }
  }
  var throttleTouchMove = throttle(touchMove, 40);
  var _sfc_main = {
    name: "fui-index-list",
    emits: ["click", "init", "scrolltolower"],
    components: {
      fIndexListItem
    },
    props: {
      //列表数据
      listData: {
        type: Array,
        default() {
          return [];
        }
      },
      height: {
        type: [Number, String],
        default: 64
      },
      color: {
        type: String,
        default: "#181818"
      },
      background: {
        type: String,
        default: "#F1F4FA"
      },
      keyColor: {
        type: String,
        default: "#7F7F7F"
      },
      activeColor: {
        type: String,
        default: "#FFFFFF"
      },
      activeBackground: {
        type: String,
        default: ""
      },
      isSelect: {
        type: Boolean,
        default: false
      },
      //checkbox未选中时边框颜色
      borderColor: {
        type: String,
        default: "#ccc"
      },
      selectedColor: {
        type: String,
        default: ""
      },
      //是否显示图片
      isSrc: {
        type: Boolean,
        default: false
      },
      //次要内容是否居右侧
      subRight: {
        type: Boolean,
        default: true
      },
      custom: {
        type: Boolean,
        default: false
      },
      //H5端使用，是否使用了默认导航栏，默认44px
      isHeader: {
        type: Boolean,
        default: false
      }
    },
    watch: {
      listData(val) {
        this.initData();
      }
    },
    computed: {
      getActiveBgColor() {
        let color = this.activeBackground;
        if (!color || color === true) {
          const app = uni && uni.$fui && uni.$fui.color;
          color = app && app.primary || "#465CFF";
        }
        return color;
      }
    },
    data() {
      let isNvue = false;
      isNvue = true;
      return {
        lists: [],
        idtHeight: 0,
        winOffsetY: 0,
        winHeight: 0,
        styles: "",
        indicators: [],
        top: -1,
        start: 0,
        touching: false,
        touchmoveIndex: -1,
        scrollViewId: "",
        touchmovable: true,
        loaded: false,
        isPC: false,
        nvueAndroid: false,
        isNvue
      };
    },
    mounted() {
      const res = uni.getSystemInfoSync();
      if (res.platform.toLocaleLowerCase() == "android") {
        this.nvueAndroid = true;
      }
      this.$nextTick(() => {
        setTimeout(() => {
          this.initData();
        }, 50);
      });
    },
    methods: {
      //滚动到底部，会触发 scrolltolower 事件
      scrolltolower() {
        this.$emit("scrolltolower", {});
      },
      getIndex(y) {
        let index = -1;
        if (this.nvueAndroid) {
          index = Math.floor(y / uni.upx2px(40));
        } else {
          index = Math.floor((y - this.start) / uni.upx2px(40));
        }
        return index;
      },
      initData() {
        this.lists = [];
        let height = 0;
        let lists2 = [];
        let tempArr = [...this.listData || []];
        for (let i = 0, len = tempArr.length; i < len; i++) {
          let model = tempArr[i];
          if (!model.data || model.data.length === 0) {
            continue;
          }
          height += 40;
          model.originalIndex = i;
          model.key = `fui_key_${Math.ceil(Math.random() * 1e6).toString(36)}`;
          lists2.push(model);
        }
        this.idtHeight = height;
        this.styles = `height:${height}rpx;`;
        this.idtHeight = uni.upx2px(height);
        this.styles = `height:${this.idtHeight}px;`;
        this.lists = lists2;
        dom.getComponentRect(this.$refs["fui_index_list"], (res) => {
          this.winOffsetY = res.size.top;
          this.winHeight = res.size.height;
          this.setStyles();
        });
        this.$nextTick(() => {
          this.$emit("init");
        });
      },
      setStyles() {
        this.indicators = [];
        this.styles = `height:${this.idtHeight}rpx;top:${this.winHeight / 2}px;-webkit-transform: translateY(-${this.idtHeight / 2}rpx);transform: translateY(-${this.idtHeight / 2}rpx)`;
        this.styles = `height:${this.idtHeight}px;top:${this.winHeight / 2}px;-webkit-transform: translateY(-${this.idtHeight / 2}px);transform: translateY(-${this.idtHeight / 2}px)`;
        let start = this.winHeight / 2 - uni.upx2px(this.idtHeight) / 2;
        this.start = start;
        this.lists.forEach((item, index) => {
          const top = start + uni.upx2px(index * 40 + 20 - 50);
          this.indicators.push(top);
        });
      },
      startEmits(idx, index) {
        let item = this.lists[idx];
        let data = item.data[index] || {};
        this.$emit("click", __spreadValues({
          index: item.originalIndex,
          letter: item.letter,
          subIndex: index
        }, data));
      },
      onTap(idx, index) {
        this.startEmits(idx, index);
      },
      onClick(e) {
        const {
          idx,
          index
        } = e;
        this.startEmits(idx, index);
      },
      touchStart(e) {
        this.touching = true;
        let pageY = this.isPC ? e.pageY : e.touches[0].pageY;
        let index = this.getIndex(pageY - this.winOffsetY);
        let item = this.lists[index];
        if (item) {
          this.scrollViewId = `fui_il_letter_${index}`;
          this.touchmoveIndex = index;
          dom.scrollToElement(this.$refs[`fui_il_letter_${index}`][0], {
            animated: false
          });
        }
      },
      touchMove(e) {
        throttleTouchMove.call(this, e);
      },
      touchEnd() {
        this.touching = false;
        this.touchmoveIndex = -1;
      },
      mousedown(e) {
        if (!this.isPC)
          return;
        this.touchStart(e);
      },
      mousemove(e) {
        if (!this.isPC)
          return;
        this.touchMove(e);
      },
      mouseleave(e) {
        if (!this.isPC)
          return;
        this.touchEnd(e);
      }
      //开发工具中移动端如果touch事件失效，请检查开发工具或者真机调试
      // letterTap(index) {
      // }
    }
  };
  function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_f_index_list_item = (0, import_vue2.resolveComponent)("f-index-list-item");
    return (0, import_vue2.openBlock)(), (0, import_vue2.createElementBlock)(
      import_vue2.Fragment,
      null,
      [
        (0, import_vue2.createCommentVNode)("\u672C\u6587\u4EF6\u7531FirstUI\u6388\u6743\u4E88\u4E25\u5929\u8D60\uFF08\u4F1A\u5458ID\uFF1A  24 42\uFF0C\u8EAB\u4EFD\u8BC1\u5C3E\u53F7\uFF1A04    30 18\uFF09\u4E13\u7528\uFF0C\u8BF7\u5C0A\u91CD\u77E5\u8BC6\u4EA7\u6743\uFF0C\u52FF\u79C1\u4E0B\u4F20\u64AD\uFF0C\u8FDD\u8005\u8FFD\u7A76\u6CD5\u5F8B\u8D23\u4EFB\u3002"),
        (0, import_vue2.createElementVNode)(
          "view",
          {
            class: "fui-index__list",
            ref: "fui_index_list",
            id: "fui_index_list"
          },
          [
            (0, import_vue2.createElementVNode)(
              "list",
              {
                class: "fui-index__list-sv",
                scrollable: true,
                showScrollbar: false,
                loadmoreoffset: 10,
                onLoadmore: _cache[0] || (_cache[0] = (...args) => $options.scrolltolower && $options.scrolltolower(...args))
              },
              [
                (0, import_vue2.createElementVNode)("cell", null, [
                  (0, import_vue2.renderSlot)(_ctx.$slots, "default")
                ]),
                ((0, import_vue2.openBlock)(true), (0, import_vue2.createElementBlock)(
                  import_vue2.Fragment,
                  null,
                  (0, import_vue2.renderList)($data.lists, (list, idx) => {
                    return (0, import_vue2.openBlock)(), (0, import_vue2.createElementBlock)(
                      import_vue2.Fragment,
                      {
                        key: list.key
                      },
                      [
                        (0, import_vue2.createElementVNode)(
                          "header",
                          {
                            ref_for: true,
                            ref: "fui_il_letter_" + idx
                          },
                          [
                            (0, import_vue2.createElementVNode)(
                              "view",
                              {
                                class: (0, import_vue2.normalizeClass)(["fui-index__list-letter", { "fui-il__key-bg": !$props.background }]),
                                style: (0, import_vue2.normalizeStyle)({ background: $props.background })
                              },
                              [
                                (0, import_vue2.createElementVNode)(
                                  "u-text",
                                  {
                                    class: "fui-il__letter-text",
                                    style: (0, import_vue2.normalizeStyle)({ color: $props.color })
                                  },
                                  (0, import_vue2.toDisplayString)(list.descr || list.letter),
                                  5
                                  /* TEXT, STYLE */
                                )
                              ],
                              6
                              /* CLASS, STYLE */
                            )
                          ],
                          512
                          /* NEED_PATCH */
                        ),
                        (0, import_vue2.createElementVNode)("cell", null, [
                          (0, import_vue2.createCommentVNode)(" \u89E3\u6784\u63D2\u69FD item\u9879\u6837\u5F0F\u5185\u5BB9\u81EA\u5B9A\u4E49 "),
                          $props.custom ? ((0, import_vue2.openBlock)(), (0, import_vue2.createElementBlock)("view", { key: 0 }, [
                            ((0, import_vue2.openBlock)(true), (0, import_vue2.createElementBlock)(
                              import_vue2.Fragment,
                              null,
                              (0, import_vue2.renderList)(list.data, (model, index) => {
                                return (0, import_vue2.openBlock)(), (0, import_vue2.createElementBlock)("view", {
                                  key: index,
                                  onClick: ($event) => $options.onTap(idx, index)
                                }, [
                                  (0, import_vue2.renderSlot)(_ctx.$slots, "item", {
                                    model,
                                    idx,
                                    index,
                                    last: list.data.length - 1 === index,
                                    isSelect: $props.isSelect,
                                    isSrc: $props.isSrc,
                                    subRight: $props.subRight
                                  })
                                ], 8, ["onClick"]);
                              }),
                              128
                              /* KEYED_FRAGMENT */
                            ))
                          ])) : ((0, import_vue2.openBlock)(), (0, import_vue2.createElementBlock)("view", { key: 1 }, [
                            ((0, import_vue2.openBlock)(true), (0, import_vue2.createElementBlock)(
                              import_vue2.Fragment,
                              null,
                              (0, import_vue2.renderList)(list.data, (model, index) => {
                                return (0, import_vue2.openBlock)(), (0, import_vue2.createBlock)(_component_f_index_list_item, {
                                  onItemClick: $options.onClick,
                                  model,
                                  idx,
                                  index,
                                  last: list.data.length - 1 === index,
                                  isSelect: $props.isSelect,
                                  borderColor: $props.borderColor,
                                  selectedColor: $props.selectedColor,
                                  isSrc: $props.isSrc,
                                  subRight: $props.subRight,
                                  key: index
                                }, null, 8, ["onItemClick", "model", "idx", "index", "last", "isSelect", "borderColor", "selectedColor", "isSrc", "subRight"]);
                              }),
                              128
                              /* KEYED_FRAGMENT */
                            ))
                          ]))
                        ])
                      ],
                      64
                      /* STABLE_FRAGMENT */
                    );
                  }),
                  128
                  /* KEYED_FRAGMENT */
                )),
                (0, import_vue2.createElementVNode)("cell", null, [
                  (0, import_vue2.renderSlot)(_ctx.$slots, "footer")
                ])
              ],
              32
              /* HYDRATE_EVENTS */
            ),
            $data.touching && $data.touchmoveIndex !== -1 ? ((0, import_vue2.openBlock)(), (0, import_vue2.createElementBlock)(
              "view",
              {
                key: 0,
                class: (0, import_vue2.normalizeClass)(["fui-il__indicator", { "fui-il__nvue-android": $data.nvueAndroid }]),
                style: (0, import_vue2.normalizeStyle)({ top: $data.idtHeight / 2 + "px" })
              },
              [
                (0, import_vue2.createElementVNode)("view", { class: "fui-il__indicator-after" }),
                (0, import_vue2.createElementVNode)(
                  "u-text",
                  { class: "fui-il__indicator-text" },
                  (0, import_vue2.toDisplayString)($data.lists[$data.touchmoveIndex] && $data.lists[$data.touchmoveIndex].letter),
                  1
                  /* TEXT */
                )
              ],
              6
              /* CLASS, STYLE */
            )) : (0, import_vue2.createCommentVNode)("v-if", true),
            !$data.isNvue || $data.styles ? ((0, import_vue2.openBlock)(), (0, import_vue2.createElementBlock)(
              "view",
              {
                key: 1,
                class: "fui-index__letter",
                style: (0, import_vue2.normalizeStyle)($data.styles),
                onTouchstart: _cache[1] || (_cache[1] = (...args) => $options.touchStart && $options.touchStart(...args)),
                onTouchmove: _cache[2] || (_cache[2] = (0, import_vue2.withModifiers)((...args) => $options.touchMove && $options.touchMove(...args), ["stop", "prevent"])),
                onTouchend: _cache[3] || (_cache[3] = (...args) => $options.touchEnd && $options.touchEnd(...args)),
                onMousedown: _cache[4] || (_cache[4] = (0, import_vue2.withModifiers)((...args) => $options.mousedown && $options.mousedown(...args), ["stop"])),
                onMousemove: _cache[5] || (_cache[5] = (0, import_vue2.withModifiers)((...args) => $options.mousemove && $options.mousemove(...args), ["stop", "prevent"])),
                onMouseleave: _cache[6] || (_cache[6] = (0, import_vue2.withModifiers)((...args) => $options.mouseleave && $options.mouseleave(...args), ["stop"]))
              },
              [
                ((0, import_vue2.openBlock)(true), (0, import_vue2.createElementBlock)(
                  import_vue2.Fragment,
                  null,
                  (0, import_vue2.renderList)($data.lists, (item, i) => {
                    return (0, import_vue2.openBlock)(), (0, import_vue2.createElementBlock)("view", {
                      class: "fui-letter__item",
                      key: i
                    }, [
                      (0, import_vue2.createCommentVNode)(' @tap="letterTap(i)" '),
                      (0, import_vue2.createElementVNode)(
                        "u-text",
                        {
                          class: (0, import_vue2.normalizeClass)(["fui-letter__key", { "fui-il__key-color": i === $data.touchmoveIndex && !$props.activeBackground }]),
                          style: (0, import_vue2.normalizeStyle)({ background: i === $data.touchmoveIndex ? $options.getActiveBgColor : "transparent", color: i === $data.touchmoveIndex ? $props.activeColor : $props.keyColor })
                        },
                        (0, import_vue2.toDisplayString)(item.letter),
                        7
                        /* TEXT, CLASS, STYLE */
                      )
                    ]);
                  }),
                  128
                  /* KEYED_FRAGMENT */
                ))
              ],
              36
              /* STYLE, HYDRATE_EVENTS */
            )) : (0, import_vue2.createCommentVNode)("v-if", true)
          ],
          512
          /* NEED_PATCH */
        )
      ],
      2112
      /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
    );
  }
  var __easycom_4 = /* @__PURE__ */ _export_sfc(_sfc_main, [["render", _sfc_render], ["styles", [_style_0]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-index-list/fui-index-list.vue"]]);

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/pages/areaCode/areaCode.js
  var import_vue3 = __toESM(require_vue());
  var import_vuex = __toESM(require_vuex());
  var _style_0$12 = { "fui-divider__wrap": { "": { "position": "relative", "textAlign": "center", "flexDirection": "row", "justifyContent": "center", "alignItems": "center", "overflow": "hidden" } }, "fui-divider__line": { "": { "position": "absolute", "height": 0.5 } }, "fui-divider__text-box": { "": { "position": "relative", "textAlign": "center", "paddingTop": 0, "paddingRight": "6rpx", "paddingBottom": 0, "paddingLeft": "6rpx", "zIndex": 1, "flexDirection": "row", "alignItems": "center", "justifyContent": "center" } }, "fui-divider__text": { "": { "paddingTop": 0, "paddingRight": "12rpx", "paddingBottom": 0, "paddingLeft": "12rpx" } } };
  var _sfc_main$12 = {
    name: "fui-divider",
    props: {
      text: {
        type: String,
        default: ""
      },
      //divider占据高度，单位rpx
      height: {
        type: [Number, String],
        default: 100
      },
      //divider宽度
      width: {
        type: String,
        default: "400rpx"
      },
      //divider颜色
      dividerColor: {
        type: String,
        default: "#CCCCCC"
      },
      //文字颜色
      color: {
        type: String,
        default: "#B2B2B2"
      },
      //文字大小 rpx
      size: {
        type: [Number, String],
        default: 24
      },
      fontWeight: {
        type: [Number, String],
        default: 400
      },
      //背景颜色，和当前页面背景色保持一致
      backgroundColor: {
        type: String,
        default: "#F1F4FA"
      }
    },
    computed: {
      getTop() {
        return Number(this.height) / 2 + "rpx";
      }
    }
  };
  function _sfc_render$12(_ctx, _cache, $props, $setup, $data, $options) {
    return (0, import_vue3.openBlock)(), (0, import_vue3.createElementBlock)(
      import_vue3.Fragment,
      null,
      [
        (0, import_vue3.createCommentVNode)("\u672C\u6587\u4EF6\u7531FirstUI\u6388\u6743\u4E88\u4E25\u5929\u8D60\uFF08\u4F1A\u5458ID\uFF1A 2 4 42\uFF0C\u8EAB\u4EFD\u8BC1\u5C3E\u53F7\uFF1A  0  4 3018\uFF09\u4E13\u7528\uFF0C\u8BF7\u5C0A\u91CD\u77E5\u8BC6\u4EA7\u6743\uFF0C\u52FF\u79C1\u4E0B\u4F20\u64AD\uFF0C\u8FDD\u8005\u8FFD\u7A76\u6CD5\u5F8B\u8D23\u4EFB\u3002"),
        (0, import_vue3.createElementVNode)(
          "view",
          {
            class: "fui-divider__wrap",
            style: (0, import_vue3.normalizeStyle)({ height: $props.height + "rpx" })
          },
          [
            (0, import_vue3.createElementVNode)(
              "view",
              {
                class: "fui-divider__line",
                style: (0, import_vue3.normalizeStyle)({ width: $props.width, background: $props.dividerColor, top: $options.getTop })
              },
              null,
              4
              /* STYLE */
            ),
            (0, import_vue3.createElementVNode)(
              "view",
              {
                class: "fui-divider__text-box",
                style: (0, import_vue3.normalizeStyle)({ backgroundColor: $props.backgroundColor })
              },
              [
                (0, import_vue3.renderSlot)(_ctx.$slots, "default"),
                $props.text ? ((0, import_vue3.openBlock)(), (0, import_vue3.createElementBlock)(
                  "u-text",
                  {
                    key: 0,
                    class: "fui-divider__text",
                    style: (0, import_vue3.normalizeStyle)({ fontWeight: $props.fontWeight, color: $props.color, fontSize: $props.size + "rpx", lineHeight: $props.size + "rpx" })
                  },
                  (0, import_vue3.toDisplayString)($props.text),
                  5
                  /* TEXT, STYLE */
                )) : (0, import_vue3.createCommentVNode)("v-if", true)
              ],
              4
              /* STYLE */
            )
          ],
          4
          /* STYLE */
        )
      ],
      2112
      /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
    );
  }
  var __easycom_22 = /* @__PURE__ */ _export_sfc(_sfc_main$12, [["render", _sfc_render$12], ["styles", [_style_0$12]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-divider/fui-divider.vue"]]);
  var lists = [{
    letter: "\u2605",
    descr: "\u5E38\u7528\u56FD\u5BB6\u6216\u5730\u533A",
    data: [{
      text: "\u4E2D\u56FD\u5927\u9646",
      subText: "+86"
    }, {
      text: "\u4E2D\u56FD\u9999\u6E2F",
      subText: "+852"
    }, {
      text: "\u4E2D\u56FD\u53F0\u6E7E",
      subText: "+886"
    }, {
      text: "\u4E2D\u56FD\u6FB3\u95E8",
      subText: "+853"
    }, {
      text: "\u7F8E\u56FD",
      subText: "+1"
    }]
  }, {
    letter: "A",
    data: [{
      text: "\u963F\u5C14\u5DF4\u5C3C\u4E9A",
      subText: "+355"
    }, {
      text: "\u963F\u5C14\u53CA\u5229\u4E9A",
      subText: "+213"
    }, {
      text: "\u963F\u5BCC\u6C57",
      subText: "+93"
    }, {
      text: "\u963F\u6839\u5EF7",
      subText: "+54"
    }, {
      text: "\u7231\u5C14\u5170",
      subText: "+353"
    }, {
      text: "\u57C3\u53CA",
      subText: "+20"
    }, {
      text: "\u57C3\u585E\u4FC4\u6BD4\u4E9A",
      subText: "+251"
    }, {
      text: "\u7231\u6C99\u5C3C\u4E9A",
      subText: "+372"
    }, {
      text: "\u963F\u62C9\u4F2F\u8054\u5408\u914B\u957F\u56FD",
      subText: "+971"
    }, {
      text: "\u963F\u9C81\u5DF4",
      subText: "+297"
    }, {
      text: "\u963F\u66FC",
      subText: "+968"
    }, {
      text: "\u5B89\u9053\u5C14",
      subText: "+376"
    }, {
      text: "\u5B89\u54E5\u62C9",
      subText: "+244"
    }, {
      text: "\u5B89\u572D\u62C9",
      subText: "+1264"
    }, {
      text: "\u5B89\u63D0\u74DC\u5C9B\u548C\u5DF4\u5E03\u8FBE",
      subText: "+1268"
    }, {
      text: "\u6FB3\u5927\u5229\u4E9A",
      subText: "+61"
    }, {
      text: "\u5965\u5730\u5229",
      subText: "+43"
    }, {
      text: "\u5965\u5170\u7FA4\u5C9B",
      subText: "+358"
    }, {
      text: "\u963F\u585E\u62DC\u7586",
      subText: "+994"
    }, {
      text: "\u963F\u68EE\u677E\u5C9B",
      subText: "+247"
    }]
  }, {
    letter: "B",
    data: [{
      text: "\u5DF4\u5DF4\u591A\u65AF\u5C9B",
      subText: "+1246"
    }, {
      text: "\u5DF4\u5E03\u4E9A\u65B0\u51E0\u5185\u4E9A",
      subText: "+675"
    }, {
      text: "\u5DF4\u54C8\u9A6C",
      subText: "+1242"
    }, {
      text: "\u767D\u4FC4\u7F57\u65AF",
      subText: "+375"
    }, {
      text: "\u767E\u6155\u5927",
      subText: "+1441"
    }, {
      text: "\u5DF4\u57FA\u65AF\u5766",
      subText: "+92"
    }, {
      text: "\u5DF4\u62C9\u572D",
      subText: "+595"
    }, {
      text: "\u5DF4\u52D2\u65AF\u5766\u9886\u571F",
      subText: "+970"
    }, {
      text: "\u5DF4\u6797",
      subText: "+973"
    }, {
      text: "\u5DF4\u62FF\u9A6C",
      subText: "+507"
    }, {
      text: "\u4FDD\u52A0\u5229\u4E9A",
      subText: "+359"
    }, {
      text: "\u5DF4\u897F",
      subText: "+55"
    }, {
      text: "\u5317\u9A6C\u91CC\u4E9A\u7EB3\u7FA4\u5C9B",
      subText: "+1670"
    }, {
      text: "\u8D1D\u5B81",
      subText: "+229"
    }, {
      text: "\u6BD4\u5229\u65F6",
      subText: "+32"
    }, {
      text: "\u51B0\u5C9B",
      subText: "+354"
    }, {
      text: "\u535A\u8328\u74E6\u7EB3",
      subText: "+267"
    }, {
      text: "\u6CE2\u591A\u9ECE\u5404",
      subText: "+1787"
    }, {
      text: "\u6CE2\u591A\u9ECE\u5404",
      subText: "+1939"
    }, {
      text: "\u6CE2\u9ED1",
      subText: "+387"
    }, {
      text: "\u6CE2\u5170",
      subText: "+48"
    }, {
      text: "\u73BB\u5229\u7EF4\u4E9A",
      subText: "+591"
    }, {
      text: "\u4F2F\u5229\u5179",
      subText: "+501"
    }, {
      text: "\u4E0D\u4E39",
      subText: "+975"
    }, {
      text: "\u5E03\u57FA\u7EB3\u6CD5\u7D22",
      subText: "+226"
    }, {
      text: "\u5E03\u9686\u8FEA",
      subText: "+257"
    }]
  }, {
    letter: "C",
    data: [{
      text: "\u671D\u9C9C",
      subText: "+850"
    }, {
      text: "\u8D64\u9053\u51E0\u5185\u4E9A",
      subText: "+240"
    }]
  }, {
    letter: "D",
    data: [{
      text: "\u4E39\u9EA6",
      subText: "+45"
    }, {
      text: "\u5FB7\u56FD",
      subText: "+49"
    }, {
      text: "\u4E1C\u5E1D\u6C76",
      subText: "+670"
    }, {
      text: "\u591A\u54E5",
      subText: "+228"
    }, {
      text: "\u591A\u7C73\u5C3C\u52A0\u5171\u548C\u56FD",
      subText: "+1809"
    }, {
      text: "\u591A\u7C73\u5C3C\u52A0\u5171\u548C\u56FD",
      subText: "+1829"
    }, {
      text: "\u591A\u7C73\u5C3C\u52A0\u5171\u548C\u56FD",
      subText: "+1849"
    }, {
      text: "\u591A\u7C73\u5C3C\u514B",
      subText: "+1767"
    }]
  }, {
    letter: "E",
    data: [{
      text: "\u5384\u74DC\u591A\u5C14",
      subText: "+593"
    }, {
      text: "\u5384\u7ACB\u7279\u91CC\u4E9A",
      subText: "+291"
    }, {
      text: "\u4FC4\u7F57\u65AF",
      subText: "+7"
    }]
  }, {
    letter: "F",
    data: [{
      text: "\u6CD5\u56FD",
      subText: "+33"
    }, {
      text: "\u6CD5\u7F57\u7FA4\u5C9B",
      subText: "+298"
    }, {
      text: "\u68B5\u8482\u5188",
      subText: "+379"
    }, {
      text: "\u6CD5\u5C5E\u6CE2\u5229\u5C3C\u897F\u4E9A",
      subText: "+689"
    }, {
      text: "\u6CD5\u5C5E\u572D\u4E9A\u90A3",
      subText: "+594"
    }, {
      text: "\u6590\u6D4E",
      subText: "+679"
    }, {
      text: "\u83F2\u5F8B\u5BBE",
      subText: "+63"
    }, {
      text: "\u82AC\u5170",
      subText: "+358"
    }, {
      text: "\u4F5B\u5F97\u89D2",
      subText: "+238"
    }, {
      text: "\u798F\u514B\u5170\u7FA4\u5C9B",
      subText: "+500"
    }]
  }, {
    letter: "G",
    data: [{
      text: "\u5188\u6BD4\u4E9A",
      subText: "+220"
    }, {
      text: "\u521A\u679C\uFF08\u5E03\uFF09",
      subText: "+242"
    }, {
      text: "\u521A\u679C\uFF08\u91D1\uFF09",
      subText: "+243"
    }, {
      text: "\u683C\u6069\u897F\u5C9B",
      subText: "+44"
    }, {
      text: "\u683C\u9675\u5170",
      subText: "+299"
    }, {
      text: "\u683C\u6797\u7EB3\u8FBE",
      subText: "+1473"
    }, {
      text: "\u54E5\u4F26\u6BD4\u4E9A",
      subText: "+57"
    }, {
      text: "\u54E5\u65AF\u8FBE\u9ECE\u52A0",
      subText: "+506"
    }, {
      text: "\u74DC\u5FB7\u7F57\u666E\u5C9B",
      subText: "+590"
    }, {
      text: "\u5173\u5C9B",
      subText: "+1671"
    }, {
      text: "\u53E4\u5DF4",
      subText: "+53"
    }, {
      text: "\u572D\u4E9A\u90A3",
      subText: "+592"
    }]
  }, {
    letter: "H",
    data: [{
      text: "\u6D77\u5730",
      subText: "+509"
    }, {
      text: "\u97E9\u56FD",
      subText: "+82"
    }, {
      text: "\u54C8\u8428\u514B\u65AF\u5766",
      subText: "+7"
    }, {
      text: "\u9ED1\u5C71\u5171\u548C\u56FD",
      subText: "+382"
    }, {
      text: "\u8377\u5170",
      subText: "+31"
    }, {
      text: "\u8377\u5C5E\u5723\u9A6C\u4E01",
      subText: "+1721"
    }, {
      text: "\u6D2A\u90FD\u62C9\u65AF",
      subText: "+504"
    }]
  }, {
    letter: "J",
    data: [{
      text: "\u52A0\u52D2\u6BD4\u8377\u5170",
      subText: "+599"
    }, {
      text: "\u52A0\u7EB3",
      subText: "+233"
    }, {
      text: "\u52A0\u62FF\u5927",
      subText: "+1"
    }, {
      text: "\u67EC\u57D4\u5BE8",
      subText: "+855"
    }, {
      text: "\u52A0\u84EC",
      subText: "+241"
    }, {
      text: "\u5409\u5E03\u63D0",
      subText: "+253"
    }, {
      text: "\u6377\u514B\u5171\u548C\u56FD",
      subText: "+420"
    }, {
      text: "\u5409\u5C14\u5409\u65AF\u65AF\u5766",
      subText: "+996"
    }, {
      text: "\u57FA\u91CC\u5DF4\u65AF",
      subText: "+686"
    }, {
      text: "\u6D25\u5DF4\u5E03\u97E6",
      subText: "+263"
    }, {
      text: "\u51E0\u5185\u4E9A",
      subText: "+224"
    }, {
      text: "\u51E0\u5185\u4E9A\u6BD4\u7ECD",
      subText: "+245"
    }]
  }, {
    letter: "K",
    data: [{
      text: "\u5F00\u66FC\u7FA4\u5C9B",
      subText: "+1345"
    }, {
      text: "\u5580\u9EA6\u9686",
      subText: "+237"
    }, {
      text: "\u5361\u5854\u5C14",
      subText: "+974"
    }, {
      text: "\u79D1\u79D1\u65AF\u7FA4\u5C9B",
      subText: "+61"
    }, {
      text: "\u514B\u7F57\u5730\u4E9A",
      subText: "+385"
    }, {
      text: "\u79D1\u6469\u7F57",
      subText: "+269"
    }, {
      text: "\u80AF\u5C3C\u4E9A",
      subText: "+254"
    }, {
      text: "\u79D1\u7279\u8FEA\u74E6",
      subText: "+225"
    }, {
      text: "\u79D1\u5A01\u7279",
      subText: "+965"
    }, {
      text: "\u5E93\u514B\u7FA4\u5C9B",
      subText: "+682"
    }, {
      text: "\u5E93\u62C9\u7D22",
      subText: "+599"
    }]
  }, {
    letter: "L",
    data: [{
      text: "\u83B1\u7D22\u6258",
      subText: "+266"
    }, {
      text: "\u8001\u631D",
      subText: "+856"
    }, {
      text: "\u62C9\u8131\u7EF4\u4E9A",
      subText: "+371"
    }, {
      text: "\u9ECE\u5DF4\u5AE9",
      subText: "+961"
    }, {
      text: "\u5229\u6BD4\u91CC\u4E9A",
      subText: "+231"
    }, {
      text: "\u5229\u6BD4\u4E9A",
      subText: "+218"
    }, {
      text: "\u5217\u652F\u6566\u58EB\u767B",
      subText: "+423"
    }, {
      text: "\u7ACB\u9676\u5B9B",
      subText: "+370"
    }, {
      text: "\u7559\u5C3C\u65FA\u5C9B",
      subText: "+262"
    }, {
      text: "\u7F57\u9A6C\u5C3C\u4E9A",
      subText: "+40"
    }, {
      text: "\u5362\u68EE\u5821",
      subText: "+352"
    }, {
      text: "\u5362\u65FA\u8FBE",
      subText: "+250"
    }]
  }, {
    letter: "M",
    data: [{
      text: "\u9A6C\u8FBE\u52A0\u65AF\u52A0",
      subText: "+261"
    }, {
      text: "\u9A6C\u5C14\u4EE3\u592B",
      subText: "+960"
    }, {
      text: "\u9A6C\u8033\u4ED6",
      subText: "+356"
    }, {
      text: "\u9A6C\u6765\u897F\u4E9A",
      subText: "+60"
    }, {
      text: "\u9A6C\u62C9\u7EF4",
      subText: "+265"
    }, {
      text: "\u9A6C\u91CC",
      subText: "+223"
    }, {
      text: "\u6BDB\u91CC\u6C42\u65AF",
      subText: "+230"
    }, {
      text: "\u6BDB\u91CC\u5854\u5C3C\u4E9A",
      subText: "+222"
    }, {
      text: "\u9A6C\u5176\u987F",
      subText: "+389"
    }, {
      text: "\u9A6C\u7ECD\u5C14\u7FA4\u5C9B",
      subText: "+692"
    }, {
      text: "\u9A6C\u63D0\u5C3C\u514B\u7FA4\u5C9B",
      subText: "+596"
    }, {
      text: "\u9A6C\u7EA6\u7279",
      subText: "+262"
    }, {
      text: "\u7F8E\u56FD",
      subText: "+1"
    }, {
      text: "\u7F8E\u5C5E\u8428\u6469\u4E9A",
      subText: "+1684"
    }, {
      text: "\u7F8E\u5C5E\u7EF4\u4EAC\u7FA4\u5C9B",
      subText: "+1340"
    }, {
      text: "\u8499\u53E4",
      subText: "+976"
    }, {
      text: "\u5B5F\u52A0\u62C9",
      subText: "+880"
    }, {
      text: "\u8499\u7279\u8272\u62C9\u7279\u5C9B",
      subText: "+1664"
    }, {
      text: "\u7F05\u7538",
      subText: "+95"
    }, {
      text: "\u5BC6\u514B\u7F57\u5C3C\u897F\u4E9A",
      subText: "+691"
    }, {
      text: "\u79D8\u9C81",
      subText: "+51"
    }, {
      text: "\u6469\u5C14\u591A\u74E6",
      subText: "+373"
    }, {
      text: "\u6469\u6D1B\u54E5",
      subText: "+212"
    }, {
      text: "\u6469\u7EB3\u54E5",
      subText: "+377"
    }, {
      text: "\u83AB\u6851\u6BD4\u514B",
      subText: "+258"
    }, {
      text: "\u58A8\u897F\u54E5",
      subText: "+52"
    }]
  }, {
    letter: "N",
    data: [{
      text: "\u7EB3\u7C73\u6BD4\u4E9A",
      subText: "+264"
    }, {
      text: "\u5357\u975E",
      subText: "+27"
    }, {
      text: "\u5357\u82CF\u4E39",
      subText: "+211"
    }, {
      text: "\u7459\u9C81",
      subText: "+674"
    }, {
      text: "\u5C3C\u6CCA\u5C14",
      subText: "+977"
    }, {
      text: "\u5C3C\u52A0\u62C9\u74DC",
      subText: "+505"
    }, {
      text: "\u5C3C\u65E5\u5C14",
      subText: "+227"
    }, {
      text: "\u5C3C\u65E5\u5229\u4E9A",
      subText: "+234"
    }, {
      text: "\u7EBD\u57C3",
      subText: "+683"
    }, {
      text: "\u8BFA\u798F\u514B\u5C9B",
      subText: "+672"
    }, {
      text: "\u632A\u5A01",
      subText: "+47"
    }]
  }, {
    letter: "P",
    data: [{
      text: "\u5E15\u52B3\u7FA4\u5C9B",
      subText: "+680"
    }, {
      text: "\u8461\u8404\u7259",
      subText: "+351"
    }]
  }, {
    letter: "Q",
    data: [{
      text: "\u4E54\u6CBB\u4E9A",
      subText: "+995"
    }]
  }, {
    letter: "R",
    data: [{
      text: "\u65E5\u672C",
      subText: "+81"
    }, {
      text: "\u745E\u5178",
      subText: "+46"
    }, {
      text: "\u745E\u58EB",
      subText: "+41"
    }]
  }, {
    letter: "S",
    data: [, {
      text: "\u8428\u5C14\u74E6\u591A",
      subText: "+503"
    }, {
      text: "\u585E\u5C14\u7EF4\u4E9A",
      subText: "+381"
    }, {
      text: "\u585E\u62C9\u5229\u6602",
      subText: "+232"
    }, {
      text: "\u585E\u5185\u52A0\u5C14",
      subText: "+221"
    }, {
      text: "\u585E\u6D66\u8DEF\u65AF",
      subText: "+357"
    }, {
      text: "\u585E\u820C\u5C14",
      subText: "+248"
    }, {
      text: "\u8428\u6469\u4E9A",
      subText: "+685"
    }, {
      text: "\u6C99\u7279\u963F\u62C9\u4F2F",
      subText: "+966"
    }, {
      text: "\u5723\u5DF4\u6CF0\u52D2\u7C73",
      subText: "+590"
    }, {
      text: "\u5723\u8BDE\u5C9B",
      subText: "+61"
    }, {
      text: "\u5723\u591A\u7F8E\u4E0E\u666E\u6797\u5E0C",
      subText: "+239"
    }, {
      text: "\u5723\u8D6B\u52D2\u62FF",
      subText: "+290"
    }, {
      text: "\u5723\u57FA\u8328\u548C\u5C3C\u7EF4\u65AF",
      subText: "+1869"
    }, {
      text: "\u5723\u5362\u897F\u4E9A",
      subText: "+1758"
    }, {
      text: "\u5723\u9A6C\u4E01",
      subText: "+590"
    }, {
      text: "\u5723\u9A6C\u529B\u8BFA",
      subText: "+378"
    }, {
      text: "\u5723\u76AE\u57C3\u5C14\u548C\u5BC6\u514B\u9686",
      subText: "+508"
    }, {
      text: "\u5723\u6587\u68EE\u7279\u548C\u683C\u6797\u7EB3\u4E01\u65AF",
      subText: "+1784"
    }, {
      text: "\u65AF\u91CC\u5170\u5361",
      subText: "+94"
    }, {
      text: "\u65AF\u6D1B\u4F10\u514B",
      subText: "+421"
    }, {
      text: "\u65AF\u6D1B\u6587\u5C3C\u4E9A",
      subText: "+386"
    }, {
      text: "\u65AF\u74E6\u5C14\u5DF4\u7279\u548C\u626C\u9A6C\u5EF6",
      subText: "+47"
    }, {
      text: "\u65AF\u5A01\u58EB\u5170",
      subText: "+268"
    }, {
      text: "\u82CF\u4E39",
      subText: "+249"
    }, {
      text: "\u82CF\u91CC\u5357",
      subText: "+597"
    }, {
      text: "\u6240\u7F57\u95E8\u7FA4\u5C9B",
      subText: "+677"
    }, {
      text: "\u7D22\u9A6C\u91CC",
      subText: "+252"
    }]
  }, {
    letter: "T",
    data: [{
      text: "\u6CF0\u56FD",
      subText: "+66"
    }, {
      text: "\u5854\u5409\u514B\u65AF\u5766",
      subText: "+992"
    }, {
      text: "\u6C64\u52A0",
      subText: "+676"
    }, {
      text: "\u5766\u6851\u5C3C\u4E9A",
      subText: "+255"
    }, {
      text: "\u7279\u514B\u65AF\u548C\u51EF\u79D1\u65AF\u7FA4\u5C9B",
      subText: "+1649"
    }, {
      text: "\u7279\u7ACB\u5C3C\u8FBE\u548C\u591A\u5DF4\u54E5",
      subText: "+1868"
    }, {
      text: "\u7279\u91CC\u65AF\u5766-\u8FBE\u5E93\u5C3C\u4E9A\u7FA4\u5C9B",
      subText: "+290"
    }, {
      text: "\u571F\u8033\u5176",
      subText: "+90"
    }, {
      text: "\u571F\u5E93\u66FC\u65AF\u5766",
      subText: "+993"
    }, {
      text: "\u7A81\u5C3C\u65AF",
      subText: "+216"
    }, {
      text: "\u6258\u514B\u52B3",
      subText: "+690"
    }, {
      text: "\u56FE\u74E6\u5362",
      subText: "+688"
    }]
  }, {
    letter: "W",
    data: [{
      text: "\u74E6\u5229\u65AF\u7FA4\u5C9B\u548C\u5BCC\u56FE\u7EB3\u7FA4\u5C9B",
      subText: "+681"
    }, {
      text: "\u74E6\u52AA\u963F\u56FE",
      subText: "+678"
    }, {
      text: "\u5371\u5730\u9A6C\u62C9",
      subText: "+502"
    }, {
      text: "\u59D4\u5185\u745E\u62C9",
      subText: "+58"
    }, {
      text: "\u6587\u83B1",
      subText: "+673"
    }, {
      text: "\u4E4C\u5E72\u8FBE",
      subText: "+256"
    }, {
      text: "\u4E4C\u514B\u5170",
      subText: "+380"
    }, {
      text: "\u4E4C\u62C9\u572D",
      subText: "+598"
    }, {
      text: "\u4E4C\u5179\u522B\u514B\u65AF\u5766",
      subText: "+998"
    }]
  }, {
    letter: "X",
    data: [{
      text: "\u897F\u73ED\u7259",
      subText: "+34"
    }, {
      text: "\u5E0C\u814A",
      subText: "+30"
    }, {
      text: "\u65B0\u52A0\u5761",
      subText: "+65"
    }, {
      text: "\u65B0\u5580\u91CC\u591A\u5C3C\u4E9A",
      subText: "+687"
    }, {
      text: "\u65B0\u897F\u5170",
      subText: "+64"
    }, {
      text: "\u5308\u7259\u5229",
      subText: "+36"
    }, {
      text: "\u897F\u6492\u54C8\u62C9",
      subText: "+212"
    }, {
      text: "\u53D9\u5229\u4E9A",
      subText: "+963"
    }]
  }, {
    letter: "Y",
    data: [{
      text: "\u7259\u4E70\u52A0",
      subText: "+1876"
    }, {
      text: "\u4E9A\u7F8E\u5C3C\u4E9A",
      subText: "+374"
    }, {
      text: "\u4E5F\u95E8",
      subText: "+967"
    }, {
      text: "\u610F\u5927\u5229",
      subText: "+39"
    }, {
      text: "\u4F0A\u62C9\u514B",
      subText: "+964"
    }, {
      text: "\u4F0A\u6717",
      subText: "+98"
    }, {
      text: "\u5370\u5EA6",
      subText: "+91"
    }, {
      text: "\u5370\u5EA6\u5C3C\u897F\u4E9A",
      subText: "+62"
    }, {
      text: "\u82F1\u56FD",
      subText: "+44"
    }, {
      text: "\u82F1\u56FD\u5C5E\u5730\u66FC\u5C9B",
      subText: "+44"
    }, {
      text: "\u82F1\u5C5E\u7EF4\u5C14\u4EAC\u7FA4\u5C9B",
      subText: "+1284"
    }, {
      text: "\u82F1\u5C5E\u5370\u5EA6\u6D0B\u9886\u5730",
      subText: "+246"
    }, {
      text: "\u4EE5\u8272\u5217",
      subText: "+972"
    }, {
      text: "\u7EA6\u65E6",
      subText: "+962"
    }, {
      text: "\u8D8A\u5357",
      subText: "+84"
    }]
  }, {
    letter: "Z",
    data: [{
      text: "\u8D5E\u6BD4\u4E9A",
      subText: "+260"
    }, {
      text: "\u6CFD\u897F\u5C9B",
      subText: "+44"
    }, {
      text: "\u4E4D\u5F97",
      subText: "+235"
    }, {
      text: "\u76F4\u5E03\u7F57\u9640",
      subText: "+350"
    }, {
      text: "\u667A\u5229",
      subText: "+56"
    }, {
      text: "\u4E2D\u975E\u5171\u548C\u56FD",
      subText: "+236"
    }, {
      text: "\u4E2D\u56FD\u5927\u9646",
      subText: "+86"
    }, {
      text: "\u4E2D\u56FD\u6FB3\u95E8",
      subText: "+853"
    }, {
      text: "\u4E2D\u56FD\u53F0\u6E7E",
      subText: "+886"
    }, {
      text: "\u4E2D\u56FD\u9999\u6E2F",
      subText: "+852"
    }]
  }];
  var _style_02 = { "fui-divider": { "": { "flex": 1, "paddingBottom": "64rpx" } } };
  var _sfc_main2 = {
    data() {
      return {
        lists,
        show: false
      };
    },
    onLoad() {
    },
    methods: __spreadProps(__spreadValues({}, (0, import_vuex.mapMutations)(["setAreaCode"])), {
      init() {
        this.show = true;
      },
      itemClick(e) {
        formatAppLog("log", "at pages/areaCode/areaCode.nvue:34", e);
        this.setAreaCode(e.subText);
        setTimeout(() => {
          uni.navigateBack();
        }, 50);
      },
      search(e) {
        uni.showToast({
          title: `\u641C\u7D22\u5173\u952E\u8BCD\uFF1A${e.detail.value}`,
          icon: "none"
        });
      }
    })
  };
  function _sfc_render2(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_fui_search_bar = resolveEasycom((0, import_vue3.resolveDynamicComponent)("fui-search-bar"), __easycom_0);
    const _component_fui_loadmore = resolveEasycom((0, import_vue3.resolveDynamicComponent)("fui-loadmore"), __easycom_2);
    const _component_fui_divider = resolveEasycom((0, import_vue3.resolveDynamicComponent)("fui-divider"), __easycom_22);
    const _component_fui_index_list = resolveEasycom((0, import_vue3.resolveDynamicComponent)("fui-index-list"), __easycom_4);
    return (0, import_vue3.openBlock)(), (0, import_vue3.createElementBlock)("scroll-view", {
      scrollY: true,
      showScrollbar: true,
      enableBackToTop: true,
      bubble: "true",
      style: { flexDirection: "column" }
    }, [
      (0, import_vue3.createVNode)(_component_fui_index_list, {
        background: "#FAFAFA",
        listData: $data.lists,
        keyColor: "#555",
        onInit: $options.init,
        onClick: $options.itemClick
      }, {
        footer: (0, import_vue3.withCtx)(() => [
          !$data.show ? ((0, import_vue3.openBlock)(), (0, import_vue3.createBlock)(_component_fui_loadmore, { key: 0 })) : (0, import_vue3.createCommentVNode)("v-if", true),
          $data.show ? ((0, import_vue3.openBlock)(), (0, import_vue3.createBlock)(_component_fui_divider, {
            key: 1,
            text: "\u6B64\u6570\u636E\u7531firstui.cn\u63D0\u4F9B",
            backgroundColor: "#fff"
          })) : (0, import_vue3.createCommentVNode)("v-if", true)
        ]),
        default: (0, import_vue3.withCtx)(() => [
          (0, import_vue3.createVNode)(_component_fui_search_bar, {
            background: "#FAFAFA",
            onSearch: $options.search
          }, null, 8, ["onSearch"])
        ]),
        _: 1
        /* STABLE */
      }, 8, ["listData", "onInit", "onClick"])
    ]);
  }
  var areaCode = /* @__PURE__ */ _export_sfc(_sfc_main2, [["render", _sfc_render2], ["styles", [_style_02]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/pages/areaCode/areaCode.nvue"]]);

  // <stdin>
  var webview = plus.webview.currentWebview();
  if (webview) {
    const __pageId = parseInt(webview.id);
    const __pagePath = "pages/areaCode/areaCode";
    let __pageQuery = {};
    try {
      __pageQuery = JSON.parse(webview.__query__);
    } catch (e) {
    }
    areaCode.mpType = "page";
    const app = Vue.createPageApp(areaCode, { $store: getApp({ allowDefault: true }).$store, __pageId, __pagePath, __pageQuery });
    app.provide("__globalStyles", Vue.useCssStyles([...__uniConfig.styles, ...areaCode.styles || []]));
    app.mount("#root");
  }
})();
