"use weex:vue";

if (typeof Promise !== 'undefined' && !Promise.prototype.finally) {
  Promise.prototype.finally = function(callback) {
    const promise = this.constructor
    return this.then(
      value => promise.resolve(callback()).then(() => value),
      reason => promise.resolve(callback()).then(() => {
        throw reason
      })
    )
  }
};

if (typeof uni !== 'undefined' && uni && uni.requireGlobal) {
  const global = uni.requireGlobal()
  ArrayBuffer = global.ArrayBuffer
  Int8Array = global.Int8Array
  Uint8Array = global.Uint8Array
  Uint8ClampedArray = global.Uint8ClampedArray
  Int16Array = global.Int16Array
  Uint16Array = global.Uint16Array
  Int32Array = global.Int32Array
  Uint32Array = global.Uint32Array
  Float32Array = global.Float32Array
  Float64Array = global.Float64Array
  BigInt64Array = global.BigInt64Array
  BigUint64Array = global.BigUint64Array
};


(() => {
  var __create = Object.create;
  var __defProp = Object.defineProperty;
  var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
  var __getOwnPropNames = Object.getOwnPropertyNames;
  var __getProtoOf = Object.getPrototypeOf;
  var __hasOwnProp = Object.prototype.hasOwnProperty;
  var __commonJS = (cb, mod) => function __require() {
    return mod || (0, cb[__getOwnPropNames(cb)[0]])((mod = { exports: {} }).exports, mod), mod.exports;
  };
  var __copyProps = (to, from, except, desc) => {
    if (from && typeof from === "object" || typeof from === "function") {
      for (let key of __getOwnPropNames(from))
        if (!__hasOwnProp.call(to, key) && key !== except)
          __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
    }
    return to;
  };
  var __toESM = (mod, isNodeMode, target) => (target = mod != null ? __create(__getProtoOf(mod)) : {}, __copyProps(
    // If the importer is in node compatibility mode or this is not an ESM
    // file that has been converted to a CommonJS file using a Babel-
    // compatible transform (i.e. "__esModule" has not been set), then set
    // "default" to the CommonJS "module.exports" for node compatibility.
    isNodeMode || !mod || !mod.__esModule ? __defProp(target, "default", { value: mod, enumerable: true }) : target,
    mod
  ));

  // vue-ns:vue
  var require_vue = __commonJS({
    "vue-ns:vue"(exports, module) {
      module.exports = Vue;
    }
  });

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/uni-app.es.js
  var import_vue = __toESM(require_vue());
  function requireNativePlugin(name) {
    return weex.requireModule(name);
  }
  function formatAppLog(type, filename, ...args) {
    if (uni.__log__) {
      uni.__log__(type, filename, ...args);
    } else {
      console[type].apply(console, [...args, filename]);
    }
  }

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/pages/demo/demo.js
  var import_vue2 = __toESM(require_vue());

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/_plugin-vue_export-helper.js
  var _export_sfc = (sfc, props) => {
    const target = sfc.__vccOpts || sfc;
    for (const [key, val] of props) {
      target[key] = val;
    }
    return target;
  };

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/pages/demo/demo.js
  var mp = requireNativePlugin("uniMP");
  var _sfc_main = {
    data() {
      return {
        title: "uni\u5C0F\u7A0B\u5E8F\u6F14\u793A",
        appid: "__UNI__43CDF4D",
        wgt: "/static/__UNI__43CDF4D.wgt"
      };
    },
    onLoad() {
      if (!mp) {
        this.title = "\u5F53\u524D\u73AF\u5883\u4E0D\u652F\u6301uni\u5C0F\u7A0B\u5E8F";
      }
    },
    methods: {
      installMP() {
        mp.getUniMPVersion(this.appid, (ret) => {
          if (0 != ret.code) {
            this.doInstallMP();
          } else {
            uni.showModal({
              title: "\u63D0\u793A",
              content: "uni\u5C0F\u7A0B\u5E8F\u5DF2\u5B89\u88C5\uFF0C\u662F\u5426\u8986\u76D6\uFF1F",
              success: (res) => {
                res.confirm && this.doInstallMP();
              }
            });
          }
          formatAppLog("log", "at pages/demo/demo.nvue:47", "getUniMPVersion: " + JSON.stringify(ret));
        });
      },
      doInstallMP() {
        mp.installUniMP({
          appid: this.appid,
          wgtFile: this.wgt
        }, (r) => {
          if (0 == r.code) {
            uni.showToast({
              title: "\u5B89\u88C5\u6210\u529F"
            });
          } else {
            uni.showModal({
              title: "\u5B89\u88C5\u5931\u8D25",
              content: JSON.stringify(r),
              showCancel: false
            });
          }
          formatAppLog("log", "at pages/demo/demo.nvue:66", "installUniMP: " + JSON.stringify(r));
        });
      },
      openMP() {
        mp.openUniMP({
          appid: this.appid
        }, (ret) => {
          if (0 != ret.code) {
            uni.showModal({
              title: "\u542F\u52A8\u5931\u8D25",
              content: JSON.stringify(ret),
              showCancel: false
            });
          }
          formatAppLog("log", "at pages/demo/demo.nvue:80", "openUniMP: " + JSON.stringify(ret));
        });
      },
      openMP2Path() {
        mp.openUniMP({
          appid: this.appid,
          path: "pages/component/view/view"
        }, (ret) => {
          if (0 != ret.code) {
            uni.showModal({
              title: "\u542F\u52A8\u5931\u8D25",
              content: JSON.stringify(ret),
              showCancel: false
            });
          }
          formatAppLog("log", "at pages/demo/demo.nvue:95", "openUniMP: " + JSON.stringify(ret));
        });
      },
      openMP2Extra() {
        mp.openUniMP({
          appid: this.appid,
          extraData: {
            name: "my name",
            id: "my id"
          }
        }, (ret) => {
          if (0 != ret.code) {
            uni.showModal({
              title: "\u542F\u52A8\u5931\u8D25",
              content: JSON.stringify(ret),
              showCancel: false
            });
          }
          formatAppLog("log", "at pages/demo/demo.nvue:113", "openUniMP: " + JSON.stringify(ret));
        });
      },
      sendEvent() {
        mp.sendUniMPEvent(
          this.appid,
          "event",
          {
            "key": "value",
            "name": "data"
          },
          (ret) => {
            formatAppLog("log", "at pages/demo/demo.nvue:124", "Host sendEvent: " + JSON.stringify(ret));
          }
        );
      },
      sendEventInterval() {
        setInterval(this.sendEvent, 5e3);
      }
    }
  };
  function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_button = (0, import_vue2.resolveComponent)("button");
    return (0, import_vue2.openBlock)(), (0, import_vue2.createElementBlock)("scroll-view", {
      scrollY: true,
      showScrollbar: true,
      enableBackToTop: true,
      bubble: "true",
      style: { flexDirection: "column" }
    }, [
      (0, import_vue2.createElementVNode)("view", { class: "content" }, [
        (0, import_vue2.createElementVNode)("u-image", {
          class: "logo",
          src: "/static/logo.png"
        }),
        (0, import_vue2.createElementVNode)("view", { class: "text-area" }, [
          (0, import_vue2.createElementVNode)(
            "u-text",
            { class: "title" },
            (0, import_vue2.toDisplayString)($data.title),
            1
            /* TEXT */
          )
        ]),
        (0, import_vue2.createElementVNode)("view", { class: "wrap" }, [
          (0, import_vue2.createVNode)(_component_button, {
            type: "primary",
            onClick: _cache[0] || (_cache[0] = ($event) => $options.installMP())
          }, {
            default: (0, import_vue2.withCtx)(() => [
              (0, import_vue2.createTextVNode)("\u5B89\u88C5Hello uni-app\u5C0F\u7A0B\u5E8F")
            ]),
            _: 1
            /* STABLE */
          }),
          (0, import_vue2.createVNode)(_component_button, {
            type: "primary",
            onClick: _cache[1] || (_cache[1] = ($event) => $options.openMP())
          }, {
            default: (0, import_vue2.withCtx)(() => [
              (0, import_vue2.createTextVNode)("\u542F\u52A8Hello uni-app\u5C0F\u7A0B\u5E8F")
            ]),
            _: 1
            /* STABLE */
          }),
          (0, import_vue2.createVNode)(_component_button, {
            type: "primary",
            onClick: _cache[2] || (_cache[2] = ($event) => $options.openMP2Path())
          }, {
            default: (0, import_vue2.withCtx)(() => [
              (0, import_vue2.createTextVNode)("\u542F\u52A8Hello uni-app\u5C0F\u7A0B\u5E8F\u76F4\u8FBE\u9875\u9762")
            ]),
            _: 1
            /* STABLE */
          }),
          (0, import_vue2.createVNode)(_component_button, {
            type: "primary",
            onClick: _cache[3] || (_cache[3] = ($event) => $options.openMP2Extra())
          }, {
            default: (0, import_vue2.withCtx)(() => [
              (0, import_vue2.createTextVNode)("\u542F\u52A8Hello uni-app\u5C0F\u7A0B\u5E8F\u4F20\u9012\u53C2\u6570")
            ]),
            _: 1
            /* STABLE */
          }),
          (0, import_vue2.createVNode)(_component_button, {
            type: "primary",
            onClick: _cache[4] || (_cache[4] = ($event) => $options.sendEvent())
          }, {
            default: (0, import_vue2.withCtx)(() => [
              (0, import_vue2.createTextVNode)("\u5411Hello uni-app\u5C0F\u7A0B\u5E8F\u53D1\u9001\u4E8B\u4EF6")
            ]),
            _: 1
            /* STABLE */
          }),
          (0, import_vue2.createVNode)(_component_button, {
            type: "primary",
            onClick: _cache[5] || (_cache[5] = ($event) => $options.sendEventInterval())
          }, {
            default: (0, import_vue2.withCtx)(() => [
              (0, import_vue2.createTextVNode)("\u5B9A\u65F6\u5411Hello uni-app\u5C0F\u7A0B\u5E8F\u53D1\u9001\u4E8B\u4EF6")
            ]),
            _: 1
            /* STABLE */
          })
        ])
      ])
    ]);
  }
  var demo = /* @__PURE__ */ _export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/pages/demo/demo.nvue"]]);

  // <stdin>
  var webview = plus.webview.currentWebview();
  if (webview) {
    const __pageId = parseInt(webview.id);
    const __pagePath = "pages/demo/demo";
    let __pageQuery = {};
    try {
      __pageQuery = JSON.parse(webview.__query__);
    } catch (e) {
    }
    demo.mpType = "page";
    const app = Vue.createPageApp(demo, { $store: getApp({ allowDefault: true }).$store, __pageId, __pagePath, __pageQuery });
    app.provide("__globalStyles", Vue.useCssStyles([...__uniConfig.styles, ...demo.styles || []]));
    app.mount("#root");
  }
})();
