"use weex:vue";

if (typeof Promise !== 'undefined' && !Promise.prototype.finally) {
  Promise.prototype.finally = function(callback) {
    const promise = this.constructor
    return this.then(
      value => promise.resolve(callback()).then(() => value),
      reason => promise.resolve(callback()).then(() => {
        throw reason
      })
    )
  }
};

if (typeof uni !== 'undefined' && uni && uni.requireGlobal) {
  const global = uni.requireGlobal()
  ArrayBuffer = global.ArrayBuffer
  Int8Array = global.Int8Array
  Uint8Array = global.Uint8Array
  Uint8ClampedArray = global.Uint8ClampedArray
  Int16Array = global.Int16Array
  Uint16Array = global.Uint16Array
  Int32Array = global.Int32Array
  Uint32Array = global.Uint32Array
  Float32Array = global.Float32Array
  Float64Array = global.Float64Array
  BigInt64Array = global.BigInt64Array
  BigUint64Array = global.BigUint64Array
};


(() => {
  var __create = Object.create;
  var __defProp = Object.defineProperty;
  var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
  var __getOwnPropNames = Object.getOwnPropertyNames;
  var __getProtoOf = Object.getPrototypeOf;
  var __hasOwnProp = Object.prototype.hasOwnProperty;
  var __commonJS = (cb, mod) => function __require() {
    return mod || (0, cb[__getOwnPropNames(cb)[0]])((mod = { exports: {} }).exports, mod), mod.exports;
  };
  var __copyProps = (to, from, except, desc) => {
    if (from && typeof from === "object" || typeof from === "function") {
      for (let key of __getOwnPropNames(from))
        if (!__hasOwnProp.call(to, key) && key !== except)
          __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
    }
    return to;
  };
  var __toESM = (mod, isNodeMode, target) => (target = mod != null ? __create(__getProtoOf(mod)) : {}, __copyProps(
    // If the importer is in node compatibility mode or this is not an ESM
    // file that has been converted to a CommonJS file using a Babel-
    // compatible transform (i.e. "__esModule" has not been set), then set
    // "default" to the CommonJS "module.exports" for node compatibility.
    isNodeMode || !mod || !mod.__esModule ? __defProp(target, "default", { value: mod, enumerable: true }) : target,
    mod
  ));

  // vue-ns:vue
  var require_vue = __commonJS({
    "vue-ns:vue"(exports, module) {
      module.exports = Vue;
    }
  });

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/fui-avatar.js
  var import_vue = __toESM(require_vue());

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/_plugin-vue_export-helper.js
  var _export_sfc = (sfc, props) => {
    const target = sfc.__vccOpts || sfc;
    for (const [key, val] of props) {
      target[key] = val;
    }
    return target;
  };

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/fui-avatar.js
  var _style_0 = { "fui-avatar__wrap": { "": { "position": "relative", "flexDirection": "row", "alignItems": "center", "justifyContent": "center" } }, "fui-avatar__img": { "": { "flex": 1 } }, "fui-avatar__text": { "": { "flex": 1, "lines": 1, "overflow": "hidden", "textOverflow": "ellipsis", "textAlign": "center" } }, "fui-avatar__size-small": { "": { "!width": "64rpx", "!height": "64rpx" } }, "fui-avatar__text-small": { "": { "!fontSize": "32rpx" } }, "fui-avatar__size-middle": { "": { "!width": "96rpx", "!height": "96rpx" } }, "fui-avatar__text-middle": { "": { "!fontSize": "44rpx" } }, "fui-avatar__size-large": { "": { "!width": "128rpx", "!height": "128rpx" } }, "fui-avatar__text-large": { "": { "!fontSize": "56rpx" } }, "fui-avatar__circle": { "": { "!borderRadius": 500 } }, "fui-avatar__square": { "": { "!borderRadius": "8rpx" } } };
  var _sfc_main = {
    name: "fui-avatar",
    emits: ["click", "error"],
    props: {
      src: {
        type: String,
        default: ""
      },
      errorSrc: {
        type: String,
        default: ""
      },
      mode: {
        type: String,
        default: "scaleToFill"
      },
      //微信小程序、百度小程序、字节跳动小程序
      //图片懒加载。只针对page与scroll-view下的image有效
      lazyLoad: {
        type: Boolean,
        default: true
      },
      //默认不解析 webP 格式，只支持网络资源 微信小程序2.9.0
      webp: {
        type: Boolean,
        default: false
      },
      background: {
        type: String,
        default: "#D1D1D1"
      },
      //small（64）、middle（96）、large（128）
      size: {
        type: String,
        default: "middle"
      },
      //图片宽度，设置大于0的数值生效，默认使用size
      width: {
        type: [Number, String],
        default: 0
      },
      //默认等宽，设置图大于0的数值且设置了图片宽度生效
      height: {
        type: [Number, String],
        default: 0
      },
      //指定头像的形状，可选值为 circle、square
      shape: {
        type: String,
        default: "circle"
      },
      //图片圆角值，默认使用shape，当设置大于等于0的数值，shape失效
      radius: {
        type: [Number, String],
        default: -1
      },
      //没有src时可以使用文本代替
      text: {
        type: String,
        default: ""
      },
      color: {
        type: String,
        default: "#fff"
      },
      //默认使用size下字体大小
      fontSize: {
        type: [Number, String],
        default: 0
      },
      fontWeight: {
        type: [Number, String],
        default: 600
      },
      marginRight: {
        type: [Number, String],
        default: 0
      },
      marginBottom: {
        type: [Number, String],
        default: 0
      },
      //在列表中的索引值
      index: {
        type: Number,
        default: 0
      },
      //其他参数
      params: {
        type: [Number, String],
        default: 0
      }
    },
    computed: {
      styles() {
        let styles = "";
        if (this.width) {
          styles = `width:${this.width}rpx;height:${this.height || this.width}rpx;`;
        }
        if (this.radius !== -1) {
          styles += `border-radius:${this.radius}rpx;`;
        }
        return styles;
      },
      wrapStyles() {
        return `background:${this.background};margin-right:${this.marginRight}rpx;margin-bottom:${this.marginBottom}rpx;${this.styles};`;
      },
      textStyles() {
        let styles = `color:${this.color};font-weight:${this.fontWeight};`;
        if (this.fontSize) {
          styles += `font-size:${this.fontSize}rpx;`;
        }
        return styles;
      }
    },
    watch: {
      src(val) {
        this.src && (this.showImg = this.src);
      }
    },
    data() {
      return {
        showImg: ""
      };
    },
    created() {
      this.src && (this.showImg = this.src);
    },
    methods: {
      handleError(e) {
        if (this.src) {
          this.errorSrc && (this.showImg = this.errorSrc);
          this.$emit("error", {
            index: this.index,
            params: this.params
          });
        }
      },
      handleClick() {
        this.$emit("click", {
          index: this.index,
          params: this.params
        });
      }
    }
  };
  function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
    return (0, import_vue.openBlock)(), (0, import_vue.createElementBlock)(
      import_vue.Fragment,
      null,
      [
        (0, import_vue.createCommentVNode)("\u672C\u6587\u4EF6\u7531FirstUI\u6388\u6743\u4E88\u4E25\u5929\u8D60\uFF08\u4F1A\u5458ID\uFF1A244   2\uFF0C\u8EAB\u4EFD\u8BC1\u5C3E\u53F7\uFF1A04   3  018\uFF09\u4E13\u7528\uFF0C\u8BF7\u5C0A\u91CD\u77E5\u8BC6\u4EA7\u6743\uFF0C\u52FF\u79C1\u4E0B\u4F20\u64AD\uFF0C\u8FDD\u8005\u8FFD\u7A76\u6CD5\u5F8B\u8D23\u4EFB\u3002"),
        (0, import_vue.createElementVNode)(
          "view",
          {
            class: (0, import_vue.normalizeClass)(["fui-avatar__wrap", [$props.width ? "" : "fui-avatar__size-" + $props.size, $props.radius === -1 ? "fui-avatar__" + $props.shape : ""]]),
            style: (0, import_vue.normalizeStyle)($options.wrapStyles),
            onClick: _cache[1] || (_cache[1] = (...args) => $options.handleClick && $options.handleClick(...args))
          },
          [
            $props.src && $props.src !== true ? ((0, import_vue.openBlock)(), (0, import_vue.createElementBlock)("u-image", {
              key: 0,
              class: (0, import_vue.normalizeClass)(["fui-avatar__img", [$props.radius === -1 ? "fui-avatar__" + $props.shape : "", $props.width ? "" : "fui-avatar__size-" + $props.size]]),
              style: (0, import_vue.normalizeStyle)($options.styles),
              src: $data.showImg,
              mode: $props.mode,
              webp: $props.webp,
              lazyLoad: $props.lazyLoad,
              onError: _cache[0] || (_cache[0] = (...args) => $options.handleError && $options.handleError(...args))
            }, null, 46, ["src", "mode", "webp", "lazyLoad"])) : (0, import_vue.createCommentVNode)("v-if", true),
            !$props.src && $props.src !== true && $props.text ? ((0, import_vue.openBlock)(), (0, import_vue.createElementBlock)(
              "u-text",
              {
                key: 1,
                class: (0, import_vue.normalizeClass)(["fui-avatar__text", [$props.width ? "" : "fui-avatar__text-" + $props.size]]),
                style: (0, import_vue.normalizeStyle)($options.textStyles)
              },
              (0, import_vue.toDisplayString)($props.text),
              7
              /* TEXT, CLASS, STYLE */
            )) : (0, import_vue.createCommentVNode)("v-if", true),
            (0, import_vue.renderSlot)(_ctx.$slots, "default")
          ],
          6
          /* CLASS, STYLE */
        )
      ],
      2112
      /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
    );
  }
  var __easycom_0 = /* @__PURE__ */ _export_sfc(_sfc_main, [["render", _sfc_render], ["styles", [_style_0]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-avatar/fui-avatar.vue"]]);

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/pages/chat/chat.js
  var import_vue4 = __toESM(require_vue());

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/uni-app.es.js
  var import_vue2 = __toESM(require_vue());
  var isString = (val) => typeof val === "string";
  function requireNativePlugin(name) {
    return weex.requireModule(name);
  }
  function resolveEasycom(component, easycom) {
    return isString(component) ? easycom : component;
  }

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/fui-icon.js
  var import_vue3 = __toESM(require_vue());
  var icons = {
    "addressbook": "\uE80C",
    "addfriends-fill": "\uE80A",
    "addfriends": "\uE80B",
    "backspace-fill": "\uE808",
    "backspace": "\uE809",
    "bankcard-fill": "\uE806",
    "bankcard": "\uE807",
    "camera-fill": "\uE804",
    "camera": "\uE805",
    "captcha-fill": "\uE802",
    "captcha": "\uE803",
    "cart-fill": "\uE800",
    "cart": "\uE801",
    "classify": "\uE7FE",
    "classify-fill": "\uE7FF",
    "comment-fill": "\uE7FC",
    "comment": "\uE7FD",
    "community-fill": "\uE7FA",
    "community": "\uE7FB",
    "coupon-fill": "\uE7F8",
    "coupon": "\uE7F9",
    "delete": "\uE7F6",
    "delete-fill": "\uE7F7",
    "edit": "\uE7F4",
    "edit-fill": "\uE7F5",
    "fabulous-fill": "\uE7F2",
    "fabulous": "\uE7F3",
    "find": "\uE7F0",
    "find-fill": "\uE7F1",
    "help-fill": "\uE7EE",
    "help": "\uE7EF",
    "home-fill": "\uE7EC",
    "home": "\uE7ED",
    "idcard-fill": "\uE7EA",
    "idcard": "\uE7EB",
    "info": "\uE7E8",
    "info-fill": "\uE7E9",
    "invite-fill": "\uE7E6",
    "invite": "\uE7E7",
    "kefu-fill": "\uE7E4",
    "kefu": "\uE7E5",
    "like-fill": "\uE7E2",
    "like": "\uE7E3",
    "location": "\uE7E0",
    "location-fill": "\uE7E1",
    "lock": "\uE7DE",
    "lock-fill": "\uE7DF",
    "mail-fill": "\uE7DC",
    "mail": "\uE7DD",
    "message": "\uE7DA",
    "message-fill": "\uE7DB",
    "mobile-fill": "\uE7D8",
    "mobile": "\uE7D9",
    "more": "\uE7D6",
    "more-fill": "\uE7D7",
    "my-fill": "\uE7D4",
    "my": "\uE7D5",
    "principal": "\uE80D",
    "notice-fill": "\uE7D2",
    "notice": "\uE7D3",
    "order": "\uE7D0",
    "order-fill": "\uE7D1",
    "picture": "\uE7CE",
    "picture-fill": "\uE7CF",
    "setup-fill": "\uE7CC",
    "setup": "\uE7CD",
    "share": "\uE7CA",
    "share-fill": "\uE7CB",
    "shop": "\uE7C8",
    "shop-fill": "\uE7C9",
    "star-fill": "\uE7C5",
    "star": "\uE7C6",
    "starhalf": "\uE7C7",
    "stepon-fill": "\uE7C3",
    "stepon": "\uE7C4",
    "wait-fill": "\uE7C1",
    "wait": "\uE7C2",
    "warning": "\uE7BF",
    "warning-fill": "\uE7C0",
    "plus": "\uE7BC",
    "plussign-fill": "\uE7BD",
    "plussign": "\uE7BE",
    "minus": "\uE7B9",
    "minussign": "\uE7BA",
    "minussign-fill": "\uE7BB",
    "close": "\uE7B8",
    "clear": "\uE7B6",
    "clear-fill": "\uE7B7",
    "checkbox-fill": "\uE7B5",
    "checkround": "\uE7B4",
    "checkbox": "\uE7B3",
    "check": "\uE7B2",
    "pulldown-fill": "\uE7AE",
    "pullup": "\uE7AF",
    "pullup-fill": "\uE7B0",
    "pulldown": "\uE7B1",
    "roundright-fill": "\uE7AC",
    "roundright": "\uE7AD",
    "arrowright": "\uE7A9",
    "arrowleft": "\uE7AA",
    "arrowdown": "\uE7AB",
    "left": "\uE7A6",
    "up": "\uE7A7",
    "right": "\uE7A8",
    "back": "\uE7A3",
    "top": "\uE7A4",
    "dropdown": "\uE7A5",
    "turningleft": "\uE79F",
    "turningup": "\uE7A0",
    "turningright": "\uE7A1",
    "turningdown": "\uE7A2",
    "refresh": "\uE79C",
    "loading": "\uE79D",
    "search": "\uE79E",
    "rotate": "\uE79B",
    "screen": "\uE79A",
    "signin": "\uE799",
    "calendar": "\uE798",
    "scan": "\uE797",
    "qrcode": "\uE796",
    "wallet": "\uE795",
    "telephone": "\uE794",
    "visible": "\uE793",
    "invisible": "\uE792",
    "menu": "\uE78E",
    "operate": "\uE78F",
    "slide": "\uE790",
    "list": "\uE791",
    "nonetwork": "\uE78D",
    "partake": "\uE78C",
    "qa": "\uE78B",
    "barchart": "\uE788",
    "piechart": "\uE789",
    "linechart": "\uE78A",
    "at": "\uE787",
    "face": "\uE77F",
    "redpacket": "\uE780",
    "suspend": "\uE781",
    "link": "\uE782",
    "keyboard": "\uE783",
    "play": "\uE784",
    "video": "\uE785",
    "voice": "\uE786",
    "sina": "\uE77A",
    "browser": "\uE77B",
    "moments": "\uE77C",
    "qq": "\uE77D",
    "wechat": "\uE77E",
    "balance": "\uE779",
    "bankcardpay": "\uE778",
    "wxpay": "\uE777",
    "alipay": "\uE776",
    "payment": "\uE818",
    "receive": "\uE817",
    "sendout": "\uE816",
    "evaluate": "\uE815",
    "aftersale": "\uE814",
    "warehouse": "\uE813",
    "transport": "\uE812",
    "delivery": "\uE811",
    "switch": "\uE810",
    "goods": "\uE80F",
    "goods-fill": "\uE80E"
  };
  var fuiicons = "/assets/fui-icon.9165208c.ttf";
  var _style_02 = { "fui-icon": { "": { "fontFamily": "fuiFont", "textDecoration": "none", "textAlign": "center" } } };
  var domModule = weex.requireModule("dom");
  domModule.addRule("fontFace", {
    "fontFamily": "fuiFont",
    "src": "url('" + fuiicons + "')"
  });
  var _sfc_main2 = {
    name: "fui-icon",
    emits: ["click"],
    props: {
      name: {
        type: String,
        default: ""
      },
      size: {
        type: [Number, String],
        default: 0
      },
      //rpx | px
      unit: {
        type: String,
        default: ""
      },
      color: {
        type: String,
        default: ""
      },
      //字重
      fontWeight: {
        type: [Number, String],
        default: "normal"
      },
      //是否禁用点击
      disabled: {
        type: Boolean,
        default: false
      },
      params: {
        type: [Number, String],
        default: 0
      },
      customPrefix: {
        type: String,
        default: ""
      },
      //是否显示为主色调，color为空时有效。【内部使用】
      primary: {
        type: Boolean,
        default: false
      }
    },
    computed: {
      getSize() {
        const size = uni.$fui && uni.$fui.fuiIcon && uni.$fui.fuiIcon.size || 64;
        const unit = uni.$fui && uni.$fui.fuiIcon && uni.$fui.fuiIcon.unit || "rpx";
        return (this.size || size) + (this.unit || unit);
      },
      primaryColor() {
        const app = uni && uni.$fui && uni.$fui.color;
        return app && app.primary || "#465CFF";
      },
      getColor() {
        const app = uni && uni.$fui && uni.$fui.fuiIcon;
        let color = this.color;
        if (!color || color && color === true) {
          color = app && app.color;
        }
        if (!color || color === true) {
          color = "#333333";
        }
        return color;
      }
    },
    data() {
      return {
        icons
      };
    },
    methods: {
      handleClick() {
        if (this.disabled)
          return;
        this.$emit("click", {
          params: this.params
        });
      }
    }
  };
  function _sfc_render2(_ctx, _cache, $props, $setup, $data, $options) {
    return (0, import_vue3.openBlock)(), (0, import_vue3.createElementBlock)(
      import_vue3.Fragment,
      null,
      [
        (0, import_vue3.createCommentVNode)("\u672C\u6587\u4EF6\u7531FirstUI\u6388\u6743\u4E88\u4E25\u5929\u8D60\uFF08\u4F1A\u5458ID\uFF1A24   42\uFF0C\u8EAB\u4EFD\u8BC1\u5C3E\u53F7\uFF1A 04  3 01 8\uFF09\u4E13\u7528\uFF0C\u8BF7\u5C0A\u91CD\u77E5\u8BC6\u4EA7\u6743\uFF0C\u52FF\u79C1\u4E0B\u4F20\u64AD\uFF0C\u8FDD\u8005\u8FFD\u7A76\u6CD5\u5F8B\u8D23\u4EFB\u3002"),
        (0, import_vue3.createElementVNode)(
          "u-text",
          {
            style: (0, import_vue3.normalizeStyle)({ color: $props.primary && (!$props.color || $props.color === true) ? $options.primaryColor : $options.getColor, fontSize: $options.getSize, lineHeight: $options.getSize, fontWeight: $props.fontWeight }),
            class: (0, import_vue3.normalizeClass)(["fui-icon", [$props.customPrefix && $props.customPrefix !== true ? $props.customPrefix : ""]]),
            onClick: _cache[0] || (_cache[0] = (...args) => $options.handleClick && $options.handleClick(...args))
          },
          (0, import_vue3.toDisplayString)($props.customPrefix && $props.customPrefix !== true ? $props.name : $data.icons[$props.name]),
          7
          /* TEXT, CLASS, STYLE */
        )
      ],
      2112
      /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
    );
  }
  var __easycom_5 = /* @__PURE__ */ _export_sfc(_sfc_main2, [["render", _sfc_render2], ["styles", [_style_02]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-icon/fui-icon.vue"]]);

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/pages/chat/chat.js
  var _style_0$2 = { "fui-safe__area-wrap": { "": { "flex": 1, "flexDirection": "row" } }, "fui-safe__area-weex": { "": { "paddingBottom": 34 } } };
  var _sfc_main$2 = {
    name: "fui-safe-area",
    props: {
      //背景颜色
      background: {
        type: String,
        default: "#FFFFFF"
      }
    },
    created() {
      this.iphonex = this.isPhoneX();
    },
    data() {
      return {
        iphonex: false
      };
    },
    methods: {
      isPhoneX() {
        const res = uni.getSystemInfoSync();
        let iphonex = false;
        let models = [
          "iphonex",
          "iphonexr",
          "iphonexsmax",
          "iphone11",
          "iphone11pro",
          "iphone11promax",
          "iphone12",
          "iphone12mini",
          "iphone12pro",
          "iphone12promax",
          "iphone13",
          "iphone13mini",
          "iphone13pro",
          "iphone13promax",
          "iphone14",
          "iphone14mini",
          "iphone14pro",
          "iphone14promax",
          "iphone15",
          "iphone15mini",
          "iphone15pro",
          "iphone15promax"
        ];
        const model = res.model.replace(/\s/g, "").toLowerCase();
        const newModel = model.split("<")[0];
        if (models.includes(model) || models.includes(newModel) || res.safeAreaInsets && res.safeAreaInsets.bottom > 0) {
          iphonex = true;
        }
        return iphonex;
      }
    }
  };
  function _sfc_render$2(_ctx, _cache, $props, $setup, $data, $options) {
    return (0, import_vue4.openBlock)(), (0, import_vue4.createElementBlock)(
      import_vue4.Fragment,
      null,
      [
        (0, import_vue4.createCommentVNode)("\u672C\u6587\u4EF6\u7531FirstUI\u6388\u6743\u4E88\u4E25\u5929\u8D60\uFF08\u4F1A\u5458ID\uFF1A2  44 2\uFF0C\u8EAB\u4EFD\u8BC1\u5C3E\u53F7\uFF1A   04 30 18\uFF09\u4E13\u7528\uFF0C\u8BF7\u5C0A\u91CD\u77E5\u8BC6\u4EA7\u6743\uFF0C\u52FF\u79C1\u4E0B\u4F20\u64AD\uFF0C\u8FDD\u8005\u8FFD\u7A76\u6CD5\u5F8B\u8D23\u4EFB\u3002"),
        (0, import_vue4.createElementVNode)(
          "view",
          {
            class: (0, import_vue4.normalizeClass)(["fui-safe__area-wrap", { "fui-safe__area-weex": $data.iphonex }]),
            style: (0, import_vue4.normalizeStyle)({ background: $props.background })
          },
          null,
          6
          /* CLASS, STYLE */
        )
      ],
      2112
      /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
    );
  }
  var __easycom_2 = /* @__PURE__ */ _export_sfc(_sfc_main$2, [["render", _sfc_render$2], ["styles", [_style_0$2]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-safe-area/fui-safe-area.vue"]]);
  var _style_0$1 = { "fui-bottom__popup-wrap": { "": { "position": "fixed", "left": 0, "right": 0, "top": 0, "bottom": 0, "zIndex": 1001, "flexDirection": "row", "alignItems": "flex-end", "justifyContent": "center", "opacity": 1e-3 } }, "fui-bottom__popup-border": { "": { "position": "absolute", "left": 0, "right": 0, "bottom": 0 } }, "fui-bottom__popup": { "": { "flex": 1, "transform": "translateY(100%)", "flexDirection": "row" } }, "fui-bp__safe-weex": { "": { "paddingBottom": 34 } } };
  var animation = requireNativePlugin("animation");
  var _sfc_main$1 = {
    name: "fui-bottom-popup",
    emits: ["close"],
    props: {
      show: {
        type: Boolean,
        default: false
      },
      //背景颜色
      background: {
        type: String,
        default: "#fff"
      },
      //圆角
      radius: {
        type: [Number, String],
        default: 24
      },
      zIndex: {
        type: [Number, String],
        default: 996
      },
      //点击遮罩 是否可关闭
      maskClosable: {
        type: Boolean,
        default: true
      },
      maskBackground: {
        type: String,
        default: "rgba(0,0,0,.6)"
      },
      //是否适配底部安全区
      safeArea: {
        type: Boolean,
        default: true
      }
    },
    data() {
      let isAndroid = false;
      let isNvue = false;
      isNvue = true;
      const res = uni.getSystemInfoSync();
      isAndroid = res.platform.toLocaleLowerCase() == "android";
      return {
        iphoneX: false,
        isNvue,
        isShow: false,
        isAndroid
      };
    },
    watch: {
      show: {
        handler(newVal) {
          if (newVal) {
            this.open();
          } else {
            this.close();
          }
        },
        immediate: true
      }
    },
    created() {
      this.iphoneX = this.isPhoneX();
    },
    methods: {
      handleClose(e) {
        if (!this.maskClosable)
          return;
        this.$emit("close", {});
      },
      isPhoneX() {
        if (!this.safeArea)
          return false;
        const res = uni.getSystemInfoSync();
        let iphonex = false;
        let models = [
          "iphonex",
          "iphonexr",
          "iphonexsmax",
          "iphone11",
          "iphone11pro",
          "iphone11promax",
          "iphone12",
          "iphone12mini",
          "iphone12pro",
          "iphone12promax",
          "iphone13",
          "iphone13mini",
          "iphone13pro",
          "iphone13promax",
          "iphone14",
          "iphone14mini",
          "iphone14pro",
          "iphone14promax"
        ];
        const model = res.model.replace(/\s/g, "").toLowerCase();
        const newModel = model.split("<")[0];
        if (models.includes(model) || models.includes(newModel) || res.safeAreaInsets && res.safeAreaInsets.bottom > 0) {
          iphonex = true;
        }
        return iphonex;
      },
      open() {
        this.isShow = true;
        this.$nextTick(() => {
          setTimeout(() => {
            this._animation(true);
          }, 50);
        });
      },
      close() {
        this._animation(false);
      },
      _animation(type) {
        if (!this.$refs["fui_bp_ani"] || !this.$refs["fui_bp_mk_ani"])
          return;
        animation.transition(
          this.$refs["fui_bp_mk_ani"].ref,
          {
            styles: {
              opacity: type ? 1 : 0
            },
            duration: 250,
            timingFunction: "ease-in-out",
            needLayout: false,
            delay: 0
            //ms
          },
          () => {
            if (!type) {
              this.isShow = false;
            }
          }
        );
        animation.transition(
          this.$refs["fui_bp_ani"].ref,
          {
            styles: {
              transform: `translateY(${type ? "0" : "100%"})`
            },
            duration: !type && this.isAndroid ? 20 : 250,
            timingFunction: "ease-in-out",
            needLayout: false,
            delay: 0
            //ms
          },
          () => {
          }
        );
      },
      stop(e, tap) {
        tap && e.stopPropagation();
      }
    }
  };
  function _sfc_render$1(_ctx, _cache, $props, $setup, $data, $options) {
    return (0, import_vue4.openBlock)(), (0, import_vue4.createElementBlock)(
      import_vue4.Fragment,
      null,
      [
        (0, import_vue4.createCommentVNode)("\u672C\u6587\u4EF6\u7531FirstUI\u6388\u6743\u4E88\u4E25\u5929\u8D60\uFF08\u4F1A\u5458ID\uFF1A24   42\uFF0C\u8EAB\u4EFD\u8BC1\u5C3E\u53F7\uFF1A0     43018\uFF09\u4E13\u7528\uFF0C\u8BF7\u5C0A\u91CD\u77E5\u8BC6\u4EA7\u6743\uFF0C\u52FF\u79C1\u4E0B\u4F20\u64AD\uFF0C\u8FDD\u8005\u8FFD\u7A76\u6CD5\u5F8B\u8D23\u4EFB\u3002"),
        $data.isShow || !$data.isNvue ? ((0, import_vue4.openBlock)(), (0, import_vue4.createElementBlock)(
          "view",
          {
            key: 0,
            class: (0, import_vue4.normalizeClass)(["fui-bottom__popup-wrap", { "fui-bottom__popwrap-show": $props.show }]),
            style: (0, import_vue4.normalizeStyle)({ zIndex: $props.zIndex, background: $props.maskBackground }),
            onClick: _cache[1] || (_cache[1] = (0, import_vue4.withModifiers)((...args) => $options.handleClose && $options.handleClose(...args), ["stop"])),
            onTouchmove: _cache[2] || (_cache[2] = (0, import_vue4.withModifiers)((...args) => $options.stop && $options.stop(...args), ["stop", "prevent"])),
            ref: "fui_bp_mk_ani"
          },
          [
            $props.radius ? ((0, import_vue4.openBlock)(), (0, import_vue4.createElementBlock)(
              "view",
              {
                key: 0,
                class: "fui-bottom__popup-border",
                style: (0, import_vue4.normalizeStyle)({ height: $props.radius + "rpx", background: $props.background })
              },
              null,
              4
              /* STYLE */
            )) : (0, import_vue4.createCommentVNode)("v-if", true),
            (0, import_vue4.createElementVNode)(
              "view",
              {
                ref: "fui_bp_ani",
                class: (0, import_vue4.normalizeClass)(["fui-bottom__popup", { "fui-bottom__popup-show": $props.show, "fui-bp__safe-weex": $data.iphoneX && $props.safeArea }]),
                style: (0, import_vue4.normalizeStyle)({ borderTopLeftRadius: $props.radius + "rpx", borderTopRightRadius: $props.radius + "rpx", background: $props.background }),
                onClick: _cache[0] || (_cache[0] = (0, import_vue4.withModifiers)(($event) => $options.stop($event, true), ["stop"]))
              },
              [
                (0, import_vue4.renderSlot)(_ctx.$slots, "default")
              ],
              6
              /* CLASS, STYLE */
            )
          ],
          38
          /* CLASS, STYLE, HYDRATE_EVENTS */
        )) : (0, import_vue4.createCommentVNode)("v-if", true)
      ],
      2112
      /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
    );
  }
  var __easycom_3 = /* @__PURE__ */ _export_sfc(_sfc_main$1, [["render", _sfc_render$1], ["styles", [_style_0$1]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-bottom-popup/fui-bottom-popup.vue"]]);
  var faceList = [
    "\u{1F600}",
    "\u{1F601}",
    "\u{1F603}",
    "\u{1F604}",
    "\u{1F605}",
    "\u{1F606}",
    "\u{1F609}",
    "\u{1F60A}",
    "\u{1F60B}",
    "\u{1F60E}",
    "\u{1F60D}",
    "\u{1F618}",
    "\u{1F617}",
    "\u{1F619}",
    "\u{1F61A}",
    "\u{1F607}",
    "\u{1F610}",
    "\u{1F611}",
    "\u{1F636}",
    "\u{1F60F}",
    "\u{1F623}",
    "\u{1F625}",
    "\u{1F62E}",
    "\u{1F62F}",
    "\u{1F62A}",
    "\u{1F62B}",
    "\u{1F634}",
    "\u{1F60C}",
    "\u{1F61B}",
    "\u{1F61C}",
    "\u{1F61D}",
    "\u{1F612}",
    "\u{1F613}",
    "\u{1F614}",
    "\u{1F615}",
    "\u{1F632}",
    "\u{1F637}",
    "\u{1F616}",
    "\u{1F61E}",
    "\u{1F61F}",
    "\u{1F624}",
    "\u{1F622}",
    "\u{1F62D}",
    "\u{1F626}",
    "\u{1F627}",
    "\u{1F628}",
    "\u{1F62C}",
    "\u{1F630}",
    "\u{1F631}",
    "\u{1F633}",
    "\u{1F635}",
    "\u{1F621}",
    "\u{1F620}",
    "\u{1F466}",
    "\u{1F467}",
    "\u{1F468}",
    "\u{1F469}",
    "\u{1F474}",
    "\u{1F475}",
    "\u{1F476}",
    "\u{1F471}",
    "\u{1F46E}",
    "\u{1F472}",
    "\u{1F473}",
    "\u{1F477}",
    "\u{1F478}",
    "\u{1F482}",
    "\u{1F385}",
    "\u{1F470}",
    "\u{1F47C}",
    "\u{1F486}",
    "\u{1F487}",
    "\u{1F64D}",
    "\u{1F64E}",
    "\u{1F645}",
    "\u{1F646}",
    "\u{1F481}",
    "\u{1F64B}",
    "\u{1F647}",
    "\u{1F64C}",
    "\u{1F64F}",
    "\u{1F464}",
    "\u{1F465}",
    "\u{1F6B6}",
    "\u{1F3C3}",
    "\u{1F46F}",
    "\u{1F483}",
    "\u{1F46B}",
    "\u{1F46C}",
    "\u{1F46D}",
    "\u{1F48F}",
    "\u{1F491}",
    "\u{1F46A}",
    "\u{1F4AA}",
    "\u{1F448}",
    "\u{1F449}",
    "\u261D",
    "\u{1F446}",
    "\u{1F447}",
    "\u270C",
    "\u270B",
    "\u{1F44C}",
    "\u{1F44D}",
    "\u{1F44E}",
    "\u270A",
    "\u{1F44A}",
    "\u{1F44B}",
    "\u{1F44F}",
    "\u{1F450}",
    "\u270D",
    "\u{1F463}",
    "\u{1F440}",
    "\u{1F442}",
    "\u{1F443}",
    "\u{1F445}",
    "\u{1F444}",
    "\u{1F48B}",
    "\u{1F453}",
    "\u{1F454}",
    "\u{1F459}",
    "\u{1F45B}",
    "\u{1F45C}",
    "\u{1F45D}",
    "\u{1F392}",
    "\u{1F4BC}",
    "\u{1F45E}",
    "\u{1F45F}",
    "\u{1F460}",
    "\u{1F461}",
    "\u{1F462}",
    "\u{1F451}",
    "\u{1F452}",
    "\u{1F3A9}",
    "\u{1F393}",
    "\u{1F484}",
    "\u{1F485}",
    "\u{1F48D}",
    "\u{1F302}",
    "\u{1F4F6}",
    "\u{1F4F3}",
    "\u{1F4F4}",
    "\u267B",
    "\u{1F3E7}",
    "\u{1F6AE}",
    "\u{1F6B0}",
    "\u267F",
    "\u{1F6B9}",
    "\u{1F6BA}",
    "\u{1F6BB}",
    "\u{1F6BC}",
    "\u{1F6BE}",
    "\u26A0",
    "\u{1F6B8}",
    "\u26D4",
    "\u{1F6AB}",
    "\u{1F6B3}",
    "\u{1F6AD}",
    "\u{1F6AF}",
    "\u{1F6B1}",
    "\u{1F6B7}",
    "\u{1F51E}",
    "\u{1F488}",
    "\u{1F648}",
    "\u{1F412}",
    "\u{1F436}",
    "\u{1F415}",
    "\u{1F429}",
    "\u{1F43A}",
    "\u{1F431}",
    "\u{1F408}",
    "\u{1F42F}",
    "\u{1F405}",
    "\u{1F406}",
    "\u{1F434}",
    "\u{1F40E}",
    "\u{1F42E}",
    "\u{1F402}",
    "\u{1F403}",
    "\u{1F404}",
    "\u{1F437}",
    "\u{1F416}",
    "\u{1F417}",
    "\u{1F43D}",
    "\u{1F40F}",
    "\u{1F411}",
    "\u{1F410}",
    "\u{1F42A}",
    "\u{1F42B}",
    "\u{1F418}",
    "\u{1F42D}",
    "\u{1F401}",
    "\u{1F400}",
    "\u{1F439}",
    "\u{1F430}",
    "\u{1F407}",
    "\u{1F43B}",
    "\u{1F428}",
    "\u{1F43C}",
    "\u{1F43E}",
    "\u{1F414}",
    "\u{1F413}",
    "\u{1F423}",
    "\u{1F424}",
    "\u{1F425}",
    "\u{1F426}",
    "\u{1F427}",
    "\u{1F438}",
    "\u{1F40A}",
    "\u{1F422}",
    "\u{1F40D}",
    "\u{1F432}",
    "\u{1F409}",
    "\u{1F433}",
    "\u{1F40B}",
    "\u{1F42C}",
    "\u{1F41F}",
    "\u{1F420}",
    "\u{1F421}",
    "\u{1F419}",
    "\u{1F41A}",
    "\u{1F40C}",
    "\u{1F41B}",
    "\u{1F41C}",
    "\u{1F41D}",
    "\u{1F41E}",
    "\u{1F98B}",
    "\u{1F490}",
    "\u{1F338}",
    "\u{1F4AE}",
    "\u{1F339}",
    "\u{1F33A}",
    "\u{1F33B}",
    "\u{1F33C}",
    "\u{1F337}",
    "\u{1F331}",
    "\u{1F332}",
    "\u{1F333}",
    "\u{1F334}",
    "\u{1F335}",
    "\u{1F33E}",
    "\u{1F33F}",
    "\u{1F340}",
    "\u{1F341}",
    "\u{1F342}",
    "\u{1F343}",
    "\u{1F30D}",
    "\u{1F30E}",
    "\u{1F30F}",
    "\u{1F310}",
    "\u{1F311}",
    "\u{1F312}",
    "\u{1F313}",
    "\u{1F314}",
    "\u{1F315}",
    "\u{1F316}",
    "\u{1F317}",
    "\u{1F318}",
    "\u{1F319}",
    "\u{1F31A}",
    "\u{1F31B}",
    "\u{1F31C}",
    "\u2600",
    "\u{1F31D}",
    "\u{1F31E}",
    "\u2B50",
    "\u{1F31F}",
    "\u{1F320}",
    "\u2601",
    "\u26C5",
    "\u2614",
    "\u26A1",
    "\u2744",
    "\u{1F525}",
    "\u{1F4A7}",
    "\u{1F30A}",
    "\u{1F3C0}",
    "\u{1F3C8}",
    "\u{1F3C9}",
    "\u{1F3BE}",
    "\u{1F3B1}",
    "\u{1F3B3}",
    "\u26F3",
    "\u{1F3A3}",
    "\u{1F3BD}",
    "\u{1F3BF}",
    "\u{1F608}",
    "\u{1F47F}",
    "\u{1F479}",
    "\u{1F47A}",
    "\u{1F480}",
    "\u2620",
    "\u{1F47B}",
    "\u{1F47D}",
    "\u{1F47E}",
    "\u{1F4A3}",
    "\u{1F30B}",
    "\u{1F5FB}",
    "\u{1F3E0}",
    "\u{1F3E1}",
    "\u{1F3E2}",
    "\u{1F3E3}",
    "\u{1F3E4}",
    "\u{1F3E5}",
    "\u{1F3E6}",
    "\u{1F3E8}",
    "\u26F2",
    "\u{1F301}",
    "\u{1F303}",
    "\u{1F306}",
    "\u{1F307}",
    "\u{1F3A0}",
    "\u{1F3A1}",
    "\u{1F3A2}",
    "\u{1F682}",
    "\u{1F68C}",
    "\u{1F68D}",
    "\u{1F68E}",
    "\u{1F68F}",
    "\u{1F690}",
    "\u{1F691}",
    "\u{1F692}",
    "\u{1F693}",
    "\u{1F694}",
    "\u{1F695}",
    "\u{1F696}",
    "\u{1F697}",
    "\u{1F698}",
    "\u{1F48C}",
    "\u{1F48E}",
    "\u{1F52A}",
    "\u{1F488}",
    "\u{1F6AA}",
    "\u{1F6BD}",
    "\u{1F6BF}",
    "\u{1F6C1}",
    "\u231B",
    "\u23F3",
    "\u231A",
    "\u23F0",
    "\u{1F388}",
    "\u{1F389}",
    "\u{1F4A4}",
    "\u{1F4A2}",
    "\u{1F4AC}",
    "\u{1F4AD}",
    "\u2668",
    "\u{1F300}",
    "\u{1F514}",
    "\u{1F515}",
    "\u2721",
    "\u271D",
    "\u{1F52F}",
    "\u{1F4DB}",
    "\u{1F530}",
    "\u{1F531}",
    "\u2B55",
    "\u2705",
    "\u2611",
    "\u2714",
    "\u2716",
    "\u274C",
    "\u274E",
    "\u2795",
    "\u2796",
    "\u2797",
    "\u27B0",
    "\u27BF",
    "\u303D",
    "\u2733",
    "\u2734",
    "\u2747",
    "\u203C",
    "\u2049",
    "\u2753",
    "\u2754",
    "\u2755",
    "\u2757",
    "\u{1F55B}",
    "\u{1F567}",
    "\u{1F550}",
    "\u{1F55C}",
    "\u{1F551}",
    "\u{1F55D}",
    "\u{1F552}",
    "\u{1F55E}",
    "\u{1F553}",
    "\u{1F55F}",
    "\u{1F554}",
    "\u{1F560}",
    "\u{1F555}",
    "\u{1F561}",
    "\u{1F556}",
    "\u{1F562}",
    "\u{1F557}",
    "\u{1F563}",
    "\u{1F558}",
    "\u{1F564}",
    "\u{1F559}",
    "\u{1F565}",
    "\u{1F55A}",
    "\u{1F566}",
    "\u23F1",
    "\u23F2",
    "\u{1F570}",
    "\u{1F498}",
    "\u2764",
    "\u{1F493}",
    "\u{1F494}",
    "\u{1F495}",
    "\u{1F496}",
    "\u{1F497}",
    "\u{1F499}",
    "\u{1F49A}",
    "\u{1F49B}",
    "\u{1F49C}",
    "\u{1F49D}",
    "\u{1F49E}",
    "\u{1F49F}",
    "\u{1F347}",
    "\u{1F348}",
    "\u{1F349}",
    "\u{1F34A}",
    "\u{1F34B}",
    "\u{1F34C}",
    "\u{1F34D}",
    "\u{1F34E}",
    "\u{1F34F}",
    "\u{1F350}",
    "\u{1F351}",
    "\u{1F352}",
    "\u{1F353}"
  ];
  var _style_03 = { "fui-wrap": { "": { "paddingBottom": "108rpx" } }, "fui-chat__list": { "": { "paddingTop": "48rpx" } }, "fui-chat__pbm": { "": { "paddingBottom": "540rpx" } }, "fui-chat__item": { "": { "width": "750rpx", "paddingLeft": "24rpx", "paddingRight": "120rpx", "marginBottom": "48rpx", "flexDirection": "row", "overflow": "hidden" } }, "fui-content__box": { "": { "flex": 1, "marginLeft": "24rpx", "flexDirection": "row", "alignItems": "center" }, ".fui-item__right ": { "marginLeft": 0, "marginRight": "24rpx", "flexDirection": "row-reverse" } }, "fui-content__text": { "": { "fontSize": "32rpx", "color": "#212121", "paddingTop": "20rpx", "paddingRight": "24rpx", "paddingBottom": "20rpx", "paddingLeft": "24rpx", "backgroundColor": "#f8f8f8", "borderTopLeftRadius": "8rpx", "borderTopRightRadius": "32rpx", "borderBottomRightRadius": "32rpx", "borderBottomLeftRadius": "32rpx", "maxWidth": "486rpx" }, ".fui-item__right ": { "borderTopLeftRadius": "32rpx", "borderTopRightRadius": "8rpx", "borderBottomRightRadius": "32rpx", "borderBottomLeftRadius": "32rpx", "backgroundColor": "#246BFD", "color": "#ffffff" } }, "fui-item__right": { "": { "paddingLeft": "120rpx", "paddingRight": "24rpx", "flexDirection": "row-reverse" } }, "fui-chat__img": { "": { "width": "160rpx", "height": "160rpx" } }, "fui-audio__box": { "": { "flexDirection": "row", "alignItems": "center", "height": "80rpx", "borderRadius": "40rpx", "backgroundColor": "rgba(36,107,253,0.08)", "paddingTop": 0, "paddingRight": "48rpx", "paddingBottom": 0, "paddingLeft": "32rpx" } }, "fui-icon__audio": { "": { "width": "32rpx", "height": "32rpx" } }, "fui-time": { "": { "fontSize": "24rpx", "lineHeight": "24rpx", "paddingLeft": "16rpx", "color": "#246BFD" } }, "fui-op__icon": { "": { "paddingLeft": "12rpx" }, ".fui-item__right ": { "paddingRight": "12rpx" } }, "fui-chatbar__wrap": { "": { "paddingTop": 6, "paddingRight": 0, "paddingBottom": 6, "paddingLeft": 0, "flexDirection": "row", "alignItems": "flex-end", "justifyContent": "space-between", "backgroundColor": "#FAFAFA" } }, "fui-chatbar__input-box": { "": { "flexDirection": "row", "flex": 1, "position": "relative" } }, "fui-chatbar__input": { "": { "flex": 1, "height": "72rpx", "paddingTop": "16rpx", "paddingRight": "20rpx", "paddingBottom": "16rpx", "paddingLeft": "20rpx", "borderRadius": "8rpx", "fontSize": "32rpx", "backgroundColor": "#ffffff" } }, "fui-chatbar__voice": { "": { "position": "absolute", "left": 0, "right": 0, "top": 0, "bottom": 0, "backgroundColor": "#ffffff", "borderRadius": "8rpx", "textAlign": "center", "alignItems": "center", "justifyContent": "center", "backgroundColor:active": "#dddddd" } }, "fui-chatbar__voice-text": { "": { "textAlign": "center", "fontWeight": "bold", "fontSize": "32rpx" } }, "fui-chatbar__icon-box": { "": { "height": "72rpx", "paddingTop": 0, "paddingRight": "16rpx", "paddingBottom": 0, "paddingLeft": "16rpx", "alignItems": "center", "justifyContent": "center", "opacity:active": 0.5 } }, "fui-btn__send": { "": { "width": "64rpx", "height": "64rpx" } }, "fui-chatbar__pdl": { "": { "paddingLeft": 0 } }, "fui-chatbar__fixed": { "": { "width": "750rpx", "position": "fixed", "left": 0, "right": 0, "bottom": 0, "backgroundColor": "#f8f8f8", "transitionProperty": "transform", "transitionDuration": 200 } }, "fui-face__show": { "": { "transform": "translateY(0)" } }, "fui-scroll__box": { "": { "position": "relative" } }, "fui-face__box": { "": { "width": "750rpx", "height": "520rpx", "backgroundColor": "#f8f8f8" } }, "fui-face__inner": { "": { "flexDirection": "row", "flexWrap": "wrap", "paddingTop": "32rpx", "paddingRight": "2rpx", "paddingBottom": "32rpx", "paddingLeft": "2rpx" } }, "fui-face__text": { "": { "width": "106rpx", "fontSize": "60rpx", "paddingTop": "16rpx", "paddingRight": 0, "paddingBottom": "16rpx", "paddingLeft": 0, "textAlign": "center", "justifyContent": "center", "alignItems": "center" } }, "fui-del__box": { "": { "position": "absolute", "right": "32rpx", "bottom": "32rpx", "width": "120rpx", "height": "72rpx", "backgroundColor": "#ffffff", "borderRadius": "16rpx", "alignItems": "center", "justifyContent": "center", "backgroundColor:active": "#dddddd" } }, "fui-custom__wrap": { "": { "width": "750rpx", "height": "520rpx", "flexDirection": "row", "position": "relative" } }, "fui-icon__close": { "": { "position": "absolute", "right": "32rpx", "top": "32rpx" } }, "fui-op__list": { "": { "flex": 1, "flexDirection": "row", "flexWrap": "wrap", "paddingTop": "100rpx", "paddingRight": "44rpx", "paddingBottom": 0, "paddingLeft": "44rpx" } }, "fui-op__item": { "": { "width": "220rpx", "alignItems": "center", "justifyContent": "center", "marginBottom": "32rpx" } }, "fui-op--img": { "": { "width": "120rpx", "height": "120rpx" } }, "fui-op--text": { "": { "fontSize": "28rpx", "paddingTop": "12rpx" } }, "@TRANSITION": { "fui-chatbar__fixed": { "property": "transform", "duration": 200 } } };
  var _sfc_main3 = {
    data() {
      let spacing = 16;
      spacing = 6;
      return {
        opList: [{
          src: "doc",
          text: "\u6587\u4EF6"
        }, {
          src: "camera",
          text: "\u62CD\u6444"
        }, {
          src: "pic",
          text: "\u7167\u7247"
        }, {
          src: "addr",
          text: "\u4F4D\u7F6E"
        }, {
          src: "user",
          text: "\u8054\u7CFB\u4EBA"
        }, {
          src: "audio",
          text: "\u8BED\u97F3\u8F93\u5165"
        }],
        faceList,
        spacing,
        //keyboard
        isVoice: true,
        focus: false,
        faceShow: false,
        msg: "",
        show: false,
        isSend: false,
        //vue3 & nvue下 rpx失效
        translateY: uni.upx2px(520)
      };
    },
    watch: {
      msg(newValue, oldValue) {
        if (newValue) {
          this.isSend = true;
        } else {
          this.isSend = false;
        }
      }
    },
    methods: {
      change() {
        if (this.msg) {
          uni.fui.toast("\u53D1\u9001\u4FE1\u606F\uFF01");
          this.msg = "";
        } else {
          this.isVoice = !this.isVoice;
          this.faceShow = false;
          uni.hideKeyboard();
          this.$refs.textareaRef.blur();
        }
      },
      focusChange() {
        this.focus = true;
      },
      blurChange() {
        this.focus = false;
      },
      faceChange() {
        this.faceShow = !this.faceShow;
        if (this.faceShow) {
          this.isVoice = true;
        }
        uni.hideKeyboard();
        this.$refs.textareaRef.blur();
      },
      faceClick(item) {
        if (!this.faceShow)
          return;
        this.msg += item;
      },
      openPopup() {
        this.show = true;
        this.faceShow = false;
        uni.hideKeyboard();
        this.$refs.textareaRef.blur();
      },
      closePopup() {
        this.show = false;
      },
      delMsg() {
        if (!this.msg)
          return;
        let number = 1;
        if (this.msg.length > 1) {
          number = this.msg.substr(this.msg.length - 2, this.msg.length).search(
            /(\ud83c[\udf00-\udfff])|(\ud83d[\udc00-\ude4f])|(\ud83d[\ude80-\udeff])/i
          ) == -1 ? 1 : 2;
        }
        this.msg = this.msg.substr(0, this.msg.length - number);
      }
    }
  };
  function _sfc_render3(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_fui_avatar = resolveEasycom((0, import_vue4.resolveDynamicComponent)("fui-avatar"), __easycom_0);
    const _component_fui_icon = resolveEasycom((0, import_vue4.resolveDynamicComponent)("fui-icon"), __easycom_5);
    const _component_fui_safe_area = resolveEasycom((0, import_vue4.resolveDynamicComponent)("fui-safe-area"), __easycom_2);
    const _component_fui_bottom_popup = resolveEasycom((0, import_vue4.resolveDynamicComponent)("fui-bottom-popup"), __easycom_3);
    return (0, import_vue4.openBlock)(), (0, import_vue4.createElementBlock)("scroll-view", {
      scrollY: true,
      showScrollbar: true,
      enableBackToTop: true,
      bubble: "true",
      style: { flexDirection: "column" }
    }, [
      (0, import_vue4.createElementVNode)("view", { class: "fui-wrap" }, [
        (0, import_vue4.createElementVNode)(
          "view",
          {
            class: (0, import_vue4.normalizeClass)(["fui-chat__list", { "fui-chat__pbm": $data.faceShow }])
          },
          [
            (0, import_vue4.createElementVNode)("view", { class: "fui-chat__item" }, [
              (0, import_vue4.createVNode)(_component_fui_avatar, { src: "/static/images/chat/avatar/img_avatar_2.png" }),
              (0, import_vue4.createElementVNode)("view", { class: "fui-content__box" }, [
                (0, import_vue4.createElementVNode)("u-text", { class: "fui-content__text" }, "FirstUI GPT\u673A\u5668\u4EBA\u804A\u5929\u5730\u5740\uFF1Achatgpt.firstui.cn\uFF0C\u4F7F\u7528\u5FAE\u4FE1\u6253\u5F00\u94FE\u63A5\u5373\u53EF\u4F53\u9A8C\uFF01")
              ])
            ]),
            (0, import_vue4.createElementVNode)("view", { class: "fui-chat__item fui-item__right" }, [
              (0, import_vue4.createVNode)(_component_fui_avatar, { src: "/static/images/chat/avatar/img_avatar_1.png" }),
              (0, import_vue4.createElementVNode)("view", { class: "fui-content__box" }, [
                (0, import_vue4.createElementVNode)("u-text", { class: "fui-content__text" }, "Can't wait to try it out! \u{1F60D}\u{1F60D}")
              ])
            ]),
            (0, import_vue4.createCommentVNode)(" \u56FE\u7247 "),
            (0, import_vue4.createElementVNode)("view", { class: "fui-chat__item" }, [
              (0, import_vue4.createVNode)(_component_fui_avatar, { src: "/static/images/chat/avatar/img_avatar_2.png" }),
              (0, import_vue4.createElementVNode)("view", { class: "fui-content__box" }, [
                (0, import_vue4.createElementVNode)("u-image", {
                  src: "/static/images/imface/emoji_11.png",
                  class: "fui-chat__img",
                  mode: "widthFix"
                })
              ])
            ]),
            (0, import_vue4.createCommentVNode)(" \u8BED\u97F3 "),
            (0, import_vue4.createElementVNode)("view", { class: "fui-chat__item fui-item__right" }, [
              (0, import_vue4.createVNode)(_component_fui_avatar, { src: "/static/images/chat/avatar/img_avatar_1.png" }),
              (0, import_vue4.createElementVNode)("view", { class: "fui-content__box" }, [
                (0, import_vue4.createElementVNode)("view", { class: "fui-audio__box" }, [
                  (0, import_vue4.createCommentVNode)(" play "),
                  (0, import_vue4.createElementVNode)("u-image", {
                    class: "fui-icon__audio",
                    src: "/static/images/chat/suspend.png",
                    mode: "widthFix"
                  }),
                  (0, import_vue4.createElementVNode)("u-text", { class: "fui-time" }, "00:21")
                ]),
                (0, import_vue4.createElementVNode)("view", { class: "fui-op__icon" }, [
                  (0, import_vue4.createVNode)(_component_fui_icon, {
                    name: "warning-fill",
                    size: 52,
                    color: "#F75555"
                  })
                ])
              ])
            ]),
            (0, import_vue4.createElementVNode)("view", { class: "fui-chat__item" }, [
              (0, import_vue4.createVNode)(_component_fui_avatar, { src: "/static/images/chat/avatar/img_avatar_2.png" }),
              (0, import_vue4.createElementVNode)("view", { class: "fui-content__box" }, [
                (0, import_vue4.createElementVNode)("u-text", { class: "fui-content__text" }, "FirstUI GPT\u673A\u5668\u4EBA\u804A\u5929\u5730\u5740\uFF1Achatgpt.firstui.cn\uFF0C\u4F7F\u7528\u5FAE\u4FE1\u6253\u5F00\u94FE\u63A5\u5373\u53EF\u4F53\u9A8C\uFF01")
              ])
            ])
          ],
          2
          /* CLASS */
        ),
        (0, import_vue4.createElementVNode)(
          "view",
          {
            class: "fui-chatbar__fixed",
            style: (0, import_vue4.normalizeStyle)({ transform: `translateY(${$data.faceShow ? 0 : $data.translateY})px` })
          },
          [
            (0, import_vue4.createElementVNode)("view", { class: "fui-chatbar__wrap" }, [
              (0, import_vue4.createElementVNode)("view", {
                class: "fui-chatbar__icon-box",
                onClick: _cache[0] || (_cache[0] = (...args) => $options.openPopup && $options.openPopup(...args))
              }, [
                (0, import_vue4.createVNode)(_component_fui_icon, { name: "plussign" })
              ]),
              (0, import_vue4.createElementVNode)("view", {
                class: "fui-chatbar__icon-box fui-chatbar__pdl",
                onClick: _cache[1] || (_cache[1] = (0, import_vue4.withModifiers)((...args) => $options.faceChange && $options.faceChange(...args), ["stop"]))
              }, [
                (0, import_vue4.createVNode)(_component_fui_icon, { name: "face" })
              ]),
              (0, import_vue4.createElementVNode)("view", { class: "fui-chatbar__input-box" }, [
                (0, import_vue4.createElementVNode)("u-textarea", {
                  ref: "textareaRef",
                  fixed: true,
                  autoHeight: "",
                  showCount: false,
                  disableDefaultPadding: "",
                  confirmType: "send",
                  class: "fui-chatbar__input",
                  cursorSpacing: $data.spacing,
                  onFocus: _cache[2] || (_cache[2] = (...args) => $options.focusChange && $options.focusChange(...args)),
                  onBlur: _cache[3] || (_cache[3] = (...args) => $options.blurChange && $options.blurChange(...args)),
                  modelValue: $data.msg,
                  onInput: _cache[4] || (_cache[4] = ($event) => $data.msg = $event.detail.value)
                }, null, 40, ["cursorSpacing", "modelValue"]),
                !$data.isVoice ? ((0, import_vue4.openBlock)(), (0, import_vue4.createElementBlock)("view", {
                  key: 0,
                  class: "fui-chatbar__voice"
                }, [
                  (0, import_vue4.createElementVNode)("u-text", { class: "fui-chatbar__voice-text" }, "\u6309\u4F4F \u8BF4\u8BDD")
                ])) : (0, import_vue4.createCommentVNode)("v-if", true)
              ]),
              (0, import_vue4.createElementVNode)("view", {
                class: "fui-chatbar__icon-box",
                onClick: _cache[5] || (_cache[5] = (...args) => $options.change && $options.change(...args))
              }, [
                !$data.isSend ? ((0, import_vue4.openBlock)(), (0, import_vue4.createBlock)(_component_fui_icon, {
                  key: 0,
                  name: $data.isVoice ? "voice" : "keyboard"
                }, null, 8, ["name"])) : ((0, import_vue4.openBlock)(), (0, import_vue4.createElementBlock)("u-image", {
                  key: 1,
                  class: "fui-btn__send",
                  src: "/static/images/chat/send.png"
                }))
              ])
            ]),
            (0, import_vue4.createElementVNode)("view", { class: "fui-scroll__box" }, [
              (0, import_vue4.createElementVNode)(
                "scroll-view",
                {
                  scrollY: "",
                  class: "fui-face__box",
                  style: (0, import_vue4.normalizeStyle)({ opacity: $data.faceShow ? 1 : 0 })
                },
                [
                  (0, import_vue4.createElementVNode)("view", { class: "fui-face__inner" }, [
                    ((0, import_vue4.openBlock)(true), (0, import_vue4.createElementBlock)(
                      import_vue4.Fragment,
                      null,
                      (0, import_vue4.renderList)($data.faceList, (item, index) => {
                        return (0, import_vue4.openBlock)(), (0, import_vue4.createElementBlock)("u-text", {
                          class: "fui-face__text fui--active",
                          key: index,
                          onClick: ($event) => $options.faceClick(item)
                        }, (0, import_vue4.toDisplayString)(item), 9, ["onClick"]);
                      }),
                      128
                      /* KEYED_FRAGMENT */
                    ))
                  ])
                ],
                4
                /* STYLE */
              ),
              (0, import_vue4.createElementVNode)("view", {
                class: "fui-del__box",
                onClick: _cache[6] || (_cache[6] = (0, import_vue4.withModifiers)((...args) => $options.delMsg && $options.delMsg(...args), ["stop"]))
              }, [
                (0, import_vue4.createVNode)(_component_fui_icon, {
                  name: "backspace",
                  size: 56
                })
              ])
            ]),
            (0, import_vue4.createVNode)(_component_fui_safe_area, { background: "#f8f8f8" })
          ],
          4
          /* STYLE */
        ),
        (0, import_vue4.createVNode)(_component_fui_bottom_popup, {
          show: $data.show,
          onClose: $options.closePopup
        }, {
          default: (0, import_vue4.withCtx)(() => [
            (0, import_vue4.createElementVNode)("view", { class: "fui-custom__wrap" }, [
              (0, import_vue4.createElementVNode)("view", { class: "fui-op__list" }, [
                ((0, import_vue4.openBlock)(true), (0, import_vue4.createElementBlock)(
                  import_vue4.Fragment,
                  null,
                  (0, import_vue4.renderList)($data.opList, (item, index) => {
                    return (0, import_vue4.openBlock)(), (0, import_vue4.createElementBlock)("view", {
                      class: "fui-op__item fui--active",
                      key: index
                    }, [
                      (0, import_vue4.createElementVNode)("u-image", {
                        class: "fui-op--img",
                        src: `/static/images/chat/img_${item.src}.png`
                      }, null, 8, ["src"]),
                      (0, import_vue4.createElementVNode)(
                        "u-text",
                        { class: "fui-op--text" },
                        (0, import_vue4.toDisplayString)(item.text),
                        1
                        /* TEXT */
                      )
                    ]);
                  }),
                  128
                  /* KEYED_FRAGMENT */
                ))
              ]),
              (0, import_vue4.createElementVNode)("view", {
                class: "fui-icon__close fui--active",
                onClick: _cache[7] || (_cache[7] = (...args) => $options.closePopup && $options.closePopup(...args))
              }, [
                (0, import_vue4.createVNode)(_component_fui_icon, {
                  name: "close",
                  size: 52
                })
              ])
            ])
          ]),
          _: 1
          /* STABLE */
        }, 8, ["show", "onClose"])
      ])
    ]);
  }
  var chat = /* @__PURE__ */ _export_sfc(_sfc_main3, [["render", _sfc_render3], ["styles", [_style_03]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/pages/chat/chat.nvue"]]);

  // <stdin>
  var webview = plus.webview.currentWebview();
  if (webview) {
    const __pageId = parseInt(webview.id);
    const __pagePath = "pages/chat/chat";
    let __pageQuery = {};
    try {
      __pageQuery = JSON.parse(webview.__query__);
    } catch (e) {
    }
    chat.mpType = "page";
    const app = Vue.createPageApp(chat, { $store: getApp({ allowDefault: true }).$store, __pageId, __pagePath, __pageQuery });
    app.provide("__globalStyles", Vue.useCssStyles([...__uniConfig.styles, ...chat.styles || []]));
    app.mount("#root");
  }
})();
