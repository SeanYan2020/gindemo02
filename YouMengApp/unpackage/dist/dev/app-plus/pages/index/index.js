"use weex:vue";

if (typeof Promise !== 'undefined' && !Promise.prototype.finally) {
  Promise.prototype.finally = function(callback) {
    const promise = this.constructor
    return this.then(
      value => promise.resolve(callback()).then(() => value),
      reason => promise.resolve(callback()).then(() => {
        throw reason
      })
    )
  }
};

if (typeof uni !== 'undefined' && uni && uni.requireGlobal) {
  const global = uni.requireGlobal()
  ArrayBuffer = global.ArrayBuffer
  Int8Array = global.Int8Array
  Uint8Array = global.Uint8Array
  Uint8ClampedArray = global.Uint8ClampedArray
  Int16Array = global.Int16Array
  Uint16Array = global.Uint16Array
  Int32Array = global.Int32Array
  Uint32Array = global.Uint32Array
  Float32Array = global.Float32Array
  Float64Array = global.Float64Array
  BigInt64Array = global.BigInt64Array
  BigUint64Array = global.BigUint64Array
};


(() => {
  var __create = Object.create;
  var __defProp = Object.defineProperty;
  var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
  var __getOwnPropNames = Object.getOwnPropertyNames;
  var __getProtoOf = Object.getPrototypeOf;
  var __hasOwnProp = Object.prototype.hasOwnProperty;
  var __commonJS = (cb, mod) => function __require() {
    return mod || (0, cb[__getOwnPropNames(cb)[0]])((mod = { exports: {} }).exports, mod), mod.exports;
  };
  var __copyProps = (to, from, except, desc) => {
    if (from && typeof from === "object" || typeof from === "function") {
      for (let key of __getOwnPropNames(from))
        if (!__hasOwnProp.call(to, key) && key !== except)
          __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
    }
    return to;
  };
  var __toESM = (mod, isNodeMode, target) => (target = mod != null ? __create(__getProtoOf(mod)) : {}, __copyProps(
    // If the importer is in node compatibility mode or this is not an ESM
    // file that has been converted to a CommonJS file using a Babel-
    // compatible transform (i.e. "__esModule" has not been set), then set
    // "default" to the CommonJS "module.exports" for node compatibility.
    isNodeMode || !mod || !mod.__esModule ? __defProp(target, "default", { value: mod, enumerable: true }) : target,
    mod
  ));

  // vue-ns:vue
  var require_vue = __commonJS({
    "vue-ns:vue"(exports, module) {
      module.exports = Vue;
    }
  });

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/uni-app.es.js
  var import_vue = __toESM(require_vue());
  var isString = (val) => typeof val === "string";
  function requireNativePlugin(name) {
    return weex.requireModule(name);
  }
  function formatAppLog(type, filename, ...args) {
    if (uni.__log__) {
      uni.__log__(type, filename, ...args);
    } else {
      console[type].apply(console, [...args, filename]);
    }
  }
  function resolveEasycom(component, easycom) {
    return isString(component) ? easycom : component;
  }

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/fui-avatar.js
  var import_vue2 = __toESM(require_vue());

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/_plugin-vue_export-helper.js
  var _export_sfc = (sfc, props) => {
    const target = sfc.__vccOpts || sfc;
    for (const [key, val] of props) {
      target[key] = val;
    }
    return target;
  };

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/fui-avatar.js
  var _style_0 = { "fui-avatar__wrap": { "": { "position": "relative", "flexDirection": "row", "alignItems": "center", "justifyContent": "center" } }, "fui-avatar__img": { "": { "flex": 1 } }, "fui-avatar__text": { "": { "flex": 1, "lines": 1, "overflow": "hidden", "textOverflow": "ellipsis", "textAlign": "center" } }, "fui-avatar__size-small": { "": { "!width": "64rpx", "!height": "64rpx" } }, "fui-avatar__text-small": { "": { "!fontSize": "32rpx" } }, "fui-avatar__size-middle": { "": { "!width": "96rpx", "!height": "96rpx" } }, "fui-avatar__text-middle": { "": { "!fontSize": "44rpx" } }, "fui-avatar__size-large": { "": { "!width": "128rpx", "!height": "128rpx" } }, "fui-avatar__text-large": { "": { "!fontSize": "56rpx" } }, "fui-avatar__circle": { "": { "!borderRadius": 500 } }, "fui-avatar__square": { "": { "!borderRadius": "8rpx" } } };
  var _sfc_main = {
    name: "fui-avatar",
    emits: ["click", "error"],
    props: {
      src: {
        type: String,
        default: ""
      },
      errorSrc: {
        type: String,
        default: ""
      },
      mode: {
        type: String,
        default: "scaleToFill"
      },
      //微信小程序、百度小程序、字节跳动小程序
      //图片懒加载。只针对page与scroll-view下的image有效
      lazyLoad: {
        type: Boolean,
        default: true
      },
      //默认不解析 webP 格式，只支持网络资源 微信小程序2.9.0
      webp: {
        type: Boolean,
        default: false
      },
      background: {
        type: String,
        default: "#D1D1D1"
      },
      //small（64）、middle（96）、large（128）
      size: {
        type: String,
        default: "middle"
      },
      //图片宽度，设置大于0的数值生效，默认使用size
      width: {
        type: [Number, String],
        default: 0
      },
      //默认等宽，设置图大于0的数值且设置了图片宽度生效
      height: {
        type: [Number, String],
        default: 0
      },
      //指定头像的形状，可选值为 circle、square
      shape: {
        type: String,
        default: "circle"
      },
      //图片圆角值，默认使用shape，当设置大于等于0的数值，shape失效
      radius: {
        type: [Number, String],
        default: -1
      },
      //没有src时可以使用文本代替
      text: {
        type: String,
        default: ""
      },
      color: {
        type: String,
        default: "#fff"
      },
      //默认使用size下字体大小
      fontSize: {
        type: [Number, String],
        default: 0
      },
      fontWeight: {
        type: [Number, String],
        default: 600
      },
      marginRight: {
        type: [Number, String],
        default: 0
      },
      marginBottom: {
        type: [Number, String],
        default: 0
      },
      //在列表中的索引值
      index: {
        type: Number,
        default: 0
      },
      //其他参数
      params: {
        type: [Number, String],
        default: 0
      }
    },
    computed: {
      styles() {
        let styles = "";
        if (this.width) {
          styles = `width:${this.width}rpx;height:${this.height || this.width}rpx;`;
        }
        if (this.radius !== -1) {
          styles += `border-radius:${this.radius}rpx;`;
        }
        return styles;
      },
      wrapStyles() {
        return `background:${this.background};margin-right:${this.marginRight}rpx;margin-bottom:${this.marginBottom}rpx;${this.styles};`;
      },
      textStyles() {
        let styles = `color:${this.color};font-weight:${this.fontWeight};`;
        if (this.fontSize) {
          styles += `font-size:${this.fontSize}rpx;`;
        }
        return styles;
      }
    },
    watch: {
      src(val) {
        this.src && (this.showImg = this.src);
      }
    },
    data() {
      return {
        showImg: ""
      };
    },
    created() {
      this.src && (this.showImg = this.src);
    },
    methods: {
      handleError(e) {
        if (this.src) {
          this.errorSrc && (this.showImg = this.errorSrc);
          this.$emit("error", {
            index: this.index,
            params: this.params
          });
        }
      },
      handleClick() {
        this.$emit("click", {
          index: this.index,
          params: this.params
        });
      }
    }
  };
  function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
    return (0, import_vue2.openBlock)(), (0, import_vue2.createElementBlock)(
      import_vue2.Fragment,
      null,
      [
        (0, import_vue2.createCommentVNode)("\u672C\u6587\u4EF6\u7531FirstUI\u6388\u6743\u4E88\u4E25\u5929\u8D60\uFF08\u4F1A\u5458ID\uFF1A244   2\uFF0C\u8EAB\u4EFD\u8BC1\u5C3E\u53F7\uFF1A04   3  018\uFF09\u4E13\u7528\uFF0C\u8BF7\u5C0A\u91CD\u77E5\u8BC6\u4EA7\u6743\uFF0C\u52FF\u79C1\u4E0B\u4F20\u64AD\uFF0C\u8FDD\u8005\u8FFD\u7A76\u6CD5\u5F8B\u8D23\u4EFB\u3002"),
        (0, import_vue2.createElementVNode)(
          "view",
          {
            class: (0, import_vue2.normalizeClass)(["fui-avatar__wrap", [$props.width ? "" : "fui-avatar__size-" + $props.size, $props.radius === -1 ? "fui-avatar__" + $props.shape : ""]]),
            style: (0, import_vue2.normalizeStyle)($options.wrapStyles),
            onClick: _cache[1] || (_cache[1] = (...args) => $options.handleClick && $options.handleClick(...args))
          },
          [
            $props.src && $props.src !== true ? ((0, import_vue2.openBlock)(), (0, import_vue2.createElementBlock)("u-image", {
              key: 0,
              class: (0, import_vue2.normalizeClass)(["fui-avatar__img", [$props.radius === -1 ? "fui-avatar__" + $props.shape : "", $props.width ? "" : "fui-avatar__size-" + $props.size]]),
              style: (0, import_vue2.normalizeStyle)($options.styles),
              src: $data.showImg,
              mode: $props.mode,
              webp: $props.webp,
              lazyLoad: $props.lazyLoad,
              onError: _cache[0] || (_cache[0] = (...args) => $options.handleError && $options.handleError(...args))
            }, null, 46, ["src", "mode", "webp", "lazyLoad"])) : (0, import_vue2.createCommentVNode)("v-if", true),
            !$props.src && $props.src !== true && $props.text ? ((0, import_vue2.openBlock)(), (0, import_vue2.createElementBlock)(
              "u-text",
              {
                key: 1,
                class: (0, import_vue2.normalizeClass)(["fui-avatar__text", [$props.width ? "" : "fui-avatar__text-" + $props.size]]),
                style: (0, import_vue2.normalizeStyle)($options.textStyles)
              },
              (0, import_vue2.toDisplayString)($props.text),
              7
              /* TEXT, CLASS, STYLE */
            )) : (0, import_vue2.createCommentVNode)("v-if", true),
            (0, import_vue2.renderSlot)(_ctx.$slots, "default")
          ],
          6
          /* CLASS, STYLE */
        )
      ],
      2112
      /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
    );
  }
  var __easycom_0 = /* @__PURE__ */ _export_sfc(_sfc_main, [["render", _sfc_render], ["styles", [_style_0]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-avatar/fui-avatar.vue"]]);

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/pages/index/index.js
  var import_vue6 = __toESM(require_vue());

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/fui-list-cell.js
  var import_vue3 = __toESM(require_vue());
  var _style_02 = { "fui-list__cell": { "": { "position": "relative", "flex": 1, "flexDirection": "row", "alignItems": "center", "justifyContent": "space-between" } }, "fui-list__item": { "": { "flex": 1 } }, "fui-cell__arrow": { "": { "height": "40rpx", "width": "40rpx", "borderTopWidth": 3, "borderRightWidth": 3, "borderBottomWidth": 0, "borderLeftWidth": 0, "borderStyle": "solid", "transform": "rotate(45deg) scale(0.5)", "borderTopRightRadius": "3rpx", "transformOrigin": "center center", "marginRight": "-5.8579rpx" } }, "fui-cell__border-top": { "": { "position": "absolute", "top": 0, "height": 0.5, "zIndex": -1 } }, "fui-cell__border-bottom": { "": { "position": "absolute", "bottom": 0, "height": 0.5, "zIndex": -1 } }, "fui-highlight": { "": { "!backgroundColor:active": "rgba(0,0,0,0.2)" } } };
  var _sfc_main2 = {
    name: "fui-list-cell",
    emits: ["click"],
    props: {
      //padding值，上、右、下、左,nvue下padding-right(右)无效
      padding: {
        type: Array,
        default() {
          return [];
        }
      },
      //margin-top 单位rpx
      marginTop: {
        type: [Number, String],
        default: 0
      },
      //margin-bottom 单位rpx
      marginBottom: {
        type: [Number, String],
        default: 0
      },
      //背景颜色
      background: {
        type: String,
        default: "#fff"
      },
      //是否有点击效果
      highlight: {
        type: Boolean,
        default: true
      },
      //是否需要右侧箭头
      arrow: {
        type: Boolean,
        default: false
      },
      arrowColor: {
        type: String,
        default: ""
      },
      //是否显示上边框
      topBorder: {
        type: Boolean,
        default: false
      },
      //是否显示下边框
      bottomBorder: {
        type: Boolean,
        default: true
      },
      //边框颜色，非nvue下传值则全局默认样式失效
      borderColor: {
        type: String,
        default: ""
      },
      //上边框left值，单位rpx
      topLeft: {
        type: [Number, String],
        default: 0
      },
      //上边框right值，单位rpx
      topRight: {
        type: [Number, String],
        default: 0
      },
      //下边框left值，单位rpx
      bottomLeft: {
        type: [Number, String],
        default: -1
      },
      //下边框right值，单位rpx
      bottomRight: {
        type: [Number, String],
        default: 0
      },
      //border-radius圆角值
      radius: {
        type: String,
        default: "0"
      },
      index: {
        type: Number,
        default: 0
      }
    },
    computed: {
      getPadding() {
        let padding = this.padding;
        if (Array.isArray(padding) && padding.length === 0) {
          const app = uni && uni.$fui && uni.$fui.fuiListCell;
          padding = app && app.padding;
          if (!padding || Array.isArray(padding) && padding.length === 0) {
            padding = ["32rpx", "32rpx"];
          }
        }
        return padding;
      },
      getArrowColor() {
        const app = uni && uni.$fui && uni.$fui.fuiListCell;
        return this.arrowColor || app && app.arrowColor || "#B2B2B2";
      },
      getBorderColor() {
        let color = this.borderColor;
        if (!color || color === true) {
          const app = uni && uni.$fui && uni.$fui.fuiListCell;
          color = app && app.borderColor || "#EEEEEE";
        }
        return color;
      },
      getBottomLeft() {
        const app = uni && uni.$fui && uni.$fui.fuiListCell;
        let left = this.bottomLeft;
        const c_left = app && app.bottomLeft;
        if (left === -1) {
          left = c_left === void 0 || c_left === null ? 32 : c_left;
        }
        return left;
      }
    },
    methods: {
      handleClick() {
        this.$emit("click", {
          index: this.index
        });
      }
    }
  };
  function _sfc_render2(_ctx, _cache, $props, $setup, $data, $options) {
    return (0, import_vue3.openBlock)(), (0, import_vue3.createElementBlock)(
      import_vue3.Fragment,
      null,
      [
        (0, import_vue3.createCommentVNode)("\u672C\u6587\u4EF6\u7531FirstUI\u6388\u6743\u4E88\u4E25\u5929\u8D60\uFF08\u4F1A\u5458ID\uFF1A2 4 4 2\uFF0C\u8EAB\u4EFD\u8BC1\u5C3E\u53F7\uFF1A043 0   1 8\uFF09\u4E13\u7528\uFF0C\u8BF7\u5C0A\u91CD\u77E5\u8BC6\u4EA7\u6743\uFF0C\u52FF\u79C1\u4E0B\u4F20\u64AD\uFF0C\u8FDD\u8005\u8FFD\u7A76\u6CD5\u5F8B\u8D23\u4EFB\u3002"),
        (0, import_vue3.createElementVNode)(
          "view",
          {
            class: (0, import_vue3.normalizeClass)(["fui-list__cell", { "fui-highlight": $props.highlight, "fui-list__cell-background": !$props.background }]),
            style: (0, import_vue3.normalizeStyle)({ paddingTop: $options.getPadding[0] || 0, paddingRight: $options.getPadding[1] || 0, paddingBottom: $options.getPadding[2] || $options.getPadding[0] || 0, paddingLeft: $options.getPadding[3] || $options.getPadding[1] || 0, background: $props.background, marginTop: $props.marginTop + "rpx", marginBottom: $props.marginBottom + "rpx", borderRadius: $props.radius }),
            onClick: _cache[0] || (_cache[0] = (...args) => $options.handleClick && $options.handleClick(...args))
          },
          [
            $props.topBorder ? ((0, import_vue3.openBlock)(), (0, import_vue3.createElementBlock)(
              "view",
              {
                key: 0,
                style: (0, import_vue3.normalizeStyle)({ background: $options.getBorderColor, left: $props.topLeft + "rpx", right: $props.topRight + "rpx" }),
                class: (0, import_vue3.normalizeClass)(["fui-cell__border-top", { "fui-cell__border-color": !$options.getBorderColor }])
              },
              null,
              6
              /* CLASS, STYLE */
            )) : (0, import_vue3.createCommentVNode)("v-if", true),
            (0, import_vue3.renderSlot)(_ctx.$slots, "default"),
            $props.bottomBorder ? ((0, import_vue3.openBlock)(), (0, import_vue3.createElementBlock)(
              "view",
              {
                key: 1,
                style: (0, import_vue3.normalizeStyle)({ background: $options.getBorderColor, left: $options.getBottomLeft + "rpx", right: $props.bottomRight + "rpx" }),
                class: (0, import_vue3.normalizeClass)(["fui-cell__border-bottom", { "fui-cell__border-color": !$options.getBorderColor }])
              },
              null,
              6
              /* CLASS, STYLE */
            )) : (0, import_vue3.createCommentVNode)("v-if", true),
            $props.arrow ? ((0, import_vue3.openBlock)(), (0, import_vue3.createElementBlock)(
              "view",
              {
                key: 2,
                class: "fui-cell__arrow",
                style: (0, import_vue3.normalizeStyle)({ "border-color": $options.getArrowColor })
              },
              null,
              4
              /* STYLE */
            )) : (0, import_vue3.createCommentVNode)("v-if", true)
          ],
          6
          /* CLASS, STYLE */
        )
      ],
      2112
      /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
    );
  }
  var __easycom_3 = /* @__PURE__ */ _export_sfc(_sfc_main2, [["render", _sfc_render2], ["styles", [_style_02]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-list-cell/fui-list-cell.vue"]]);

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/fui-button.js
  var import_vue4 = __toESM(require_vue());
  var _style_03 = { "fui-button__wrap": { "": { "position": "relative" } }, "fui-button": { "": { "borderWidth": 0, "borderStyle": "solid", "position": "relative", "paddingLeft": 0, "paddingRight": 0, "borderWidth::after": 0, "borderStyle::after": "solid", "borderColor::after": "#000000" } }, "fui-button__flex-1": { "": { "flex": 1 } }, "fui-button__text": { "": { "textAlign": "center", "flexDirection": "row", "alignItems": "center", "!justifyContent": "center", "!paddingLeft": 0 } }, "fui-button__opacity": { "": { "opacity": 0.5 } }, "fui-text__bold": { "": { "fontWeight": "bold" } }, "fui-button__link": { "": { "!borderColor": "rgba(0,0,0,0)", "!backgroundColor": "rgba(0,0,0,0)" } } };
  var _sfc_main3 = {
    name: "fui-button",
    emits: ["click", "getuserinfo", "contact", "getphonenumber", "error", "opensetting", "chooseavatar", "launchapp"],
    props: {
      //样式类型：primary，success， warning，danger，link，purple，gray
      type: {
        type: String,
        default: "primary"
      },
      //按钮背景色，当传入值时type失效
      background: {
        type: String,
        default: ""
      },
      //按钮显示文本
      text: {
        type: String,
        default: ""
      },
      //按钮字体颜色
      color: {
        type: String,
        default: ""
      },
      //按钮禁用背景色
      disabledBackground: {
        type: String,
        default: ""
      },
      //按钮禁用字体颜色
      disabledColor: {
        type: String,
        default: ""
      },
      borderWidth: {
        type: String,
        default: "0.5px"
      },
      borderColor: {
        type: String,
        default: ""
      },
      //V1.9.8+ 按钮大小，优先级高于width和height，medium、small、mini
      btnSize: {
        type: String,
        default: ""
      },
      //宽度
      width: {
        type: String,
        default: "100%"
      },
      //高度
      height: {
        type: String,
        default: ""
      },
      //字体大小，单位rpx
      size: {
        type: [Number, String],
        default: 0
      },
      bold: {
        type: Boolean,
        default: false
      },
      //['20rpx','30rpx','20rpx','30rpx']->[上，右，下，左]
      margin: {
        type: Array,
        default() {
          return ["0", "0"];
        }
      },
      //圆角
      radius: {
        type: String,
        default: ""
      },
      plain: {
        type: Boolean,
        default: false
      },
      disabled: {
        type: Boolean,
        default: false
      },
      loading: {
        type: Boolean,
        default: false
      },
      formType: {
        type: String,
        default: ""
      },
      openType: {
        type: String,
        default: ""
      },
      //支付宝小程序
      //当 open-type 为 getAuthorize 时，可以设置 scope 为：phoneNumber、userInfo
      scope: {
        type: String,
        default: ""
      },
      appParameter: {
        type: String,
        default: ""
      },
      index: {
        type: [Number, String],
        default: 0
      }
    },
    computed: {
      getTypeColor() {
        const app = uni && uni.$fui && uni.$fui.color;
        let colors = {
          primary: app && app.primary || "#465CFF",
          success: app && app.success || "#09BE4F",
          warning: app && app.warning || "#FFB703",
          danger: app && app.danger || "#FF2B2B",
          link: "transparent",
          purple: app && app.purple || "#6831FF",
          gray: "#F8F8F8"
        };
        return colors[this.type] || "transparent";
      },
      getBackground() {
        let color = this.getTypeColor;
        if (this.disabled || this.plain) {
          color = "transparent";
        }
        if (!this.disabled && !this.plain && this.background) {
          color = this.background;
        }
        return color;
      },
      getColor() {
        let color = "#fff";
        if (this.color) {
          color = this.disabled && this.disabledBackground ? this.disabledColor : this.color;
        } else {
          if (this.disabled && this.disabledBackground) {
            color = this.disabledColor || "#FFFFFF";
          } else {
            const app = uni && uni.$fui && uni.$fui.color;
            const primary = app && app.primary || "#465CFF";
            color = this.type === "gray" ? primary : "#FFFFFF";
          }
        }
        return color;
      },
      getSize() {
        let size = this.size || uni && uni.$fui && uni.$fui.fuiButton && uni.$fui.fuiButton.size || 32;
        if (this.btnSize === "small") {
          size = size > 28 ? 28 : size;
        } else if (this.btnSize === "mini") {
          size = size > 28 ? 24 : size;
        }
        return `${size}rpx`;
      },
      getWidth() {
        let width = this.width;
        if (this.btnSize && this.btnSize !== true) {
          width = {
            "medium": "400rpx",
            "small": "200rpx",
            "mini": "120rpx"
          }[this.btnSize] || width;
        }
        return width;
      },
      getHeight() {
        let height = this.height || uni && uni.$fui && uni.$fui.fuiButton && uni.$fui.fuiButton.height || "96rpx";
        if (this.btnSize && this.btnSize !== true) {
          height = {
            "medium": "84rpx",
            "small": "72rpx",
            "mini": "64rpx"
          }[this.btnSize] || height;
        }
        return height;
      },
      getRadius() {
        const radius = uni && uni.$fui && uni.$fui.fuiButton && uni.$fui.fuiButton.radius || "16rpx";
        return this.radius || radius;
      }
    },
    data() {
      let isNvue = false;
      isNvue = true;
      return {
        isNvue,
        time: 0,
        trigger: false,
        pc: false
      };
    },
    created() {
    },
    methods: {
      handleStart(e) {
      },
      handleClick() {
        if (this.disabled || !this.trigger)
          return;
        this.time = 0;
      },
      handleTap() {
        if (this.disabled)
          return;
        this.$emit("click", {
          index: Number(this.index)
        });
      },
      handleEnd(e) {
      },
      bindgetuserinfo({
        detail = {}
      } = {}) {
        this.$emit("getuserinfo", detail);
      },
      bindcontact({
        detail = {}
      } = {}) {
        this.$emit("contact", detail);
      },
      bindgetphonenumber({
        detail = {}
      } = {}) {
        this.$emit("getphonenumber", detail);
      },
      binderror({
        detail = {}
      } = {}) {
        this.$emit("error", detail);
      },
      bindopensetting({
        detail = {}
      } = {}) {
        this.$emit("opensetting", detail);
      },
      bindchooseavatar({
        detail = {}
      } = {}) {
        this.$emit("chooseavatar", detail);
      },
      bindlaunchapp({
        detail = {}
      } = {}) {
        this.$emit("launchapp", detail);
      }
    }
  };
  function _sfc_render3(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_button = (0, import_vue4.resolveComponent)("button");
    return (0, import_vue4.openBlock)(), (0, import_vue4.createElementBlock)(
      import_vue4.Fragment,
      null,
      [
        (0, import_vue4.createCommentVNode)("\u672C\u6587\u4EF6\u7531FirstUI\u6388\u6743\u4E88\u4E25\u5929\u8D60\uFF08\u4F1A\u5458ID\uFF1A24 4  2\uFF0C\u8EAB\u4EFD\u8BC1\u5C3E\u53F7\uFF1A0 4   3 018\uFF09\u4E13\u7528\uFF0C\u8BF7\u5C0A\u91CD\u77E5\u8BC6\u4EA7\u6743\uFF0C\u52FF\u79C1\u4E0B\u4F20\u64AD\uFF0C\u8FDD\u8005\u8FFD\u7A76\u6CD5\u5F8B\u8D23\u4EFB\u3002"),
        (0, import_vue4.createElementVNode)(
          "view",
          {
            class: (0, import_vue4.normalizeClass)(["fui-button__wrap", [!$options.getWidth || $options.getWidth === "100%" || $options.getWidth === true ? "fui-button__flex-1" : "", $props.disabled && !$props.disabledBackground ? "fui-button__opacity" : ""]]),
            style: (0, import_vue4.normalizeStyle)({ width: $options.getWidth, height: $options.getHeight, marginTop: $props.margin[0] || 0, marginRight: $props.margin[1] || 0, marginBottom: $props.margin[2] || $props.margin[0] || 0, marginLeft: $props.margin[3] || $props.margin[1] || 0, borderRadius: $options.getRadius, background: $options.getBackground }),
            onTouchstart: _cache[0] || (_cache[0] = (...args) => $options.handleStart && $options.handleStart(...args)),
            onTouchend: _cache[1] || (_cache[1] = (...args) => $options.handleClick && $options.handleClick(...args)),
            onTouchcancel: _cache[2] || (_cache[2] = (...args) => $options.handleEnd && $options.handleEnd(...args))
          },
          [
            (0, import_vue4.createVNode)(_component_button, {
              class: (0, import_vue4.normalizeClass)(["fui-button", [
                $props.bold ? "fui-text__bold" : "",
                $data.time && ($props.plain || $props.type === "link") ? "fui-button__opacity" : "",
                !$props.background && !$props.disabledBackground && !$props.plain ? "fui-button__" + $props.type : "",
                !$options.getWidth || $options.getWidth === "100%" || $options.getWidth === true ? "fui-button__flex-1" : "",
                $data.time && !$props.plain && $props.type !== "link" ? "fui-button__active" : "",
                $data.pc && !$props.disabled ? $props.plain || $props.type === "link" ? "fui-button__opacity-pc" : "fui-button__active-pc" : ""
              ]]),
              style: (0, import_vue4.normalizeStyle)({
                width: $options.getWidth,
                height: $options.getHeight,
                lineHeight: $options.getHeight,
                background: $props.disabled ? $props.disabledBackground || $options.getTypeColor : $props.plain ? "transparent" : $options.getBackground,
                borderWidth: !$props.borderColor || !$data.isNvue ? "0" : $props.borderWidth,
                borderColor: $props.borderColor ? $props.borderColor : $props.disabled && $props.disabledBackground ? $props.disabledBackground : $props.background || "transparent",
                borderRadius: $options.getRadius,
                fontSize: $options.getSize,
                color: $options.getColor
              }),
              loading: $props.loading,
              "form-type": $props.formType,
              "open-type": $props.openType,
              "app-parameter": $props.appParameter,
              onGetuserinfo: $options.bindgetuserinfo,
              onGetphonenumber: $options.bindgetphonenumber,
              onContact: $options.bindcontact,
              onError: $options.binderror,
              onOpensetting: $options.bindopensetting,
              onChooseavatar: $options.bindchooseavatar,
              onLaunchapp: $options.bindlaunchapp,
              disabled: $props.disabled,
              scope: $props.scope,
              onClick: (0, import_vue4.withModifiers)($options.handleTap, ["stop"])
            }, {
              default: (0, import_vue4.withCtx)(() => [
                $props.text ? ((0, import_vue4.openBlock)(), (0, import_vue4.createElementBlock)(
                  "u-text",
                  {
                    key: 0,
                    class: (0, import_vue4.normalizeClass)(["fui-button__text", { "fui-btn__gray-color": !$props.background && !$props.disabledBackground && !$props.plain && $props.type === "gray" && $props.color === "#fff", "fui-text__bold": $props.bold }]),
                    style: (0, import_vue4.normalizeStyle)({ fontSize: $options.getSize, lineHeight: $options.getSize, color: $options.getColor })
                  },
                  (0, import_vue4.toDisplayString)($props.text),
                  7
                  /* TEXT, CLASS, STYLE */
                )) : (0, import_vue4.createCommentVNode)("v-if", true),
                (0, import_vue4.renderSlot)(_ctx.$slots, "default")
              ]),
              _: 3
              /* FORWARDED */
            }, 8, ["class", "style", "loading", "form-type", "open-type", "app-parameter", "onGetuserinfo", "onGetphonenumber", "onContact", "onError", "onOpensetting", "onChooseavatar", "onLaunchapp", "disabled", "scope", "onClick"])
          ],
          38
          /* CLASS, STYLE, HYDRATE_EVENTS */
        )
      ],
      2112
      /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
    );
  }
  var __easycom_6 = /* @__PURE__ */ _export_sfc(_sfc_main3, [["render", _sfc_render3], ["styles", [_style_03]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-button/fui-button.vue"]]);

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/fui-icon.js
  var import_vue5 = __toESM(require_vue());
  var icons = {
    "addressbook": "\uE80C",
    "addfriends-fill": "\uE80A",
    "addfriends": "\uE80B",
    "backspace-fill": "\uE808",
    "backspace": "\uE809",
    "bankcard-fill": "\uE806",
    "bankcard": "\uE807",
    "camera-fill": "\uE804",
    "camera": "\uE805",
    "captcha-fill": "\uE802",
    "captcha": "\uE803",
    "cart-fill": "\uE800",
    "cart": "\uE801",
    "classify": "\uE7FE",
    "classify-fill": "\uE7FF",
    "comment-fill": "\uE7FC",
    "comment": "\uE7FD",
    "community-fill": "\uE7FA",
    "community": "\uE7FB",
    "coupon-fill": "\uE7F8",
    "coupon": "\uE7F9",
    "delete": "\uE7F6",
    "delete-fill": "\uE7F7",
    "edit": "\uE7F4",
    "edit-fill": "\uE7F5",
    "fabulous-fill": "\uE7F2",
    "fabulous": "\uE7F3",
    "find": "\uE7F0",
    "find-fill": "\uE7F1",
    "help-fill": "\uE7EE",
    "help": "\uE7EF",
    "home-fill": "\uE7EC",
    "home": "\uE7ED",
    "idcard-fill": "\uE7EA",
    "idcard": "\uE7EB",
    "info": "\uE7E8",
    "info-fill": "\uE7E9",
    "invite-fill": "\uE7E6",
    "invite": "\uE7E7",
    "kefu-fill": "\uE7E4",
    "kefu": "\uE7E5",
    "like-fill": "\uE7E2",
    "like": "\uE7E3",
    "location": "\uE7E0",
    "location-fill": "\uE7E1",
    "lock": "\uE7DE",
    "lock-fill": "\uE7DF",
    "mail-fill": "\uE7DC",
    "mail": "\uE7DD",
    "message": "\uE7DA",
    "message-fill": "\uE7DB",
    "mobile-fill": "\uE7D8",
    "mobile": "\uE7D9",
    "more": "\uE7D6",
    "more-fill": "\uE7D7",
    "my-fill": "\uE7D4",
    "my": "\uE7D5",
    "principal": "\uE80D",
    "notice-fill": "\uE7D2",
    "notice": "\uE7D3",
    "order": "\uE7D0",
    "order-fill": "\uE7D1",
    "picture": "\uE7CE",
    "picture-fill": "\uE7CF",
    "setup-fill": "\uE7CC",
    "setup": "\uE7CD",
    "share": "\uE7CA",
    "share-fill": "\uE7CB",
    "shop": "\uE7C8",
    "shop-fill": "\uE7C9",
    "star-fill": "\uE7C5",
    "star": "\uE7C6",
    "starhalf": "\uE7C7",
    "stepon-fill": "\uE7C3",
    "stepon": "\uE7C4",
    "wait-fill": "\uE7C1",
    "wait": "\uE7C2",
    "warning": "\uE7BF",
    "warning-fill": "\uE7C0",
    "plus": "\uE7BC",
    "plussign-fill": "\uE7BD",
    "plussign": "\uE7BE",
    "minus": "\uE7B9",
    "minussign": "\uE7BA",
    "minussign-fill": "\uE7BB",
    "close": "\uE7B8",
    "clear": "\uE7B6",
    "clear-fill": "\uE7B7",
    "checkbox-fill": "\uE7B5",
    "checkround": "\uE7B4",
    "checkbox": "\uE7B3",
    "check": "\uE7B2",
    "pulldown-fill": "\uE7AE",
    "pullup": "\uE7AF",
    "pullup-fill": "\uE7B0",
    "pulldown": "\uE7B1",
    "roundright-fill": "\uE7AC",
    "roundright": "\uE7AD",
    "arrowright": "\uE7A9",
    "arrowleft": "\uE7AA",
    "arrowdown": "\uE7AB",
    "left": "\uE7A6",
    "up": "\uE7A7",
    "right": "\uE7A8",
    "back": "\uE7A3",
    "top": "\uE7A4",
    "dropdown": "\uE7A5",
    "turningleft": "\uE79F",
    "turningup": "\uE7A0",
    "turningright": "\uE7A1",
    "turningdown": "\uE7A2",
    "refresh": "\uE79C",
    "loading": "\uE79D",
    "search": "\uE79E",
    "rotate": "\uE79B",
    "screen": "\uE79A",
    "signin": "\uE799",
    "calendar": "\uE798",
    "scan": "\uE797",
    "qrcode": "\uE796",
    "wallet": "\uE795",
    "telephone": "\uE794",
    "visible": "\uE793",
    "invisible": "\uE792",
    "menu": "\uE78E",
    "operate": "\uE78F",
    "slide": "\uE790",
    "list": "\uE791",
    "nonetwork": "\uE78D",
    "partake": "\uE78C",
    "qa": "\uE78B",
    "barchart": "\uE788",
    "piechart": "\uE789",
    "linechart": "\uE78A",
    "at": "\uE787",
    "face": "\uE77F",
    "redpacket": "\uE780",
    "suspend": "\uE781",
    "link": "\uE782",
    "keyboard": "\uE783",
    "play": "\uE784",
    "video": "\uE785",
    "voice": "\uE786",
    "sina": "\uE77A",
    "browser": "\uE77B",
    "moments": "\uE77C",
    "qq": "\uE77D",
    "wechat": "\uE77E",
    "balance": "\uE779",
    "bankcardpay": "\uE778",
    "wxpay": "\uE777",
    "alipay": "\uE776",
    "payment": "\uE818",
    "receive": "\uE817",
    "sendout": "\uE816",
    "evaluate": "\uE815",
    "aftersale": "\uE814",
    "warehouse": "\uE813",
    "transport": "\uE812",
    "delivery": "\uE811",
    "switch": "\uE810",
    "goods": "\uE80F",
    "goods-fill": "\uE80E"
  };
  var fuiicons = "/assets/fui-icon.9165208c.ttf";
  var _style_04 = { "fui-icon": { "": { "fontFamily": "fuiFont", "textDecoration": "none", "textAlign": "center" } } };
  var domModule = weex.requireModule("dom");
  domModule.addRule("fontFace", {
    "fontFamily": "fuiFont",
    "src": "url('" + fuiicons + "')"
  });
  var _sfc_main4 = {
    name: "fui-icon",
    emits: ["click"],
    props: {
      name: {
        type: String,
        default: ""
      },
      size: {
        type: [Number, String],
        default: 0
      },
      //rpx | px
      unit: {
        type: String,
        default: ""
      },
      color: {
        type: String,
        default: ""
      },
      //字重
      fontWeight: {
        type: [Number, String],
        default: "normal"
      },
      //是否禁用点击
      disabled: {
        type: Boolean,
        default: false
      },
      params: {
        type: [Number, String],
        default: 0
      },
      customPrefix: {
        type: String,
        default: ""
      },
      //是否显示为主色调，color为空时有效。【内部使用】
      primary: {
        type: Boolean,
        default: false
      }
    },
    computed: {
      getSize() {
        const size = uni.$fui && uni.$fui.fuiIcon && uni.$fui.fuiIcon.size || 64;
        const unit = uni.$fui && uni.$fui.fuiIcon && uni.$fui.fuiIcon.unit || "rpx";
        return (this.size || size) + (this.unit || unit);
      },
      primaryColor() {
        const app = uni && uni.$fui && uni.$fui.color;
        return app && app.primary || "#465CFF";
      },
      getColor() {
        const app = uni && uni.$fui && uni.$fui.fuiIcon;
        let color = this.color;
        if (!color || color && color === true) {
          color = app && app.color;
        }
        if (!color || color === true) {
          color = "#333333";
        }
        return color;
      }
    },
    data() {
      return {
        icons
      };
    },
    methods: {
      handleClick() {
        if (this.disabled)
          return;
        this.$emit("click", {
          params: this.params
        });
      }
    }
  };
  function _sfc_render4(_ctx, _cache, $props, $setup, $data, $options) {
    return (0, import_vue5.openBlock)(), (0, import_vue5.createElementBlock)(
      import_vue5.Fragment,
      null,
      [
        (0, import_vue5.createCommentVNode)("\u672C\u6587\u4EF6\u7531FirstUI\u6388\u6743\u4E88\u4E25\u5929\u8D60\uFF08\u4F1A\u5458ID\uFF1A24   42\uFF0C\u8EAB\u4EFD\u8BC1\u5C3E\u53F7\uFF1A 04  3 01 8\uFF09\u4E13\u7528\uFF0C\u8BF7\u5C0A\u91CD\u77E5\u8BC6\u4EA7\u6743\uFF0C\u52FF\u79C1\u4E0B\u4F20\u64AD\uFF0C\u8FDD\u8005\u8FFD\u7A76\u6CD5\u5F8B\u8D23\u4EFB\u3002"),
        (0, import_vue5.createElementVNode)(
          "u-text",
          {
            style: (0, import_vue5.normalizeStyle)({ color: $props.primary && (!$props.color || $props.color === true) ? $options.primaryColor : $options.getColor, fontSize: $options.getSize, lineHeight: $options.getSize, fontWeight: $props.fontWeight }),
            class: (0, import_vue5.normalizeClass)(["fui-icon", [$props.customPrefix && $props.customPrefix !== true ? $props.customPrefix : ""]]),
            onClick: _cache[0] || (_cache[0] = (...args) => $options.handleClick && $options.handleClick(...args))
          },
          (0, import_vue5.toDisplayString)($props.customPrefix && $props.customPrefix !== true ? $props.name : $data.icons[$props.name]),
          7
          /* TEXT, CLASS, STYLE */
        )
      ],
      2112
      /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
    );
  }
  var __easycom_5 = /* @__PURE__ */ _export_sfc(_sfc_main4, [["render", _sfc_render4], ["styles", [_style_04]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-icon/fui-icon.vue"]]);

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/pages/index/index.js
  var _style_0$4 = { "fui-badge__wrap": { "": { "height": "36rpx", "color": "#FFFFFF", "fontSize": "24rpx", "lineHeight": "36rpx", "borderRadius": 100, "flexDirection": "row", "alignItems": "center", "justifyContent": "center", "textAlign": "center", "zIndex": 10 } }, "fui-badge__dot": { "": { "!height": 8, "!width": 8, "borderRadius": 100, "zIndex": 10 } }, "fui-badge__text-color": { "": { "!color": "#FF2B2B" } }, "fui-badge__trans-origin": { "": { "transformOrigin": "center center" } }, "fui-badge__absolute": { "": { "position": "absolute" } } };
  uni.getSystemInfoSync();
  var _sfc_main$5 = {
    name: "fui-badge",
    emits: ["click"],
    props: {
      value: {
        type: [Number, String],
        default: ""
      },
      max: {
        type: [Number, String],
        default: -1
      },
      //类型：primary，success，warning，danger，purple，white
      type: {
        type: String,
        default: "primary"
      },
      //背景色，如果设置背景则type失效
      background: {
        type: String,
        default: ""
      },
      //字体颜色
      color: {
        type: String,
        default: "#FFFFFF"
      },
      //是否显示为圆点
      dot: {
        type: Boolean,
        default: false
      },
      //margin-top值，单位rpx
      marginTop: {
        type: [Number, String],
        default: 0
      },
      //margin-left值，单位rpx
      marginLeft: {
        type: [Number, String],
        default: 0
      },
      //margin-right值，单位rpx
      marginRight: {
        type: [Number, String],
        default: 0
      },
      //是否绝对定位
      absolute: {
        type: Boolean,
        default: false
      },
      top: {
        type: String,
        default: "-8rpx"
      },
      right: {
        type: String,
        default: "-18rpx"
      },
      //缩放比例
      scaleRatio: {
        type: Number,
        default: 1
      }
    },
    data() {
      let isNvue = false;
      isNvue = true;
      return {
        isNvue,
        width: 0,
        showValue: ""
      };
    },
    computed: {
      getBgColor() {
        let color = this.background;
        if (!color && this.type) {
          const app = uni && uni.$fui && uni.$fui.color;
          const colors = {
            primary: app && app.primary || "#465CFF",
            success: app && app.success || "#09BE4F",
            warning: app && app.warning || "#FFB703",
            danger: app && app.danger || "#FF2B2B",
            purple: app && app.purple || "#6831FF",
            white: "#FFFFFF"
          };
          color = colors[this.type] || colors.primary;
        }
        return color;
      }
    },
    watch: {
      value(val) {
        this.getWidth();
      }
    },
    mounted() {
      this.getWidth();
    },
    methods: {
      _getTextWidth(text) {
        let sum = 0;
        for (let i = 0, len = text.length; i < len; i++) {
          if (text.charCodeAt(i) >= 0 && text.charCodeAt(i) <= 255)
            sum = sum + 1;
          else
            sum = sum + 2;
        }
        const px = uni.upx2px(text.length > 1 ? 32 : 24);
        var strCode = text.charCodeAt();
        let multiplier = 12;
        if (strCode >= 65 && strCode <= 90) {
          multiplier = 15;
        }
        return sum / 2 * multiplier + px + "px";
      },
      getWidth() {
        let max = Number(this.max);
        let val = Number(this.value);
        let value = "";
        if (isNaN(val) || max === -1) {
          value = this.value;
        } else {
          value = val > max ? `${max}+` : val;
        }
        this.showValue = value;
        this.width = this.dot ? "8px" : this._getTextWidth(String(value));
      },
      handleClick() {
        this.$emit("click");
      }
    }
  };
  function _sfc_render$5(_ctx, _cache, $props, $setup, $data, $options) {
    return (0, import_vue6.openBlock)(), (0, import_vue6.createElementBlock)(
      import_vue6.Fragment,
      null,
      [
        (0, import_vue6.createCommentVNode)("\u672C\u6587\u4EF6\u7531FirstUI\u6388\u6743\u4E88\u4E25\u5929\u8D60\uFF08\u4F1A\u5458ID\uFF1A  244 2\uFF0C\u8EAB\u4EFD\u8BC1\u5C3E\u53F7\uFF1A04 3 01   8\uFF09\u4E13\u7528\uFF0C\u8BF7\u5C0A\u91CD\u77E5\u8BC6\u4EA7\u6743\uFF0C\u52FF\u79C1\u4E0B\u4F20\u64AD\uFF0C\u8FDD\u8005\u8FFD\u7A76\u6CD5\u5F8B\u8D23\u4EFB\u3002"),
        $data.showValue || $props.dot ? ((0, import_vue6.openBlock)(), (0, import_vue6.createElementBlock)(
          "u-text",
          {
            key: 0,
            class: (0, import_vue6.normalizeClass)([$props.dot ? "fui-badge__dot" : "fui-badge__wrap", $props.background ? "" : "fui-badge__bg-" + $props.type, $props.absolute ? "fui-badge__absolute" : "", $props.scaleRatio != 1 && $data.isNvue ? "fui-badge__trans-origin" : "", !$props.background && $props.type === "white" ? "fui-badge__text-color" : ""]),
            style: (0, import_vue6.normalizeStyle)({ top: $props.absolute ? $props.top : "auto", right: $props.absolute ? $props.right : "auto", zoom: $props.scaleRatio, transform: $data.isNvue ? `scale(${$props.scaleRatio})` : "scale(1)", marginTop: $props.marginTop + "rpx", marginLeft: $props.marginLeft + "rpx", marginRight: $props.marginRight + "rpx", width: $data.width, color: $props.color, background: $options.getBgColor }),
            onClick: _cache[0] || (_cache[0] = (...args) => $options.handleClick && $options.handleClick(...args))
          },
          (0, import_vue6.toDisplayString)($props.dot ? "" : $data.showValue),
          7
          /* TEXT, CLASS, STYLE */
        )) : (0, import_vue6.createCommentVNode)("v-if", true)
      ],
      2112
      /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
    );
  }
  var __easycom_1 = /* @__PURE__ */ _export_sfc(_sfc_main$5, [["render", _sfc_render$5], ["styles", [_style_0$4]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-badge/fui-badge.vue"]]);
  var mpwxs = {};
  var mpjs$1 = {};
  var BindingX = requireNativePlugin("bindingx");
  var dom$1 = requireNativePlugin("dom");
  var animation$2 = requireNativePlugin("animation");
  var bindingx$1 = {
    data() {
      return {};
    },
    watch: {
      show(newVal) {
        if (this.stop)
          return;
        this.stop = true;
        if (newVal) {
          this.open(newVal);
        } else {
          this.close();
        }
      },
      buttons(newVal) {
        this.init();
      }
    },
    created() {
      this.group = this.getParent();
      if (this.group.children !== void 0) {
        this.group.children.push(this);
      }
    },
    mounted() {
      this.$nextTick(() => {
        this.box = this.getEl(this.$refs["fui_swipea_wrap"]);
        this.selector = this.getEl(this.$refs["fui_swipea_content"]);
        this.rightButton = this.getEl(this.$refs["fui_swipea_buttons"]);
      });
      this.init();
    },
    methods: {
      init() {
        this.$nextTick(() => {
          this.x = 0;
          this.button = {
            show: false
          };
          setTimeout(() => {
            this.getSelectorQuery();
          }, 200);
        });
      },
      handleClick(e, index2, item) {
        e.stopPropagation();
        this.$emit("click", {
          item,
          index: index2,
          param: this.param
        });
      },
      touchstart(e) {
        if (this.disabled)
          return;
        if (this.stop)
          return;
        this.stop = true;
        if (this.autoClose) {
          this.group && this.group.closeAuto(this);
        }
        const rightWidth = this.button.right.width || 0;
        let expression = this.range(this.x, -rightWidth, 0);
        let rightExpression = this.range(this.x + rightWidth, 0, rightWidth);
        this.eventpan = BindingX.bind({
          anchor: this.box,
          eventType: "pan",
          props: [{
            element: this.selector,
            property: "transform.translateX",
            expression
          }, {
            element: this.rightButton,
            property: "transform.translateX",
            expression: rightExpression
          }]
        }, (e2) => {
          if (e2.state === "end") {
            this.x = e2.deltaX + this.x;
            this.isclick = true;
            this.bindTiming(e2.deltaX);
          }
        });
      },
      touchend(e) {
        if (this.isopen && !this.isclick && this.clickClose) {
          this.open(false);
        }
      },
      bindTiming(x) {
        const left = this.x;
        this.button.right.width || 0;
        const threshold = Number(this.threshold);
        if (!this.isopen) {
          if (left < -threshold) {
            this.open(true);
          } else {
            this.open(false);
          }
        } else {
          if (x < threshold && x > 0 || x < -threshold) {
            this.open(true);
          } else {
            this.open(false);
          }
        }
      },
      range(num, mix, max) {
        return `min(max(x+${num}, ${mix}), ${max})`;
      },
      open(type) {
        this.animation(type);
      },
      close() {
        this.animation(false);
      },
      animation(type) {
        const rightWidth = this.button.right.width || 0;
        if (this.eventpan && this.eventpan.token) {
          BindingX.unbind({
            token: this.eventpan.token,
            eventType: "pan"
          });
        }
        if (type) {
          Promise.all([
            this.move(this.selector, -rightWidth),
            this.move(this.rightButton, 0)
          ]).then(() => {
            this.setEmit(-rightWidth, type);
          });
        } else {
          Promise.all([
            this.move(this.selector, 0),
            this.move(this.rightButton, rightWidth)
          ]).then(() => {
            this.setEmit(0, type);
          });
        }
      },
      setEmit(x, type) {
        const rightWidth = this.button.right.width;
        this.isopen = this.isopen || false;
        this.stop = false;
        this.isclick = false;
        if (this.isopen !== type && this.x !== x) {
          if (type && rightWidth > 0) {
            this.$emit("change", {
              isOpen: true,
              param: this.param
            });
          }
          if (!type) {
            this.$emit("change", {
              isOpen: false,
              param: this.param
            });
          }
        }
        this.x = x;
        this.isopen = type;
      },
      move(ref, value) {
        return new Promise((resolve, reject) => {
          animation$2.transition(ref, {
            styles: {
              transform: `translateX(${value})`
            },
            duration: 150,
            //ms
            timingFunction: "linear",
            needLayout: false,
            delay: 0
            //ms
          }, function(res) {
            resolve(res);
          });
        });
      },
      getEl(el) {
        return el.ref;
      },
      getSelectorQuery() {
        Promise.all([
          this.getDom()
        ]).then((data) => {
          this.open(this.show);
        });
      },
      getDom() {
        return new Promise((resolve, reject) => {
          dom$1.getComponentRect(this.$refs["fui_swipea_buttons"], (data) => {
            if (data) {
              this.button["right"] = data.size;
              resolve(data);
            } else {
              reject();
            }
          });
        });
      }
    }
  };
  var _style_0$3 = { "fui-swipe__action-wrap": { "": { "position": "relative" } }, "fui-swipe__action-inner": { "": { "position": "relative" } }, "fui-swipe__action-left": { "": { "flex": 1 } }, "fui-swipe__action-right": { "": { "flexDirection": "row", "position": "absolute", "top": 0, "bottom": 0, "right": 0, "transform": "translateX(100%)" } }, "fui-swipe__action-btn": { "": { "flex": 1, "flexDirection": "row", "justifyContent": "center", "alignItems": "center", "paddingTop": 0, "paddingRight": "48rpx", "paddingBottom": 0, "paddingLeft": "48rpx" } }, "fui-swipe__action-ani": { "": { "transitionProperty": "transform", "transitionDuration": 300, "transitionTimingFunction": "cubic-bezier(0.165,0.84,0.44,1)" } }, "@TRANSITION": { "fui-swipe__action-ani": { "property": "transform", "duration": 300, "timingFunction": "cubic-bezier(0.165,0.84,0.44,1)" } } };
  var dangerColor = uni && uni.$fui && uni.$fui.color && uni.$fui.color.danger || "#FF2B2B";
  var _sfc_main$4 = {
    name: "fui-swipe-action",
    mixins: [mpwxs, mpjs$1, bindingx$1],
    emits: ["click", "change"],
    props: {
      buttons: {
        type: Array,
        default() {
          return [{
            text: "\u5220\u9664",
            background: dangerColor
          }];
        }
      },
      size: {
        type: [Number, String],
        default: 32
      },
      color: {
        type: String,
        default: "#fff"
      },
      show: {
        type: Boolean,
        default: false
      },
      threshold: {
        type: [Number, String],
        default: 30
      },
      disabled: {
        type: Boolean,
        default: false
      },
      autoClose: {
        type: Boolean,
        default: true
      },
      //v2.1.0+ 点击当前菜单是否立即关闭菜单
      clickClose: {
        type: Boolean,
        default: true
      },
      //1.9.9+
      marginTop: {
        type: [Number, String],
        default: 0
      },
      //1.9.9+
      marginBottom: {
        type: [Number, String],
        default: 0
      },
      param: {
        type: [Number, String],
        default: 0
      }
    },
    beforeUnmount() {
      this.__beforeUnmount = true;
      this.unInstall();
    },
    methods: {
      unInstall() {
        if (this.group) {
          this.group.children.forEach((item, index2) => {
            if (item === this) {
              this.group.children.splice(index2, 1);
            }
          });
        }
      },
      //获取父元素实例
      getParent(name = "fui-swipeaction-group") {
        let parent = this.$parent;
        let parentName = parent.$options.name;
        while (parentName !== name) {
          parent = parent.$parent;
          if (!parent)
            return false;
          parentName = parent.$options.name;
        }
        return parent;
      }
    }
  };
  function _sfc_render$4(_ctx, _cache, $props, $setup, $data, $options) {
    return (0, import_vue6.openBlock)(), (0, import_vue6.createElementBlock)(
      import_vue6.Fragment,
      null,
      [
        (0, import_vue6.createCommentVNode)("\u672C\u6587\u4EF6\u7531FirstUI\u6388\u6743\u4E88\u4E25\u5929\u8D60\uFF08\u4F1A\u5458ID\uFF1A244   2\uFF0C\u8EAB\u4EFD\u8BC1\u5C3E\u53F7\uFF1A0 4 3   018\uFF09\u4E13\u7528\uFF0C\u8BF7\u5C0A\u91CD\u77E5\u8BC6\u4EA7\u6743\uFF0C\u52FF\u79C1\u4E0B\u4F20\u64AD\uFF0C\u8FDD\u8005\u8FFD\u7A76\u6CD5\u5F8B\u8D23\u4EFB\u3002"),
        (0, import_vue6.createElementVNode)(
          "view",
          {
            class: "fui-swipe__action-wrap",
            style: (0, import_vue6.normalizeStyle)({ marginTop: $props.marginTop + "rpx", marginBottom: $props.marginBottom + "rpx" }),
            ref: "fui_swipea_wrap",
            onHorizontalpan: _cache[0] || (_cache[0] = (...args) => _ctx.touchstart && _ctx.touchstart(...args)),
            onTouchend: _cache[1] || (_cache[1] = (...args) => _ctx.touchend && _ctx.touchend(...args))
          },
          [
            (0, import_vue6.createElementVNode)(
              "view",
              {
                class: "fui-swipe__action-right",
                ref: "fui_swipea_buttons"
              },
              [
                (0, import_vue6.renderSlot)(_ctx.$slots, "buttons", {}, () => [
                  ((0, import_vue6.openBlock)(true), (0, import_vue6.createElementBlock)(
                    import_vue6.Fragment,
                    null,
                    (0, import_vue6.renderList)($props.buttons, (item, index2) => {
                      return (0, import_vue6.openBlock)(), (0, import_vue6.createElementBlock)("view", {
                        class: "fui-swipe__action-btn",
                        style: (0, import_vue6.normalizeStyle)({ background: item.background }),
                        key: index2,
                        onClick: (0, import_vue6.withModifiers)(($event) => _ctx.handleClick($event, index2, item), ["stop"])
                      }, [
                        (0, import_vue6.createElementVNode)(
                          "u-text",
                          {
                            class: "fui-swipe__action-text",
                            style: (0, import_vue6.normalizeStyle)({ fontSize: (item.size || $props.size) + "rpx", color: item.color || $props.color })
                          },
                          (0, import_vue6.toDisplayString)(item.text),
                          5
                          /* TEXT, STYLE */
                        )
                      ], 12, ["onClick"]);
                    }),
                    128
                    /* KEYED_FRAGMENT */
                  ))
                ])
              ],
              512
              /* NEED_PATCH */
            ),
            (0, import_vue6.createElementVNode)(
              "view",
              {
                class: "fui-swipe__action-left",
                ref: "fui_swipea_content"
              },
              [
                (0, import_vue6.renderSlot)(_ctx.$slots, "default")
              ],
              512
              /* NEED_PATCH */
            )
          ],
          36
          /* STYLE, HYDRATE_EVENTS */
        )
      ],
      2112
      /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
    );
  }
  var __easycom_32 = /* @__PURE__ */ _export_sfc(_sfc_main$4, [["render", _sfc_render$4], ["styles", [_style_0$3]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-swipe-action/fui-swipe-action.vue"]]);
  var _sfc_main$3 = {
    name: "fui-swipeaction-group",
    data() {
      return {};
    },
    created() {
      this.children = [];
    },
    methods: {
      reset() {
        this.children.forEach((child) => {
          child.init();
        });
      },
      close() {
        this.children.forEach((child) => {
          child.close();
        });
      },
      closeAuto(child) {
        if (this.openItem && this.openItem !== child) {
          this.openItem.close();
        }
        this.openItem = child;
      }
    }
  };
  function _sfc_render$3(_ctx, _cache, $props, $setup, $data, $options) {
    return (0, import_vue6.openBlock)(), (0, import_vue6.createElementBlock)(
      import_vue6.Fragment,
      null,
      [
        (0, import_vue6.createCommentVNode)("\u672C\u6587\u4EF6\u7531FirstUI\u6388\u6743\u4E88\u4E25\u5929\u8D60\uFF08\u4F1A\u5458ID\uFF1A24 4  2\uFF0C\u8EAB\u4EFD\u8BC1\u5C3E\u53F7\uFF1A0  4 30 1 8\uFF09\u4E13\u7528\uFF0C\u8BF7\u5C0A\u91CD\u77E5\u8BC6\u4EA7\u6743\uFF0C\u52FF\u79C1\u4E0B\u4F20\u64AD\uFF0C\u8FDD\u8005\u8FFD\u7A76\u6CD5\u5F8B\u8D23\u4EFB\u3002"),
        (0, import_vue6.createElementVNode)("view", null, [
          (0, import_vue6.renderSlot)(_ctx.$slots, "default")
        ])
      ],
      2112
      /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
    );
  }
  var __easycom_4 = /* @__PURE__ */ _export_sfc(_sfc_main$3, [["render", _sfc_render$3], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-swipeaction-group/fui-swipeaction-group.vue"]]);
  var _style_0$2 = { "fui-empty__wrap": { "": { "flex": 1, "flexDirection": "column", "alignItems": "center", "justifyContent": "center" } }, "fui-empty__fixed": { "": { "position": "fixed", "left": 0, "top": 0, "right": 0, "bottom": 0, "zIndex": 99 } }, "fui-empty__title": { "": { "textAlign": "center", "fontWeight": "500", "paddingTop": "48rpx" } }, "fui-empty__desc": { "": { "textAlign": "center", "fontWeight": "normal", "paddingTop": "8rpx" } } };
  var _sfc_main$2 = {
    name: "fui-empty",
    props: {
      src: {
        type: String,
        default: ""
      },
      width: {
        type: [Number, String],
        default: 576
      },
      height: {
        type: [Number, String],
        default: 318
      },
      title: {
        type: String,
        default: ""
      },
      color: {
        type: String,
        default: "#333333"
      },
      size: {
        type: [Number, String],
        default: 32
      },
      descr: {
        type: String,
        default: ""
      },
      descrColor: {
        type: String,
        default: "#B2B2B2"
      },
      descrSize: {
        type: [Number, String],
        default: 24
      },
      isFixed: {
        type: Boolean,
        default: false
      },
      marginTop: {
        type: [Number, String],
        default: 0
      }
    }
  };
  function _sfc_render$2(_ctx, _cache, $props, $setup, $data, $options) {
    return (0, import_vue6.openBlock)(), (0, import_vue6.createElementBlock)(
      import_vue6.Fragment,
      null,
      [
        (0, import_vue6.createCommentVNode)("\u672C\u6587\u4EF6\u7531FirstUI\u6388\u6743\u4E88\u4E25\u5929\u8D60\uFF08\u4F1A\u5458ID\uFF1A   2442\uFF0C\u8EAB\u4EFD\u8BC1\u5C3E\u53F7\uFF1A 043    018\uFF09\u4E13\u7528\uFF0C\u8BF7\u5C0A\u91CD\u77E5\u8BC6\u4EA7\u6743\uFF0C\u52FF\u79C1\u4E0B\u4F20\u64AD\uFF0C\u8FDD\u8005\u8FFD\u7A76\u6CD5\u5F8B\u8D23\u4EFB\u3002"),
        (0, import_vue6.createElementVNode)(
          "view",
          {
            class: (0, import_vue6.normalizeClass)(["fui-empty__wrap", { "fui-empty__fixed": $props.isFixed }]),
            style: (0, import_vue6.normalizeStyle)({ marginTop: $props.marginTop + "rpx" })
          },
          [
            $props.src ? ((0, import_vue6.openBlock)(), (0, import_vue6.createElementBlock)("u-image", {
              key: 0,
              src: $props.src,
              style: (0, import_vue6.normalizeStyle)({ width: $props.width + "rpx", height: $props.height + "rpx" }),
              mode: "widthFix"
            }, null, 12, ["src"])) : (0, import_vue6.createCommentVNode)("v-if", true),
            $props.title ? ((0, import_vue6.openBlock)(), (0, import_vue6.createElementBlock)(
              "u-text",
              {
                key: 1,
                class: (0, import_vue6.normalizeClass)(["fui-empty__title", { "fui-empty__title-color": !$props.color }]),
                style: (0, import_vue6.normalizeStyle)({ color: $props.color, fontSize: $props.size + "rpx" })
              },
              (0, import_vue6.toDisplayString)($props.title),
              7
              /* TEXT, CLASS, STYLE */
            )) : (0, import_vue6.createCommentVNode)("v-if", true),
            $props.descr ? ((0, import_vue6.openBlock)(), (0, import_vue6.createElementBlock)(
              "u-text",
              {
                key: 2,
                class: (0, import_vue6.normalizeClass)(["fui-empty__desc", { "fui-empty__descr-color": !$props.descrColor }]),
                style: (0, import_vue6.normalizeStyle)({ color: $props.descrColor, fontSize: $props.descrSize + "rpx" })
              },
              (0, import_vue6.toDisplayString)($props.descr),
              7
              /* TEXT, CLASS, STYLE */
            )) : (0, import_vue6.createCommentVNode)("v-if", true),
            (0, import_vue6.renderSlot)(_ctx.$slots, "default")
          ],
          6
          /* CLASS, STYLE */
        )
      ],
      2112
      /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
    );
  }
  var __easycom_62 = /* @__PURE__ */ _export_sfc(_sfc_main$2, [["render", _sfc_render$2], ["styles", [_style_0$2]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-empty/fui-empty.vue"]]);
  var mpjs = {};
  var animation$1 = requireNativePlugin("animation");
  var bindingx = {
    data() {
      return {
        startX: 0,
        startY: 0,
        lastLeft: 0,
        lastTop: 0,
        isMove: false
      };
    },
    created() {
      this.refFab = null;
      this.loop = null;
    },
    mounted() {
      this.$nextTick(() => {
        setTimeout(() => {
          this.refFab = this.getEl(this.$refs["fui_fab_move_ref"]);
        }, 50);
      });
    },
    methods: {
      getEl(el) {
        return el.ref || el[0].ref;
      },
      _aniMove(x, y) {
        if (!this.refFab || !this.isDrag)
          return;
        animation$1.transition(this.refFab, {
          styles: {
            transform: `translate(${x}px,${y}px)`
          },
          duration: 0,
          timingFunction: "linear",
          needLayout: false,
          delay: 0
        }, () => {
          if (Math.abs(x) > 0.1 || Math.abs(y) > 0.1) {
            this.isMove = true;
          }
        });
      },
      touchstart(e) {
        if (!this.isDrag)
          return;
        var touch = e.touches || e.changedTouches;
        this.startX = touch[0].screenX;
        this.startY = touch[0].screenY;
      },
      touchmove(e) {
        if (!this.isDrag)
          return;
        var touch = e.touches || e.changedTouches;
        let pageX = touch[0].screenX, pageY = touch[0].screenY;
        var left = pageX - this.startX + this.lastLeft;
        left = left < -this.eLeft ? -this.eLeft : left;
        left = left > this.maxWidth ? this.maxWidth : left;
        this.startX = pageX;
        var top = pageY - this.startY + this.lastTop;
        top = top < -this.eTop ? -this.eTop : top;
        top = top > this.maxHeight ? this.maxHeight : top;
        this.startY = pageY;
        this.lastLeft = left;
        this.lastTop = top;
        this._aniMove(left, top);
      },
      touchend(e) {
        clearTimeout(this.loop);
        this.loop = setTimeout(() => {
          this.isMove = false;
        }, 50);
      }
    }
  };
  var _style_0$1 = { "fui-fab__mask": { "": { "position": "fixed", "left": 0, "right": 0, "top": 0, "bottom": 0, "opacity": 0, "transitionDuration": 250, "transitionProperty": "opacity", "transitionTimingFunction": "ease-in-out", "transform": "translateX(-100%)" } }, "fui-fab__mask-show": { "": { "opacity": 1, "transform": "translateX(0)" } }, "fui-fab__btn-wrap": { "": { "flexDirection": "column", "justifyContent": "center", "position": "fixed", "bottom": "120rpx" } }, "fui-fab__wrap-left": { "": { "alignItems": "flex-start", "left": "80rpx" } }, "fui-fab__wrap-right": { "": { "alignItems": "flex-end", "right": "80rpx" } }, "fui-fab__btn-list": { "": { "flexDirection": "column" } }, "fui-fab__list-left": { "": { "transformOrigin": "0 100%", "alignItems": "flex-start" } }, "fui-fab__list-right": { "": { "transformOrigin": "100% 100%", "alignItems": "flex-end" } }, "fui-fab__btn-hidden": { "": { "width": 0, "height": 0 } }, "fui-fab__button-box": { "": { "flexDirection": "row", "justifyContent": "flex-end", "alignItems": "center", "marginBottom": "32rpx" } }, "fui-fab__button-left": { "": { "flexDirection": "row-reverse", "justifyContent": "flex-start" } }, "fui-fab__button-right": { "": { "flexDirection": "row", "justifyContent": "flex-end" } }, "fui-fab__btn-text": { "": { "paddingLeft": "24rpx", "paddingRight": "24rpx", "fontWeight": "normal" } }, "fui-fab__button": { "": { "borderRadius": 100, "flexDirection": "row", "alignItems": "center", "justifyContent": "center" } }, "fui-fab__btn-abbr": { "": { "textAlign": "center", "fontWeight": "normal" } }, "fui-fab__btn-main": { "": { "borderRadius": 100, "alignItems": "center", "justifyContent": "center", "transform": "rotate(0deg)", "overflow": "hidden" } }, "fui-fab__btn-inner": { "": { "alignItems": "center", "justifyContent": "center" } }, "@TRANSITION": { "fui-fab__mask": { "duration": 250, "property": "opacity", "timingFunction": "ease-in-out" } } };
  var animation = requireNativePlugin("animation");
  var dom = requireNativePlugin("dom");
  var _sfc_main$1 = {
    name: "fui-fab",
    mixins: [mpjs, bindingx],
    emits: ["click", "change"],
    // components:{
    // 	fuiIcon
    // },
    props: {
      fabs: {
        type: Array,
        default() {
          return [];
        }
      },
      position: {
        type: String,
        default: "right"
      },
      distance: {
        type: [Number, String],
        default: 80
      },
      bottom: {
        type: [Number, String],
        default: 120
      },
      width: {
        type: [Number, String],
        default: 108
      },
      background: {
        type: String,
        default: ""
      },
      color: {
        type: String,
        default: "#fff"
      },
      mask: {
        type: Boolean,
        default: true
      },
      maskBackground: {
        type: String,
        default: "rgba(0,0,0,.6)"
      },
      maskClosable: {
        type: Boolean,
        default: false
      },
      zIndex: {
        type: [Number, String],
        default: 99
      },
      //V1.9.8+
      isDrag: {
        type: Boolean,
        default: false
      }
    },
    computed: {
      getStyles() {
        let styles = `bottom:${this.bottom}rpx;z-index:${this.zIndex};`;
        if (this.position === "left") {
          styles += `left:${this.distance}rpx;`;
        } else {
          styles += `right:${this.distance}rpx;`;
        }
        return styles;
      },
      getStyle() {
        return `background:${this.maskBackground};z-index:${Number(this.zIndex) - 10};`;
      },
      getBgColor() {
        let color = this.background;
        if (!color || color === true) {
          const app = uni && uni.$fui && uni.$fui.color;
          color = app && app.primary || "#465CFF";
        }
        return color;
      }
    },
    watch: {
      isShow(val) {
        this.$emit("change", {
          isShow: val
        });
      },
      position(val) {
        this.resetNum++;
        this.$nextTick(() => {
          setTimeout(() => {
            this._getSize();
          }, 80);
        });
      }
    },
    data() {
      return {
        isShow: false,
        isHidden: true,
        timer: null,
        maxWidth: 0,
        maxHeight: 0,
        eLeft: 0,
        eTop: 0,
        resetNum: 0
      };
    },
    mounted() {
      this.$nextTick(() => {
        if (!this.$refs["fui_fab_ani"])
          return;
        let styles = {
          transform: "scale(0)",
          opacity: 0
        };
        animation.transition(
          this.$refs["fui_fab_ani"].ref,
          {
            styles,
            duration: 0,
            needLayout: false,
            delay: 0
          },
          () => {
          }
        );
        setTimeout(() => {
          this._getSize();
        }, 50);
      });
    },
    methods: {
      stop() {
      },
      _getSize() {
        if (!this.isDrag)
          return;
        const sys = uni.getSystemInfoSync();
        dom.getComponentRect(this.$refs["fui_fab_move_ref"], (ret) => {
          const size = ret.size;
          if (size) {
            this.maxWidth = sys.windowWidth - size.width - size.left;
            this.maxHeight = sys.windowHeight - size.height - size.top;
            this.eLeft = size.left || 0;
            this.eTop = size.top || 0;
          }
        });
      },
      _animation(type) {
        let styles = {
          transform: `scale(${type ? 1 : 0})`,
          opacity: type ? 1 : 0
        };
        if (!this.$refs["fui_fab_ani"] || !this.$refs["fui_fm_ani"])
          return;
        if (this.mask && this.$refs["fui_mask_ani"]) {
          animation.transition(
            this.$refs["fui_mask_ani"].ref,
            {
              styles: {
                transform: `translateX(${type ? "0" : "-100%"})`
              },
              duration: 0,
              needLayout: false,
              delay: 0
              //ms
            },
            () => {
            }
          );
        }
        animation.transition(
          this.$refs["fui_fm_ani"].ref,
          {
            styles: {
              transform: `rotate(${type ? "135deg" : "0deg"})`
            },
            duration: 250,
            timingFunction: "ease-in-out",
            needLayout: false,
            delay: 0
            //ms
          },
          () => {
            if (!type) {
              this.isHidden = true;
            }
          }
        );
        animation.transition(
          this.$refs["fui_fab_ani"].ref,
          {
            styles,
            duration: 250,
            timingFunction: "ease-in-out",
            needLayout: false,
            delay: 0
            //ms
          },
          () => {
          }
        );
      },
      handleClick: function(e, index2) {
        e.stopPropagation();
        if (this.isMove) {
          this.isMove = false;
          return;
        }
        this.isHidden = false;
        clearTimeout(this.timer);
        this.$nextTick(() => {
          if (index2 === -1 && this.fabs.length > 0) {
            this.isShow = !this.isShow;
            this._animation(this.isShow);
          } else {
            this.$emit("click", {
              index: index2
            });
            this.isShow = false;
            this._animation(this.isShow);
          }
        });
      },
      maskClick(e) {
        e.stopPropagation();
        if (!this.maskClosable)
          return;
        this.isShow = false;
        this._animation(this.isShow);
      }
    }
  };
  function _sfc_render$1(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_fui_icon = resolveEasycom((0, import_vue6.resolveDynamicComponent)("fui-icon"), __easycom_5);
    return (0, import_vue6.openBlock)(), (0, import_vue6.createElementBlock)(
      import_vue6.Fragment,
      null,
      [
        (0, import_vue6.createCommentVNode)("\u672C\u6587\u4EF6\u7531FirstUI\u6388\u6743\u4E88\u4E25\u5929\u8D60\uFF08\u4F1A\u5458ID\uFF1A 24 4 2\uFF0C\u8EAB\u4EFD\u8BC1\u5C3E\u53F7\uFF1A 0  430  18\uFF09\u4E13\u7528\uFF0C\u8BF7\u5C0A\u91CD\u77E5\u8BC6\u4EA7\u6743\uFF0C\u52FF\u79C1\u4E0B\u4F20\u64AD\uFF0C\u8FDD\u8005\u8FFD\u7A76\u6CD5\u5F8B\u8D23\u4EFB\u3002"),
        (0, import_vue6.createElementVNode)(
          "view",
          {
            onTouchmove: _cache[6] || (_cache[6] = (0, import_vue6.withModifiers)((...args) => $options.stop && $options.stop(...args), ["stop", "prevent"]))
          },
          [
            $props.mask ? ((0, import_vue6.openBlock)(), (0, import_vue6.createElementBlock)(
              "view",
              {
                key: 0,
                class: (0, import_vue6.normalizeClass)(["fui-fab__mask", { "fui-fab__mask-show": $data.isShow }]),
                style: (0, import_vue6.normalizeStyle)($options.getStyle),
                ref: "fui_mask_ani",
                onClick: _cache[0] || (_cache[0] = (0, import_vue6.withModifiers)((...args) => $options.maskClick && $options.maskClick(...args), ["stop"]))
              },
              null,
              6
              /* CLASS, STYLE */
            )) : (0, import_vue6.createCommentVNode)("v-if", true),
            (0, import_vue6.createElementVNode)(
              "view",
              {
                class: (0, import_vue6.normalizeClass)(["fui-fab__btn-wrap", [$props.position === "left" ? "fui-fab__wrap-left" : "fui-fab__wrap-right"]]),
                onTouchstart: _cache[2] || (_cache[2] = (...args) => _ctx.touchstart && _ctx.touchstart(...args)),
                onTouchmove: _cache[3] || (_cache[3] = (0, import_vue6.withModifiers)((...args) => _ctx.touchmove && _ctx.touchmove(...args), ["stop", "prevent"])),
                onTouchend: _cache[4] || (_cache[4] = (...args) => _ctx.touchend && _ctx.touchend(...args)),
                onTouchcancel: _cache[5] || (_cache[5] = (...args) => _ctx.touchend && _ctx.touchend(...args)),
                ref: "fui_fab_move_ref",
                style: (0, import_vue6.normalizeStyle)($options.getStyles)
              },
              [
                (0, import_vue6.createElementVNode)(
                  "view",
                  {
                    class: (0, import_vue6.normalizeClass)(["fui-fab__btn-list", { "fui-fab__btn-hidden": $data.isHidden, "fui-fab__list-ani": $data.isShow, "fui-fab__list-left": $props.position === "left", "fui-fab__list-right": $props.position === "right" }]),
                    ref: "fui_fab_ani"
                  },
                  [
                    ((0, import_vue6.openBlock)(true), (0, import_vue6.createElementBlock)(
                      import_vue6.Fragment,
                      null,
                      (0, import_vue6.renderList)($props.fabs, (btn, idx) => {
                        return (0, import_vue6.openBlock)(), (0, import_vue6.createElementBlock)("view", {
                          class: (0, import_vue6.normalizeClass)(["fui-fab__button-box", [$props.position === "left" ? "fui-fab__button-left" : "fui-fab__button-right"]]),
                          key: idx,
                          onClick: (0, import_vue6.withModifiers)(($event) => $options.handleClick($event, idx), ["stop"])
                        }, [
                          btn.text ? ((0, import_vue6.openBlock)(), (0, import_vue6.createElementBlock)(
                            "u-text",
                            {
                              key: 0,
                              class: "fui-fab__btn-text",
                              style: (0, import_vue6.normalizeStyle)({ fontSize: (btn.size || 32) + "rpx", color: btn.color || "#fff", textAlign: $props.position === "left" ? "left" : "right" })
                            },
                            (0, import_vue6.toDisplayString)(btn.text),
                            5
                            /* TEXT, STYLE */
                          )) : (0, import_vue6.createCommentVNode)("v-if", true),
                          (0, import_vue6.createElementVNode)(
                            "view",
                            {
                              class: (0, import_vue6.normalizeClass)(["fui-fab__button", { "fui-fab__btn-color": !$options.getBgColor && !btn.background }]),
                              style: (0, import_vue6.normalizeStyle)({ width: $props.width + "rpx", height: $props.width + "rpx", background: btn.background || $options.getBgColor })
                            },
                            [
                              btn.name ? ((0, import_vue6.openBlock)(), (0, import_vue6.createBlock)(_component_fui_icon, {
                                key: 0,
                                name: btn.name,
                                color: btn.abbrColor || "#fff",
                                size: btn.abbrSize || 64
                              }, null, 8, ["name", "color", "size"])) : (0, import_vue6.createCommentVNode)("v-if", true),
                              !btn.name && btn.src ? ((0, import_vue6.openBlock)(), (0, import_vue6.createElementBlock)("u-image", {
                                key: 1,
                                src: btn.src,
                                style: (0, import_vue6.normalizeStyle)({ width: (btn.width || 56) + "rpx", height: (btn.height || 56) + "rpx", borderRadius: (btn.radius || 0) + "rpx" }),
                                mode: "widthFix"
                              }, null, 12, ["src"])) : (0, import_vue6.createCommentVNode)("v-if", true),
                              !btn.name && !btn.src && btn.abbr ? ((0, import_vue6.openBlock)(), (0, import_vue6.createElementBlock)(
                                "u-text",
                                {
                                  key: 2,
                                  class: "fui-fab__btn-abbr",
                                  style: (0, import_vue6.normalizeStyle)({ fontSize: (btn.abbrSize || 36) + "rpx", lineHeight: (btn.abbrSize || 36) + "rpx", color: btn.abbrColor || "#fff" })
                                },
                                (0, import_vue6.toDisplayString)(btn.abbr),
                                5
                                /* TEXT, STYLE */
                              )) : (0, import_vue6.createCommentVNode)("v-if", true)
                            ],
                            6
                            /* CLASS, STYLE */
                          )
                        ], 10, ["onClick"]);
                      }),
                      128
                      /* KEYED_FRAGMENT */
                    ))
                  ],
                  2
                  /* CLASS */
                ),
                (0, import_vue6.createElementVNode)(
                  "view",
                  {
                    class: (0, import_vue6.normalizeClass)(["fui-fab__btn-main", { "fui-fab__btn-color": !$options.getBgColor }]),
                    style: (0, import_vue6.normalizeStyle)({ width: $props.width + "rpx", height: $props.width + "rpx", background: $options.getBgColor }),
                    onClick: _cache[1] || (_cache[1] = (0, import_vue6.withModifiers)(($event) => $options.handleClick($event, -1), ["stop"]))
                  },
                  [
                    (0, import_vue6.createElementVNode)(
                      "view",
                      {
                        class: (0, import_vue6.normalizeClass)(["fui-fab__btn-inner", { "fui-fab__btn-ani": $data.isShow }]),
                        ref: "fui_fm_ani"
                      },
                      [
                        (0, import_vue6.renderSlot)(_ctx.$slots, "default", {}, () => [
                          (0, import_vue6.createVNode)(_component_fui_icon, {
                            name: "plus",
                            color: $props.color,
                            size: 80
                          }, null, 8, ["color"])
                        ])
                      ],
                      2
                      /* CLASS */
                    )
                  ],
                  6
                  /* CLASS, STYLE */
                )
              ],
              38
              /* CLASS, STYLE, HYDRATE_EVENTS */
            )
          ],
          32
          /* HYDRATE_EVENTS */
        )
      ],
      2112
      /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
    );
  }
  var __easycom_7 = /* @__PURE__ */ _export_sfc(_sfc_main$1, [["render", _sfc_render$1], ["styles", [_style_0$1]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-fab/fui-fab.vue"]]);
  var _style_05 = { "fui-wrap": { "": { "paddingBottom": "64rpx" } }, "fui-btn__box": { "": { "width": "750rpx", "paddingTop": "96rpx", "paddingRight": "48rpx", "paddingBottom": "96rpx", "paddingLeft": "48rpx" } }, "fui-chat__item": { "": { "flex": 1, "flexDirection": "row", "alignItems": "center", "justifyContent": "space-between" } }, "fui-chat__left": { "": { "flexDirection": "row", "alignItems": "center", "overflow": "hidden", "flex": 1 } }, "fui-img__box": { "": { "width": "96rpx", "height": "96rpx", "position": "relative" } }, "fui-status": { "": { "width": "28rpx", "height": "28rpx", "position": "absolute", "right": 0, "bottom": 0, "borderRadius": "16rpx", "backgroundColor": "#BDBDBD", "borderWidth": 1, "borderStyle": "solid", "borderColor": "#ffffff" } }, "fui-status__online": { "": { "backgroundColor": "#246BFD" } }, "fui-info__box": { "": { "paddingLeft": "20rpx", "paddingRight": "144rpx" } }, "fui-name": { "": { "fontWeight": "700", "fontSize": "36rpx" } }, "fui-content": { "": { "fontSize": "28rpx", "color": "#616161", "paddingTop": "12rpx" } }, "fui-chat__right": { "": { "flexDirection": "column", "alignItems": "flex-end" } }, "fui-time": { "": { "fontSize": "26rpx", "color": "#616161", "paddingTop": "16rpx" } } };
  var _sfc_main5 = {
    data() {
      return {
        list: [],
        listData: [{
          avatar: "/static/images/chat/avatar/img_avatar_1.png",
          name: "Aileen Fullbright",
          msg: "Can't wait to try it out! \u{1F60D}\u{1F60D}",
          num: 1,
          time: "20:00",
          status: 1
        }, {
          avatar: "/static/images/chat/avatar/img_avatar_2.png",
          name: "\u738B\u5C0F\u5C0F",
          msg: "\u8FEB\u4E0D\u53CA\u5F85\u5730\u60F3\u8BD5\u8BD5\uFF01 \u{1F60D}\u{1F60D}",
          num: 1,
          time: "\u6628\u5929",
          status: 1
        }, {
          avatar: "/static/images/chat/avatar/img_avatar_3.png",
          name: "Freida Varnes",
          msg: "Sky blue. Trying it now! \u{1F602}",
          num: 0,
          time: "\u6628\u5929",
          status: 0
        }, {
          avatar: "/static/images/chat/avatar/img_def_1.png",
          name: "Aileen Fullbright",
          msg: "Wow, this is really epic \u{1F44D}",
          num: 0,
          time: "\u524D\u5929 12:00",
          status: 0
        }, {
          avatar: "/static/images/chat/avatar/img_def.png",
          name: "Aileen Fullbright",
          msg: "Can't wait to try it out! \u{1F60D}\u{1F60D}",
          num: 1,
          time: "20:00",
          status: 1
        }, {
          avatar: "/static/images/chat/avatar/img_avatar_2.png",
          name: "\u738B\u5C0F\u5C0F",
          msg: "\u8FEB\u4E0D\u53CA\u5F85\u5730\u60F3\u8BD5\u8BD5\uFF01 \u{1F60D}\u{1F60D}",
          num: 1,
          time: "\u6628\u5929",
          status: 1
        }, {
          avatar: "/static/images/chat/avatar/img_avatar_3.png",
          name: "Freida Varnes",
          msg: "Sky blue. Trying it now! \u{1F602}",
          num: 0,
          time: "\u6628\u5929",
          status: 0
        }, {
          avatar: "/static/images/chat/avatar/img_def_1.png",
          name: "Aileen Fullbright",
          msg: "Wow, this is really epic \u{1F44D}",
          num: 0,
          time: "\u524D\u5929 12:00",
          status: 0
        }, {
          avatar: "/static/images/chat/avatar/img_def_1.png",
          name: "Aileen Fullbright",
          msg: "Wow, this is really epic \u{1F44D}",
          num: 0,
          time: "\u524D\u5929 12:00",
          status: 0
        }, {
          avatar: "/static/images/chat/avatar/img_def_1.png",
          name: "Aileen Fullbright",
          msg: "Wow, this is really epic \u{1F44D}",
          num: 0,
          time: "\u524D\u5929 12:00",
          status: 0
        }],
        fabs: [{
          name: "my",
          text: "\u8054\u7CFB\u4EBA"
        }, {
          name: "community",
          text: "\u4E2A\u4EBA\u4E2D\u5FC3"
        }, {
          name: "message",
          text: "\u804A\u5929"
        }]
      };
    },
    computed: {
      //用于判断用户是否登录
      isLogin() {
        return uni.$cloud.store("isLogin");
      }
    },
    onShow() {
      formatAppLog("log", "at pages/index/index.nvue:130", "\u67E5\u770B\u5224\u65AD\u767B\u5F55-A", this.isLogin);
    },
    methods: {
      onClick(e, index2) {
        formatAppLog("log", "at pages/index/index.nvue:134", e, index2);
        const item = this.list[index2];
        uni.fui.toast(`\u5220\u9664 ${item.name}`);
      },
      startChat() {
        this.list = [...this.listData];
      },
      chat(e) {
        formatAppLog("log", "at pages/index/index.nvue:143", e);
        uni.fui.href("../chat/chat");
      },
      handleClick(e) {
        formatAppLog("log", "at pages/index/index.nvue:147", e);
        const index2 = e.index;
        let url = "../chat/chat";
        if (index2 === 0) {
          url = "../contacts/contacts";
        } else if (index2 === 1) {
          url = "../my/my";
        }
        uni.fui.href(url);
      }
    }
  };
  function _sfc_render5(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_fui_avatar = resolveEasycom((0, import_vue6.resolveDynamicComponent)("fui-avatar"), __easycom_0);
    const _component_fui_badge = resolveEasycom((0, import_vue6.resolveDynamicComponent)("fui-badge"), __easycom_1);
    const _component_fui_list_cell = resolveEasycom((0, import_vue6.resolveDynamicComponent)("fui-list-cell"), __easycom_3);
    const _component_fui_swipe_action = resolveEasycom((0, import_vue6.resolveDynamicComponent)("fui-swipe-action"), __easycom_32);
    const _component_fui_swipeaction_group = resolveEasycom((0, import_vue6.resolveDynamicComponent)("fui-swipeaction-group"), __easycom_4);
    const _component_fui_button = resolveEasycom((0, import_vue6.resolveDynamicComponent)("fui-button"), __easycom_6);
    const _component_fui_empty = resolveEasycom((0, import_vue6.resolveDynamicComponent)("fui-empty"), __easycom_62);
    const _component_fui_fab = resolveEasycom((0, import_vue6.resolveDynamicComponent)("fui-fab"), __easycom_7);
    return (0, import_vue6.openBlock)(), (0, import_vue6.createElementBlock)("scroll-view", {
      scrollY: true,
      showScrollbar: true,
      enableBackToTop: true,
      bubble: "true",
      style: { flexDirection: "column" }
    }, [
      (0, import_vue6.createElementVNode)("view", { class: "fui-wrap" }, [
        (0, import_vue6.createVNode)(_component_fui_swipeaction_group, null, {
          default: (0, import_vue6.withCtx)(() => [
            ((0, import_vue6.openBlock)(true), (0, import_vue6.createElementBlock)(
              import_vue6.Fragment,
              null,
              (0, import_vue6.renderList)($data.list, (item, index2) => {
                return (0, import_vue6.openBlock)(), (0, import_vue6.createBlock)(_component_fui_swipe_action, {
                  onClick: ($event) => $options.onClick($event, index2),
                  key: index2
                }, {
                  default: (0, import_vue6.withCtx)(() => [
                    (0, import_vue6.createVNode)(_component_fui_list_cell, {
                      "bottom-border": false,
                      onClick: ($event) => $options.chat(item)
                    }, {
                      default: (0, import_vue6.withCtx)(() => [
                        (0, import_vue6.createElementVNode)("view", { class: "fui-chat__item" }, [
                          (0, import_vue6.createElementVNode)("view", { class: "fui-chat__left" }, [
                            (0, import_vue6.createElementVNode)("view", { class: "fui-img__box" }, [
                              (0, import_vue6.createVNode)(_component_fui_avatar, {
                                src: item.avatar
                              }, null, 8, ["src"]),
                              (0, import_vue6.createElementVNode)(
                                "view",
                                {
                                  class: (0, import_vue6.normalizeClass)(["fui-status", { "fui-status__online": item.status == 1 }])
                                },
                                null,
                                2
                                /* CLASS */
                              )
                            ]),
                            (0, import_vue6.createElementVNode)("view", { class: "fui-info__box fui-ellipsis" }, [
                              (0, import_vue6.createElementVNode)(
                                "u-text",
                                { class: "fui-name fui-ellipsis" },
                                (0, import_vue6.toDisplayString)(item.name),
                                1
                                /* TEXT */
                              ),
                              (0, import_vue6.createElementVNode)(
                                "u-text",
                                { class: "fui-content fui-ellipsis" },
                                (0, import_vue6.toDisplayString)(item.msg),
                                1
                                /* TEXT */
                              )
                            ])
                          ]),
                          (0, import_vue6.createElementVNode)("view", { class: "fui-chat__right" }, [
                            item.num > 0 ? ((0, import_vue6.openBlock)(), (0, import_vue6.createBlock)(_component_fui_badge, {
                              key: 0,
                              type: "danger",
                              value: item.num
                            }, null, 8, ["value"])) : (0, import_vue6.createCommentVNode)("v-if", true),
                            (0, import_vue6.createElementVNode)(
                              "u-text",
                              { class: "fui-time" },
                              (0, import_vue6.toDisplayString)(item.time),
                              1
                              /* TEXT */
                            )
                          ])
                        ])
                      ]),
                      _: 2
                      /* DYNAMIC */
                    }, 1032, ["onClick"])
                  ]),
                  _: 2
                  /* DYNAMIC */
                }, 1032, ["onClick"]);
              }),
              128
              /* KEYED_FRAGMENT */
            ))
          ]),
          _: 1
          /* STABLE */
        }),
        $data.list.length === 0 ? ((0, import_vue6.openBlock)(), (0, import_vue6.createBlock)(_component_fui_empty, {
          key: 0,
          isFixed: "",
          width: "400",
          height: "400",
          src: "/static/images/common/video_call.png",
          title: "\u6B22\u8FCE\u4F7F\u7528FirstUI Chat !",
          descr: "FirstUI Chat\u5C06\u60A8\u4E0E\u5BB6\u4EBA\u548C\u670B\u53CB\u8054\u7CFB\u8D77\u6765\u3002\u73B0\u5728\u5F00\u59CB\u804A\u5929\uFF01"
        }, {
          default: (0, import_vue6.withCtx)(() => [
            (0, import_vue6.createElementVNode)("view", { class: "fui-btn__box" }, [
              (0, import_vue6.createVNode)(_component_fui_button, {
                radius: "80rpx",
                text: "\u5F00\u59CB\u65B0\u804A\u5929",
                onClick: $options.startChat
              }, null, 8, ["onClick"])
            ])
          ]),
          _: 1
          /* STABLE */
        })) : (0, import_vue6.createCommentVNode)("v-if", true),
        (0, import_vue6.createVNode)(_component_fui_fab, {
          zIndex: "999",
          background: "#246BFD",
          position: "right",
          fabs: $data.fabs,
          onClick: $options.handleClick
        }, null, 8, ["fabs", "onClick"])
      ])
    ]);
  }
  var index = /* @__PURE__ */ _export_sfc(_sfc_main5, [["render", _sfc_render5], ["styles", [_style_05]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/pages/index/index.nvue"]]);

  // <stdin>
  var webview = plus.webview.currentWebview();
  if (webview) {
    const __pageId = parseInt(webview.id);
    const __pagePath = "pages/index/index";
    let __pageQuery = {};
    try {
      __pageQuery = JSON.parse(webview.__query__);
    } catch (e) {
    }
    index.mpType = "page";
    const app = Vue.createPageApp(index, { $store: getApp({ allowDefault: true }).$store, __pageId, __pagePath, __pageQuery });
    app.provide("__globalStyles", Vue.useCssStyles([...__uniConfig.styles, ...index.styles || []]));
    app.mount("#root");
  }
})();
