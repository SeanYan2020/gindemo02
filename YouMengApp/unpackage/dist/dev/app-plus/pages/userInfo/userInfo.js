"use weex:vue";

if (typeof Promise !== 'undefined' && !Promise.prototype.finally) {
  Promise.prototype.finally = function(callback) {
    const promise = this.constructor
    return this.then(
      value => promise.resolve(callback()).then(() => value),
      reason => promise.resolve(callback()).then(() => {
        throw reason
      })
    )
  }
};

if (typeof uni !== 'undefined' && uni && uni.requireGlobal) {
  const global = uni.requireGlobal()
  ArrayBuffer = global.ArrayBuffer
  Int8Array = global.Int8Array
  Uint8Array = global.Uint8Array
  Uint8ClampedArray = global.Uint8ClampedArray
  Int16Array = global.Int16Array
  Uint16Array = global.Uint16Array
  Int32Array = global.Int32Array
  Uint32Array = global.Uint32Array
  Float32Array = global.Float32Array
  Float64Array = global.Float64Array
  BigInt64Array = global.BigInt64Array
  BigUint64Array = global.BigUint64Array
};


(() => {
  var __create = Object.create;
  var __defProp = Object.defineProperty;
  var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
  var __getOwnPropNames = Object.getOwnPropertyNames;
  var __getProtoOf = Object.getPrototypeOf;
  var __hasOwnProp = Object.prototype.hasOwnProperty;
  var __commonJS = (cb, mod) => function __require() {
    return mod || (0, cb[__getOwnPropNames(cb)[0]])((mod = { exports: {} }).exports, mod), mod.exports;
  };
  var __copyProps = (to, from, except, desc) => {
    if (from && typeof from === "object" || typeof from === "function") {
      for (let key of __getOwnPropNames(from))
        if (!__hasOwnProp.call(to, key) && key !== except)
          __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
    }
    return to;
  };
  var __toESM = (mod, isNodeMode, target) => (target = mod != null ? __create(__getProtoOf(mod)) : {}, __copyProps(
    // If the importer is in node compatibility mode or this is not an ESM
    // file that has been converted to a CommonJS file using a Babel-
    // compatible transform (i.e. "__esModule" has not been set), then set
    // "default" to the CommonJS "module.exports" for node compatibility.
    isNodeMode || !mod || !mod.__esModule ? __defProp(target, "default", { value: mod, enumerable: true }) : target,
    mod
  ));

  // vue-ns:vue
  var require_vue = __commonJS({
    "vue-ns:vue"(exports, module) {
      module.exports = Vue;
    }
  });

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/fui-avatar.js
  var import_vue = __toESM(require_vue());

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/_plugin-vue_export-helper.js
  var _export_sfc = (sfc, props) => {
    const target = sfc.__vccOpts || sfc;
    for (const [key, val] of props) {
      target[key] = val;
    }
    return target;
  };

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/fui-avatar.js
  var _style_0 = { "fui-avatar__wrap": { "": { "position": "relative", "flexDirection": "row", "alignItems": "center", "justifyContent": "center" } }, "fui-avatar__img": { "": { "flex": 1 } }, "fui-avatar__text": { "": { "flex": 1, "lines": 1, "overflow": "hidden", "textOverflow": "ellipsis", "textAlign": "center" } }, "fui-avatar__size-small": { "": { "!width": "64rpx", "!height": "64rpx" } }, "fui-avatar__text-small": { "": { "!fontSize": "32rpx" } }, "fui-avatar__size-middle": { "": { "!width": "96rpx", "!height": "96rpx" } }, "fui-avatar__text-middle": { "": { "!fontSize": "44rpx" } }, "fui-avatar__size-large": { "": { "!width": "128rpx", "!height": "128rpx" } }, "fui-avatar__text-large": { "": { "!fontSize": "56rpx" } }, "fui-avatar__circle": { "": { "!borderRadius": 500 } }, "fui-avatar__square": { "": { "!borderRadius": "8rpx" } } };
  var _sfc_main = {
    name: "fui-avatar",
    emits: ["click", "error"],
    props: {
      src: {
        type: String,
        default: ""
      },
      errorSrc: {
        type: String,
        default: ""
      },
      mode: {
        type: String,
        default: "scaleToFill"
      },
      //微信小程序、百度小程序、字节跳动小程序
      //图片懒加载。只针对page与scroll-view下的image有效
      lazyLoad: {
        type: Boolean,
        default: true
      },
      //默认不解析 webP 格式，只支持网络资源 微信小程序2.9.0
      webp: {
        type: Boolean,
        default: false
      },
      background: {
        type: String,
        default: "#D1D1D1"
      },
      //small（64）、middle（96）、large（128）
      size: {
        type: String,
        default: "middle"
      },
      //图片宽度，设置大于0的数值生效，默认使用size
      width: {
        type: [Number, String],
        default: 0
      },
      //默认等宽，设置图大于0的数值且设置了图片宽度生效
      height: {
        type: [Number, String],
        default: 0
      },
      //指定头像的形状，可选值为 circle、square
      shape: {
        type: String,
        default: "circle"
      },
      //图片圆角值，默认使用shape，当设置大于等于0的数值，shape失效
      radius: {
        type: [Number, String],
        default: -1
      },
      //没有src时可以使用文本代替
      text: {
        type: String,
        default: ""
      },
      color: {
        type: String,
        default: "#fff"
      },
      //默认使用size下字体大小
      fontSize: {
        type: [Number, String],
        default: 0
      },
      fontWeight: {
        type: [Number, String],
        default: 600
      },
      marginRight: {
        type: [Number, String],
        default: 0
      },
      marginBottom: {
        type: [Number, String],
        default: 0
      },
      //在列表中的索引值
      index: {
        type: Number,
        default: 0
      },
      //其他参数
      params: {
        type: [Number, String],
        default: 0
      }
    },
    computed: {
      styles() {
        let styles = "";
        if (this.width) {
          styles = `width:${this.width}rpx;height:${this.height || this.width}rpx;`;
        }
        if (this.radius !== -1) {
          styles += `border-radius:${this.radius}rpx;`;
        }
        return styles;
      },
      wrapStyles() {
        return `background:${this.background};margin-right:${this.marginRight}rpx;margin-bottom:${this.marginBottom}rpx;${this.styles};`;
      },
      textStyles() {
        let styles = `color:${this.color};font-weight:${this.fontWeight};`;
        if (this.fontSize) {
          styles += `font-size:${this.fontSize}rpx;`;
        }
        return styles;
      }
    },
    watch: {
      src(val) {
        this.src && (this.showImg = this.src);
      }
    },
    data() {
      return {
        showImg: ""
      };
    },
    created() {
      this.src && (this.showImg = this.src);
    },
    methods: {
      handleError(e) {
        if (this.src) {
          this.errorSrc && (this.showImg = this.errorSrc);
          this.$emit("error", {
            index: this.index,
            params: this.params
          });
        }
      },
      handleClick() {
        this.$emit("click", {
          index: this.index,
          params: this.params
        });
      }
    }
  };
  function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
    return (0, import_vue.openBlock)(), (0, import_vue.createElementBlock)(
      import_vue.Fragment,
      null,
      [
        (0, import_vue.createCommentVNode)("\u672C\u6587\u4EF6\u7531FirstUI\u6388\u6743\u4E88\u4E25\u5929\u8D60\uFF08\u4F1A\u5458ID\uFF1A244   2\uFF0C\u8EAB\u4EFD\u8BC1\u5C3E\u53F7\uFF1A04   3  018\uFF09\u4E13\u7528\uFF0C\u8BF7\u5C0A\u91CD\u77E5\u8BC6\u4EA7\u6743\uFF0C\u52FF\u79C1\u4E0B\u4F20\u64AD\uFF0C\u8FDD\u8005\u8FFD\u7A76\u6CD5\u5F8B\u8D23\u4EFB\u3002"),
        (0, import_vue.createElementVNode)(
          "view",
          {
            class: (0, import_vue.normalizeClass)(["fui-avatar__wrap", [$props.width ? "" : "fui-avatar__size-" + $props.size, $props.radius === -1 ? "fui-avatar__" + $props.shape : ""]]),
            style: (0, import_vue.normalizeStyle)($options.wrapStyles),
            onClick: _cache[1] || (_cache[1] = (...args) => $options.handleClick && $options.handleClick(...args))
          },
          [
            $props.src && $props.src !== true ? ((0, import_vue.openBlock)(), (0, import_vue.createElementBlock)("u-image", {
              key: 0,
              class: (0, import_vue.normalizeClass)(["fui-avatar__img", [$props.radius === -1 ? "fui-avatar__" + $props.shape : "", $props.width ? "" : "fui-avatar__size-" + $props.size]]),
              style: (0, import_vue.normalizeStyle)($options.styles),
              src: $data.showImg,
              mode: $props.mode,
              webp: $props.webp,
              lazyLoad: $props.lazyLoad,
              onError: _cache[0] || (_cache[0] = (...args) => $options.handleError && $options.handleError(...args))
            }, null, 46, ["src", "mode", "webp", "lazyLoad"])) : (0, import_vue.createCommentVNode)("v-if", true),
            !$props.src && $props.src !== true && $props.text ? ((0, import_vue.openBlock)(), (0, import_vue.createElementBlock)(
              "u-text",
              {
                key: 1,
                class: (0, import_vue.normalizeClass)(["fui-avatar__text", [$props.width ? "" : "fui-avatar__text-" + $props.size]]),
                style: (0, import_vue.normalizeStyle)($options.textStyles)
              },
              (0, import_vue.toDisplayString)($props.text),
              7
              /* TEXT, CLASS, STYLE */
            )) : (0, import_vue.createCommentVNode)("v-if", true),
            (0, import_vue.renderSlot)(_ctx.$slots, "default")
          ],
          6
          /* CLASS, STYLE */
        )
      ],
      2112
      /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
    );
  }
  var __easycom_0 = /* @__PURE__ */ _export_sfc(_sfc_main, [["render", _sfc_render], ["styles", [_style_0]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-avatar/fui-avatar.vue"]]);

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/pages/userInfo/userInfo.js
  var import_vue7 = __toESM(require_vue());

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/uni-app.es.js
  var import_vue2 = __toESM(require_vue());
  var isString = (val) => typeof val === "string";
  function resolveEasycom(component, easycom) {
    return isString(component) ? easycom : component;
  }

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/fui-list-cell.js
  var import_vue3 = __toESM(require_vue());
  var _style_02 = { "fui-list__cell": { "": { "position": "relative", "flex": 1, "flexDirection": "row", "alignItems": "center", "justifyContent": "space-between" } }, "fui-list__item": { "": { "flex": 1 } }, "fui-cell__arrow": { "": { "height": "40rpx", "width": "40rpx", "borderTopWidth": 3, "borderRightWidth": 3, "borderBottomWidth": 0, "borderLeftWidth": 0, "borderStyle": "solid", "transform": "rotate(45deg) scale(0.5)", "borderTopRightRadius": "3rpx", "transformOrigin": "center center", "marginRight": "-5.8579rpx" } }, "fui-cell__border-top": { "": { "position": "absolute", "top": 0, "height": 0.5, "zIndex": -1 } }, "fui-cell__border-bottom": { "": { "position": "absolute", "bottom": 0, "height": 0.5, "zIndex": -1 } }, "fui-highlight": { "": { "!backgroundColor:active": "rgba(0,0,0,0.2)" } } };
  var _sfc_main2 = {
    name: "fui-list-cell",
    emits: ["click"],
    props: {
      //padding值，上、右、下、左,nvue下padding-right(右)无效
      padding: {
        type: Array,
        default() {
          return [];
        }
      },
      //margin-top 单位rpx
      marginTop: {
        type: [Number, String],
        default: 0
      },
      //margin-bottom 单位rpx
      marginBottom: {
        type: [Number, String],
        default: 0
      },
      //背景颜色
      background: {
        type: String,
        default: "#fff"
      },
      //是否有点击效果
      highlight: {
        type: Boolean,
        default: true
      },
      //是否需要右侧箭头
      arrow: {
        type: Boolean,
        default: false
      },
      arrowColor: {
        type: String,
        default: ""
      },
      //是否显示上边框
      topBorder: {
        type: Boolean,
        default: false
      },
      //是否显示下边框
      bottomBorder: {
        type: Boolean,
        default: true
      },
      //边框颜色，非nvue下传值则全局默认样式失效
      borderColor: {
        type: String,
        default: ""
      },
      //上边框left值，单位rpx
      topLeft: {
        type: [Number, String],
        default: 0
      },
      //上边框right值，单位rpx
      topRight: {
        type: [Number, String],
        default: 0
      },
      //下边框left值，单位rpx
      bottomLeft: {
        type: [Number, String],
        default: -1
      },
      //下边框right值，单位rpx
      bottomRight: {
        type: [Number, String],
        default: 0
      },
      //border-radius圆角值
      radius: {
        type: String,
        default: "0"
      },
      index: {
        type: Number,
        default: 0
      }
    },
    computed: {
      getPadding() {
        let padding = this.padding;
        if (Array.isArray(padding) && padding.length === 0) {
          const app = uni && uni.$fui && uni.$fui.fuiListCell;
          padding = app && app.padding;
          if (!padding || Array.isArray(padding) && padding.length === 0) {
            padding = ["32rpx", "32rpx"];
          }
        }
        return padding;
      },
      getArrowColor() {
        const app = uni && uni.$fui && uni.$fui.fuiListCell;
        return this.arrowColor || app && app.arrowColor || "#B2B2B2";
      },
      getBorderColor() {
        let color = this.borderColor;
        if (!color || color === true) {
          const app = uni && uni.$fui && uni.$fui.fuiListCell;
          color = app && app.borderColor || "#EEEEEE";
        }
        return color;
      },
      getBottomLeft() {
        const app = uni && uni.$fui && uni.$fui.fuiListCell;
        let left = this.bottomLeft;
        const c_left = app && app.bottomLeft;
        if (left === -1) {
          left = c_left === void 0 || c_left === null ? 32 : c_left;
        }
        return left;
      }
    },
    methods: {
      handleClick() {
        this.$emit("click", {
          index: this.index
        });
      }
    }
  };
  function _sfc_render2(_ctx, _cache, $props, $setup, $data, $options) {
    return (0, import_vue3.openBlock)(), (0, import_vue3.createElementBlock)(
      import_vue3.Fragment,
      null,
      [
        (0, import_vue3.createCommentVNode)("\u672C\u6587\u4EF6\u7531FirstUI\u6388\u6743\u4E88\u4E25\u5929\u8D60\uFF08\u4F1A\u5458ID\uFF1A2 4 4 2\uFF0C\u8EAB\u4EFD\u8BC1\u5C3E\u53F7\uFF1A043 0   1 8\uFF09\u4E13\u7528\uFF0C\u8BF7\u5C0A\u91CD\u77E5\u8BC6\u4EA7\u6743\uFF0C\u52FF\u79C1\u4E0B\u4F20\u64AD\uFF0C\u8FDD\u8005\u8FFD\u7A76\u6CD5\u5F8B\u8D23\u4EFB\u3002"),
        (0, import_vue3.createElementVNode)(
          "view",
          {
            class: (0, import_vue3.normalizeClass)(["fui-list__cell", { "fui-highlight": $props.highlight, "fui-list__cell-background": !$props.background }]),
            style: (0, import_vue3.normalizeStyle)({ paddingTop: $options.getPadding[0] || 0, paddingRight: $options.getPadding[1] || 0, paddingBottom: $options.getPadding[2] || $options.getPadding[0] || 0, paddingLeft: $options.getPadding[3] || $options.getPadding[1] || 0, background: $props.background, marginTop: $props.marginTop + "rpx", marginBottom: $props.marginBottom + "rpx", borderRadius: $props.radius }),
            onClick: _cache[0] || (_cache[0] = (...args) => $options.handleClick && $options.handleClick(...args))
          },
          [
            $props.topBorder ? ((0, import_vue3.openBlock)(), (0, import_vue3.createElementBlock)(
              "view",
              {
                key: 0,
                style: (0, import_vue3.normalizeStyle)({ background: $options.getBorderColor, left: $props.topLeft + "rpx", right: $props.topRight + "rpx" }),
                class: (0, import_vue3.normalizeClass)(["fui-cell__border-top", { "fui-cell__border-color": !$options.getBorderColor }])
              },
              null,
              6
              /* CLASS, STYLE */
            )) : (0, import_vue3.createCommentVNode)("v-if", true),
            (0, import_vue3.renderSlot)(_ctx.$slots, "default"),
            $props.bottomBorder ? ((0, import_vue3.openBlock)(), (0, import_vue3.createElementBlock)(
              "view",
              {
                key: 1,
                style: (0, import_vue3.normalizeStyle)({ background: $options.getBorderColor, left: $options.getBottomLeft + "rpx", right: $props.bottomRight + "rpx" }),
                class: (0, import_vue3.normalizeClass)(["fui-cell__border-bottom", { "fui-cell__border-color": !$options.getBorderColor }])
              },
              null,
              6
              /* CLASS, STYLE */
            )) : (0, import_vue3.createCommentVNode)("v-if", true),
            $props.arrow ? ((0, import_vue3.openBlock)(), (0, import_vue3.createElementBlock)(
              "view",
              {
                key: 2,
                class: "fui-cell__arrow",
                style: (0, import_vue3.normalizeStyle)({ "border-color": $options.getArrowColor })
              },
              null,
              4
              /* STYLE */
            )) : (0, import_vue3.createCommentVNode)("v-if", true)
          ],
          6
          /* CLASS, STYLE */
        )
      ],
      2112
      /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
    );
  }
  var __easycom_3 = /* @__PURE__ */ _export_sfc(_sfc_main2, [["render", _sfc_render2], ["styles", [_style_02]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-list-cell/fui-list-cell.vue"]]);

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/fui-list.js
  var import_vue4 = __toESM(require_vue());
  var _style_03 = { "fui-list__wrap": { "": { "flex": 1 } }, "fui-list__title": { "": { "lineHeight": 1 } }, "fui-list__footer": { "": { "lineHeight": 1 } }, "fui-list__container": { "": { "position": "relative", "flex": 1 } }, "fui-list__border-top": { "": { "position": "absolute", "top": 0, "height": 0.5, "zIndex": -1 } }, "fui-list__border-bottom": { "": { "position": "absolute", "bottom": 0, "height": 0.5, "zIndex": -1 } } };
  var _sfc_main3 = {
    name: "fui-list",
    props: {
      //fui-list margin-top值,单位rpx
      marginTop: {
        type: [Number, String],
        default: 0
      },
      //标题
      title: {
        type: String,
        default: ""
      },
      //标题颜色
      color: {
        type: String,
        default: "#7F7F7F"
      },
      //标题字体大小 单位rpx
      size: {
        type: [Number, String],
        default: 28
      },
      //标题padding值['32rpx','32rpx','20rpx','32rpx'] 上、右、下、左
      padding: {
        type: Array,
        default() {
          return ["32rpx", "32rpx", "20rpx", "32rpx"];
        }
      },
      //标题背景色
      background: {
        type: String,
        default: "transparent"
      },
      //底部说明文字
      footer: {
        type: String,
        default: ""
      },
      //底部说明文字颜色
      footerColor: {
        type: String,
        default: "#7F7F7F"
      },
      //底部说明文字大小 单位rpx
      footerSize: {
        type: [Number, String],
        default: 28
      },
      //底部说明文本padding值['20rpx','32rpx','0','32rpx'] 上、右、下、左
      footerPadding: {
        type: Array,
        default() {
          return ["20rpx", "32rpx", "0"];
        }
      },
      //是否显示上边框
      topBorder: {
        type: Boolean,
        default: true
      },
      //是否显示下边框
      bottomBorder: {
        type: Boolean,
        default: false
      },
      //边框颜色，非nvue下传值则全局默认样式失效
      borderColor: {
        type: String,
        default: "#EEEEEE"
      },
      //上边框left值，单位rpx
      topLeft: {
        type: [Number, String],
        default: 0
      },
      //上边框right值，单位rpx
      topRight: {
        type: [Number, String],
        default: 0
      },
      //下边框left值，单位rpx
      bottomLeft: {
        type: [Number, String],
        default: 0
      },
      //下边框right值，单位rpx
      bottomRight: {
        type: [Number, String],
        default: 0
      }
    }
  };
  function _sfc_render3(_ctx, _cache, $props, $setup, $data, $options) {
    return (0, import_vue4.openBlock)(), (0, import_vue4.createElementBlock)(
      import_vue4.Fragment,
      null,
      [
        (0, import_vue4.createCommentVNode)("\u672C\u6587\u4EF6\u7531FirstUI\u6388\u6743\u4E88\u4E25\u5929\u8D60\uFF08\u4F1A\u5458ID\uFF1A24 4  2\uFF0C\u8EAB\u4EFD\u8BC1\u5C3E\u53F7\uFF1A0 43 0   18\uFF09\u4E13\u7528\uFF0C\u8BF7\u5C0A\u91CD\u77E5\u8BC6\u4EA7\u6743\uFF0C\u52FF\u79C1\u4E0B\u4F20\u64AD\uFF0C\u8FDD\u8005\u8FFD\u7A76\u6CD5\u5F8B\u8D23\u4EFB\u3002"),
        (0, import_vue4.createElementVNode)(
          "view",
          {
            class: "fui-list__wrap",
            style: (0, import_vue4.normalizeStyle)({ marginTop: $props.marginTop + "rpx" })
          },
          [
            $props.title ? ((0, import_vue4.openBlock)(), (0, import_vue4.createElementBlock)(
              "view",
              {
                key: 0,
                class: "fui-list__title",
                style: (0, import_vue4.normalizeStyle)({ paddingTop: $props.padding[0] || 0, paddingRight: $props.padding[1] || 0, paddingBottom: $props.padding[2] || $props.padding[0] || 0, paddingLeft: $props.padding[3] || $props.padding[1] || 0, background: $props.background })
              },
              [
                (0, import_vue4.createElementVNode)(
                  "u-text",
                  {
                    style: (0, import_vue4.normalizeStyle)({ color: $props.color, fontSize: $props.size + "rpx" })
                  },
                  (0, import_vue4.toDisplayString)($props.title),
                  5
                  /* TEXT, STYLE */
                )
              ],
              4
              /* STYLE */
            )) : (0, import_vue4.createCommentVNode)("v-if", true),
            (0, import_vue4.createElementVNode)("view", { class: "fui-list__container" }, [
              $props.topBorder ? ((0, import_vue4.openBlock)(), (0, import_vue4.createElementBlock)(
                "view",
                {
                  key: 0,
                  style: (0, import_vue4.normalizeStyle)({ background: $props.borderColor, left: $props.topLeft + "rpx", right: $props.topRight + "rpx" }),
                  class: (0, import_vue4.normalizeClass)(["fui-list__border-top", { "fui-list__border-color": !$props.borderColor }])
                },
                null,
                6
                /* CLASS, STYLE */
              )) : (0, import_vue4.createCommentVNode)("v-if", true),
              (0, import_vue4.renderSlot)(_ctx.$slots, "default"),
              $props.bottomBorder ? ((0, import_vue4.openBlock)(), (0, import_vue4.createElementBlock)(
                "view",
                {
                  key: 1,
                  style: (0, import_vue4.normalizeStyle)({ background: $props.borderColor, left: $props.bottomLeft + "rpx", right: $props.bottomRight + "rpx" }),
                  class: (0, import_vue4.normalizeClass)(["fui-list__border-bottom", { "fui-list__border-color": !$props.borderColor }])
                },
                null,
                6
                /* CLASS, STYLE */
              )) : (0, import_vue4.createCommentVNode)("v-if", true)
            ]),
            $props.footer ? ((0, import_vue4.openBlock)(), (0, import_vue4.createElementBlock)(
              "view",
              {
                key: 1,
                class: "fui-list__footer",
                style: (0, import_vue4.normalizeStyle)({ paddingTop: $props.footerPadding[0] || 0, paddingRight: $props.footerPadding[1] || 0, paddingBottom: $props.footerPadding[2] || $props.footerPadding[0] || 0, paddingLeft: $props.footerPadding[3] || $props.footerPadding[1] || 0 })
              },
              [
                (0, import_vue4.createElementVNode)(
                  "u-text",
                  {
                    style: (0, import_vue4.normalizeStyle)({ color: $props.footerColor, fontSize: $props.footerSize + "rpx" })
                  },
                  (0, import_vue4.toDisplayString)($props.footer),
                  5
                  /* TEXT, STYLE */
                )
              ],
              4
              /* STYLE */
            )) : (0, import_vue4.createCommentVNode)("v-if", true)
          ],
          4
          /* STYLE */
        )
      ],
      2112
      /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
    );
  }
  var __easycom_4 = /* @__PURE__ */ _export_sfc(_sfc_main3, [["render", _sfc_render3], ["styles", [_style_03]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-list/fui-list.vue"]]);

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/fui-icon.js
  var import_vue5 = __toESM(require_vue());
  var icons = {
    "addressbook": "\uE80C",
    "addfriends-fill": "\uE80A",
    "addfriends": "\uE80B",
    "backspace-fill": "\uE808",
    "backspace": "\uE809",
    "bankcard-fill": "\uE806",
    "bankcard": "\uE807",
    "camera-fill": "\uE804",
    "camera": "\uE805",
    "captcha-fill": "\uE802",
    "captcha": "\uE803",
    "cart-fill": "\uE800",
    "cart": "\uE801",
    "classify": "\uE7FE",
    "classify-fill": "\uE7FF",
    "comment-fill": "\uE7FC",
    "comment": "\uE7FD",
    "community-fill": "\uE7FA",
    "community": "\uE7FB",
    "coupon-fill": "\uE7F8",
    "coupon": "\uE7F9",
    "delete": "\uE7F6",
    "delete-fill": "\uE7F7",
    "edit": "\uE7F4",
    "edit-fill": "\uE7F5",
    "fabulous-fill": "\uE7F2",
    "fabulous": "\uE7F3",
    "find": "\uE7F0",
    "find-fill": "\uE7F1",
    "help-fill": "\uE7EE",
    "help": "\uE7EF",
    "home-fill": "\uE7EC",
    "home": "\uE7ED",
    "idcard-fill": "\uE7EA",
    "idcard": "\uE7EB",
    "info": "\uE7E8",
    "info-fill": "\uE7E9",
    "invite-fill": "\uE7E6",
    "invite": "\uE7E7",
    "kefu-fill": "\uE7E4",
    "kefu": "\uE7E5",
    "like-fill": "\uE7E2",
    "like": "\uE7E3",
    "location": "\uE7E0",
    "location-fill": "\uE7E1",
    "lock": "\uE7DE",
    "lock-fill": "\uE7DF",
    "mail-fill": "\uE7DC",
    "mail": "\uE7DD",
    "message": "\uE7DA",
    "message-fill": "\uE7DB",
    "mobile-fill": "\uE7D8",
    "mobile": "\uE7D9",
    "more": "\uE7D6",
    "more-fill": "\uE7D7",
    "my-fill": "\uE7D4",
    "my": "\uE7D5",
    "principal": "\uE80D",
    "notice-fill": "\uE7D2",
    "notice": "\uE7D3",
    "order": "\uE7D0",
    "order-fill": "\uE7D1",
    "picture": "\uE7CE",
    "picture-fill": "\uE7CF",
    "setup-fill": "\uE7CC",
    "setup": "\uE7CD",
    "share": "\uE7CA",
    "share-fill": "\uE7CB",
    "shop": "\uE7C8",
    "shop-fill": "\uE7C9",
    "star-fill": "\uE7C5",
    "star": "\uE7C6",
    "starhalf": "\uE7C7",
    "stepon-fill": "\uE7C3",
    "stepon": "\uE7C4",
    "wait-fill": "\uE7C1",
    "wait": "\uE7C2",
    "warning": "\uE7BF",
    "warning-fill": "\uE7C0",
    "plus": "\uE7BC",
    "plussign-fill": "\uE7BD",
    "plussign": "\uE7BE",
    "minus": "\uE7B9",
    "minussign": "\uE7BA",
    "minussign-fill": "\uE7BB",
    "close": "\uE7B8",
    "clear": "\uE7B6",
    "clear-fill": "\uE7B7",
    "checkbox-fill": "\uE7B5",
    "checkround": "\uE7B4",
    "checkbox": "\uE7B3",
    "check": "\uE7B2",
    "pulldown-fill": "\uE7AE",
    "pullup": "\uE7AF",
    "pullup-fill": "\uE7B0",
    "pulldown": "\uE7B1",
    "roundright-fill": "\uE7AC",
    "roundright": "\uE7AD",
    "arrowright": "\uE7A9",
    "arrowleft": "\uE7AA",
    "arrowdown": "\uE7AB",
    "left": "\uE7A6",
    "up": "\uE7A7",
    "right": "\uE7A8",
    "back": "\uE7A3",
    "top": "\uE7A4",
    "dropdown": "\uE7A5",
    "turningleft": "\uE79F",
    "turningup": "\uE7A0",
    "turningright": "\uE7A1",
    "turningdown": "\uE7A2",
    "refresh": "\uE79C",
    "loading": "\uE79D",
    "search": "\uE79E",
    "rotate": "\uE79B",
    "screen": "\uE79A",
    "signin": "\uE799",
    "calendar": "\uE798",
    "scan": "\uE797",
    "qrcode": "\uE796",
    "wallet": "\uE795",
    "telephone": "\uE794",
    "visible": "\uE793",
    "invisible": "\uE792",
    "menu": "\uE78E",
    "operate": "\uE78F",
    "slide": "\uE790",
    "list": "\uE791",
    "nonetwork": "\uE78D",
    "partake": "\uE78C",
    "qa": "\uE78B",
    "barchart": "\uE788",
    "piechart": "\uE789",
    "linechart": "\uE78A",
    "at": "\uE787",
    "face": "\uE77F",
    "redpacket": "\uE780",
    "suspend": "\uE781",
    "link": "\uE782",
    "keyboard": "\uE783",
    "play": "\uE784",
    "video": "\uE785",
    "voice": "\uE786",
    "sina": "\uE77A",
    "browser": "\uE77B",
    "moments": "\uE77C",
    "qq": "\uE77D",
    "wechat": "\uE77E",
    "balance": "\uE779",
    "bankcardpay": "\uE778",
    "wxpay": "\uE777",
    "alipay": "\uE776",
    "payment": "\uE818",
    "receive": "\uE817",
    "sendout": "\uE816",
    "evaluate": "\uE815",
    "aftersale": "\uE814",
    "warehouse": "\uE813",
    "transport": "\uE812",
    "delivery": "\uE811",
    "switch": "\uE810",
    "goods": "\uE80F",
    "goods-fill": "\uE80E"
  };
  var fuiicons = "/assets/fui-icon.9165208c.ttf";
  var _style_04 = { "fui-icon": { "": { "fontFamily": "fuiFont", "textDecoration": "none", "textAlign": "center" } } };
  var domModule = weex.requireModule("dom");
  domModule.addRule("fontFace", {
    "fontFamily": "fuiFont",
    "src": "url('" + fuiicons + "')"
  });
  var _sfc_main4 = {
    name: "fui-icon",
    emits: ["click"],
    props: {
      name: {
        type: String,
        default: ""
      },
      size: {
        type: [Number, String],
        default: 0
      },
      //rpx | px
      unit: {
        type: String,
        default: ""
      },
      color: {
        type: String,
        default: ""
      },
      //字重
      fontWeight: {
        type: [Number, String],
        default: "normal"
      },
      //是否禁用点击
      disabled: {
        type: Boolean,
        default: false
      },
      params: {
        type: [Number, String],
        default: 0
      },
      customPrefix: {
        type: String,
        default: ""
      },
      //是否显示为主色调，color为空时有效。【内部使用】
      primary: {
        type: Boolean,
        default: false
      }
    },
    computed: {
      getSize() {
        const size = uni.$fui && uni.$fui.fuiIcon && uni.$fui.fuiIcon.size || 64;
        const unit = uni.$fui && uni.$fui.fuiIcon && uni.$fui.fuiIcon.unit || "rpx";
        return (this.size || size) + (this.unit || unit);
      },
      primaryColor() {
        const app = uni && uni.$fui && uni.$fui.color;
        return app && app.primary || "#465CFF";
      },
      getColor() {
        const app = uni && uni.$fui && uni.$fui.fuiIcon;
        let color = this.color;
        if (!color || color && color === true) {
          color = app && app.color;
        }
        if (!color || color === true) {
          color = "#333333";
        }
        return color;
      }
    },
    data() {
      return {
        icons
      };
    },
    methods: {
      handleClick() {
        if (this.disabled)
          return;
        this.$emit("click", {
          params: this.params
        });
      }
    }
  };
  function _sfc_render4(_ctx, _cache, $props, $setup, $data, $options) {
    return (0, import_vue5.openBlock)(), (0, import_vue5.createElementBlock)(
      import_vue5.Fragment,
      null,
      [
        (0, import_vue5.createCommentVNode)("\u672C\u6587\u4EF6\u7531FirstUI\u6388\u6743\u4E88\u4E25\u5929\u8D60\uFF08\u4F1A\u5458ID\uFF1A24   42\uFF0C\u8EAB\u4EFD\u8BC1\u5C3E\u53F7\uFF1A 04  3 01 8\uFF09\u4E13\u7528\uFF0C\u8BF7\u5C0A\u91CD\u77E5\u8BC6\u4EA7\u6743\uFF0C\u52FF\u79C1\u4E0B\u4F20\u64AD\uFF0C\u8FDD\u8005\u8FFD\u7A76\u6CD5\u5F8B\u8D23\u4EFB\u3002"),
        (0, import_vue5.createElementVNode)(
          "u-text",
          {
            style: (0, import_vue5.normalizeStyle)({ color: $props.primary && (!$props.color || $props.color === true) ? $options.primaryColor : $options.getColor, fontSize: $options.getSize, lineHeight: $options.getSize, fontWeight: $props.fontWeight }),
            class: (0, import_vue5.normalizeClass)(["fui-icon", [$props.customPrefix && $props.customPrefix !== true ? $props.customPrefix : ""]]),
            onClick: _cache[0] || (_cache[0] = (...args) => $options.handleClick && $options.handleClick(...args))
          },
          (0, import_vue5.toDisplayString)($props.customPrefix && $props.customPrefix !== true ? $props.name : $data.icons[$props.name]),
          7
          /* TEXT, CLASS, STYLE */
        )
      ],
      2112
      /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
    );
  }
  var __easycom_5 = /* @__PURE__ */ _export_sfc(_sfc_main4, [["render", _sfc_render4], ["styles", [_style_04]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-icon/fui-icon.vue"]]);

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/fui-button.js
  var import_vue6 = __toESM(require_vue());
  var _style_05 = { "fui-button__wrap": { "": { "position": "relative" } }, "fui-button": { "": { "borderWidth": 0, "borderStyle": "solid", "position": "relative", "paddingLeft": 0, "paddingRight": 0, "borderWidth::after": 0, "borderStyle::after": "solid", "borderColor::after": "#000000" } }, "fui-button__flex-1": { "": { "flex": 1 } }, "fui-button__text": { "": { "textAlign": "center", "flexDirection": "row", "alignItems": "center", "!justifyContent": "center", "!paddingLeft": 0 } }, "fui-button__opacity": { "": { "opacity": 0.5 } }, "fui-text__bold": { "": { "fontWeight": "bold" } }, "fui-button__link": { "": { "!borderColor": "rgba(0,0,0,0)", "!backgroundColor": "rgba(0,0,0,0)" } } };
  var _sfc_main5 = {
    name: "fui-button",
    emits: ["click", "getuserinfo", "contact", "getphonenumber", "error", "opensetting", "chooseavatar", "launchapp"],
    props: {
      //样式类型：primary，success， warning，danger，link，purple，gray
      type: {
        type: String,
        default: "primary"
      },
      //按钮背景色，当传入值时type失效
      background: {
        type: String,
        default: ""
      },
      //按钮显示文本
      text: {
        type: String,
        default: ""
      },
      //按钮字体颜色
      color: {
        type: String,
        default: ""
      },
      //按钮禁用背景色
      disabledBackground: {
        type: String,
        default: ""
      },
      //按钮禁用字体颜色
      disabledColor: {
        type: String,
        default: ""
      },
      borderWidth: {
        type: String,
        default: "0.5px"
      },
      borderColor: {
        type: String,
        default: ""
      },
      //V1.9.8+ 按钮大小，优先级高于width和height，medium、small、mini
      btnSize: {
        type: String,
        default: ""
      },
      //宽度
      width: {
        type: String,
        default: "100%"
      },
      //高度
      height: {
        type: String,
        default: ""
      },
      //字体大小，单位rpx
      size: {
        type: [Number, String],
        default: 0
      },
      bold: {
        type: Boolean,
        default: false
      },
      //['20rpx','30rpx','20rpx','30rpx']->[上，右，下，左]
      margin: {
        type: Array,
        default() {
          return ["0", "0"];
        }
      },
      //圆角
      radius: {
        type: String,
        default: ""
      },
      plain: {
        type: Boolean,
        default: false
      },
      disabled: {
        type: Boolean,
        default: false
      },
      loading: {
        type: Boolean,
        default: false
      },
      formType: {
        type: String,
        default: ""
      },
      openType: {
        type: String,
        default: ""
      },
      //支付宝小程序
      //当 open-type 为 getAuthorize 时，可以设置 scope 为：phoneNumber、userInfo
      scope: {
        type: String,
        default: ""
      },
      appParameter: {
        type: String,
        default: ""
      },
      index: {
        type: [Number, String],
        default: 0
      }
    },
    computed: {
      getTypeColor() {
        const app = uni && uni.$fui && uni.$fui.color;
        let colors = {
          primary: app && app.primary || "#465CFF",
          success: app && app.success || "#09BE4F",
          warning: app && app.warning || "#FFB703",
          danger: app && app.danger || "#FF2B2B",
          link: "transparent",
          purple: app && app.purple || "#6831FF",
          gray: "#F8F8F8"
        };
        return colors[this.type] || "transparent";
      },
      getBackground() {
        let color = this.getTypeColor;
        if (this.disabled || this.plain) {
          color = "transparent";
        }
        if (!this.disabled && !this.plain && this.background) {
          color = this.background;
        }
        return color;
      },
      getColor() {
        let color = "#fff";
        if (this.color) {
          color = this.disabled && this.disabledBackground ? this.disabledColor : this.color;
        } else {
          if (this.disabled && this.disabledBackground) {
            color = this.disabledColor || "#FFFFFF";
          } else {
            const app = uni && uni.$fui && uni.$fui.color;
            const primary = app && app.primary || "#465CFF";
            color = this.type === "gray" ? primary : "#FFFFFF";
          }
        }
        return color;
      },
      getSize() {
        let size = this.size || uni && uni.$fui && uni.$fui.fuiButton && uni.$fui.fuiButton.size || 32;
        if (this.btnSize === "small") {
          size = size > 28 ? 28 : size;
        } else if (this.btnSize === "mini") {
          size = size > 28 ? 24 : size;
        }
        return `${size}rpx`;
      },
      getWidth() {
        let width = this.width;
        if (this.btnSize && this.btnSize !== true) {
          width = {
            "medium": "400rpx",
            "small": "200rpx",
            "mini": "120rpx"
          }[this.btnSize] || width;
        }
        return width;
      },
      getHeight() {
        let height = this.height || uni && uni.$fui && uni.$fui.fuiButton && uni.$fui.fuiButton.height || "96rpx";
        if (this.btnSize && this.btnSize !== true) {
          height = {
            "medium": "84rpx",
            "small": "72rpx",
            "mini": "64rpx"
          }[this.btnSize] || height;
        }
        return height;
      },
      getRadius() {
        const radius = uni && uni.$fui && uni.$fui.fuiButton && uni.$fui.fuiButton.radius || "16rpx";
        return this.radius || radius;
      }
    },
    data() {
      let isNvue = false;
      isNvue = true;
      return {
        isNvue,
        time: 0,
        trigger: false,
        pc: false
      };
    },
    created() {
    },
    methods: {
      handleStart(e) {
      },
      handleClick() {
        if (this.disabled || !this.trigger)
          return;
        this.time = 0;
      },
      handleTap() {
        if (this.disabled)
          return;
        this.$emit("click", {
          index: Number(this.index)
        });
      },
      handleEnd(e) {
      },
      bindgetuserinfo({
        detail = {}
      } = {}) {
        this.$emit("getuserinfo", detail);
      },
      bindcontact({
        detail = {}
      } = {}) {
        this.$emit("contact", detail);
      },
      bindgetphonenumber({
        detail = {}
      } = {}) {
        this.$emit("getphonenumber", detail);
      },
      binderror({
        detail = {}
      } = {}) {
        this.$emit("error", detail);
      },
      bindopensetting({
        detail = {}
      } = {}) {
        this.$emit("opensetting", detail);
      },
      bindchooseavatar({
        detail = {}
      } = {}) {
        this.$emit("chooseavatar", detail);
      },
      bindlaunchapp({
        detail = {}
      } = {}) {
        this.$emit("launchapp", detail);
      }
    }
  };
  function _sfc_render5(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_button = (0, import_vue6.resolveComponent)("button");
    return (0, import_vue6.openBlock)(), (0, import_vue6.createElementBlock)(
      import_vue6.Fragment,
      null,
      [
        (0, import_vue6.createCommentVNode)("\u672C\u6587\u4EF6\u7531FirstUI\u6388\u6743\u4E88\u4E25\u5929\u8D60\uFF08\u4F1A\u5458ID\uFF1A24 4  2\uFF0C\u8EAB\u4EFD\u8BC1\u5C3E\u53F7\uFF1A0 4   3 018\uFF09\u4E13\u7528\uFF0C\u8BF7\u5C0A\u91CD\u77E5\u8BC6\u4EA7\u6743\uFF0C\u52FF\u79C1\u4E0B\u4F20\u64AD\uFF0C\u8FDD\u8005\u8FFD\u7A76\u6CD5\u5F8B\u8D23\u4EFB\u3002"),
        (0, import_vue6.createElementVNode)(
          "view",
          {
            class: (0, import_vue6.normalizeClass)(["fui-button__wrap", [!$options.getWidth || $options.getWidth === "100%" || $options.getWidth === true ? "fui-button__flex-1" : "", $props.disabled && !$props.disabledBackground ? "fui-button__opacity" : ""]]),
            style: (0, import_vue6.normalizeStyle)({ width: $options.getWidth, height: $options.getHeight, marginTop: $props.margin[0] || 0, marginRight: $props.margin[1] || 0, marginBottom: $props.margin[2] || $props.margin[0] || 0, marginLeft: $props.margin[3] || $props.margin[1] || 0, borderRadius: $options.getRadius, background: $options.getBackground }),
            onTouchstart: _cache[0] || (_cache[0] = (...args) => $options.handleStart && $options.handleStart(...args)),
            onTouchend: _cache[1] || (_cache[1] = (...args) => $options.handleClick && $options.handleClick(...args)),
            onTouchcancel: _cache[2] || (_cache[2] = (...args) => $options.handleEnd && $options.handleEnd(...args))
          },
          [
            (0, import_vue6.createVNode)(_component_button, {
              class: (0, import_vue6.normalizeClass)(["fui-button", [
                $props.bold ? "fui-text__bold" : "",
                $data.time && ($props.plain || $props.type === "link") ? "fui-button__opacity" : "",
                !$props.background && !$props.disabledBackground && !$props.plain ? "fui-button__" + $props.type : "",
                !$options.getWidth || $options.getWidth === "100%" || $options.getWidth === true ? "fui-button__flex-1" : "",
                $data.time && !$props.plain && $props.type !== "link" ? "fui-button__active" : "",
                $data.pc && !$props.disabled ? $props.plain || $props.type === "link" ? "fui-button__opacity-pc" : "fui-button__active-pc" : ""
              ]]),
              style: (0, import_vue6.normalizeStyle)({
                width: $options.getWidth,
                height: $options.getHeight,
                lineHeight: $options.getHeight,
                background: $props.disabled ? $props.disabledBackground || $options.getTypeColor : $props.plain ? "transparent" : $options.getBackground,
                borderWidth: !$props.borderColor || !$data.isNvue ? "0" : $props.borderWidth,
                borderColor: $props.borderColor ? $props.borderColor : $props.disabled && $props.disabledBackground ? $props.disabledBackground : $props.background || "transparent",
                borderRadius: $options.getRadius,
                fontSize: $options.getSize,
                color: $options.getColor
              }),
              loading: $props.loading,
              "form-type": $props.formType,
              "open-type": $props.openType,
              "app-parameter": $props.appParameter,
              onGetuserinfo: $options.bindgetuserinfo,
              onGetphonenumber: $options.bindgetphonenumber,
              onContact: $options.bindcontact,
              onError: $options.binderror,
              onOpensetting: $options.bindopensetting,
              onChooseavatar: $options.bindchooseavatar,
              onLaunchapp: $options.bindlaunchapp,
              disabled: $props.disabled,
              scope: $props.scope,
              onClick: (0, import_vue6.withModifiers)($options.handleTap, ["stop"])
            }, {
              default: (0, import_vue6.withCtx)(() => [
                $props.text ? ((0, import_vue6.openBlock)(), (0, import_vue6.createElementBlock)(
                  "u-text",
                  {
                    key: 0,
                    class: (0, import_vue6.normalizeClass)(["fui-button__text", { "fui-btn__gray-color": !$props.background && !$props.disabledBackground && !$props.plain && $props.type === "gray" && $props.color === "#fff", "fui-text__bold": $props.bold }]),
                    style: (0, import_vue6.normalizeStyle)({ fontSize: $options.getSize, lineHeight: $options.getSize, color: $options.getColor })
                  },
                  (0, import_vue6.toDisplayString)($props.text),
                  7
                  /* TEXT, CLASS, STYLE */
                )) : (0, import_vue6.createCommentVNode)("v-if", true),
                (0, import_vue6.renderSlot)(_ctx.$slots, "default")
              ]),
              _: 3
              /* FORWARDED */
            }, 8, ["class", "style", "loading", "form-type", "open-type", "app-parameter", "onGetuserinfo", "onGetphonenumber", "onContact", "onError", "onOpensetting", "onChooseavatar", "onLaunchapp", "disabled", "scope", "onClick"])
          ],
          38
          /* CLASS, STYLE, HYDRATE_EVENTS */
        )
      ],
      2112
      /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
    );
  }
  var __easycom_6 = /* @__PURE__ */ _export_sfc(_sfc_main5, [["render", _sfc_render5], ["styles", [_style_05]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-button/fui-button.vue"]]);

  // ../../../../../../Users/sean/go/src/gindemo02/YouMengApp/unpackage/dist/dev/.nvue/pages/userInfo/userInfo.js
  var _style_0$2 = {};
  var _sfc_main$2 = {
    name: "fui-col",
    props: {
      //栅格占据的列数
      span: {
        type: Number,
        default: 24
      },
      //栅格左侧的间隔格数
      offset: {
        type: Number,
        default: 0
      },
      //栅格向右移动格数
      pushLeft: {
        type: Number,
        default: -1
      },
      //栅格向左移动格数
      pullRight: {
        type: Number,
        default: -1
      },
      //max-width:767px 响应式栅格数或者栅格属性对象
      //Number时表示在此屏幕宽度下，栅格占据的列数。Object时可配置多个描述{span: 4, offset: 4}
      xs: {
        type: [Number, Object],
        default: -1
      },
      //max-width:768px 响应式栅格数或者栅格属性对象
      sm: {
        type: [Number, Object],
        default: -1
      },
      //max-width:992px 响应式栅格数或者栅格属性对象
      md: {
        type: [Number, Object],
        default: -1
      },
      //max-width:1200px 响应式栅格数或者栅格属性对象
      lg: {
        type: [Number, Object],
        default: -1
      },
      //max-width:1920px 响应式栅格数或者栅格属性对象
      xl: {
        type: [Number, Object],
        default: -1
      }
    },
    data() {
      return {
        classList: "fui-col",
        gutter: 0,
        sizeClass: "",
        parentWidth: 0,
        nvueWidth: 0,
        marginLeft: 0,
        right: 0,
        left: 0
      };
    },
    created() {
      let parent = this.$parent;
      while (parent && parent.$options.componentName !== "fuiRow") {
        parent = parent.$parent;
      }
      this.updateGutter(parent.gutter);
      parent.$watch("gutter", (gutter) => {
        this.updateGutter(gutter);
      });
      this.updateNvueWidth(parent.width);
      parent.$watch("width", (width) => {
        this.updateNvueWidth(width);
      });
    },
    computed: {
      sizeChange() {
        let {
          span,
          offset,
          pullRight,
          pushLeft
        } = this;
        return `${span}-${offset}-${pullRight}-${pushLeft}`;
      }
    },
    watch: {
      sizeChange: {
        immediate: true,
        handler(newVal) {
          this.updateNvueWidth(this.parentWidth);
        }
      }
    },
    methods: {
      updateGutter(parentGutter) {
        parentGutter = Number(parentGutter);
        if (!isNaN(parentGutter)) {
          this.gutter = parentGutter / 2;
        }
      },
      updateNvueWidth(width) {
        this.parentWidth = width;
        ["span", "offset", "pull", "push"].forEach((size) => {
          const curSize = this[size];
          if ((curSize || curSize === 0) && curSize !== -1) {
            let RPX = 1 / 24 * curSize * width;
            RPX = Number(RPX);
            switch (size) {
              case "span":
                this.nvueWidth = RPX;
                break;
              case "offset":
                this.marginLeft = RPX;
                break;
              case "pull":
                this.right = RPX;
                break;
              case "push":
                this.left = RPX;
                break;
            }
          }
        });
      },
      updateCol() {
        let classList = ["fui-col"];
        classList.push("fui-col-" + this.span);
        classList.push("fui-col-offset-" + this.offset);
        if (this.pushLeft !== -1) {
          this.pushLeft && classList.push("fui-col-push-" + this.pushLeft);
        }
        if (this.pullRight !== -1) {
          this.pullRight && classList.push("fui-col-pull-" + this.pullRight);
        }
        this.screenSizeSet("xs", classList);
        this.screenSizeSet("sm", classList);
        this.screenSizeSet("md", classList);
        this.screenSizeSet("lg", classList);
        this.screenSizeSet("xl", classList);
        this.classList = classList;
      },
      screenSizeSet(screen, classList) {
        if (typeof this[screen] === "number" && this[screen] !== -1) {
          classList.push("fui-col-" + screen + "-" + this[screen]);
        } else if (typeof this[screen] === "object") {
          typeof this[screen].offset === "number" && classList.push("fui-col-" + screen + "-offset-" + this[screen].offset);
          typeof this[screen].pushLeft === "number" && classList.push("fui-col-" + screen + "-push-" + this[screen].pushLeft);
          typeof this[screen].pullRight === "number" && classList.push("fui-col-" + screen + "-pull-" + this[screen].pullRight);
          typeof this[screen].span === "number" && classList.push("fui-col-" + screen + "-" + this[screen].span);
        }
      }
    }
  };
  function _sfc_render$2(_ctx, _cache, $props, $setup, $data, $options) {
    return (0, import_vue7.openBlock)(), (0, import_vue7.createElementBlock)(
      import_vue7.Fragment,
      null,
      [
        (0, import_vue7.createCommentVNode)("\u672C\u6587\u4EF6\u7531FirstUI\u6388\u6743\u4E88\u4E25\u5929\u8D60\uFF08\u4F1A\u5458ID\uFF1A2  44 2\uFF0C\u8EAB\u4EFD\u8BC1\u5C3E\u53F7\uFF1A0 43 01   8\uFF09\u4E13\u7528\uFF0C\u8BF7\u5C0A\u91CD\u77E5\u8BC6\u4EA7\u6743\uFF0C\u52FF\u79C1\u4E0B\u4F20\u64AD\uFF0C\u8FDD\u8005\u8FFD\u7A76\u6CD5\u5F8B\u8D23\u4EFB\u3002"),
        (0, import_vue7.createElementVNode)(
          "view",
          {
            style: (0, import_vue7.normalizeStyle)({
              paddingLeft: `${Number($data.gutter)}rpx`,
              paddingRight: `${Number($data.gutter)}rpx`,
              width: `${$data.nvueWidth}rpx`,
              position: "relative",
              marginLeft: `${$data.marginLeft}rpx`,
              left: `${$data.right === 0 ? $data.left : -$data.right}rpx`
            })
          },
          [
            (0, import_vue7.renderSlot)(_ctx.$slots, "default")
          ],
          4
          /* STYLE */
        )
      ],
      2112
      /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
    );
  }
  var __easycom_1 = /* @__PURE__ */ _export_sfc(_sfc_main$2, [["render", _sfc_render$2], ["styles", [_style_0$2]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-col/fui-col.vue"]]);
  var _style_0$1 = { "fui-row__box": { "": { "flex": 1, "position": "relative", "flexDirection": "row" } }, "fui-row__flex": { "": { "flexDirection": "row" } }, "fui-row__middle": { "": { "alignItems": "center" } }, "fui-row__bottom": { "": { "alignItems": "flex-end" } }, "fui-row__end": { "": { "justifyContent": "flex-end" } }, "fui-row__center": { "": { "justifyContent": "center" } }, "fui-row__space-around": { "": { "justifyContent": "space-around" } }, "fui-row__space-between": { "": { "justifyContent": "space-between" } } };
  var _sfc_main$1 = {
    name: "fui-row",
    componentName: "fuiRow",
    props: {
      //是否为flex布局
      isFlex: {
        type: Boolean,
        default: false
      },
      //flex 布局下的水平排列方式 start/end/center/space-around/space-between
      justify: {
        type: String,
        default: "start"
      },
      //flex 布局下的垂直排列方式	top/middle/bottom
      align: {
        type: String,
        default: "top"
      },
      marginTop: {
        type: String,
        default: "0"
      },
      marginBottom: {
        type: String,
        default: "0"
      },
      //栅格间隔
      gutter: {
        type: Number,
        default: 0
      },
      // nvue如果使用span等属性，需要配置宽度
      width: {
        type: [String, Number],
        default: 750
      }
    },
    data() {
      return {
        flex: false
      };
    },
    watch: {
      isFlex(val) {
      }
    },
    created() {
      this.flex = true;
    },
    computed: {
      marginValue() {
        return 0;
      },
      justifyClass() {
        return this.justify !== "start" ? `fui-row__${this.justify}` : "";
      },
      alignClass() {
        return this.align !== "top" ? `fui-row__${this.align}` : "";
      }
    }
  };
  function _sfc_render$1(_ctx, _cache, $props, $setup, $data, $options) {
    return (0, import_vue7.openBlock)(), (0, import_vue7.createElementBlock)(
      import_vue7.Fragment,
      null,
      [
        (0, import_vue7.createCommentVNode)("\u672C\u6587\u4EF6\u7531FirstUI\u6388\u6743\u4E88\u4E25\u5929\u8D60\uFF08\u4F1A\u5458ID\uFF1A 2 4 42\uFF0C\u8EAB\u4EFD\u8BC1\u5C3E\u53F7\uFF1A0 4   30 18\uFF09\u4E13\u7528\uFF0C\u8BF7\u5C0A\u91CD\u77E5\u8BC6\u4EA7\u6743\uFF0C\u52FF\u79C1\u4E0B\u4F20\u64AD\uFF0C\u8FDD\u8005\u8FFD\u7A76\u6CD5\u5F8B\u8D23\u4EFB\u3002"),
        (0, import_vue7.createElementVNode)(
          "view",
          {
            class: (0, import_vue7.normalizeClass)(["fui-row__box", [$data.flex ? "fui-row__flex" : "", $options.justifyClass, $options.alignClass]]),
            style: (0, import_vue7.normalizeStyle)({
              marginTop: $props.marginTop,
              marginBottom: $props.marginBottom,
              marginLeft: `-${$options.marginValue}rpx`,
              marginRight: `-${$options.marginValue}rpx`
            })
          },
          [
            (0, import_vue7.renderSlot)(_ctx.$slots, "default")
          ],
          6
          /* CLASS, STYLE */
        )
      ],
      2112
      /* STABLE_FRAGMENT, DEV_ROOT_FRAGMENT */
    );
  }
  var __easycom_2 = /* @__PURE__ */ _export_sfc(_sfc_main$1, [["render", _sfc_render$1], ["styles", [_style_0$1]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/components/firstui/fui-row/fui-row.vue"]]);
  var _style_06 = { "fui-list__item": { "": { "flex": 1, "width": 100, "display": "flex", "alignItems": "center", "justifyContent": "flex-start" } }, "fui-user__avatar": { "": { "display": "flex", "alignItems": "center", "justifyContent": "center" } }, "fui-text__title": { "": { "width": "160rpx" } }, "fui-user__title": { "": { "fontSize": "38rpx", "fontWeight": "800", "marginBottom": "6rpx" } }, "fui-user__explain": { "": { "marginBottom": "6rpx", "fontSize": "28rpx", "color": "#7F7F7F", "flex": 1 } }, "fui-text__explain": { "": { "fontSize": "28rpx", "color": "#7F7F7F", "flex": 1 } } };
  var _sfc_main6 = {
    data() {
      return {};
    },
    methods: {}
  };
  function _sfc_render6(_ctx, _cache, $props, $setup, $data, $options) {
    const _component_fui_avatar = resolveEasycom((0, import_vue7.resolveDynamicComponent)("fui-avatar"), __easycom_0);
    const _component_fui_col = resolveEasycom((0, import_vue7.resolveDynamicComponent)("fui-col"), __easycom_1);
    const _component_fui_row = resolveEasycom((0, import_vue7.resolveDynamicComponent)("fui-row"), __easycom_2);
    const _component_fui_list_cell = resolveEasycom((0, import_vue7.resolveDynamicComponent)("fui-list-cell"), __easycom_3);
    const _component_fui_list = resolveEasycom((0, import_vue7.resolveDynamicComponent)("fui-list"), __easycom_4);
    const _component_fui_icon = resolveEasycom((0, import_vue7.resolveDynamicComponent)("fui-icon"), __easycom_5);
    const _component_fui_button = resolveEasycom((0, import_vue7.resolveDynamicComponent)("fui-button"), __easycom_6);
    return (0, import_vue7.openBlock)(), (0, import_vue7.createElementBlock)("scroll-view", {
      scrollY: true,
      showScrollbar: true,
      enableBackToTop: true,
      bubble: "true",
      style: { flexDirection: "column" }
    }, [
      (0, import_vue7.createElementVNode)("view", null, [
        (0, import_vue7.createVNode)(_component_fui_row, { "margin-bottom": "24rpx" }, {
          default: (0, import_vue7.withCtx)(() => [
            (0, import_vue7.createVNode)(_component_fui_col, {
              span: 6,
              class: "fui-user__avatar"
            }, {
              default: (0, import_vue7.withCtx)(() => [
                (0, import_vue7.createVNode)(_component_fui_avatar, {
                  text: "\u5218",
                  radius: 12,
                  background: "#FFB703"
                })
              ]),
              _: 1
              /* STABLE */
            }),
            (0, import_vue7.createVNode)(_component_fui_col, {
              span: 18,
              class: "fui-user__info"
            }, {
              default: (0, import_vue7.withCtx)(() => [
                (0, import_vue7.createElementVNode)("u-text", { class: "fui-user__title" }, " \u67D0\u67D0\u67D0\u4EBA "),
                (0, import_vue7.createElementVNode)("u-text", { class: "fui-user__explain" }, " \u6635\u79F0\uFF1A "),
                (0, import_vue7.createElementVNode)("u-text", { class: "fui-user__explain" }, " \u5730\u70B9\uFF1A ")
              ]),
              _: 1
              /* STABLE */
            })
          ]),
          _: 1
          /* STABLE */
        }),
        (0, import_vue7.createVNode)(_component_fui_list, null, {
          default: (0, import_vue7.withCtx)(() => [
            (0, import_vue7.createVNode)(_component_fui_list_cell, { arrow: "" }, {
              default: (0, import_vue7.withCtx)(() => [
                (0, import_vue7.createElementVNode)("u-text", { class: "fui-text__title" }, "\u5907\u6CE8\u548C\u6807\u7B7E")
              ]),
              _: 1
              /* STABLE */
            }),
            (0, import_vue7.createVNode)(_component_fui_list_cell, null, {
              default: (0, import_vue7.withCtx)(() => [
                (0, import_vue7.createElementVNode)("u-text", { class: "fui-text__title" }, "\u624B\u673A\u53F7"),
                (0, import_vue7.createElementVNode)("u-text", { class: "fui-text__explain" }, "\u8BF4\u660E\u6587\u5B57")
              ]),
              _: 1
              /* STABLE */
            }),
            (0, import_vue7.createVNode)(_component_fui_list_cell, { arrow: "" }, {
              default: (0, import_vue7.withCtx)(() => [
                (0, import_vue7.createElementVNode)("u-text", { class: "fui-text__title" }, "\u670B\u53CB\u6743\u9650"),
                (0, import_vue7.createElementVNode)("u-text", { class: "fui-text__explain" }, "\u804A\u5929\u3001\u5BA0\u7269\u5708")
              ]),
              _: 1
              /* STABLE */
            })
          ]),
          _: 1
          /* STABLE */
        }),
        (0, import_vue7.createElementVNode)("view", { class: "" }, [
          (0, import_vue7.createVNode)(_component_fui_button, {
            radius: "0",
            type: "gray"
          }, {
            default: (0, import_vue7.withCtx)(() => [
              (0, import_vue7.createVNode)(_component_fui_icon, {
                name: "comment",
                size: 38
              }),
              (0, import_vue7.createElementVNode)("u-text", null, "\u53D1\u6D88\u606F")
            ]),
            _: 1
            /* STABLE */
          }),
          (0, import_vue7.createVNode)(_component_fui_button, {
            radius: "0",
            type: "gray"
          }, {
            default: (0, import_vue7.withCtx)(() => [
              (0, import_vue7.createElementVNode)("u-text", null, "\u53D1\u9001\u597D\u53CB\u7533\u8BF7")
            ]),
            _: 1
            /* STABLE */
          })
        ])
      ])
    ]);
  }
  var userInfo = /* @__PURE__ */ _export_sfc(_sfc_main6, [["render", _sfc_render6], ["styles", [_style_06]], ["__file", "/Users/sean/go/src/gindemo02/YouMengApp/pages/userInfo/userInfo.nvue"]]);

  // <stdin>
  var webview = plus.webview.currentWebview();
  if (webview) {
    const __pageId = parseInt(webview.id);
    const __pagePath = "pages/userInfo/userInfo";
    let __pageQuery = {};
    try {
      __pageQuery = JSON.parse(webview.__query__);
    } catch (e) {
    }
    userInfo.mpType = "page";
    const app = Vue.createPageApp(userInfo, { $store: getApp({ allowDefault: true }).$store, __pageId, __pagePath, __pageQuery });
    app.provide("__globalStyles", Vue.useCssStyles([...__uniConfig.styles, ...userInfo.styles || []]));
    app.mount("#root");
  }
})();
