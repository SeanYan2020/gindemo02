import request from './request'
import config from '@/config.js'
import route from './route'
import {
	getStore
} from './store'



const cloud = {
	/**
	 * 存储器
	 */
	store: function(value) {
		let store = getStore();
		return store.getter(value)
	},
	/**
	 * 存储器
	 */
	isLogin: function(value) {
		let store = getStore();
		return store.isLogin();
	},
	/**
	 * 设置前缀
	 */
	prefix: function() {
		return {}
	},
	/**
	 * 执行登录
	 */
	login: function(url, params = null, data = null) {
		return new Promise((resolve, reject) => {
			let store = getStore();
			store.dispatch("login", () => {
				return cloud.post(url, params, data)
			}).then(data => {
				resolve(data)
			}).catch(error => {
				reject(error)
			})
		})
	},
	/**
	 * 执行退出
	 */
	logout: function() {
		return new Promise((resolve, reject) => {
			let store = getStore();
			store.dispatch("logout").then(data => {
				resolve(data)
			}).catch(error => {
				reject(error)
			})
		})
	},
	/**
	 * 数据POST方法
	 * @param url
	 * @param params
	 * @param data
	 * @returns {*}
	 */
	post: function(url, params = null, data = null) {
		if (url.indexOf("/") === 0) {
			url = url.slice(1)
		}
		//设置数据结构
		if (data === null) {
			data = params;
			params = null
		}
		//重置URL地址
		url = `/api/` + url;
		return request({
			url: url,
			method: 'POST',
			headers: {
				'x-api-type': 'app'
			},
			params: Object.assign(cloud.prefix(), params),
			data: data
		})
	},

	/**
	 * 数据POST方法
	 * @param url
	 * @param params
	 * @param data
	 * @returns {*}
	 */
	upload: function(url, params = null, data = null) {
		if (url.indexOf("/") === 0) {
			url = url.slice(1);
		}
		//设置数据结构
		if (data === null) {
			data = params;
			params = null;
		}
		//重置URL地址
		url = config.AppURL + `/api/` + url;
		console.log("获取的参数", url, data)
		return new Promise((resolve, reject) => {
			uni.uploadFile({
				url: url,
				filePath: data,
				name: 'file',
				success: function(res) {

					if (res.statusCode === 200) {
						let data = JSON.parse(res.data)
						console.log("上传结果", data)
						resolve(data)
					} else {
						reject(res)
					}

				},
				fail: function(err) {
					reject(err)
				}
			})
		})

	},

	/**
	 * 数据GET方法
	 * @param url
	 * @param params
	 * @returns {*}
	 */
	get: function(url, params = null) {
		if (url.indexOf("/") === 0) {
			url = url.slice(1)
		}
		//重置URL地址
		url = `/api/` + url;
		return request({
			url: url,
			method: 'POST',
			headers: {
				'x-api-type': 'app'
			},
			params: Object.assign(cloud.prefix(), params),
		})
	},

	/**
	 * 数据POST方法
	 * @param url
	 * @param params
	 * @param data
	 * @returns {*}
	 */
	page: function(url, {
		size = 10,
		page = 1
	}, params = null, data = null) {
		if (url.indexOf("/") === 0) {
			url = url.slice(1)
		}
		//设置数据结构
		if (data === null) {
			data = params;
			params = null
		}
		//重置URL地址
		url = `/api/` + url;
		return request({
			url: url,
			method: 'POST',
			headers: {
				'x-api-type': 'app'
			},
			params: Object.assign(cloud.prefix(), {
				page,
				size
			}, params),
			data: data
		})
	},
	/**
	 * 页面跳转
	 * @param {Object} url
	 * @param {Object} query
	 */
	go: function(url, query) {
		if (Object.prototype.toString.call(url) === '[object Object]') {
			if (url.query) {
				url.query = Object.assign(url.query, query)
			}
		} else {
			url = {
				path: url,
				params: query
			}
		}
		route.push(url)
	}


}

export default cloud