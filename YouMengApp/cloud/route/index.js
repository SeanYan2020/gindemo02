/**
 * 路由跳转方法，该方法相对于直接使用uni.xxx的好处是使用更加简单快捷
 * 并且带有路由拦截功能
 */

/**
 * @description 对象转url参数
 * @param {object} data,对象
 * @param {Boolean} isPrefix,是否自动加上"?"
 * @param {string} arrayFormat 规则 indices|brackets|repeat|comma
 */
function queryParams(data = {}, isPrefix = true, arrayFormat = 'brackets') {
  const prefix = isPrefix ? '?' : ''
  const _result = []
  if (['indices', 'brackets', 'repeat', 'comma'].indexOf(arrayFormat) === -1) arrayFormat = 'brackets'
  for (const key in data) {
    const value = data[key]
    // 去掉为空的参数
    if (['', undefined, null].indexOf(value) >= 0) {
      continue
    }
    // 如果值为数组，另行处理
    if (value.constructor === Array) {
      // e.g. {ids: [1, 2, 3]}
      switch (arrayFormat) {
        case 'indices':
          // 结果: ids[0]=1&ids[1]=2&ids[2]=3
          for (let i = 0; i < value.length; i++) {
            _result.push(`${key}[${i}]=${value[i]}`)
          }
          break
        case 'brackets':
          // 结果: ids[]=1&ids[]=2&ids[]=3
          value.forEach((_value) => {
            _result.push(`${key}[]=${_value}`)
          })
          break
        case 'repeat':
          // 结果: ids=1&ids=2&ids=3
          value.forEach((_value) => {
            _result.push(`${key}=${_value}`)
          })
          break
        case 'comma':
          // 结果: ids=1,2,3
          let commaStr = ''
          value.forEach((_value) => {
            commaStr += (commaStr ? ',' : '') + _value
          })
          _result.push(`${key}=${commaStr}`)
          break
        default:
          value.forEach((_value) => {
            _result.push(`${key}[]=${_value}`)
          })
      }
    } else {
      _result.push(`${key}=${value}`)
    }
  }
  return _result.length ? prefix + _result.join('&') : ''
}

/**
 * 是否数组
 */
function array(value) {
  if (typeof Array.isArray === 'function') {
    return Array.isArray(value)
  }
  return Object.prototype.toString.call(value) === '[object Array]'
}

/**
 * @description 获取当前页面路径
 */
function page() {
  const pages = getCurrentPages()
  // 某些特殊情况下(比如页面进行redirectTo时的一些时机)，pages可能为空数组
  return `/${pages[pages.length - 1]?.route ?? ''}`
}

/**
 * @description 深度克隆
 * @param {object} obj 需要深度克隆的对象
 * @returns {*} 克隆后的对象或者原值（不是对象）
 */
function deepClone(obj) {
  // 对常见的“非”值，直接返回原来值
  if ([null, undefined, NaN, false].includes(obj)) return obj
  if (typeof obj !== 'object' && typeof obj !== 'function') {
    // 原始类型直接返回
    return obj
  }
  const o = array(obj) ? [] : {}
  for (const i in obj) {
    if (obj.hasOwnProperty(i)) {
      o[i] = typeof obj[i] === 'object' ? deepClone(obj[i]) : obj[i]
    }
  }
  return o
}

/**
 * @description JS对象深度合并
 * @param {object} target 需要拷贝的对象
 * @param {object} source 拷贝的来源对象
 * @returns {object|boolean} 深度合并后的对象或者false（入参有不是对象）
 */
function deepMerge(target = {}, source = {}) {
  target = deepClone(target)
  if (typeof target !== 'object' || typeof source !== 'object') return false
  for (const prop in source) {
    if (!source.hasOwnProperty(prop)) continue
    if (prop in target) {
      if (typeof target[prop] !== 'object') {
        target[prop] = source[prop]
      } else if (typeof source[prop] !== 'object') {
        target[prop] = source[prop]
      } else if (target[prop].concat && source[prop].concat) {
        target[prop] = target[prop].concat(source[prop])
      } else {
        target[prop] = deepMerge(target[prop], source[prop])
      }
    } else {
      target[prop] = source[prop]
    }
  }
  return target
}


class Router {
  constructor() {
    // 原始属性定义
    this.config = {
      type: 'navigateTo',
      url: '',
      delta: 1, // navigateBack页面后退时,回退的层数
      params: {}, // 传递的参数
      animationType: 'pop-in', // 窗口动画,只在APP有效
      animationDuration: 300, // 窗口动画持续时间,单位毫秒,只在APP有效
      intercept: false // 是否需要拦截
    }
    // 因为route方法是需要对外赋值给另外的对象使用，同时route内部有使用this，会导致route失去上下文
    // 这里在构造函数中进行this绑定
    this.push = this.route.bind(this)
  }

  // 判断url前面是否有"/"，如果没有则加上，否则无法跳转
  addRootPath(url) {
    return url[0] === '/' ? url : `/${url}`
  }

  // 整合路由参数
  mixinParam(url, params) {
    url = url && this.addRootPath(url)

    // 使用正则匹配，主要依据是判断是否有"/","?","="等，如“/page/index/index?name=mary"
    // 如果有url中有get参数，转换后无需带上"?"
    let query = ''
    if (/.*\/.*\?.*=.*/.test(url)) {
      // object对象转为get类型的参数
      query = queryParams(params, false)
      // 因为已有get参数,所以后面拼接的参数需要带上"&"隔开
      return url += `&${query}`
    }
    // 直接拼接参数，因为此处url中没有后面的query参数，也就没有"?/&"之类的符号
    query = queryParams(params)
    return url += query
  }

  // 对外的方法名称
  async route(options = {}, params = {}) {
    // 合并用户的配置和内部的默认配置
    let mergeConfig = {}
    //处理接口
    options.url = options.url ? options.url : options.path

    if (typeof options === 'string') {
      // 如果options为字符串，则为route(url, params)的形式
      mergeConfig.url = this.mixinParam(options, params)
      mergeConfig.type = 'navigateTo'
    } else {
      mergeConfig = deepClone(options, this.config)
      // 否则正常使用mergeConfig中的url和params进行拼接
      mergeConfig.url = this.mixinParam(options.url, options.params)
    }

    // 如果本次跳转的路径和本页面路径一致，不执行跳转，防止用户快速点击跳转按钮，造成多次跳转同一个页面的问题
    if (mergeConfig.url === page()) return

    if (params.intercept) {
      this.config.intercept = params.intercept
    }
    // params参数也带给拦截器
    mergeConfig.params = params
    // 合并内外部参数
    mergeConfig = deepMerge(this.config, mergeConfig)

    console.log("参数", mergeConfig)

    // 判断用户是否定义了拦截器
    this.openPage(mergeConfig)
  }

  // 执行路由跳转
  openPage(config) {
    // 解构参数
    const {
      url,
      type,
      delta,
      animationType,
      animationDuration
    } = config
    if (config.type === 'navigateTo' || config.type === 'to') {
      uni.navigateTo({
        url,
        animationType,
        animationDuration,
        fail: () => {
          uni.switchTab({
            url
          })
        }
      })
    }
    if (config.type === 'redirectTo' || config.type === 'redirect') {
      uni.redirectTo({
        url
      })
    }
    if (config.type === 'switchTab' || config.type === 'tab') {
      uni.switchTab({
        url
      })
    }
    if (config.type === 'reLaunch' || config.type === 'launch') {
      uni.reLaunch({
        url
      })
    }
    if (config.type === 'navigateBack' || config.type === 'back') {
      uni.navigateBack({
        delta
      })
    }
  }
}

export default new Router()
