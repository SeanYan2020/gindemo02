为了便于理解uni小程序API的使用，需要知道两种uni-app项目，一种是uni-app宿主App项目（简称“宿主App”），一种是uni-app小程序App项目（简称“uni小程序”），以下将分别从这两种项目作为调用者的角度来介绍相关API。


### 宿主App使用的API  
宿主App是通过`uniMP`模块管理uni小程序，云端打包时需要在宿主App项目的manifest.json中配置使用uni小程序模块。
在"app-plus"->"modules"节点下添加"UniMP"模块，示例如下：
```json
  "app-plus" : {
    "modules" : {
        "UniMP": {
          "description": "uni小程序"
        }
    },
    //...
  }
```

> HBuilderX中自带的标准基座已经包含uni小程序模块，可以直接调用uni小程序相关API  
> 配置后需提交云端打包后才能生效，真机运行时推荐使用[自定义调试基座](https://ask.dcloud.net.cn/article/35115)


#### 加载uniMP模块
在调用uni小程序API时需先调用[uni.requireNativePlugin](https://uniapp.dcloud.io/api/extend/native-plugin?id=requirenativeplugin)方法加载uni小程序管理模块。  
示例如下：
```js
const mp = uni.requireNativePlugin('uniMP');
//通过mp可调用方法操作uni小程序
mp.openUniMP({
  appid:'小程序的appid'
});
```

>注：如果返回的mp为undefined，说明当前环境不支持uni小程序


<a id="installUniMP"/>

#### installUniMP(options,callback)
通过wgt安装`uni小程序`到宿主App中，示例如下：
```js
mp.installUniMP({
  appid: 'uni小程序的appid',
  wgtFile: 'uni小程序的wgt文件路径'
}, (ret)=>{
  console.log('installUniMP: '+JSON.stringify(ret));
});
```

##### options参数说明  
JSON Object类型  
|属性|类型 |默认值|必填|说明||:--|:--|:--|:--|:--||appid|String|无|是|uni小程序的appid|
|wgtFile|String|无|是|uni小程序的wgt文件路径|
##### callback回调参数说明
JSON Object类型  

|属性|类型 |说明||:--|:--|:--||type|String|"success" 表示成功， "fail" 表示失败||code|Number| 成功返回 0，失败返回相应 code 码|
|message|String|失败描述|


<a id="openUniMP"/>

#### openUniMP(options,callback)
打开`uni小程序`，示例如下：
```js
mp.openUniMP({
  appid: 'uni小程序的appid'
}, (ret)=>{
  console.log('openUniMP: '+JSON.stringify(ret));
});
```

##### options参数说明  
JSON Object类型  
|属性|类型 |默认值|必填|说明||:--|:--|:--|:--|:--||appid|String|无|是|uni小程序的appid|
|icon|String|无|否|启动小程序 splash 页面显示的图片，默认显示 app logo|
|path|String|无|否|HBuilderX3.3.1+版本新增，打开的页面路径，如果为空则打开首页。path 中 ? 后面的部分会成为 query，注意：传入此参数后应用会重启|
|extraData|Object|无|否|HBuilderX3.3.1+版本新增，需要传递给目标小程序的数据。小程序接收参考`uni小程序中获取启动参数`|
|fromAppid|String|无|否|HBuilderX3.3.1+版本新增，打开uni小程序的来源appid|
|scene| Number|无|否|HBuilderX3.3.1+版本新增，启动小程序的场景值，由宿主自定义|
##### callback回调参数说明  
JSON Object类型  

|属性|类型 |说明||:--|:--|:--||type|String|"success" 表示成功， "fail" 表示失败||code|Number| 成功返回 0,失败返回相应 code 码|
|message|String|失败描述|


<a id="closeUniMP"/>

#### closeUniMP(appid,callback)  
关闭已经运行的`uni小程序`，示例如下：
``` js
mp.closeUniMP('uni小程序的appid', (ret)=>{
  console.log('closeUniMP: '+JSON.stringify(ret));
});
```

> HBuilderX3.4.3+版本新增支持 

##### 参数说明  

|属性|类型 |默认值|必填|说明||:--|:--|:--|:--|:--||appid|String|无|是|uni小程序的appid|
|callback|Function|无|是|关闭操作结果回调函数|

##### callback回调参数说明  
JSON Object类型  

|属性|类型 |说明||:--|:--|:--||type|String|"success" 表示成功， "fail" 表示失败||code|Number| 成功返回 0,失败返回相应 code 码|
|message|String|失败描述|


<a id="hideUniMP"/>

#### hideUniMP(appid,callback)  
隐藏运行在前台的`uni小程序`，示例如下：
``` js
mp.hideUniMP('uni小程序的appid', (ret)=>{
  console.log('hideUniMP: '+JSON.stringify(ret));
});
```

> HBuilderX3.4.3+版本新增支持 

##### 参数说明  

|属性|类型 |默认值|必填|说明||:--|:--|:--|:--|:--||appid|String|无|是|uni小程序的appid|
|callback|Function|无|是|隐藏操作结果回调函数|

##### callback回调参数说明  
JSON Object类型  

|属性|类型 |说明||:--|:--|:--||type|String|"success" 表示成功， "fail" 表示失败||code|Number| 成功返回 0,失败返回相应 code 码|
|message|String|失败描述|


<a id="showUniMP"/>

#### showUniMP(appid,callback)  
将运行在后台的`uni小程序`切换到前台显示，示例如下：
``` js
mp.showUniMP('uni小程序的appid', (ret)=>{
  console.log('showUniMP: '+JSON.stringify(ret));
});
```

> HBuilderX3.4.3+版本新增支持 

##### 参数说明  

|属性|类型 |默认值|必填|说明||:--|:--|:--|:--|:--||appid|String|无|是|uni小程序的appid|
|callback|Function|无|是|显示操作结果回调函数|

##### callback回调参数说明  
JSON Object类型  

|属性|类型 |说明||:--|:--|:--||type|String|"success" 表示成功， "fail" 表示失败||code|Number| 成功返回 0,失败返回相应 code 码|
|message|String|失败描述|


<a id="getUniMPVersion"/>

#### getUniMPVersion(appid,callback)
获取安装的`uni小程序`版本信息，示例如下：
```js
mp.getUniMPVersion('uni小程序的appid', (ret)=>{
  console.log('getUniMPVersion: '+JSON.stringify(ret));
});
```

##### 参数说明  

|属性|类型 |默认值|必填|说明||:--|:--|:--|:--|:--||appid|String|无|是|uni小程序的appid|
|callback|Function|无|是|获取版本信息回调函数|

##### callback回调参数说明  
JSON Object类型  

|属性|类型 |说明||:--|:--|:--||type|String|"success" 表示成功， "fail" 表示失败||code|Number| 成功返回 0,失败返回相应 code 码|
|message|String|失败描述|
|versionInfo|Object|uni小程序的版本信息，参考`versionInfo参数说明`|

##### versionInfo参数说明
|属性|类型 |说明||:--|:--|:--||name|String|应用版本名称 (对应 manifest.json 中的 versionName)||code|String|应用版本号 (对应 manifest.json 中的 versionCode)|


<a id="onUniMPEventReceive"/>

#### onUniMPEventReceive(callback)
监听`uni小程序`发送的event事件，建议在宿主App的应用生命周期`onLaunch`中进行监听。在`uni小程序`中调用[uni.sendHostEvent](#sendHostEvent)发送event事件后触发。
示例如下：
```js
export default {
  onLaunch: function() {
    console.log('Host App Launch');
    
    //监听uni小程序发送的事件
    mp.onUniMPEventReceive(ret=>{
			console.log('Receive event from MP: '+JSON.stringify(ret));
    });
  },
  //...
}
```

##### callback回调参数说明  
JSON Object类型  

|属性|类型 |说明||:--|:--|:--|
|fromAppid|String|发送事件的uni小程序的appid|| event |String|事件名称，uni小程序中调用sendHostEvent传入的event参数|
| data |String 或 Object|传递的数据，小程序中调用sendHostEvent传入的data参数|


<a id="sendUniMPEvent"/>

#### sendUniMPEvent(appid,event,data,callback)
宿主App向`uni小程序`发送event事件，在`uni小程序`中调用[uni.onHostEventReceive](#onHostEventReceive)监听。
示例如下：
```js
mp.sendUniMPEvent('uni小程序的appid', 'event事件名称', '要传递的数据', (ret) => {
  //发送消息成功回调
  console.log('Send event to MP: '+JSON.stringify(ret));
});
```

> HBuilderX3.3.9+版本新增支持：宿主在小程序运行期间向其发送数据进行通讯  
> 注：uni小程序必须处于运行状态，否则会发送失败  

##### 参数说明  
|参数|类型 |说明||:--|:--|:--|
| appid |String|目标小程序appid|
| event |String|事件名称|
| data |String 或 Object|传递的数据|




### uni小程序使用的API  

#### 获取启动参数
`宿主App`启动uni小程序传入的extraData参数可在小程序项目App.vue的应用生命周期`onLaunch`、`onShow`中获取，
示例如下：
```js
//首次启动
onLaunch: function(info) {
	console.log('App Launch:',info);
}
//从后台切换到前台
onShow: function(info) {
	console.log('App Launch:',info);
}
```

> HBuilderX3.3.1+版本新增支持

##### info参数说明
JSON Object类型  

|属性|类型 |说明||:--|:--|:--||path |String|打开的页面路径||query |Object| 页面参数|
|scene | Number |启动小程序的场景值|
|referrerInfo| Object |启动小程序时传递的数据，参考`ReferrerInfo `|

##### ReferrerInfo参数说明
|属性|类型 |说明||:--|:--|:--||appId |String| 打开uni小程序时的来源应用appid||extraData |Object| 启动时传递的数据|


<a id="sendHostEvent"/>

#### uni.sendHostEvent(event,data,ret,callback)
uni小程序向`宿主App`发送event事件，在`宿主App`中可调用[onUniMPEventReceive](#onUniMPEventReceive)监听接收event事件。
示例如下：
```
uni.sendHostEvent(event, data, (ret) => {
  //发送消息成功回调
  console.log('Send event to Host: '+JSON.stringify(ret));
});
```

#### 参数说明
|参数|类型 |说明||:--|:--|:--|
| event |String|事件名称|
| data |String 或 Object|传递的数据|
| callback | Function(ret) | 操作回调函数，ret参数为`SendHostEventRet`类型 |

#### callback回调参数说明  
JSON Object类型  

|属性|类型 |说明||:--|:--|:--||type|String|"success" 表示成功， "fail" 表示失败||code|Number| 成功返回 0,失败返回相应 code 码|


<a id="onHostEventReceive"/>

#### uni.onHostEventReceive(callback)
uni小程序监听`宿主App`发送的event事件，建议在uni小程序应用生命周期`onLaunch`中监听。在`uni宿主App`中调用[sendUniMPEvent](#sendUniMPEvent)发送event事件后触发。
示例如下：
```
export default {
  onLaunch: function() {
    console.log('App Launch');
    
    //监听宿主App通讯数据
    uni.onHostEventReceive((event,data)=>{
			console.log('Receive event('+event+') from HOST: '+JSON.stringify(data));
    });
  },
  //...
}
```

#### callback回调参数说明
|参数|类型 |说明||:--|:--|:--|
| event |String|事件名称，宿主调用sendUniMPEvent传入的event参数|
| data |String 或 Object|传递的数据，宿主调用sendUniMPEvent传入的data参数|

