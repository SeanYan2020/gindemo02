package routers

import (
	"fmt"
	"gindemo02/middlewares"
	"reflect"
	"strings"

	"github.com/gin-gonic/gin"
)

// type ControllerConstructor func() interface{}

func RoutersInit(r *gin.Engine) {
	apiRouters := r.Group("/api", middlewares.InitMiddleware)
	{

		// // 创建一个映射表，将控制器名称映射到它们的构造函数
		// Constructors := map[string]ControllerConstructor{
		// 	"UserController":    func() interface{} { return new(controllers.UserController) },
		// 	"ArticleController": func() interface{} { return new(controllers.ArticleController) },
		// 	"LoginController":   func() interface{} { return new(controllers.LoginController) },
		// 	"ContactController": func() interface{} { return new(controllers.ContactController) },
		// }

		apiRouters.Any("/:model/:action", func(c *gin.Context) {
			model := c.Param("model")
			action := c.Param("action")
			// 根据需要选择并调用适当的构造函数
			controllerName := strings.Title(model) + "Controller" // 这可以是一个动态变化的值
			constructor, ok := Constructors[controllerName]
			fmt.Println("我是聊天推送的控制器", controllerName)
			if !ok {
				fmt.Println("Constructor not found")
				return
			}

			controller := constructor()

			// 获取UserController的反射类型（'UserController'）
			controllerValue := reflect.ValueOf(controller)

			// 获取方法名为'Test'的方法
			reflectMethod := controllerValue.MethodByName(strings.Title(action))

			// 检查方法是否存在
			if !reflectMethod.IsValid() {
				fmt.Println("Method not found")
				return
			}

			reflectMethod.Call([]reflect.Value{reflect.ValueOf(c)})

			// 调用方法
			// 将反射得到的方法转换为 gin.HandlerFunc 类型
			// handlerFunc, _ := reflectMethod.Interface().(func(*gin.Context))
			// return handlerFunc
			// result := handlerFunc.(c)

			// apiRouters.GET("/index", handlerFunc)

			// apiRouters.GET("/", func(c *gin.Context) {
			// 	c.String(200, "我是一个api接口")
			// })
		})

	}

}
