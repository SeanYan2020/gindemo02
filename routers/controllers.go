package routers

import "gindemo02/controllers"

type ControllerConstructor func() interface{}

var Constructors = map[string]ControllerConstructor{
	"UserController":    func() interface{} { return new(controllers.UserController) },
	"AccountController": func() interface{} { return new(controllers.AccountController) },
	"ArticleController": func() interface{} { return new(controllers.ArticleController) },
	"LoginController":   func() interface{} { return new(controllers.LoginController) },
	"ContactController": func() interface{} { return new(controllers.ContactController) },
	"ChatController":    func() interface{} { return new(controllers.ChatController) },
}
