package models

import (
	"gorm.io/gorm"
)

type User struct {
	Id       int
	Username string
	Age      int
	Email    string
	AddTime  int
}

func (User) Migrate(db *gorm.DB) error {
	return db.AutoMigrate(&User{})
}
