package models

import (
	"fmt"
	"log"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
)

var DB *gorm.DB
var err error

func init() {

	dsn := "gindemo02:6mpuSZ4MxsiKbRTr4Q@tcp(mysql.hk.run:3306)/gindemo02?charset=utf8mb4&parseTime=True&loc=Local"
	config := &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			TablePrefix:   "qn_", // 表前缀
			SingularTable: true,  // 禁用表名复数
		}}
	DB, err = gorm.Open(mysql.Open(dsn), config)

	if err != nil {
		fmt.Println(err)
	}

	// 自动创建和更新表结构
	err = DB.AutoMigrate(
		&User{}, &Article{}, &Account{}, &Contact{}, &Community{},
	)
	if err != nil {
		log.Fatalf("Failed to auto migrate database: %v", err)
	}

}
