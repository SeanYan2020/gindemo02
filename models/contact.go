package models

import (
	"time"

	"gorm.io/gorm"
)

// 好友和群
type Contact struct {
	Id        int64
	Ownerid   int64
	Dstobj    int64
	Cate      int
	Memo      string
	Status    int
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt gorm.DeletedAt `gorm:"index"`
}

const (
	CONCAT_CATE_USER      = 0x01
	CONCAT_CATE_COMMUNITY = 0x02
)
