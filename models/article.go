package models

import (
	"time"

	"gorm.io/gorm"
)

type Article struct {
	Id        int
	Title     string
	CateId    int
	State     int
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt gorm.DeletedAt `gorm:"index"`
}
