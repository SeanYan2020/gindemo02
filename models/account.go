package models

import (
	"time"

	"gorm.io/gorm"
)

type Account struct {
	Id        int64          `json:"id"`
	Username  string         `json:"username,omitempty"`
	Password  string         `json:"-,omitempty"`
	Code      string         `json:"code,omitempty" gorm:"-"`
	Email     string         `json:"email"`
	Mobile    string         `json:"mobile,omitempty"`
	Role      string         `json:"role"`
	Type      int            `json:"type"`
	Status    int            `json:"status"`
	CreatedAt time.Time      `json:"createdAt"`
	UpdatedAt time.Time      `json:"updatedAt"`
	DeletedAt gorm.DeletedAt `json:"deletedAt" gorm:"index"`
}
