package models

import (
	"time"

	"github.com/dgrijalva/jwt-go"
)

// 时间戳转换成日期
func UnixToTime(timestamp int) string {
	t := time.Unix(int64(timestamp), 0)
	return t.Format("2006-01-02 15:04:05")
}

// 日期转换成时间戳 2020-05-02 15:04:05
func DateToUnix(str string) int64 {
	template := "2006-01-02 15:04:05"
	t, err := time.ParseInLocation(template, str, time.Local)
	if err != nil {
		return 0
	}
	return t.Unix()
}

// 获取时间戳
func GetUnix() int64 {
	return time.Now().Unix()
}

// 获取当前的日期
func GetDate() string {
	template := "2006-01-02 15:04:05"
	return time.Now().Format(template)
}

// 获取年月日
func GetDay() string {
	template := "20060102"
	return time.Now().Format(template)
}

// 获取Token信息
func GenerateToken(userId int64, mobile string, expTime int64, secretKey string) (string, error) {
	// 创建一个新的Token
	token := jwt.New(jwt.SigningMethodHS256)

	// 创建一个Claims
	claims := token.Claims.(jwt.MapClaims)
	claims["userId"] = userId
	claims["mobile"] = mobile
	claims["exp"] = time.Now().Add(time.Duration(expTime) * time.Hour).Unix()

	// 使用指定的secret签名并生成一个token
	tokenString, err := token.SignedString([]byte(secretKey))

	if err != nil {
		return "", err
	}

	return tokenString, nil
}

// 检查Token是否快过期，时间单位是分钟
func IsTokenExpiring(tokenString string, warningMinutes int64, secretKey []byte) bool {
	// Parse the token
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return secretKey, nil
	})

	// Handle any error
	if err != nil {
		return false
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		expireTime := int64(claims["exp"].(float64))
		now := time.Now().Unix()

		// Check if the token is about to expire
		if now > expireTime-warningMinutes*60 {
			return true
		}
	}

	return false
}
