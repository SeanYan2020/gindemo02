package models

import (
	"time"

	"gorm.io/gorm"
)

// 好友和群
type Community struct {
	Id        int64 //群名称
	Name      string
	Ownerid   int64 //群主ID
	Icon      string
	Cate      int
	Memo      string //描述
	Status    int
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt gorm.DeletedAt `gorm:"index"`
}
