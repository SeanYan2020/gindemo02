package main

import (
	"fmt"
	"gindemo02/routers"
	"html/template"
	"time"

	"github.com/gin-gonic/gin"
)

// 时间戳转换成日期
func UnixToTime(timestamp int) string {
	fmt.Println(timestamp)
	t := time.Unix(int64(timestamp), 0)
	return t.Format("2006-01-02 15:04:05")
}

func main() {
	r := gin.Default()

	r.SetFuncMap(template.FuncMap{
		"UnixToTime": UnixToTime,
	})

	//加载模板 放在配置路由前面
	r.LoadHTMLGlob("views/**/*")
	//配置静态web目录   第一个参数表示路由, 第二个参数表示映射的目录
	r.Static("/home", "./views/home")

	routers.RoutersInit(r)
	routers.RoutersChat(r)

	r.Run(":9891")
}
